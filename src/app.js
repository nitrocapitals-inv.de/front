import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';



import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/index.css';
import './assets/css/style.css'

import './assets/css/form.css'
import './assets/css/select2.css'

import './assets/css/styled41d.css'
import './assets/css/popup.css'
import './assets/css/contact.css'
import './assets/css/video.css'
import './assets/css/spiegel_de.css'

import './assets/css/bit_index.css'
import './assets/css/bit_register.css'
import './assets/css/bit_intgrtn.css'
import './assets/css/bit_popup.css'
import './assets/css/tsl_autoptimize.css'
import './assets/css/tsl_style.css'
import './assets/css/rmg_style.css'
import './assets/css/btn_style.css'
import './assets/css/alice-carousel.css'

import BitcoinProfit from './pages/BitcoinProfit'
import TeslaInvestmentDe from './pages/TeslaInvestmentDe'
import BlankPage from './pages/Blank'
import VerifiedPage from './pages/VerifiedPage'
import AmazonPage from './pages/AmazonPage.jsx'
import AmazonTestPage from './pages/AmazonTestPage.jsx'
import AmazonPage2 from './pages/AmazonPage2'
import ThanksPage from './pages/ThanksPage';
import ThanksPage2 from './pages/ThanksPage2';
import ThanksPage3 from './pages/ThanksPage3';

import BlogPage from './pages/Blog';
import Blog_1_1 from './pages/Blog_1_1';
import BlogTestPage from './pages/BlogTest';
import HomePage from './pages/Home';
import RedPage from './pages/RedPage';
import ContactPage from './pages/ContactPage';
import SpiegelDe from './pages/SpiegelDe';
import DogeCoinPage from './pages/DogeCoinPage';
import UnsubscribePage from './pages/UnsubscribePage';

import BitcoinSystemDe from './pages/BitcoinSystemDe';
import BitcoinSystemDe2 from './pages/BitcoinSystemDe2';
import BitcoinsWealth from './pages/BitcoinsWealth';
import BitcoinsWealth2 from './pages/BitcoinsWealth2';
import IProfit from './pages/IProfit';
import Remgue1 from './pages/Remgue1';
import Remgue1_1 from './pages/Remgue1_1';
import Remgue2 from './pages/Remgue2';
import Remgue2_2 from "./pages/Remgue2_2";
import Remgue3_3 from "./pages/Remgue3_3";

class App extends Component {
    constructor(props) {
        super(props);

    }
/*

    https://nitrocapitals-inv.de/bitcoinProfit_de
    https://nitrocapitals-inv.de/tesla-investment-de
    https://nitrocapitals-inv.de/L1_de
    https://nitrocapitals-inv.de/L1_1_de
    https://nitrocapitals-inv.de/v1_btc_de
    https://nitrocapitals-inv.de/investstocks_de
    https://nitrocapitals-inv.de/amz1_de
    https://nitrocapitals-inv.de/amz2_de
    https://nitrocapitals-inv.de/doge_de
    https://nitrocapitals-inv.de/spiegel_de

    https://nitrocapitals-inv.de/bitcoin-system-de
    https://nitrocapitals-inv.de/remgue_1_de
    https://nitrocapitals-inv.de/remgue_2_de
    https://nitrocapitals-inv.de/bitcoin-wealth-de
    https://nitrocapitals-inv.de/bitcoin-wealth2-de

*/

    render() {
        return (
            <React.Fragment>
                <Suspense fallback={<div className="loading" />}>
                    <Router>
                        <Switch>
                            <Route exact  path="/bitcoin-system-2-de">
                                <IProfit />
                            </Route>
                            <Route exact  path="/bitcoin-system-1-de">
                                <BitcoinSystemDe />
                            </Route>
                            <Route exact  path="/bitcoin-system-11-de">
                                <BitcoinSystemDe2 />
                            </Route>
                            <Route exact  path="/remgue_1_de">
                                <Remgue1 />
                            </Route>
                            <Route exact  path="/remgue_11_de">
                                <Remgue1_1 />
                            </Route>
                            <Route exact  path="/remgue_2_de">
                                <Remgue2 />
                            </Route>
                            <Route exact  path="/remgue_22_de">
                                <Remgue2_2 />
                            </Route>
                            <Route exact  path="/remgue_33_de">
                                <Remgue3_3 />
                            </Route>
                            <Route exact  path="/bitcoin-wealth-de">
                                <BitcoinsWealth />
                            </Route>
                            <Route exact  path="/bitcoin-wealth2-de">
                                <BitcoinsWealth2 />
                            </Route>
                            <Route exact  path="/thanksPage_de">
                                <ThanksPage />
                            </Route>
                            <Route exact  path="/bitcoinProfit_de">
                                <BitcoinProfit />
                            </Route>
                            <Route exact  path="/tesla-investment-de">
                                <TeslaInvestmentDe />
                            </Route>

                            <Route exact  path="/thanksPage2_de">
                                <ThanksPage2 />
                            </Route>

                            <Route exact  path="/thanksPage3_de">
                                <ThanksPage3 />
                            </Route>
                            <Route exact  path="/L1_de">
                                <BlogPage />
                            </Route>
                            <Route exact  path="/L1_1_de">
                                <Blog_1_1 />
                            </Route>
                            <Route exact  path="/L1_test_de">
                                <BlogTestPage />
                            </Route>
                            <Route exact  path="/v1_btc_de">
                                <HomePage />
                            </Route>

                            <Route exact  path="/investstocks_de">
                                <RedPage />
                            </Route>
                            <Route exact  path="/contact">
                                <ContactPage />
                            </Route>
                            <Route exact  path="/amz1_de">
                                <AmazonPage />
                            </Route>
                            <Route exact  path="/amz1_test_de">
                                <AmazonTestPage />
                            </Route>
                            <Route exact  path="/amz2_de">
                                <AmazonPage2 />
                            </Route>
                            <Route exact  path="/doge_de">
                                <DogeCoinPage />
                            </Route>
                            <Route exact  path="/spiegel_de">
                                <SpiegelDe />
                            </Route>

                            <Route exact  path="/verified/:id?">
                                <VerifiedPage />
                            </Route>
                            <Route exact  path="/unsubscribe">
                                <UnsubscribePage />
                            </Route>


                            <Route exact  path="/blank">
                                <BlankPage />
                            </Route>

                            <Route exact  path="/">
                                <AmazonPage />
                            </Route>
                            <Redirect to="/"/>
                        </Switch>
                    </Router>
                </Suspense>
            </React.Fragment>
        );
    }
}


export default connect()(App);
