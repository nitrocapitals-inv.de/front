import React, {useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import {Modal} from "react-bootstrap";
import {Container, Row, Col} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import Privacy from '../modal/privacy'
import Terms from '../modal/terms'
import LogoBlack from "../../assets/img/mini-logo3.png";

export const Footer = ({logoStatus}) => {
    const [termShow, setTermShow] = useState(false)
    const [privacyShow, setPrivacyShow] = useState(false)
    const { pathname } = useLocation();


    useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);
    return (
        <>
            <footer>
                <div className="footer-info">
                    <div className="footer-info-content">

                        <div className="info-content">
                            <div className="footer_top_" >

                                <Container fluid>
                                <Row>
                                    <Col style={{textAlign : 'left', padding: '0px'}}>
                                        {
                                            logoStatus
                                                ? <img src={LogoBlack} alt="ADStraders" style={{width :'290px', height:'auto'}}   className="logo"/>
                                                : null
                                        }
                                    </Col>
                                </Row>

                                <Row style={{textAlign : 'center'}}>
                                    <Col xs={12} md={4}>
                                        <p style={{cursor:'pointer', fontWeight : 'bold'}} onClick={() => setTermShow(true)} data-target="#terms">
                                            Geschäftsbedingungen
                                        </p>
                                    </Col>
                                    <Col xs={12} md={4}>
                                        <p style={{cursor:'pointer', fontWeight : 'bold', }} onClick={() => setPrivacyShow(true)}
                                            data-target="#policy">Datenschutz-Bestimmungen
                                        </p>
                                    </Col>
                                    <Col xs={12} md={4}>
                                        <p style={{cursor:'pointer', fontWeight : 'bold',  }}
                                            data-target="#policy">
                                            <NavLink to='/contact' style={{color: 'black', textDecoration: 'none'  }}>
                                                Kontakteieren sie uns
                                            </NavLink>

                                        </p>
                                    </Col>

                                </Row>
                                </Container>
                            </div>
                            <Row>
                                <Col>
                                    <p className="description" style={{textAlign : 'left', paddingLeft:'13px'}}>
                                        Risikowarnung: Der <br/><br/>
                                        Handel mit Devisen (Devisen) oder CFDs (Differenzkontrakten) auf Margin birgt ein hohes Risiko und ist möglicherweise nicht für alle Anleger geeignet. <br/>
                                        Es besteht die Möglichkeit, dass Sie einen Verlust in Höhe Ihrer gesamten Anlage erleiden oder diesen übersteigen. <br/>
                                        Daher sollten Sie kein Geld investieren oder riskieren, dessen Verlust Sie sich nicht leisten können. <br/>
                                        Bevor Sie Ex 80-Dienste nutzen, nehmen Sie bitte alle mit dem Handel verbundenen Risiken zur Kenntnis. <br/>
                                        Der Inhalt dieser Website darf nicht als persönliche Beratung ausgelegt werden. Wir empfehlen Ihnen, sich von einem unabhängigen Finanzberater beraten zu lassen. <br/>
                                        Wir verwenden Cookies für die folgenden Zwecke: um bestimmte Funktionen des Dienstes zu ermöglichen, um Analysen bereitzustellen, um Ihre Präferenzen zu speichern, um die Bereitstellung von Werbung zu ermöglichen, einschließlich verhaltensbezogener Werbung. <br/>
                                        Wir verwenden sowohl Sitzungs- als auch dauerhafte Cookies für den Dienst und verwenden verschiedene Arten von Cookies, um den Dienst auszuführen. <br/>
                                        <br/><br/>
                                        © 2021 Alle Rechte vorbehalten
                                    </p>
                                </Col>
                            </Row>
                            <div className="info">
                                <div className="descriptions_">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <Modal
                size="lg"
                show={termShow}
                onHide={() => setTermShow(false)}
                aria-labelledby="example-modal-sizes-title-lg"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                        Geschäftsbedingungen
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Terms/>
                </Modal.Body>
            </Modal>
            <Modal
                size="lg"
                show={privacyShow}
                onHide={() => setPrivacyShow(false)}
                aria-labelledby="example-modal-sizes-title-lg"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                        Datenschutz-Bestimmungen
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Privacy/>
                </Modal.Body>
            </Modal>

        </>
    )
}
