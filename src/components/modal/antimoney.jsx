export default function antimoney() {
    return (
        <>
            <div className="container">
                <div className="align-items-center">
                    <div className="w-100">


                        <div id="elementor-tab-content-1713"
                             className="elementor-tab-content elementor-clearfix elementor-active" data-tab="3"
                             role="tabpanel" aria-labelledby="elementor-tab-title-1713" tabIndex="0"
                             ><p>Anti-Money Laundering Policy</p><p><br/>NitroCapitals does not
                            tolerate money laundering and supports the fight against money launderers. NitroCapitals
                            follows the guidelines set by the UK’s Joint Money Laundering Steering Group. The UK is a
                            full member of the Financial Action Task Force (FATF), the intergovernmental body whose
                            purpose is to combat money laundering and terrorist financing.<br/>NitroCapitals now has
                            policies in place to deter people from laundering money. These policies include:<br/>ensuring
                            clients have valid proof of identification maintaining records of identification
                            information<br/>determining that clients are not known or suspected terrorists by
                            checking their names against lists of known or suspected terrorists<br/>informing
                            clients that the information they provide may be used to verify their
                            identity<br/>closely following clients’ money transactions</p><p>Not
                            accepting cash, money orders, third party transactions, exchange houses transfer or Western
                            Union transfers.<br/>Money laundering occurs when funds from an illegal/criminal activity
                            are
                            moved through the financial system in such a way as to make it appear that the funds
                            have come from legitimate sources.<br/>Money Laundering usually follows three stages:<br/>firstly,
                            cash or cash equivalents are placed into the financial system<br/>secondly, money is
                            transferred or moved to other accounts (e.g. futures accounts) through a series
                            of financial transactions designed to obscure the origin of the money (e.g.
                            executing trades with little or no financial risk or transferring account
                            balances to other accounts)<br/>And finally, the funds are re-introduced into the
                            economy so that the funds appear to have come from legitimate sources (e.g.
                            closing a futures account and transferring the funds to a bank account).<br/>Trading
                            accounts are one vehicle that can be used to launder illicit funds or to
                            hide the true owner of the funds. In particular, a trading account can
                            be used to execute financial transactions that help obscure the origins
                            of the funds.<br/>NitroCapitals directs funds withdrawals back to the
                            original source of remittance, as a preventative measure.<br/>International
                            Anti-Money Laundering requires financial services institutions
                            to be aware of potential money laundering abuses that could
                            occur in a customer account and implement a compliance program
                            to deter, detect and report potential suspicious activity.<br/>These
                            guidelines have been implemented to protect NitroCapitals
                            and its clients.<br/>For questions/comments regarding these
                            guidelines, please contact us at
                            support@NitroCapitals.net</p><p>What is Money
                            Laundering?</p><p>Money laundering is the act of hiding money obtained illegally, so the
                            source appears legitimate. We adhere to strict laws rendering it illegal for us or any of
                            our employees or agents to knowingly engage in or attempt to engage in any activities
                            remotely related to money laundering. Our anti-money laundering policies increase investor
                            protection and client security services, as well as offer safe payment processes.<br/>Identification<br/>The
                            first safeguard against money laundering is sophisticated Know-Your-Client (KYC)
                            verification. To ensure compliance with standard AML regulations, we require you to
                            submit the following documentation:<br/>· Proof of Identification: A clear copy of the
                            front and back of your government-issued photo ID, i.e. a valid passport, driver’s
                            license, or national ID card.<br/>· Proof of Residence: An official document issued
                            within the last 3 months, clearly stating your name and address as registered
                            with NitroCapitals. This can be a utility bill (i.e. water, electric, or
                            landline), or bank statement. Please make sure your copy includes:</p><p>· Your
                            full, legal name<br/>· Your full residential address<br/>· Date of issue (within the last 3
                            months)<br/>· Name of the issuing authority with an official logo or stamp<br/>· Copy of
                            the front and back of your credit card: To ensure your privacy and security, only
                            the last 4 digits of your credit card must be visible. You can also cover the last 3
                            digits on the back of your card (CVV code).<br/>Please note that regulatory
                            requirements differ in certain countries and we may need to request further
                            documentation from you.</p><p>Monitoring</p><p>We do not accept third-party
                            payments. All deposits must be made in your own name and match the KYC documents submitted.
                            Due to AML regulatory policies, all funds withdrawn must be returned to the exact source
                            from which they were received. This means that funds originally deposited via bank transfer
                            will be returned to the same account when a withdrawal request is made. Equally, if you
                            deposited via credit card, your funds will be returned to the same credit card when a
                            withdrawal request is made.<br/>We do not, under any circumstances accept cash deposits or
                            disburse cash when requests for withdrawal are made.<br/>Reporting<br/>AML regulation
                            requires us to monitor and track suspicious transactions and report such activities
                            to the relevant law enforcement agencies. We also reserve the right to deny a
                            transfer at any stage if we believe the transfer to be connected in any manner to
                            criminal activity or money laundering. The law prohibits us from informing customers
                            about reports submitted to law enforcement agencies concerning suspicious activity.
                        </p><p>&nbsp;</p></div>
                    </div>


                </div>
            </div>
        </>
    )
}
