export default function deposit() {
    return (
        <>
            <div className="container">
                <div className="align-items-center">
                    <div className="w-100">


                        <div className="clearfix colelem" id="u5964-145" data-muse-uid="U5964"
                             data-muse-type="txt_frame" data-ibe-flags="txtStyleSrc">
                            <h2>Terms and Conditions</h2>

                            <div id="elementor-tab-content-1712"
                                 className="elementor-tab-content elementor-clearfix elementor-active" data-tab="2"
                                 role="tabpanel" aria-labelledby="elementor-tab-title-1712" tabIndex="0"
                                 ><p>Deposit And Withdrawal Policy</p><p>Trading in any
                                investment opportunity that may generate profit requires NitroCapitals customers to
                                deposit money on their online account. Profits may be withdrawn from the online account.<br/>Deposits
                                and withdrawals are regulated by this WD policy as well as the generally applicable
                                terms and conditions.</p><p>Deposits</p><p>You, the Client, have to perform all the
                                deposits from a source (e.g. single bank account). If you want to start trading, you
                                should make sure this account is in your country of residence and in your name. In order
                                to certify that a SWIFT confirmation is authentic, it has to be sent to NitroCapitals to
                                confirm the origin of the money which will be used for trading. If you don’t comply with
                                this WD policy, you may be prevented from depositing the money via Bank/Wire Transfer.
                                If you did not login and traded from your account within six (6) months (“Dormant
                                Account”), your Dormant Account will be subject to a deduction of 10 % each month (the
                                “Dormant Fee”).</p><p>Withdrawals</p><p>According to generally acceptable AML rules and
                                regulations, withdrawals must be performed only through the same bank account or
                                credit/debit card that you used to deposit the funds.<br/>Unless we agree otherwise,
                                withdrawals from the Account may only be made in the same currency in which the
                                respective deposit was made.<br/>In addition, when you deposit or withdraw money for
                                trading purposes using alternative payment methods, you should be aware that
                                additional fees and restrictions may apply. Withdrawals are subjected to
                                withdrawals processing and handling fees. Those fees will be deducted from the
                                transferred withdrawn amount. The fees schedule is available on
                                NitroCapitals<br/>Without derogating of the foregoing, NitroCapitals may execute
                                withdrawals to a different facility than the one used for the deposit,
                                subject to Anti Money-Laundering regulations.<br/>Furthermore, when it comes
                                to withdrawals, Client may be required to present additional information
                                and documents.</p><p>Withdrawals Fees</p><p>Withdrawals will be charged
                                a transaction fee of the following:<br/>50.00 USD/GBP/EURO for wire transfers; 25.00
                                USD/GBP/EURO for credit cards plus a processing fee of 10.00 USD/7.00 EUR/5.00 GBP;
                                25.00 USD/GBP/EURO for ePayments. A levy of 10% of the withdrawal amount will be
                                charged to any withdrawal from an account that has not executed more than 200 in
                                turnover and/or from accounts that have not been verified.<br/>Minimum withdrawal for
                                wire transfers is 250.00 USD/GBP/EURO. Minimum withdrawal on any other method is
                                100.00 USD/GBP/EURO. Note that these charges exclude the transaction fee
                                imposed; for instance, if a minimum of $250 is to be withdrawn by bank wire
                                transfer, a transaction fee of $50 will be charged.<br/>Fees may change depends
                                on the processing system and/or bank of NitroCapitals.</p><p>Non-Deposited
                                Funds</p><p>Funds appearing on Clients’ account may include agreed or voluntary bonuses
                                and incentives, or any other sums not directly deposited by the Client or gained from
                                trading on account of actually deposited funds (“Non-Deposited Funds”). Please note
                                unless otherwise explicitly agreed, Non-Deposited Funds are not available for
                                withdrawal. Further, due to technical limitations, Non-Deposited Funds may be assigned
                                to Client’s account in certain occasions (for example, for the technical purpose of
                                allowing the closing of positions or an indebted account).<br/>Without derogating from
                                the abovementioned, bonuses issued to Client by NitroCapitals may only be withdrawn
                                subject to execution of a minimum trading volume of 25 times the deposit amount plus
                                the bonus issued (“Minimum Trading Volume“).<br/>Submitting A Withdrawal Request</p>
                                <p>In order to process your withdrawal request, you must:</p><p>• Open a withdrawal
                                    request from client area.<br/>• Print the [withdrawal.pdf] form. Client will log in
                                    to his account through the website, click on withdrawal, fill up the information
                                    and fill up the withdrawal form.<br/>• Sign the printed form.<br/>• All compliance
                                    documentation must have been received and approved by NitroCapitals
                                    compliance officer in order to proceed with the withdrawal.<br/>• Beneficiary
                                    Name must match the name on the trading account. Requests to transfer
                                    funds to third party will not be processed.</p><p>IMPORTANT: ACCOUNT
                                    HOLDER IS REQUIRED TO MONITOR ACCOUNT REGULARLY, AND ENSURE THAT AVAILABLE MARGIN
                                    EXISTS IN THE ACCOUNT PRIOR TO SUBMITTING THIS REQUEST, AS SUCH WITHDRAWAL MAY HAVE
                                    AN IMPACT ON EXISTING OPEN POSITIONS OR TRADING STRATEGY USED.</p><p>Typical
                                    Withdrawal Processing Time</p><p>The time it takes for the money to reach your
                                    credit card or bank account that has been used to deposit funds may vary (usually up
                                    to five business days). Note that it might take longer for withdrawals to bank
                                    accounts due to the additional security procedures in force.<br/>The request will
                                    generally be processed by NitroCapitals within 2 to 5 working days of receipt.
                                    In order to avoid any delays please review your information carefully before
                                    submitting your request. NitroCapitals assumes no responsibility for errors or
                                    inaccuracies made by the account holder. Corresponding withdrawals will take 3
                                    to 5 working days to process. NitroCapitals cannot monitor and is not
                                    responsible in any way for the Client’s Credit Card Company or bank’s internal
                                    procedures. Client must follow up with the credit card or respective bank
                                    independently.<br/>Funds are released to your credit account once your credit
                                    card merchant has debited the funds from our account. This process may take
                                    up to 2-14 working days or more to reflect on your credit card account
                                    balance. If you do not have online access to your credit card, it should
                                    appear on the next billing statement(s) depending on your card’s billing
                                    cycle.<br/>Please note clearly that we are not committed to any time frame
                                    and that any additional charges imposed by third parties shall be
                                    deducted from the deposit or the withdrawal, as applicable.<br/>Additional
                                    Charges: If the receiving bank uses an intermediary bank to
                                    send/receive funds, you may incur additional fees charged by the
                                    intermediary bank. These charges are usually placed for transmitting
                                    the wire for your bank. NitroCapitals is not involved with and nor
                                    has any control over these additional fees. Please check with your
                                    financial institution for more information.</p><p>Credit/Debit
                                    Cards</p><p>For Credit card deposits, when you choose an account in a different
                                    currency than USD (United States Dollar), your credit card will be debited properly
                                    in accordance with amount deposited and the applicable exchange rates. In addition
                                    to the exchanged sum deposited, additional credit cards fees may apply (as a result,
                                    in such cases you may notice discrepancies between the sum of deposit and the sum
                                    charged on your credit card). Customers must accept these slight variations that can
                                    occur and won’t try to charge this back.<br/>If you have used a credit card to
                                    deposit money, performed online trading and decide to cash in on your winnings,
                                    the same credit card must be used.<br/>Amount of withdrawal per credit card is
                                    only allowable to an equal amount of money deposited per credit card or
                                    less. Greater amounts must be wire-transferred to a bank account.</p>
                                <p>Currency</p><p>Your Account may comprise of different currencies. These will be
                                    subject to the following conditions:<br/>We may accept payments into the account in
                                    different currencies and any payments due to or from us and any net balances on
                                    the account shall be reported by us in the respective currency; The account is
                                    maintained in US Dollars, Euro or GB Pounds (“Base Currencies”) and any other
                                    currency will be converted at the exchange rate existing at the point of
                                    conversion (“Exchange Rate”); if the Client send funds in another currency than
                                    his account’s currency, we will apply an exchange rate to our discretion.<br/>We
                                    will generally settle trades or perform any required setoffs and deductions
                                    in the relevant currency where the account comprises such currency ledger,
                                    save that where such currency balance is insufficient, we may settle trades
                                    in any currency using the Exchange Rate.<br/>Additional Conditions<br/>Please
                                    note this policy cannot be exhaustive, and additional conditions or
                                    requirements may apply at any time due to regulations and policies,
                                    including those set in order to prevent money laundering. Please note
                                    any and all usage of the site and services is subject to the Terms and
                                    Conditions, as may be amended from time to time by NitroCapitals , at
                                    its sole discretion.<br/>For queries concerning policy matters, please
                                    contact us anytime.</p><p>Refund and Return policy</p><p>The Refund
                                    Policy has been developed for the purpose of reducing the Company’s financial and
                                    legal risks of the Company, as well as observing principles of anti-money laundering
                                    and counter terrorist activity.</p><p>The Company has the right to unilaterally
                                    block the access to the Secure Client Area, suspend trading activity of any accounts
                                    held by the Trader, cancel a request for transfer/ withdrawal, or make a refund, if
                                    the source of funds or the Client’s activities contradict the anti-money laundering
                                    and counter terrorist financing policy.</p><p>The Company does not cancel the
                                    implemented trade transactions, therefore the Company has the right to return the
                                    funds to the remitter, if within one month from the date of recharge, no trading
                                    activity has been recorded on the trading accounts.</p><p>&nbsp;</p><p>The Company
                                    has the right, under certain objective reasons and, if it is necessary, to make a
                                    refund of funds received via any payment system including credit/debit cards.
                                    Furthermore, the refund will be made to electronic wallets and bank details, which
                                    have been used by the Client when paying in the funds.</p><p>Should the Company
                                    classify the activities of the Client as inappropriate or contradicting the usual
                                    purpose of the Company’s services usage, where there is a direct, or indirect,
                                    illegal or dishonest intent, the Company reserves the right to act within the
                                    framework of this document, without informing the Client in advance. All direct or
                                    indirect losses; expenses connected with transfer of funds are reimbursed to the
                                    Company from the Client’s funds.</p><p>&nbsp;</p><p>When replenishing a trade
                                    account with any card (e.g.: credit card, debit card, prepaid card), the Client
                                    agrees not to lodge a request to the bank to charge back the payment which has
                                    already been credited to the trade account or to the provider of a credit/debit bank
                                    card, both during and after the use of the Company’s services. Any such attempt will
                                    be treated by the Company as a breach of the Terms for the provision of services,
                                    for which the Client may be subjected to responsibility in accordance with national
                                    legislation. If the Company still withdraws the transaction payment back, we reserve
                                    the right to block the access to the Secure Client Area, freeze the Client’s current
                                    balance and send the monetary funds back to the account of the Client, after payment
                                    of all services and fees.</p><p>&nbsp;</p></div>

                        </div>

                    </div>


                </div>
            </div>
        </>
    )
}
