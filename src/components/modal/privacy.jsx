export default function privacy () {
    return (
        <>
            <div className="container">
                <div className="elementor-widget-wrap elementor-element-populated">
                    <p >
                        DATENSCHUTZ-BESTIMMUNGEN
                    </p>
                    <p >
                        Durch den Zugriff, den Besuch oder die Nutzung dieser Website oder einer anderen darauf befindlichen Website erklären Sie unweigerlich und vorbehaltlos Ihre verbindliche Zustimmung zu allen unten aufgeführten Bedingungen.
                    </p>
                    <br/>
                    <br/> • Ihre Privatsphäre ist das Wichtigste. Diese Datenschutzrichtlinie zeigt, wie wir Informationen über Website-Benutzer sammeln, wofür wir sie verwenden und was Sie tun können, um Ihre Privatsphäre zu schützen.
                    <br/> • Diese Richtlinie gilt für alle Informationen, die auf unserer Website gesammelt und übermittelt werden. Möglicherweise dürfen Sie Finanztransaktionen ausführen, Anfragen stellen, Daten übermitteln und sich registrieren, um Materialien zu erhalten usw. Spezifische personenbezogene Daten, die auf unserer Website gesammelt werden können, können den Namen des Benutzers, die Wohnadresse, E-Mail-Adresse, Kontaktnummern, Bank umfassen Konto- und Kreditkartennummer usw., unabhängig davon, ob Sie sich selbst oder eine dritte Partei betreffen, wenn Sie im Namen dieser Partei handeln. Informationen aus mehreren Quellen können von unserer Website kombiniert werden. Sie können einen Teil Ihrer personenbezogenen Daten regelmäßig aktualisieren, indem Sie auf Ihr Site-Konto zugreifen und gemäß den Anweisungen zum „Aktualisieren Ihrer Daten“ fortfahren. Sie bestätigen hiermit, dass Sie darauf hingewiesen wurden, dass einige Ihrer Angaben aufgrund gesetzlicher Vorschriften nicht geändert werden dürfen.
                    <br/> •  Die Website kann auch andere Arten von Informationen abrufen, die von den Servern unserer Website protokolliert werden, einschließlich Ihrer IP-Adresse, Ihres Browsertyps und Ihrer Sprache sowie des Datums und der Uhrzeit Ihres Besuchs, um bestimmte Benutzertrends umzusetzen oder den Zugriff bestimmter Benutzer zu blockieren unsere Internetseite. Davon abgesehen kann unsere Website Ihrem Computer einen oder mehrere Cookies zuweisen, die Informationen abrufen können, um den Zugriff auf die Website zu ermöglichen, Ihr Online-Erlebnis zu personalisieren und Standard- oder Nicht-Standard-Internettools wie Web Beacons oder Überwachung zu verwenden Programme, die Informationen sammeln, die Ihre Nutzung der Website verfolgen und es ermöglichen, unsere Dienste und Werbeaktionen anzupassen.
                    <br/> • Personenbezogene Daten, die Sie uns zur Verfügung gestellt haben, werden ohne Ihre vorherige Zustimmung nicht an Dritte weitergegeben, es sei denn, diese Daten
                    <br/> • A. Muss gegenüber unseren verbundenen Unternehmen offengelegt werden
                    <br/> • B. uns öffentlich bekannt waren und vor dem Zeitpunkt der Offenlegung durch Sie allgemein öffentlich zugänglich gemacht wurden.
                    <br/> • C. Öffentlich bekannt und allgemein zugänglich gemacht werden, nachdem Sie uns diese offengelegt haben, ohne dass wir etwas tun oder nicht tun.
                    <br/> • D. Ist gesetzlich oder gesetzlich vorgeschrieben. In diesem Fall werden wir Sie so früh wie möglich über die vorgeschlagene Offenlegung informieren (einschließlich einer Kopie einer schriftlichen Anfrage), damit Sie diese Offenlegung einschränken oder einschränken können.
                    <br/> • E. Wird uns von einem Dritten in anderer Weise als unter Verletzung unserer Vertraulichkeitsverpflichtungen aus diesem Vertrag zur Verfügung gestellt oder bekannt gegeben; von uns unabhängig ohne Bezug auf die von Ihnen mitgeteilten Informationen entwickelt wurden oder uns zum Zeitpunkt der Weitergabe durch Sie uneingeschränkt bekannt waren, wie durch schriftliche Unterlagen nachgewiesen und nachgewiesen werden muss.
                    <br/> • F. Die Offenlegung ist erforderlich, damit wir unsere Rechte schützen können.
                    <br/> • Um unbefugten Zugriff zu verhindern, die Datengenauigkeit zu wahren und die korrekte Verwendung von Informationen zu gewährleisten, haben wir geeignete physische, elektronische und verwaltungstechnische Verfahren eingeführt, um die online erfassten Informationen zu schützen und zu sichern.
                    <br/> • Um die Privatsphäre von Kindern zu schützen, sammeln oder speichern wir niemals wissentlich Informationen von Benutzern unter 18 Jahren; Kein Teil unserer Website ist darauf ausgelegt, Personen unter 18 Jahren anzusprechen. 8 Auf Ihren Wunsch können Sie wählen und verlangen, keine E-Mail-Newsletter von der Website zu erhalten, und Cookies löschen oder ablehnen, indem Sie Ihre Browsereinstellungen ändern.
                    <br/> •  Auf Ihren Wunsch können Sie wählen und verlangen, keine E-Mail-Newsletter von der Website zu erhalten, und Cookies löschen oder ablehnen, indem Sie Ihre Browsereinstellungen ändern.
                    <br/>
                    <br/>

                </div>
            </div>
        </>
    )
}
