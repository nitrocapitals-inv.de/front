import React, {useState,} from "react";
import {useForm} from "react-hook-form";
import ReactCustomFlagSelect from "react-custom-flag-select";
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";

import HeaderImg from '../assets/img/header-img.png'
import AvailableAtAmazon from '../assets/img/available_at_amazon.png'

import Asset1 from '../assets/img/Asset1.png'
import Asset2 from '../assets/img/Asset2.png'
import Asset3 from '../assets/img/Asset3.png'
import Asset4 from '../assets/img/Asset4.png'
import Image from '../assets/img/image.png'
import PaymentMethods from '../assets/img/payment-methods.png'
import LogoBlack from '../assets/img/logo-black.6f8c30a.png'
import {Modal} from "react-bootstrap";
import {Footer} from "../components/layout/footer";


const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

let imgs = [
    '//cdn-baff.s3.amazonaws.com/lp/flags/ca.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/kz.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/ru.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/eg.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/za.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/gr.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/nl.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/be.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/fr.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/es.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/hu.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/it.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/ro.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/ch.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/at.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/gg.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/im.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/je.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/gb.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/dk.svg',
    '//cdn-baff.s3.amazonaws.com/lp/flags/se.svg',
];

const FLAG_SELECTOR_OPTION_LIST = [
    { id: "1", name: "1",  displayText: "1",  flag: imgs[0] },
    { id: "KZ", name: "7",  displayText: "7",  flag: imgs[1] },
    { id: "RU", name: "7",  displayText: "7",  flag: imgs[2] },
    { id: "20", name: "20",  displayText: "20",  flag: imgs[3] },
    { id: "27", name: "27",  displayText: "27",  flag: imgs[4] },
    { id: "30", name: "30",  displayText: "30",  flag: imgs[5] },
    { id: "31", name: "31",  displayText: "31",  flag: imgs[6] },
    { id: "32", name: "32",  displayText: "32",  flag: imgs[7] },
    { id: "33", name: "33",  displayText: "33",  flag: imgs[8] },
    { id: "34", name: "34",  displayText: "34",  flag: imgs[9] },
    { id: "36", name: "36",  displayText: "36",  flag: imgs[10] },
    { id: "39", name: "39",  displayText: "39",  flag: imgs[11] },
    { id: "40", name: "40",  displayText: "40",  flag: imgs[12] },
    { id: "41", name: "41",  displayText: "41",  flag: imgs[13] },
    { id: "43", name: "43",  displayText: "43",  flag: imgs[14] },
    { id: "44", name: "44",  displayText: "44",  flag: imgs[15] },
    { id: "44", name: "44",  displayText: "44",  flag: imgs[16] },
    { id: "44", name: "44",  displayText: "44",  flag: imgs[17] },
    { id: "44", name: "44",  displayText: "44",  flag: imgs[18] },
    { id: "45", name: "45",  displayText: "45",  flag: imgs[19] },
    { id: "46", name: "46",  displayText: "46",  flag: imgs[20] },
];

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',


        };
        this.submit = this.submit.bind(this);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        })
    }
    handleRegistration = (event) => {
        console.log("@", this.state.checked)
        if (this.state.firstName) {
            this.setState({firstNameErr: false})
        } else {
            this.setState({firstNameErr: true})
        }
        if (this.state.lastName) {
            this.setState({lastNameErr: false})
        } else {
            this.setState({lastNameErr: true})
        }
        if (this.state.email) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }
        if (this.state.phone) {
            this.setState({phoneErr: false})
        } else {
            this.setState({phoneErr: true})
        }
        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()
    }

    _scrollToTop() {
        window.scrollTo({top:0, behavior :'smooth'})
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <>
                <section id="headerSection" className="header-section">
                    <div className="container">
                        <img className="img header-img" src={HeaderImg} alt=""/>
                        <div className="row">
                            <div className="col-lg-4 col-md-12 col-8 offset-lg-4 offset-4">
                                <div className="logo-wrapper">
                                    <div className="heading-text">
                                        <div className="row-one">Mit</div>
                                        <div className="row-two">AMAZON</div>
                                        <div className="row-three">
                                            Geld verdienen. Steigern Sie Ihre potenziellen Gewinne
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 form-out form-desktop" data-select2-id="10">
                                <div className="form-wrapper" id="signin-d">
                                    <div className="form-text">
                                        Investieren Sie in <span className="font-weight-bold">Amazon</span>
                                    </div>
                                    <div className="reg-form">
                                        <form className="x-form registration-form ltr">
                                            <div className="x-content">
                                                <div className="x-row">
                                                    <div className="x-half-row">
                                                        <div
                                                            className={`input x-input-must-validate ${this.state.firstNameErr ? 'x-input-error' : ''}`}>
                                                            <input type="text" placeholder="Vorname"
                                                                   name="firstName"
                                                                   onChange={this.handleChange("firstName")}
                                                                   value={this.state.firstName}
                                                            />
                                                            {
                                                                this.state.firstNameErr ? <label
                                                                    className="x-error">Erforderlich</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                    <div className="x-half-row">
                                                        <div
                                                            className={`input x-input-must-validate ${this.state.lastNameErr ? 'x-input-error' : ''}`}>
                                                            <input type="text" placeholder="Nachname"
                                                                   onChange={this.handleChange("lastName")}
                                                                   value={this.state.lastName}
                                                            />
                                                            {
                                                                this.state.lastNameErr ? <label
                                                                    className="x-error">Erforderlich</label> : null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="x-row">
                                                    <div
                                                        className={`input x-input-must-validate ${this.state.emailErr ? 'x-input-error' : ''}`}>
                                                        <label
                                                            className="icon-right" htmlFor="email"><i
                                                            className="x-icon icon-envelope-o"></i></label><input
                                                        type="email"
                                                        placeholder="E-Mail-Adresse"
                                                        onChange={this.handleChange("email")}
                                                        value={this.state.email}
                                                        autoComplete="off" className="field-error"/>
                                                        {
                                                            this.state.emailErr ? <label className="x-error">Ungültige
                                                                Adresse</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <div className="x-country-replace"></div>
                                                <div className="x-phone-replace" data-select2-id="9">
                                                    <div className="x-flex-row" data-select2-id="8">
                                                        <div className='input'>
                                                            <ReactCustomFlagSelect
                                                                attributesWrapper={{
                                                                    id: "areaCodeWrapper",
                                                                    tabIndex: "1"
                                                                }} //Optional.[Object].Modify wrapper general attributes.
                                                                attributesButton={{id: "areaCodeButton"}} //Optional.[Object].Modify button general attributes.
                                                                attributesInput={{
                                                                    id: "areaCode",
                                                                    name: "areaCode"
                                                                }} //Optional.[Object].Modify hidden input general attributes.
                                                                value={currentItem.id} //Optional.[String].Default: "".
                                                                disabled={false} //Optional.[Bool].Default: false.
                                                                showSearch={true} ////Optional.[Bool].Default: false.
                                                                showArrow={true} //Optional.[Bool].Default: true.
                                                                animate={true} //Optional.[Bool].Default: false.
                                                                optionList={FLAG_SELECTOR_OPTION_LIST} //Required.[Array of Object(s)].Default: [].
                                                                // selectOptionListItemHtml={<div>us</div>} //Optional.[Html].Default: none. The custom select options item html that will display in dropdown list. Use it if you think the default html is ugly.
                                                                // selectHtml={<div>us</div>} //Optional.[Html].Default: none. The custom html that will display when user choose. Use it if you think the default html is ugly.
                                                                classNameWrapper="" //Optional.[String].Default: "".
                                                                classNameContainer="" //Optional.[String].Default: "".
                                                                classNameOptionListContainer="" //Optional.[String].Default: "".
                                                                classNameOptionListItem="" //Optional.[String].Default: "".
                                                                classNameDropdownIconOptionListItem={{color: 'red', }} //Optional.[String].Default: "".
                                                                customStyleWrapper={{}} //Optional.[Object].Default: {}.
                                                                customStyleContainer={{
                                                                    background: '#fff',
                                                                    border: "2px solid rgba(0,0,0,.12)",
                                                                    borderRadius: '3px',
                                                                    fontSize: '16px',
                                                                    height: '48px',
                                                                    width: '100%',
                                                                    outline: '0'
                                                                }} //Optional.[Object].Default: {}.
                                                                customStyleSelect={{width: "80px"}} //Optional.[Object].Default: {}.
                                                                customStyleOptionListItem={{

                                                                }} //Optional.[Object].Default: {}.
                                                                customStyleOptionListContainer={{
                                                                    maxHeight: '300px', overflow: 'auto', width: '120px',
                                                                }} //Optional.[Object].Default: {}.
                                                                onChange={(areaCode) => {
                                                                    this.setState({areaCode: areaCode}, () => {
                                                                        this.handlePhoneChange(phone);
                                                                    });
                                                                }}
                                                            />
                                                        </div>

                                                        <div
                                                            className={`input x-input-must-validate ${this.state.phoneErr ? 'x-input-error' : ''}`}>
                                                            <label className="icon-right" htmlFor="phone"><i
                                                                className="x-icon icon-phone"></i></label><input
                                                            type="text" placeholder="555 12 34 56"
                                                            onChange={this.handleChange("phone")}
                                                            value={this.state.phone}
                                                            className="field-error"/>
                                                            {
                                                                this.state.phoneErr ? <label className="x-error">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="x-recaptcha-replace"></div>


                                                <div className="x-row">
                                                    <div className="input x-input-error">
                                                        <input type="checkbox"
                                                               name="checked"
                                                               id="cb_cond_1_0"
                                                               checked={this.state.checked}
                                                               onChange={this.handleInputChange}
                                                               className="field-error"/>&nbsp;
                                                        <label htmlFor="cb_cond_1_0" className="checkbox">Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.</label>
                                                        {
                                                            this.state.checkedErr ? <label className="x-error">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <button
                                                    onClick={this.handleRegistration}
                                                    type="button" className="x-form-button"
                                                    style={{background: '#FFF', color: '#000'}}>JETZT ANFANGEN
                                                </button>
                                                <div className="x-text-notice">
                                                    <div>Um in Aktien zu investieren, müssen Sie mindestens 18 Jahre alt
                                                        sein. Mindesteinzahlung erforderlich € 250
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="invest-section">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="container text-center">
                        <span className="invest-text text-center col-11">Investieren Sie in ein echtes
                          <br className="d-none d-md-inline"/>
                          "Billionen-Dollar-Unternehmen”
                          </span>
                                <img className="invest-arrow text-center col-11" src={AvailableAtAmazon}
                                     alt="available_at_amazon"/>
                            </div>
                            <div className="col-md-8 offset-md-2 form-out form-mobile">
                                <div className="form-wrapper" id="signin-m">
                                    <div className="reg-form"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="how-work-section">
                    <div className="container">
                        <div className="how-wrapper">
                            <h3 className="how-title text-center bold">
                                Das Starten ist einfach. Zugriff bekommen auf:
                            </h3>
                            <div className="row">
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset1} alt="Asset1"/>
                                        <div className="asset-text">Sichere und autorisierte Plattform</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset2} alt="Asset2"/>
                                        <div className="asset-text">
                                            Verdienen Sie in 24 Stunden auf Ihrem Konto
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset3} alt="Asset3"/>
                                        <div className="asset-text">Einfache Lernwerkzeuge</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset4} alt="Asset4"/>
                                        <div className="asset-text-last">
                                            24 Stunden kontinuierliche Unterstützung
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="futures-section">
                    <div className="container">
                        <div className="
                        col-xl-6 col-lg-8 col-md-8
                        offset-xl-2 offset-lg-3 offset-md-2
                        futures-text
                      ">
                            <div className="futures-title">Eine echte "Billionärsfirma"</div>
                            <p>
                                Die Marktkapitalisierung von Amazon erreichte 2018 1 Billion Dollar.
                                Laut Bloomberg könnte Amazon bis 2025 einen jährlichen Bruttoumsatz
                                von über 1 Billion USD erzielen.
                            </p>
                            <p>
                                Mit steigenden Markteinnahmen ist Amazon der unbestrittene
                                Marktführer. Investoren und Personen, die am Amazon-Modell
                                teilnehmen, können weiterhin von den Entwicklungsaussichten für
                                Amazon profitieren.
                            </p>
                        </div>
                        <div className="futures-wrap">
                            <img className="hand" src={Image} alt="hand-image"/>
                            <button className="btn-d scroll-to-form-d"
                                    onClick={this._scrollToTop.bind(this)}
                                    >
                                Entdecken Sie, wie Sie von Amazon-Investitionen profitieren können
                            </button>
                            <button className="btn-m scroll-to-form-m"
                                    onClick={this._scrollToTop.bind(this)}
                            >
                                Entdecken Sie, wie Sie von Amazon-Investitionen profitieren können
                            </button>
                            <img className="payments" src={PaymentMethods} alt="payment-methods"/>
                        </div>
                    </div>
                </section>


                <Footer/>

                <Modal
                    size="lg"
                    show={this.state.lgShow}
                    onHide={() => this.setState({lgShow: false})}
                    aria-labelledby="example-modal-sizes-title-lg"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                            Terms And Conditions
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <h2>ADStraders</h2>
                            <h2>
                                Please read these terms and conditions carefully before using
                                this site
                            </h2>
                            <ol>
                                <li>
                                    This client agreement, together with any Schedule(s) and
                                    accompanying documents, as amended from time to time
                                    (hereafter the Agreement) sets out the terms of the contract
                                    between you and us.
                                </li>
                                <li>
                                    I understand all such terms and that these Terms and
                                    Conditions, together with all policies are an inseparable part
                                    of a binding agreement between me and ADStraders (the
                                    Agreement).
                                </li>
                                <li>
                                    I am over 18 and the information provided in this application
                                    is true and correct and that I will notify ADStraders of any
                                    material changes.
                                </li>
                                <li>
                                    ADStraders may not be able to ascertain the appropriateness of
                                    any product to me, including due to lack of experience or
                                    undisclosed information.
                                </li>
                                <li>
                                    I have entered full, accurate and truthful details, including
                                    my residential address and identity.
                                </li>
                                <li>
                                    ADStraders may at any time without limitation amend any of the
                                    terms set out in this agreement by posting such information on
                                    our website.
                                </li>
                                <li>
                                    I confirm that I got an opportunity to trade on a demo
                                    platform and/or read a tutorial and I understand as a
                                    consequence how to trade and the RISKS that are involved. In
                                    addition, I have read and understood the risks that are
                                    involved in trading in Forex.
                                </li>
                                <li>
                                    By filling the Subscription Form, I request and agree to open
                                    an account for myself and confirm that I have read and
                                    understood all policies and terms including the Risk
                                    Disclosure and Terms and Conditions.
                                </li>
                            </ol>
                            <h2>Preamble</h2>
                            <p>
                                ADStraders offers access to its website and Services to clients
                                that accept the terms and conditions. By using the website, you
                                explicitly confirm that you have read these Terms and Conditions
                                and agree to them.
                            </p>
                            <p>
                                The following terms and conditions apply to all users and in
                                case you require additional information on any of the subjects
                                presented below, feel free to contact our customer support at
                                any time. You must first read and then accept these Terms and
                                Conditions in order to benefit of the best Services provided by
                                ADStraders and the agreement is effective upon acceptance. If
                                you dot agree to be bound by the terms and conditions of the
                                present agreement, make sure to inform us in writing. If
                                ADStraders decides to make any amendments they will be posted
                                online and in order to keep trading on the website, you might be
                                asked to accept the new terms.
                            </p>
                            <p>
                                In order to be eligible to operate your trading with ADStraders
                                you must be over the age of 18 and to fully understand the risks
                                involved. You hereby acknowledge that you have the knowledge and
                                experience to trade and understand the risks involved. We cat be
                                held responsible for any damage or loss and we are not
                                responsible for verifying whether you possess sufficient
                                knowledge or whether your trading choices are wise.
                            </p>
                            <p>
                                The same conditions apply regarding legal restrictions. The fact
                                that you can access our website and use the tools ADStraders
                                offers doest necessarily mean that it is legal in your country
                                to do so.
                            </p>
                            <p>
                                Users accessing this site are required to inform themselves
                                about the jurisdiction restrictions and you are hereby declaring
                                that the funds you are using for trading are not originating
                                from any criminal activity or otherwise breach any law.
                            </p>
                            <p>
                                Do not abuse the license or Services provided to you. By
                                accepting the Terms and Conditions you will be granted a limited
                                license, but you cat deep-link to the website or copy and resell
                                the materials appearing on ADStraders. The information provided
                                by us should be used entirely for executing transactions inside
                                or within ADStraders.
                            </p>
                            <p>
                                Please note that closing or suspending your account due to a
                                breach of the terms of service and explicitly the Acceptable Use
                                Policy, may entail loss due to the closing of positions.
                            </p>
                            <p>
                                Do not provide false or inaccurate information. The license
                                granted will be terminated if we notice that the data you
                                provide is not accurate and you will cease to access the
                                Services. You agree that ADStraders can terminate your access to
                                any of the Services offered for online trading at its sole
                                discretion and with or without notice and close your
                                transaction.
                            </p>
                            <p>
                                The present terms and conditions state that under no
                                circumstances shall ADStraders or its employees be liable for
                                lost profits or any kind of damage occurring during trading, due
                                to connection to our website, including negligence. In any
                                proven circumstance, the liability of ADStraders is limited to
                                our last monthly commissions received of money you transferred
                                or deposited in your trading account.
                            </p>
                            <h2>Terms and conditions</h2>
                            <ol>
                                <li>
                                    The Preamble to these terms of service is inseparable part
                                    thereof and together with all company policies constitute a
                                    binding Agreement between the User and ADStraders.
                                </li>
                            </ol>
                            <p>
                                You and any person making use of the Site are referred to
                                hereunder as User and/or Client.
                            </p>
                            <ol>
                                <li>
                                    By accessing, visiting and/or using this Site, any person so
                                    doing (hereinafter: User) unequivocally and unreservedly
                                    expresses his or her binding agreement to any and all of these
                                    Terms and Conditions, constituting a binding agreement between
                                    the User and ADStraders, and undertakes to fully comply
                                    therewith. Any and all activity on, with and/or via this Site
                                    shall be governed by these Terms and Conditions.
                                </li>
                            </ol>
                            <p>
                                By using this site in any way you confirm your irrevocable
                                acceptance and agree to the following terms and conditions.
                            </p>
                            <p>
                                If you do not accept any of these terms and conditions or any of
                                the policies you cannot use ADStraders Services and please stop
                                using this site at once.
                            </p>
                            <ol>
                                <li>
                                    Permitted Use: Any User must be aged 18 or older and legally
                                    permitted to engage in usage of this Site under the Saint
                                    Vincent and the Grenadines laws applicable to him/her. User
                                    represents that he or she has full legal capacity to enter
                                    into legally binding agreements. Users may not make commercial
                                    and/or serial and/or automated use of this Site. This Site
                                    strictly forbids any use of data mining, data gathering,
                                    bandwidth theft, offline browsing plug-ins and software and/or
                                    download and/or batch download or access and any access via
                                    any software except for the main common and official web
                                    browsers.
                                </li>
                                <li>
                                    Your Account and the Bank Account: Upon receipt of your
                                    application, we may carry out credit or other checks as we
                                    deem appropriate from time to time including without limit
                                    obtaining references from your bank, employer or credit
                                    agencies (if applicable). Credit reference agencies will
                                    record details of the search irrespective of whether your
                                    application proceeds or not. We reserve the right to carry out
                                    further credit checks at anytime whilst this Agreement is in
                                    force.
                                </li>
                            </ol>
                            <ol>
                                <li>
                                    You acknowledge and accept that we may use credit scoring
                                    methods to assess your application. This may affect our
                                    decision on whether to accept the application or whether to
                                    alter the way in which your Account will operate.
                                </li>
                                <li>
                                    You must notify us immediately of any material changes to any
                                    of the information set out in your Application Form.
                                </li>
                                <li>
                                    In the event that your Application Form is accepted we will
                                    open an Account for you and provide you with a user ID and
                                    account number. You must not disclose these details to any
                                    other person. In the event that you believe that these details
                                    are known by a third party then you will notify us
                                    immediately. You will assist us in investigating any misuse of
                                    your Account.
                                </li>
                                <li>
                                    You confirm that we are not obliged to confirm or check the
                                    identity of anyone using or quoting your Account.
                                </li>
                                <li>
                                    You confirm that we shall be entitled (but not obliged) to
                                    make any payments owed to you to one single account for all
                                    sums. We may (but are not obliged to) agree to transfer monies
                                    to different Bank Accounts. Notwithstanding the fact that we
                                    agree to do so, we shall not be liable for any mistakes made
                                    by us in the amount transferred provided that the aggregate
                                    sum transferred pursuant to this Agreement is correct.
                                </li>
                                <li>
                                    Funds appearing on Client account may include agreed or
                                    voluntary bonuses and incentives, or any other sums not
                                    directly deposited by the Client or gained from trading on
                                    account of actually deposited funds (Non-Deposited Funds).
                                    Please note unless otherwise explicitly agreed, Non-Deposited
                                    Funds are not available for immediate withdrawal. Further, due
                                    to technical limitations, Non-Deposited Funds may be assigned
                                    to Cliens account in certain occasions (for example, for the
                                    technical purpose of allowing the closing of positions or an
                                    indebted account). PLEASE NOTE NON-DEPOSITED FUNDS, including
                                    profits gained on account of or derived of the same, are not
                                    Cliens funds. If a withdrawal of Non-Deposited Funds has been
                                    confirmed, ADStraders shall have full right to reclaim any and
                                    all such funds.
                                </li>
                            </ol>
                            <ol>
                                <li>
                                    Further Covenants: In addition to the above and without
                                    limiting the generality of this clause, you:
                                </li>
                            </ol>
                            <ol>
                                <li>
                                    Confirm that the Bank Account details are complete and
                                    accurate and that you will notify us immediately if these
                                    change and will provide us with such documentation as we
                                    request in respect of such revised Bank Account;
                                </li>
                                <li>
                                    Confirm that (unless otherwise agreed by us) the Bank Account
                                    relates to a bank account opened in your country of main
                                    residence;
                                </li>
                                <li>
                                    Acknowledge and accept that we are under no obligation to
                                    transfer any monies to or accept any monies from any account
                                    other than the Bank Account.
                                </li>
                                <li>
                                    Except in the case of fraud (that not includes fraud from a
                                    third party), we do not accept responsibility for any loss or
                                    damage suffered by you as a result of your trading on monies
                                    deposited in or credited to your Account in error by or upon
                                    our behalf.
                                </li>
                                <li>
                                    We reserve the right to close or suspend your Account at any
                                    time in accordance with the terms of this Agreement.
                                </li>
                                <li>
                                    Acknowledge explicitly that ADStraders has the right to change
                                    the amount of margin that is allowable per any trading due to
                                    market volatility, without prior notice.
                                </li>
                                <li>
                                    Multiple Accounts: Except as otherwise expressly provided in
                                    this Agreement, if you have more than one Account with us,
                                    each Account will be treated entirely separately. Therefore,
                                    any credit on one Account (including monies deposited as
                                    margin) will not discharge your liabilities in respect of
                                    another Account unless we exercise our rights under this
                                    Agreement.
                                </li>
                                <li>
                                    Joint Accounts/beneficiaries: If an Account belongs to
                                    multiple users or to a corporation, company, partnership or
                                    any other corporate body, all beneficiaries or signatories
                                    will be required to approve a withdrawal.
                                </li>
                            </ol>
                            <ol>
                                <li>
                                    Linking to this Site: Creating or maintaining any link from
                                    another site to any page on this site, without ADStraders
                                    written permission is prohibited. Running or displaying this
                                    Site or any information or material displayed on this Site in
                                    frames or through similar means on another Site without our
                                    prior written permission is prohibited.
                                </li>
                                <li>
                                    IP and Copyright Notice: All texts, graphics, sounds,
                                    information, designs, applications, content, source codes and
                                    object code files, and other material displayed on or that can
                                    be downloaded from this Site are protected by copyright,
                                    trademark and other laws and may not be used except as
                                    permitted in these Terms and Conditions or with prior written
                                    permission of the owner of such material (hereinafter:
                                    information or data ). The information on this Site belongs to
                                    ADStraders or its respective affiliates and suppliers and may
                                    not be copied or used without prior approval. You may not
                                    modify the information or materials displayed on or that can
                                    be downloaded from this Site in any way or reproduce or
                                    publicly display, perform, or distribute or otherwise use any
                                    such information or materials for any public or commercial
                                    purpose. Any unauthorized use of any such information or
                                    materials may violate copyright laws, trademark laws, laws of
                                    privacy and publicity, and other laws.
                                </li>
                                <li>
                                    Force majeure: Whilst we will endeavor to comply with our
                                    obligations in a timely manner we will incur no liability
                                    whatsoever for any partial or non-performance of our
                                    obligations by reason of any cause beyond our reasonable
                                    control including but not limited to any communications,
                                    Systems or computer failure, market default, suspension,
                                    failure or closure, or the imposition or change (including a
                                    change of interpretation) of Saint Vincent and the Grenadines
                                    laws or any law or governmental requirement and we shall not
                                    be held liable for any loss you may incur as a result thereof.
                                </li>
                            </ol>
                            <ol>
                                <li>
                                    Without prejudice to the generality of this clause, the
                                    following events shall be considered as an event of force
                                    majeure:
                                </li>
                                <ol>
                                    <ol>
                                        <li>
                                            where we are (in our opinion) unable to maintain an
                                            orderly market as a consequence of civil unrest,
                                            terrorism, strikes, riots or power or communication
                                            failure;
                                        </li>
                                        <li>excessive volatility in the financial markets;</li>
                                        <li>
                                            suspension, closure or liquidation of underlying markets.
                                        </li>
                                    </ol>
                                </ol>
                                <li>
                                    Without prejudice to the generality of this clause, in the
                                    event of force majeure we will be entitled to:
                                </li>
                                <ol>
                                    <ol>
                                        <li>alter trading times;</li>
                                        <li>alter the Margin Requirement;</li>
                                        <li>close or cancel any open contracts/positions.</li>
                                    </ol>
                                </ol>
                            </ol>
                            <ol>
                                <li>
                                    Trademarks: Certain trademarks, trade names, service marks and
                                    logos used or displayed on this Site are registered and
                                    unregistered trademarks, trade names and service marks of
                                    ADStraders and its affiliates. Other trademarks, trade names
                                    and service marks used or displayed on this Site are the
                                    registered and unregistered trademarks, trade names and
                                    service marks of their respective owners. Nothing contained on
                                    this Site grants or should be construed as granting, by
                                    implication, estoppel, or otherwise, any license or right to
                                    use any trademarks, trade names, service marks or logos
                                    displayed on this Site without the written permission of
                                </li>
                                <li>
                                    Logins Security: It is your responsibility to maintain your
                                    Account at all such times. This includes ensuring that the
                                    required level of margin is in place. If you have more than
                                    one Account, this responsibility will relate to each Account
                                    separately, unless we have agreed otherwise in writing with
                                    you.
                                </li>
                            </ol>
                            <p>
                                Every person using a login (whether or not in fact such person
                                is a duly authorized client) will be deemed to be authorized to
                                enter into the System and/or (as the case may be) to give any
                                other instructions or communications on behalf of ADStraders
                                client that is represented by the login according to the
                                registration information provided by ADStraders. ADStraders will
                                act on such instructions without being obliged to obtain any
                                further written or other confirmation, and, for the avoidance of
                                doubt, the relevant transaction fees shall become payable.
                            </p>
                            <ol>
                                <li>
                                    Unlawful usage: You are prohibited from making any unlawful
                                    usage of this Site, and are informed and aware to the fact
                                    that in addition to these terms and conditions and any and all
                                    policies for usage of this Site. ADStraders does not encourage
                                    conduct that would be considered a criminal offense or give
                                    rise to civil liability, or otherwise violate any law. In
                                    addition to any remedies that we may have at law or in equity,
                                    if we determine, in our sole discretion, that you have
                                    violated or are likely to violate the foregoing prohibitions,
                                    we may take any action we deem necessary to cure or prevent
                                    the violation, including without limitation, the immediate
                                    removal of the related materials and/or User from this Site.
                                    We will fully cooperate with any law enforcement authorities
                                    or court order or subpoena requesting or directing us to
                                    disclose the identity of anyone posting such materials.
                                </li>
                                <li>
                                    User undertakes that any and all information provided by him
                                    is true, accurate, complete and up-to-date.
                                </li>
                                <li>
                                    User undertakes to indemnify, defend and hold ADStraders
                                    harmless, as well as any and all of its subsidiaries, agents,
                                    employees and/or officers, against or from any liabilities,
                                    obligations, claims, debts, expenses etc., in any way
                                    connected with any misuse or abuse of the site, information or
                                    Services provided or contained herein, including in particular
                                    (but not limited to) any breach of these Terms and Conditions
                                    and/or violation of any law whatsoever (including any
                                    violation or infringement of any third party rights) and/or
                                    any breach of any applicable third party terms and conditions;
                                </li>
                                <li>
                                    No Warranties express or implied: USE THIS SITE AT YOUR OWN
                                    RISK. THE INFORMATION, MATERIALS AND SERVICES PROVIDED ON OR
                                    THROUGH THIS WEBSITE ARE PROVIDED AS IS WITHOUT ANY EXPRESS OR
                                    IMPLIED WARRANTIES OF ANY KIND INCLUDING WARRANTIES OF
                                    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
                                    NON-INFRINGEMENT OF INTELLECTUAL PROPERTY.
                                </li>
                            </ol>
                            <p>
                                You must read the Risk Warning Notice and all the other
                                documents supplied to you in connection with this Agreement very
                                carefully. DO NOT submit the Online Application Form if you are
                                unsure as to the effects of this Agreement or the nature of the
                                risks involved. By clicking on the Submit button and submitting
                                the Online Application Form to us, then you are acknowledging
                                that you have read the documents supplied and that you
                                understand and accept the terms of this Agreement.
                            </p>
                            <ol>
                                <li>
                                    ADStraders and/or any or all of its subsidiaries, affiliated
                                    companies, agents, employees, lawyers, trustees, bankers
                                    and/or officers does not regulate and/or oversee and/or verify
                                    the information published on this Site, including articles,
                                    trading information, quotes, et cetera. The compans sole
                                    responsibility and endeavor is to facilitate trading and
                                    provide information and articles deemed useful. ADStraders
                                    shall not incur any liability, in any way, or otherwise bear
                                    any damages and/or expenses to any user or third party, except
                                    for return of last monthly fees (to the extent that such fees
                                    were actually paid to ADStraders), and subject to the terms
                                    set herein. ADStraders shall not be liable to denial of
                                    service on any grounds, whether general, personal or specific.
                                    ADStraders shall not be liable in any way for information,
                                    software, products and/or Services provided by third parties.
                                </li>
                                <li>
                                    The Site may be, at any time and at ADStraders sole
                                    discretion, be off-line for maintenance or for any other
                                    reason, and service may be denied, temporarily and/or
                                    continually and/or indefinitely, of any person at no liability
                                    to ADStraders. ADStraders doest guarantee that you will be
                                    able to access the website at any time and location and it
                                    doest make any warranties with respect to the web site
                                    content. Without limiting the foregoing, ADStraders will not
                                    be held responsible for an impossibility of executing trading
                                    orders, due to letdowns in the operation of informational
                                    Systems due to technical faults, which are beyond its control.
                                </li>
                                <li>
                                    Neither ADStraders nor any of its respective affiliates,
                                    subsidiaries, agents, employees and officers warrant the
                                    accuracy or completeness of the information, materials or
                                    Services provided on or through this website. The information,
                                    materials and Services provided on or through this website may
                                    be out of date, and neither ADStraders nor any of its
                                    respective affiliates makes any commitment or assumes any duty
                                    to verify, validate or update such information, materials or
                                    Services. Except as explicitly set herein and under specific
                                    conditions, We DO NOT provide any advisory service. All
                                    transactions, investments and decisions are at your own
                                    discretion and risk.
                                </li>
                                <li>
                                    ADStraders and/or any or all of its subsidiaries, agents,
                                    employees and/or officers, does not assume any responsibility,
                                    or will be liable, for any damages to, or any viruses that may
                                    infect, your computer, telecommunication equipment, or other
                                    property caused by or arising from your access to, use of, or
                                    browsing this Site, or your downloading of any information or
                                    materials from this Site.
                                </li>
                                <li>
                                    in no event will ADStraders or any of its respective officers,
                                    directors, employees, shareholders, affiliates, agents,
                                    successors or assigns, nor any party involved in the creation,
                                    production or transmission of this website, be liable towards
                                    you or anyone else for any indirect, special, punitive,
                                    incidental or consequential damages (including, without
                                    limitation, those resulting from lost profits, lost data or
                                    business interruption) arising out of the use, inability to
                                    use, or the results of use of this website, any websites
                                    linked to this website, or the materials, information or
                                    Services contained on any or all such websites, whether based
                                    on warranty, contract, tort or any other legal theory and
                                    whether or not advised of the possibility of such damages. the
                                    foregoing limitations of liability do not apply to the extent
                                    prohibited by law.
                                </li>
                                <li>
                                    in the event of any problem with this website or any content,
                                    you agree that your sole remedy is to cease using this
                                    website. if you are a registered user you may receive a return
                                    of fees received by ADStraders within the previous month with
                                    respect to Services that were fully denied as a result of an
                                    act or omission by ADStraders.com. all claims against
                                    ADStraders shall become obsolete within 12 months of the
                                    occurrence giving rise to the claim. in the event of any
                                    problem with the information, products or Services that you
                                    have purchased on or through this website, you agree that your
                                    sole remedy, if any, aside of the above said, is from the
                                    3rdparty provider of such information, products or Services.
                                </li>
                                <li>
                                    The above disclaimers mean that ADStraders does not undertake
                                    to provide any service and/or any and all functionality on the
                                    Site. This also means if you believe you have any claim
                                    against ADStraders it should be presented with no delay and
                                    shall be null and void within 12 months of the first time it
                                    came to be.
                                </li>
                                <li>
                                    Revisions to these Terms and Conditions: Without prejudice to
                                    the above, User acknowledges and accepts that ADStraders and
                                    its legal advisors are fully entitled at all times to amend,
                                    add to and/or revoke any and all of these Terms and
                                    Conditions, at its sole discretion, without giving User any
                                    notice thereof. Any such amendment, addition or revocation
                                    shall become fully effective and binding upon being posted on
                                    Site. You should visit this page to review the then current
                                    Terms and Conditions binding on you. Certain provisions of
                                    these Terms and Conditions may be added to or superseded by
                                    legal notices or terms located on particular pages of this
                                    Site. This means that these Terms and Conditions may be
                                    reasonably amended from time to time by ADStraders, and shall
                                    apply to any user immediately. ADStraders shall maintain a
                                    link to these Terms and Conditions on every page on the Site,
                                    and state the last date these Terms and Conditions were
                                    updated on.
                                </li>
                                <li>
                                    Choice of Law; Jurisdiction: These Terms and Conditions
                                    supersede any other agreement between you and ADStraders to
                                    the extent necessary to resolve any inconsistency or ambiguity
                                    between them. A printed version of these Terms and Conditions
                                    will be admissible in judicial and administrative proceedings
                                    based upon or relating to these Terms and Conditions to the
                                    same extent and subject to the same conditions as other
                                    business documents and records originally generated and
                                    maintained in printed form. These Terms and Conditions, as
                                    well as any dispute arising therefrom or in connection
                                    therewith shall be brought before the courts of law which
                                    shall have exclusive jurisdiction over the same.
                                </li>
                                <li>
                                    Dispute Resolution; Arbitration: The parties will attempt in
                                    good faith to negotiate a settlement to any claim or dispute
                                    between them arising out of or in connection with this
                                    agreement. If the parties fail to agree upon terms of
                                    settlement, either side may submit the dispute to confidential
                                    arbitration proceedings by a sole arbitrator under the ICC ADR
                                    Rules, whose decision shall be final and binding. The
                                    arbitration proceedings shall be conducted in English, in a
                                    place mutually agreed by Parties. Without derogating of the
                                    provisions above, this clause explicitly sets exclusive
                                    jurisdiction to said arbitration process, and neither party
                                    shall be entitled to submit any dispute to the courts of its
                                    domicile which contradicts said arbitration process.
                                </li>
                                <li>
                                    Termination: You or we may suspend or terminate your account
                                    or your use of this Site at any time, for any reason or for no
                                    reason. Except for the withdrawal of accumulated funds, you
                                    will not be entitled to any remedy for discontinuing the
                                    Services, all subject to our WD policy and applicable laws. We
                                    reserve the right to change, suspend, or discontinue all or
                                    any aspect of this Site at any time without notice.
                                </li>
                                <li>
                                    Additional Assistance: If you do not understand any of the
                                    foregoing Terms and Conditions or if you have any questions or
                                    comments, we invite you to contact us at any time.
                                </li>
                            </ol>
                            <p>
                                This means you should contact us beforehand if anything of these
                                Terms and Conditions is unclear, unfair or unacceptable to you.
                            </p>
                            <ol>
                                <li>
                                    Our Commitment to Security and Privacy: To prevent
                                    unauthorized access, maintain data accuracy, and ensure the
                                    correct use of information, we have put in place the
                                    appropriate and reasonable physical, electronic, and
                                    managerial procedures to safeguard and secure the information
                                    we collect online. Please see our Privacy Policy for further
                                    information.
                                </li>
                                <li>
                                    Assignment to Third Parties: ADStraders is fully entitled to
                                    assign, grant, transfer or sublicense any and all of its
                                    rights provided for herein, including any rights with regards
                                    to information or data, wholly or in part, to any third party
                                    whatsoever. This means that the agreement between User and
                                    ADStraders on the terms set under the Terms and Conditions may
                                    be assigned, in full or in part by ADStraders, but not by the
                                    User. These privileges as well as the limitations of liability
                                    are ADStraders one-sided prerogatives under these Terms and
                                    Conditions but this Site would not be feasible without them.
                                </li>
                                <li>
                                    No Waiver: ADStraders acquiescence to any breach of These
                                    Terms and/or failure to exercise any right provided for herein
                                    shall be without prejudice to ADStraders legal rights and
                                    remedies, and shall not be held to preclude and/or debar it
                                    from exercising or seeking any of the same.
                                </li>
                                <li>
                                    Severability: Should any provision herein be deemed void or
                                    invalid by any court of law having proper jurisdiction, such
                                    provision shall be severed, and shall not in any way vitiate
                                    or detract from the effect and/or validity of any or all
                                    remaining provisions herein.
                                </li>
                            </ol>
                            <p>
                                This means that if a specific article in these Terms and
                                Conditions is ruled by a competent court as unenforceable for
                                any reason, then such ruling shall only apply to the respective
                                specific articles or provisions and not to this entire
                                agreement.
                            </p>
                            <p>
                                How to Contact Us: Should you have other questions or concerns,
                                please contact us anytime.
                            </p>
                            <h2>Deposit and withdrawal policy (WD policy)</h2>
                            <p>
                                Trading in any investment opportunity that may generate profit
                                requires ADStraders customers to deposit money on their online
                                account. Profits may be withdrawn from the online account.
                            </p>
                            <p>
                                Deposits and withdrawals are regulated by this WD policy as well
                                as the generally applicable terms and conditions.
                            </p>
                            <h2>Deposits</h2>
                            <p>
                                You, the Client, have to perform all the deposits from a source
                                (e.g. single bank account). If you want to start trading, you
                                should make sure this account is in your country of residence
                                and in your name. In order to certify that a SWIFT confirmation
                                is authentic, it has to be sent to ADStraders to confirm the
                                origin of the money which will be used for trading. If you dot
                                comply with this WD policy, you may be prevented from depositing
                                the money via Bank/Wire Transfer. If you did not login and
                                traded from your account within three (3) months, your Account
                                will be subject to a deduction of 10 % each month.
                            </p>
                            <h2>Withdrawals</h2>
                            <p>
                                According to generally acceptable rules, withdrawals must be
                                performed only through the same bank account or credit/debit
                                card that you used to deposit the funds.
                            </p>
                            <p>
                                Unless we agree otherwise, withdrawals from the Account may only
                                be made in the same currency in which the respective deposit was
                                made.
                            </p>
                            <p>
                                In addition, when you deposit or withdraw money for trading
                                purposes using payment methods, you should be aware that
                                additional fees and restrictions may apply. Withdrawals are
                                subjected to withdrawals processing and handling fees. Those
                                fees will be deducted from the transferred withdrawn amount. The
                                fees schedule is available on ADStraders.
                            </p>
                            <p>
                                Without derogating of the foregoing, ADStraders may execute
                                withdrawals to a different facility than the one used for the
                                deposit.
                            </p>
                            <p>
                                Furthermore, when it comes to withdrawals, Client may be
                                required to present additional information and documents.
                            </p>
                            <h2>Non-deposited funds</h2>
                            <p>
                                Funds appearing on Client account may include agreed or
                                voluntary bonuses and incentives, or any other sums not directly
                                deposited by the Client or gained from trading on account of
                                actually deposited funds (Non-Deposited Funds). Please note
                                unless otherwise explicitly agreed, Non-Deposited Funds are not
                                available for withdrawal. Further, due to technical limitations,
                                Non-Deposited Funds may be assigned to Cliens account in certain
                                occasions (for example, for the technical purpose of allowing
                                the closing of positions or an indebted account).
                            </p>
                            <p>
                                Without derogating from the abovementioned, bonuses issued to
                                Client by ADStraders may only be withdrawn subject to execution
                                of a minimum trading volume of 30 times the deposit amount plus
                                the bonus issued (Minimum Trading Volume ).
                            </p>
                            <h2>Submitting a withdrawal request</h2>
                            <p>In order to process your withdrawal request, you must:</p>
                            <ol>
                                <li>
                                    Print the [withdrawal.pdf] form. Client will log in to his
                                    account through the website, click on withdrawal, fill up the
                                    information and fill up the withdrawal form.
                                </li>
                                <li>Sign the printed form.</li>
                                <li>
                                    Send the form to us via e-mail as a scanned image (JPG, GIF,
                                    PNG, non-password protected PDF) to.
                                </li>
                            </ol>
                            <p>
                                All compliance documentation must have been received and
                                approved by ADStraders compliance officer in order to proceed
                                with the withdrawal.
                            </p>
                            <p>
                                Beneficiary Name must match the name on the trading account.
                                Requests to transfer funds to third party will not be processed.
                            </p>
                            <p>
                                Important: Account holder is required to monitor account
                                regularly, and ensure that available margin exists in the
                                account prior to submitting this request, as such withdrawal may
                                have an impact on existing open positions or trading strategy
                                used.
                            </p>
                            <h2>Typical withdrawal processing time</h2>
                            <p>
                                The time it takes for the money to reach your credit card or
                                bank account that has been used to deposit funds may vary
                                (usually up to five business days). Note that it might take
                                longer for withdrawals to bank accounts due to the additional
                                security procedures in force.
                            </p>
                            <p>
                                The request will generally be processed by ADStraders within 2-5
                                business days of receipt. In order to avoid any delays please
                                review your information carefully before submitting your
                                request. ADStraders assumes no responsibility for errors or
                                inaccuracies made by the account holder.
                            </p>
                            <p>
                                Corresponding withdrawals will take 3 to 5 business days to
                                process. ADStraders cannot monitor and is not responsible in any
                                way for the Cliens Credit Card Company or bans internal
                                procedures. Client must follow up with the credit card or
                                respective bank independently.
                            </p>
                            <p>
                                Funds are released to your credit account once your credit card
                                merchant has debited the funds from our account. This process
                                may take up to 5 business days or more to reflect on your credit
                                card account balance. If you do not have online access to your
                                credit card, it should appear on the next billing statement(s)
                                depending on your cars billing cycle.
                            </p>
                            <p>
                                Please note clearly that we are not committed to any time frame
                                and that any additional charges imposed by third parties shall
                                be deducted from the deposit or the withdrawal, as applicable.
                            </p>
                            <p>
                                Additional Charges: If the receiving bank uses an intermediary
                                bank to send/receive funds, you may incur additional fees
                                charged by the intermediary bank. These charges are usually
                                placed for transmitting the wire for your bank. ADStraders is
                                not involved with and nor has any control over these additional
                                fees. Please check with your financial institution for more
                                information.
                            </p>
                            <h2>Credit/debit cards</h2>
                            <p>
                                For Credit card deposits, when you choose an account in a
                                different currency than USD (United States Dollar), your credit
                                card will be debited properly in accordance with amount
                                deposited and the applicable exchange rates. In addition to the
                                exchanged sum deposited, additional credit cards fees may apply
                                (as a result, in such cases you may notice discrepancies between
                                the sum of deposit and the sum charged on your credit card).
                                Customers must accept these slight variations that can occur and
                                wot try to charge this back.
                            </p>
                            <p>
                                If you have used a credit card to deposit money, performed
                                online trading and decide to cash in on your winnings, the same
                                credit card must be used.
                            </p>
                            <p>
                                Amount of withdrawal per credit card is only allowable to an
                                equal amount of money deposited per credit card or less. Greater
                                amounts must be wire-transferred to a bank account.
                            </p>
                            <h2>Currency</h2>
                            <p>
                                Your Account may comprise of different currencies. These will be
                                subject to the following conditions:
                            </p>
                            <p>
                                We may accept payments into the account in different currencies
                                and any payments due to or from us and any net balances on the
                                account shall be reported by us in the respective currency; The
                                account is maintained in PLN, US Dollars, Euro (Base Currencies)
                                and any other currency will be converted at the exchange rate
                                existing at the point of conversion (Exchange Rate); if the
                                Client send funds in another currency than his accouns currency,
                                we will apply an exchange rate to our discretion.
                            </p>
                            <p>
                                We will generally settle trades or perform any required setoffs
                                and deductions in the relevant currency where the account
                                comprises such currency ledger, save that where such currency
                                balance is insufficient, we may settle trades in any currency
                                using the Exchange Rate.
                            </p>
                            <h2>Additional conditions</h2>
                            <p>
                                Please note this policy cannot be exhaustive, and additional
                                conditions or requirements may apply at any time due to
                                policies, including those set in order to prevent money
                                laundering. Please note any and all usage of the site and
                                Services is subject to the Terms and Conditions, as may be
                                amended from time to time by ADStraders, at its sole discretion.
                            </p>
                            <p>
                                For queries concerning policy matters, please contact us
                                anytime.
                            </p>
                            <h2>Risk disclosure and risk disclaimer policy</h2>
                            <p>
                                ADStraders strives to provide its customers with the best tools
                                for trading, including introductory guidance for the live
                                trading environment. ADStraders does not manage or advise on
                                investments.
                            </p>
                            <p>
                                ADStraders allows you to trade in highly speculative investments
                                which involve a significant risk of loss. Such trading is not
                                suitable for all investors so you must ensure that you fully
                                understand the risks before trading.
                            </p>
                            <p>
                                You accept that the transactions you perform with ADStraders may
                                involve financial instruments that are not:
                            </p>
                            <ol>
                                <li>Traded on any stock or investment exchange;</li>
                                <li>Readily realizable investments.</li>
                            </ol>
                            <p>Risks associated with the Services include:</p>
                            <ol>
                                <li>
                                    All investment is subject to risk and the degree of risk is a
                                    matter of judgment and cannot be accurately pre-determined.
                                </li>
                                <li>
                                    Trading in Financial Instruments is generally regarded as
                                    involving a high degree of risk compared with other common
                                    forms of investment such as recognized collective investment
                                    schemes and debt and equity securities.
                                </li>
                                <li>
                                    We give no warranty or promise as to the performance or
                                    profitability of your Account with us or your investments or
                                    any part thereof.
                                </li>
                                <li>
                                    The value of investments and the income derived from them can
                                    fall as well as rise and is not guaranteed.
                                </li>
                            </ol>
                            <p>
                                FOREX IS HIGHLY SPECULATIVE, CARRIES A HIGH LEVEL OF RISK AND
                                MAY NOT BE SUITABLE FOR ALL INVESTORS. CLIENT MAY SUSTAIN A LOSS
                                OF SOME OR ALL OF CLIENS INVESTED CAPITAL; THEREFORE, CLIENT
                                SHOULD NOT SPECULATE WITH CAPITAL THAT CLIENT CANNOT AFFORD TO
                                LOSE. CLIENT SHOULD BE AWARE OF ALL THE RISKS ASSOCIATED WITH
                                TRADING ON MARGIN. CLIENT HEREBY CONFIRMS THAT HE IS
                                KNOWLEDGABLE AS FOR THE ABOVEMENTIONED RISKS.
                            </p>
                            <p>
                                Due to the high risk nature of trading, we explicitly do not
                                make any EXPRESS OR IMPLIED warranties or guarantees that you
                                will make any profit or that you will not lose any OR all of
                                your DEPOSITED investments.
                            </p>
                            <p>
                                Further, ADStraders is not responsible for the accuracy of
                                information or content provided by third parties, including site
                                and information linked to or presented in this website.
                            </p>
                            <p>
                                Please note any and all usage of the site and Services is
                                subject to the Terms and Conditions, as may be amended from time
                                to time by ADStraders, at its sole discretion.
                            </p>
                            <p>
                                There is a possibility that you may sustain a partial or total
                                loss of your investment funds when trading. Even if you make
                                money, you should be aware that the market can quickly turn.
                                Never trade with more than you can afford to lose.
                            </p>
                            <h2>Acceptable use policy [AUP]</h2>
                            <p>
                                Important: You are hereby advised that any breach of this
                                acceptable use policy or otherwise manipulating, abusing or
                                exploiting ADStraders online trading Services offered to the
                                public, may result in significant monetary and other damages to
                                ADStraders and/or third parties, including other users and
                                traders like yourself, and ADStraders shall be entitled to seek
                                any remedy available to it hereunder or under law, including an
                                injunctive RELIEF.
                            </p>
                            <p>
                                Without derogating of the provisions of the following Acceptable
                                Use Policy, any of the following activities are explicitly
                                prohibited and shall be deemed fundamental breach of the Terms
                                of Service Agreement:
                            </p>
                            <ul>
                                <li>
                                    Unauthorized automated access to the Services and Systems.
                                </li>
                                <li>Latency exploitation in trading.</li>
                                <li>
                                    Coordinated trade through multiple accounts (including trading
                                    in tandem).
                                </li>
                                <li>
                                    Any reverse engineering of the software or the Services.
                                </li>
                            </ul>
                            <ol>
                                <li>
                                    <h2>Scope of AUP</h2>
                                </li>
                            </ol>
                            <p>
                                1.1. The AUP applies to all Systems and Services offered by
                                ADStraders, without exceptions.
                            </p>
                            <p>
                                1.2. The AUP applies to everyone, including all clients, users
                                and visitors (User Or You).
                            </p>
                            <p>
                                1.3. The prohibited activities and uses set out in this AUP are
                                not a complete list. If you are unsure about any contemplated
                                action you should contact ADStraders Immediately.
                            </p>
                            <ol>
                                <li>
                                    <h2>Automated Access</h2>
                                </li>
                            </ol>
                            <p>
                                2.1. You may not use, under any circumstances, any software
                                which automatically accesses or operates on ADStraders Systems,
                                websites of Systems, unless such software is officially provided
                                by ADStraders.
                            </p>
                            <ol>
                                <li>
                                    <h2>Prohibited Activities</h2>
                                </li>
                            </ol>
                            <p>
                                3.1. ADStraders shall not use, and will take reasonable actions
                                to ensure that no user or third party shall use its Systems in
                                any of the following ways:
                            </p>
                            <p>
                                3.1.1. Fraudulently or in connection with any criminal
                                offense.<br/>3.1.2. To send, knowingly receive, upload,
                                download, or use any material which is offensive, abusive,
                                indecent, defamatory, obscene or menacing, or in breach of
                                copyright, confidence, privacy and/or any other rights.
                            </p>
                            <p>
                                3.1.3. To cause annoyance, inconvenience or anxiety.<br/>3.1.4.
                                To spam or to send or provide unsolicited advertising or
                                promotional material or, knowingly to receive responses to any
                                spam, unsolicited advertising or promotional material sent or
                                provided by any third party.
                            </p>
                            <p>
                                3.1.5. In any way which, in ADStraders reasonable opinion, is or
                                is likely to be detrimental to the provision of the ADStraders
                                service to the company or any of ADStraders or its shareholders'
                                and/or affiliates' and/or directors and/or lawyers and/or
                                trustees and/or bankers and/or customers and/or business and/or
                                reputation.
                            </p>
                            <p>
                                3.1.6. In contravention of any licenses or third party rights.
                            </p>
                            <p>
                                3.1.7. To attempt to interfere with any ADStraders service to
                                any user, host or network this includes without limitation:
                            </p>
                            <p>This Includes Without Limitation:</p>
                            <p>3.1.7.1. Flooding of networks;</p>
                            <p>3.1.7.2. DOS (Denial of Service) attacks of any sort;</p>
                            <p>
                                3.1.7.3. Deliberate attempts to overload a service and attempts
                                to crash a host;
                            </p>
                            <p>
                                3.1.7.4. Any attempt to abuse, manipulate or benefit of an
                                error, software Bug, security backdoor or breach, latency
                                differences;
                            </p>
                            <p>3.1.7.5. Resale of Services;</p>
                            <p>
                                3.1.7.6. Any sort of automated, cooperative or Multi-User
                                Operation (explicitly including tandem-trading) to circumvent or
                                exploit the Services or Systems;
                            </p>
                            <p>
                                3.1.7.7. For transmitting E-mails, code or files which contain
                                computer viruses or corrupted data or Trojan horses or tools
                                which compromise the security of web sites or user data. This
                                explicitly includes spyware and malware of any sort.
                            </p>
                            <p>
                                3.2. You acknowledge and agree that the list of prohibited
                                activities set out is a non-exhaustive list. ADStraders reserves
                                the right to cancel any deal and/or hold and offset any funds
                                and/or demand any fund or right due to inacceptable use of its
                                Services, taking advantage of mistakes or abusing its Systems.
                            </p>
                            <p>
                                3.3. You will not allow the use of the System or access to any
                                person who is not a Client and will not copy, distribute,
                                publish, transmit, display, modify, prepare derivative works
                                based on, report or otherwise use the System in whole or in part
                                for the use of any other person.
                            </p>
                            <p>
                                3.4. The trademarks and logos displayed on the System are
                                registered trademarks of ADStraders and/or respective persons.
                                any use of such trademarks may only be allowed after the written
                                consent has been obtained.
                            </p>
                            <ol>
                                <li>
                                    <h2>Network Security</h2>
                                </li>
                            </ol>
                            <p>4.1. You shall not:</p>
                            <p>
                                4.1.1. Violate or attempt to violate ADStraders security or
                                attempt to interfere or interfere with ADStraders networks,
                                authentication measures, servers or equipment.
                            </p>
                            <p>
                                4.1.2. Attempt to circumvent user authentication or security of
                                any host, network or account which includes accessing data not
                                intended to the recipient, logging onto a server where access is
                                not authorized or probing the security of other networks.
                            </p>
                            <p>
                                4.1.3. Attempt to gain access to any account or computer
                                resource not belonging to you through ADStraders System and/or
                                Services.
                            </p>
                            <ol>
                                <li>
                                    <h2>Violation of this AUP</h2>
                                </li>
                            </ol>
                            <p>
                                5.1. ADStraders shall be entitled to take organisational,
                                automated and discretionary measures to ensure compliance with
                                this AUP and all other rules and policies by all users.
                            </p>
                            <p>
                                5.2. Any right of ADStraders hereunder, explicitly including the
                                right to impose a sanction, shall not be deemed as to obligate
                                ADStraders in any way to exercise such right (or impose such
                                sanction).<br/>5.3. Failure to exercise any right of ADStraders
                                herein shall not be deemed a waiver of such right, including the
                                imposition of a specific sanction.
                            </p>
                            <p>
                                5.4. ADStraders may stop any person from accessing the System
                                and may take such adequate measures as ADStraders deems
                                necessary to prevent such access, if ADStraders becomes aware of
                                any circumstances which give reason to believe that a login is
                                being misused
                            </p>
                            <p>
                                5.5. If you or any other person violate any term of this AUP,
                                ADStraders may, without notice to ADStraders and without any
                                liability to ADStraders and/or its clients and/or any other
                                party:
                            </p>
                            <p>
                                5.5.1. Require clarifications, authentications, documentations
                                or other proof or details relevant to an investigation.
                            </p>
                            <p>
                                5.5.2. Prevent client access to the System for good cause at its
                                discretion.
                            </p>
                            <p>
                                5.5.3. Repair, rectify and/or rollback cliens transactions to
                                perform restitution for and by ADStraders or third party.
                            </p>
                            <p>
                                5.5.4. Offset, deduct and/or confiscate funds and/or information
                                as required at its discretion in accordance with law and/or any
                                court order or instructions from a competent investigative or
                                law enforcement authority.
                            </p>
                            <p>
                                5.5.5. Suspend or terminate ADStraders use of its Systems and/or
                                service; or
                            </p>
                            <p>5.5.6. Take any action as it considers appropriate.</p>
                            <p>
                                For the avoidance of doubt, the exercise by ADStraders of its
                                rights hereunder will not require ADStraders to compensate the
                                user for loss of access to the ADStraders System and service.
                            </p>
                            <p>
                                5.6. A violation of this AUP by a person having only indirect
                                access to the ADStraders service through you, will be considered
                                a violation by you, including whether or not the violation was
                                in with your knowledge or consent. You are responsible for any
                                and all actions of its Clients to whom it directly or indirectly
                                provides its Services.
                            </p>
                            <p>
                                5.7. ADStraders may cooperate with System administrators or
                                other network or Computing Services providers to enforce this
                                AUP or a policy of another provider.
                            </p>
                            <p>
                                5.8. ADStraders may involve, and will cooperate with, law
                                enforcement if criminal activity is suspected.
                            </p>
                            <h2>Additional conditions</h2>
                            <p>
                                Please note this policy cannot be exhaustive, and additional
                                conditions or requirements may apply at any time due to
                                technological changes, experience and policies, including those
                                set in order to prevent money laundering. This AUP is subject to
                                change at ADStraders sole discretion at any time and any change
                                is effective when posted on ADStraders website or otherwise made
                                known to ADStraders.
                            </p>
                            <p>
                                Please note any and all usage of the site and Services is
                                subject to the Terms and Conditions, as may be amended from time
                                to time by ADStraders, at its sole discretion.
                            </p>
                            <p>
                                For queries concerning policy matters, please contact us at any
                                time.
                            </p>
                        </div>

                    </Modal.Body>
                </Modal>

            </>
        );
    }
}
