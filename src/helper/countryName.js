exports.CountryName = (value) => {
    if (value === 'AT') {
        return "Austria"
    }
    else if (value === 'CZ') {
        return "Czechia"
    }
    else if (value === 'DK') {
        return "Denmark"
    }else if (value === 'FI') {
        return "Finland"
    }else if (value === 'DE') {
        return "Germany"
    }else if (value === 'GR') {
        return "Greece"
    }else if (value === 'HU') {
        return "Hungary"
    }else if (value === 'IS') {
        return "Iceland"
    }else if (value === 'IE') {
        return "Ireland"
    }else if (value === 'IT') {
        return "Italy"
    }else if (value === 'LV') {
        return "Latvia"
    }else if (value === 'LI') {
        return "Liechtenstein"
    }else if (value === 'LT') {
        return "Lithuania"
    }else if (value === 'LU') {
        return "Luxembourg"
    }else if (value === 'MT') {
        return "Malta"
    }else if (value === 'NL') {
        return "Netherlands"
    } else if (value === 'NO') {
        return "Norway"
    }else if (value === 'PL') {
        return "Poland"
    }else if (value === 'PT') {
        return "Portugal"
    }else if (value === 'SK') {
        return "Slovakia"
    }else if (value === 'SI') {
        return "Slovenia"
    }else if (value === 'ES') {
        return "Spain"
    }else if (value === 'SE') {
        return "Sweden"
    }else if (value === 'CH') {
        return "Switzerland"
    }else if (value === 'GB') {
        return "United Kingdom"
    }else if (value === 'IL') {
        return "Israel"
    }else if (value === 'GE') {
        return "Georgia"
    }else if (value === 'FR') {
        return "France"
    }else if (value === 'BE') {
        return "Belgium"
    }else  {
        return "United Kingdom"
    }
}

exports.CountryInitialName = (value) => {

    if (value === '43') {
        return "AT"
    }
    else if (value === '420') {
        return "CZ"
    }
    else if (value === '45') {
        return "DK"
    }else if (value === '358') {
        return "FI"
    }else if (value === '49') {
        return "DE"
    }else if (value === '30') {
        return "GR"
    }else if (value === '36') {
        return "HU"
    }else if (value === '354') {
        return "IS"
    }else if (value === '353') {
        return "IE"
    }else if (value === '39') {
        return "IT"
    }else if (value === '371') {
        return "LV"
    }else if (value === '423') {
        return "LI"
    }else if (value === '370') {
        return "LT"
    }else if (value === '352') {
        return "LU"
    }else if (value === '356') {
        return "MT"
    }else if (value === '31') {
        return "NL"
    } else if (value === '47') {
        return "NO"
    }else if (value === '48') {
        return "PL"
    }else if (value === '351') {
        return "PT"
    }else if (value === '421') {
        return "SK"
    }else if (value === '386') {
        return "SI"
    }else if (value === '34') {
        return "ES"
    }else if (value === '46') {
        return "SE"
    }else if (value === '41') {
        return "CH"
    }else if (value === '44') {
        return "GB"
    }else if (value === '972') {
        return "IL"
    }else if (value === '995') {
        return "GE"
    }else if (value === '33') {
        return "FR"
    }else if (value === '32') {
        return "BE"
    }else  {
        return "GB"
    }
}
