/*export const phoneNumberLength = [
    { id: "43", name: "AT",  displayText: "43",  flag: imgs[0] },
    { id: "420", name: "CZ",  displayText: "420",  flag: imgs[1] },
    { id: "45", name: "DK",  displayText: "45",  flag: imgs[2] },
    { id: "358", name: "FI",  displayText: "358",  flag: imgs[3] },
    { id: "49", name: "DE",  displayText: "49",  flag: imgs[4] },
    { id: "30", name: "GR",  displayText: "30",  flag: imgs[5] },
    { id: "36", name: "HU",  displayText: "36",  flag: imgs[6] },
    { id: "354", name: "IS",  displayText: "354",  flag: imgs[7] },
    { id: "353", name: "IE",  displayText: "353",  flag: imgs[8] },
    { id: "39", name: "IT",  displayText: "39",  flag: imgs[9] },
    { id: "371", name: "LV",  displayText: "371",  flag: imgs[10] },
    { id: "423", name: "LI",  displayText: "423",  flag: imgs[11] },
    { id: "370", name: "LT",  displayText: "370",  flag: imgs[12] },
    { id: "352", name: "LU",  displayText: "352",  flag: imgs[13] },
    { id: "356", name: "MT",  displayText: "356",  flag: imgs[14] },
    { id: "31", name: "NL",  displayText: "31",  flag: imgs[15] },
    { id: "47", name: "NO",  displayText: "47",  flag: imgs[16] },
    { id: "48", name: "PL",  displayText: "48",  flag: imgs[17] },
    { id: "351", name: "PT",  displayText: "351",  flag: imgs[18] },
    { id: "421", name: "SK",  displayText: "421",  flag: imgs[19] },
    { id: "386", name: "SI",  displayText: "386",  flag: imgs[20] },
    { id: "34", name: "ES",  displayText: "34",  flag: imgs[21] },
    { id: "46", name: "SE",  displayText: "46",  flag: imgs[22] },
    { id: "41", name: "CH",  displayText: "41",  flag: imgs[23] },
    { id: "44", name: "GB",  displayText: "44",  flag: imgs[24] },
    { id: "972", name: "IL",  displayText: "972",  flag: imgs[25] },
    { id: "995", name: "GE",  displayText: "995",  flag: imgs[26] },
];*/
exports.PhoneNumberLength = (value, length) => {

    if (value === '43') {
        if (length < 9 || length > 15) { return false } else { return true }
    }
    else if (value === '420') {
        if (length < 8 || length > 10) { return false } else { return true }
    }
    else if (value === '45') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '358') {
        if (length < 9 || length > 10) { return false } else { return true }
    }
    else if (value === '49') {
        if (length < 6 || length > 10) { return false } else { return true }
    }
    else if (value === '30') {
        if (length < 9 || length > 10) { return false } else { return true }
    }
    else if (value === '36') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '354') {
        if (length < 6 || length > 7) { return false } else { return true }
    }
    else if (value === '353') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '39') {
        if (length < 9 || length > 10) { return false } else { return true }
    }
    else if (value === '371') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '423') {
        if (length < 9 || length > 10) { return false } else { return true }
    }
    else if (value === '370') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '352') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '352') {
        if (length < 9 || length > 10) { return false } else { return true }
    }
    else if (value === '356') {
        if (length < 8 || length > 9) { return false } else { return true }
    }
    else if (value === '31') {
        if (length < 9 || length > 9) { return false } else { return true }
    }
    else if (value === '47') {
        if (length < 4 || length > 12) { return false } else { return true }
    }
    else if (value === '48') {
        if (length < 9 || length > 9) { return false } else { return true }
    }
    else if (value === '351') {
        if (length < 9 || length > 9) { return false } else { return true }
    }
    else if (value === '421') {
        if (length < 9 || length > 9) { return false } else { return true }
    }
    else if (value === '386') {
        if (length < 8 || length > 8) { return false } else { return true }
    }
    else if (value === '34') {
        if (length < 9 || length > 10) { return false } else { return true }
    }
    else if (value === '46') {
        if (length < 6 || length > 9) { return false } else { return true }
    }
    else if (value === '41') {
        if (length < 9 || length > 9) { return false } else { return true }
    }
    else if (value === '44') {
        if (length < 7 || length > 10) { return false } else { return true }
    }
    else if (value === '972') {
        if (length < 8 || length > 10) { return false } else { return true }
    }
    else if (value === '972') {
        if (length < 8 || length > 10) { return false } else { return true }
    }
    else if (value === '995') {
        if (length < 9 || length > 9) { return false } else { return true }
    }else {
        return true
    }
}