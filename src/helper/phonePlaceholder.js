
exports.PhonePlaceholder = (value) => {
    if (value === '43') {
        return "664123456"
    }
    else if (value === '420') {
        return "601123456"
    }
    else if (value === '45') {
        return "32123456"
    }else if (value === '358') {
        return "412345678"
    }else if (value === '49') {
        return "15123456789"
    }else if (value === '30') {
        return "691 234 5678"
    }else if (value === '36') {
        return "20 123 4567"
    }else if (value === '354') {
        return "611 1234"

    }else if (value === '353') {
        return "85 0123456"
    }else if (value === '39') {
        return "312 3456789"
    }else if (value === '371') {
        return "21 234567"
    }else if (value === '423') {
        return "660 234 567"
    }

    else if (value === '370') {
        return "612 34567"
    }else if (value === '352') {
        return "628 123 456"
    }else if (value === '356') {
        return "9696 1234"
    }else if (value === '31') {
        return "6 12345678"
    } else if (value === '47') {
        return "NO"
    }else if (value === '48') {
        return "PL"
    }else if (value === '351') {
        return "PT"
    }else if (value === '421') {
        return "SK"
    }else if (value === '386') {
        return "SI"
    }else if (value === '34') {
        return "612 34 56 78"
    }else if (value === '46') {
        return "SE"
    }else if (value === '41') {
        return "CH"
    }else if (value === '44') {
        return "7400123456"
    }else if (value === '972') {
        return "IL"
    }else if (value === '995') {
        return "GE"
    }else  {
        return "Telefonnummer"
    }
}