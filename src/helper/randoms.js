exports.randomAmount = () => {
    let min = 300
    let max = 2000
    let a = Math.floor(Math.random() * (max - min + 1)) + min
    return a;
}
exports.randomCripto = () => {
    let min = 1
    let max = 15
    let a = Math.floor(Math.random() * (max - min + 1)) + min

    if (a < 5) {
        return 'EOS/ETH'
    } else if (a > 5 && a < 10){
       return ' BTC/ETH'
    } else
    {
        return 'ETH/LTC'
    }
}