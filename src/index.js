import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './store/index';

const App = React.lazy(() => import(/* webpackChunkName: "App" */'./app' ));

ReactDOM.render(
    <Provider store={configureStore()}>
        <Suspense fallback={<div className="loading" />}>
            <App />
        </Suspense>
    </Provider>,
    document.getElementById('root')
);
