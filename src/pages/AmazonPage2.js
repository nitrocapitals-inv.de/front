import React, {Component} from 'react'
import {connect} from "react-redux";
import ReactCustomFlagSelect from "react-custom-flag-select";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'

import '../assets/css/style2.css'
import BgTop1920 from '../assets/img/bg-top-1920_1608831144.jpg'
import BgTop767 from '../assets/img/bg-top-767_1608831216.jpg'
import BgTop1196 from '../assets/img/bg-top-1196_1608831184.jpg'

import Woman767 from '../assets/img/woman-767_1608831207.png'
import Woman1196 from '../assets/img/woman-1196_1608831169.png'
import Woman1920 from '../assets/img/woman-1920_1608831123.png'
import ArtImage1920 from '../assets/img/art-image-1920.jpg'
import PaymentMethods from '../assets/img/payment-methods3.png'
import {FLAG_SELECTOR_OPTION_LIST, Flags} from '../helper/country'
import {CountryInitialName, CountryName} from '../helper/countryName'
import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'

import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";


import {Footer} from "../components/layout/footer";
import {FakeUsers} from "../helper/data";


const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;

class AmazonPage2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            phoneValue: '',
        }
    }

    componentDidMount() {
        this.props.checkCountryRequest()
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {
        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if (this.state.lastName.length <= 3 || this.state.lastName >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
            } else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
        }


        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }

        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }
        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                phone : this.state.phoneValue,
                areaCodeName: this.state.areaCodeName,
                Source: process.env.REACT_APP_IP_WEB + '' + window.location.pathname.split('/')[1],
                domenSource: process.env.REACT_APP_DOMEN_SOURCE,
                affiliateId: process.env.REACT_APP_ID,
                Token: process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)

        }
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <>
                <div className="wrap">
                    <section className="top">
                        <div className="top__containerA containerA">
                            <div className="top__text" style={{textAlign: 'center'}}>
                                <h1 className="top__title">
                                    <div className="top__title_upper" >
                                        <p>INVESTIEREN SIE IN DIE
                                            BESTEN:</p></div>
                                    <div className="top__company">
                                        <div data-editable="" data-name="top__company"><p>AMAZON</p></div>
                                    </div>
                                </h1>
                                <h2>
                                    <div className="top__subtitle">
                                        <div data-editable="" data-name="top__subtitle"><p>Jetzt investieren</p></div>
                                    </div>
                                </h2>
                            </div>
                        </div>

                        <div className="top__bg top__bg_desktop desktop-only" data-editable=""
                             data-name="top__bg_desktop">
                            <div className="top__bg-img-box" data-ce-tag="bkgimg"
                                 style={{
                                     backgroundImage: `url(${BgTop1920})`,
                                     backgroundSize: 'cover',
                                     backgroundPosition: 'center'
                                 }}>
                                &nbsp;
                            </div>
                        </div>

                        <div className="top__bg top__bg_tablet tablet-only" data-editable="" data-name="top__bg_tablet">
                            <div className="top__bg-img-box" data-ce-tag="bkgimg"
                                 style={{
                                     backgroundImage: `url(${BgTop1196})`,
                                     backgroundSize: 'cover',
                                     backgroundPosition: 'center'
                                 }}>
                                &nbsp;
                            </div>
                        </div>

                        <div className="top__bg top__bg_main-mobile mobile-only" data-editable=""
                             data-name="top__bg_main-mobile">
                            <div className="top__bg-img-box" data-ce-tag="bkgimg"
                                 style={{
                                     backgroundImage: `url(${BgTop767})`,
                                     backgroundSize: 'cover',
                                     backgroundPosition: 'center'
                                 }}>
                                &nbsp;
                            </div>
                        </div>

                        <div className="top__img top__img_desktop desktop-only" data-editable=""
                             data-name="top__img_desktop"><img alt=""
                                                               height="174"
                                                               style={{
                                                                   height: 'inherit',
                                                                   width: 'auto',
                                                                   maxWidth: '100%'
                                                               }}
                                                               width="600"
                                                               src={Woman1920}/>
                        </div>
                        <div className="top__img top__img_tablet tablet-only" data-editable=""
                             data-name="top__img_tablet"><img alt=""
                                                              height="174"
                                                              style={{
                                                                  height: 'inherit',
                                                                  width: 'auto',
                                                                  maxWidth: '100%'
                                                              }}
                                                              width="600"
                                                              src={Woman1196}/>
                        </div>

                        <div className="top__img top__img_mobile mobile-only" data-editable=""
                             data-name="top__img_mobile"><img alt=""
                                                              height="174"
                                                              style={{
                                                                  height: 'inherit',
                                                                  width: 'auto',
                                                                  maxWidth: '100%'
                                                              }}
                                                              width="600"
                                                              src={Woman767}/>
                        </div>


                    </section>


                    <section className="formA">
                        <div className="formA__containerA containerA">
                            <div className="formA__top">
                                <div className="formA__main">
                                    <div className="formA__title" data-editable="" data-name="formA__title">
                                        <p>Investieren Sie in Amazon
                                        </p>
                                    </div>

                                    <div className="formA__body">
                                        <div id="formWrapper1" className="formWrapper">

                                            <form method="POST"
                                                  className="formLead form-horizontal registration-form sv-skin">
                                                <div className="row">
                                                    <div className="col-md-12 form-group"
                                                         style={{textAlign: 'left'}}>
                                                        <input type="text" name="first_name" placeholder="Ihr Name"
                                                               className="form-control bfh-firstname"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-red">Erforderlich</label> : null
                                                        }
                                                    </div>

                                                    <div className="col-md-12 form-group"
                                                         style={{textAlign: 'left'}}>
                                                        <input type="text" name="last_name"
                                                               placeholder="dein Nachname"
                                                               className="form-control"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-red">Erforderlich</label> : null
                                                        }
                                                    </div>

                                                    <div className="col-md-12 form-group" style={{marginBottom:'0px'}}>
                                                        <PhoneInput
                                                            style={{
                                                                height: '40px',
                                                                border: '1px solid #616161',
                                                                borderRadius: '5px',
                                                                padding: '8px',
                                                                width: '100%'
                                                            }}
                                                            placeholder="Enter phone number"
                                                            defaultCountry={this.props.country.country}
                                                            international
                                                            onChange={(e) => {this.setState({phoneValue : e})}}
                                                            value={this.state.phoneValue}

                                                        />
                                                        {
                                                            this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                Telefonnummer</label> : null
                                                        }


                                                    </div>

                                                    <br/>
                                                    <br/>
                                                    <div className="col-md-12 form-group"
                                                         style={{textAlign: 'left'}}>
                                                        <input type="text" name="email" placeholder="Email"
                                                               className="form-control bfh-email" required=""
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />

                                                        {
                                                            this.state.emailErr ? <label className="x-error-red">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ?
                                                                <label className="x-error-red">Diese, E-Mail-Adresse ist
                                                                    bereits registriert. Versuchen Sie es mit einem
                                                                    anderen</label> : null
                                                        }
                                                    </div>
                                                </div>
                                            </form>


                                            <button
                                                onClick={this.handleRegistration}
                                                 type="button"
                                                    className="trade3Btn">Informationen
                                                anfordern
                                            </button>

                                        </div>
                                        <div data-editable="" data-name="form__text" className="form__text">

                                            <div>
                                                <input
                                                    style={{float:'left', marginTop:'6px'}}
                                                    type="checkbox"
                                                    checked={this.state.checked}
                                                    onChange={this.handleInputChange}
                                                    name="terms"/>
                                            <div style={{marginLeft:'20px'}}>
                                                <label htmlFor="terms"> Wenn Sie hier klicken, stimmen Sie der
                                                    Verarbeitung
                                                    und Weitergabe Ihrer Daten zu</label><p
                                                style={{marginTop: '10px'}}>Um in
                                                Amazon zu investieren müssen Sie mindestens 18 Jahre alt sein. Das
                                                erforderliche Mindestkapital ist 250 €.</p>
                                            </div>
                                                <p style={{textAlign:'left'}}>
                                                    {
                                                        this.state.checkedErr ?
                                                            <label className="x-error-red">Verpflichtend</label> : null
                                                    }
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </section>

                    <section className="offer">
                        <div className="offer__containerA containerA"><h2 data-editable="" data-name="offer__title"
                                                                          className="offer__title">
                            &nbsp;</h2>
                            <p>Der Einstieg ist einfach. Erhalten Sie Zugang zu:</p>
                            <ul className="offer__list">
                                <li className="offer__list-item">
                                    <div className="offer__list-img">
                                        <div className="offer__list-img-box" data-editable=""
                                             data-name="offer__list-img_1">
                                            <svg width="44" height="48" viewBox="0 0 44 48" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M21.7352 47.4074C21.6284 47.4074 21.5216 47.3846 21.4241 47.3343L20.6071 46.9321C7.51122 40.4554 0 30.9576 0 20.8794V6.80632C0 6.42696 0.311033 6.12072 0.696343 6.12072H6.9495C9.78594 6.12072 12.0932 4.39302 12.0932 2.27224V1.09301C12.0932 0.713643 12.4042 0.40741 12.7895 0.40741H30.6855C31.0708 0.40741 31.3818 0.713643 31.3818 1.09301V2.27224C31.3818 4.39302 33.6891 6.12072 36.5255 6.12072H42.7787C43.164 6.12072 43.475 6.42696 43.475 6.80632V20.8794C43.475 30.9576 35.9638 40.4554 22.8679 46.9321L22.0509 47.3343C21.9487 47.3846 21.8419 47.4074 21.7352 47.4074ZM1.39269 7.49192V20.8794C1.39269 30.4229 8.62537 39.4682 21.2338 45.7026L21.7352 45.9494L22.2365 45.7026C34.845 39.4682 42.0777 30.4183 42.0777 20.8794V7.49192H36.5255C32.9231 7.49192 29.9892 5.15174 29.9892 2.27224V1.7786H13.4858V2.27224C13.4858 5.15174 10.5519 7.49192 6.9495 7.49192H1.39269Z"
                                                    fill="#0EA138"></path>
                                                <path
                                                    d="M21.8838 33.6132C15.6446 33.6132 10.5706 28.6174 10.5706 22.4745C10.5706 16.3315 15.6446 11.3358 21.8838 11.3358C28.123 11.3358 33.1971 16.3315 33.1971 22.4745C33.1971 28.6129 28.123 33.6132 21.8838 33.6132ZM21.8838 12.7024C16.4152 12.7024 11.9632 17.0857 11.9632 22.4699C11.9632 27.8587 16.4152 32.2374 21.8838 32.2374C27.3571 32.2374 31.8044 27.8541 31.8044 22.4699C31.8044 17.0857 27.3524 12.7024 21.8838 12.7024Z"
                                                    fill="#0EA138"></path>
                                                <path
                                                    d="M22.6033 28.0553H21.3267C19.9479 28.0553 18.8291 26.9538 18.8291 25.5963C18.8291 25.2169 19.1401 24.9107 19.5254 24.9107C19.9108 24.9107 20.2218 25.2169 20.2218 25.5963C20.2218 26.195 20.7185 26.6841 21.3267 26.6841H22.6033C23.2114 26.6841 23.7081 26.195 23.7081 25.5963V24.3394C23.7081 23.7406 23.2114 23.2515 22.6033 23.2515H21.3267C19.9479 23.2515 18.8291 22.15 18.8291 20.7925V19.5356C18.8291 18.1781 19.9479 17.0766 21.3267 17.0766H22.6033C23.982 17.0766 25.1008 18.1781 25.1008 19.5356V19.5905C25.1008 19.9698 24.7898 20.2761 24.4045 20.2761C24.0192 20.2761 23.7081 19.9698 23.7081 19.5905V19.5356C23.7081 18.9369 23.2114 18.4478 22.6033 18.4478H21.3267C20.7185 18.4478 20.2218 18.9369 20.2218 19.5356V20.7925C20.2218 21.3913 20.7185 21.8804 21.3267 21.8804H22.6033C23.982 21.8804 25.1008 22.9819 25.1008 24.3394V25.5963C25.1008 26.9538 23.982 28.0553 22.6033 28.0553Z"
                                                    fill="#0EA138"></path>
                                                <path
                                                    d="M21.9673 18.2558C21.582 18.2558 21.271 17.9496 21.271 17.5702V15.678C21.271 15.2986 21.582 14.9924 21.9673 14.9924C22.3526 14.9924 22.6637 15.2986 22.6637 15.678V17.5702C22.6637 17.9496 22.3526 18.2558 21.9673 18.2558Z"
                                                    fill="#0EA138"></path>
                                                <path
                                                    d="M21.9673 29.9521C21.582 29.9521 21.271 29.6459 21.271 29.2665V27.3743C21.271 26.9949 21.582 26.6887 21.9673 26.6887C22.3526 26.6887 22.6637 26.9949 22.6637 27.3743V29.2665C22.6637 29.6413 22.3526 29.9521 21.9673 29.9521Z"
                                                    fill="#0EA138"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div className="offer__list-text" data-editable="" data-name="offer__list-text_1">
                                        <p>Sichere und
                                            regulierte Plattform</p></div>
                                </li>
                                <li className="offer__list-item">
                                    <div className="offer__list-img">
                                        <div className="offer__list-img-box" data-editable=""
                                             data-name="offer__list-img_2">
                                            <svg width="47" height="47" viewBox="0 0 47 47" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M23.6763 27.0311C16.2119 27.0311 10.1348 20.9701 10.1348 13.5155C10.1348 6.06102 16.2074 0 23.6763 0C31.1406 0 37.2178 6.06102 37.2178 13.5155C37.2178 20.9701 31.1406 27.0311 23.6763 27.0311ZM23.6763 1.35291C16.9575 1.35291 11.4903 6.80963 11.4903 13.5155C11.4903 20.2215 16.9575 25.6782 23.6763 25.6782C30.3951 25.6782 35.8623 20.2215 35.8623 13.5155C35.8623 6.80963 30.3951 1.35291 23.6763 1.35291Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M7.43269 45.2909H0.677753C0.302729 45.2909 0 44.9887 0 44.6144V30.0165C0 29.6422 0.302729 29.3401 0.677753 29.3401H7.43269C7.80771 29.3401 8.11044 29.6422 8.11044 30.0165V44.6099C8.11044 44.9842 7.80771 45.2909 7.43269 45.2909ZM1.35551 43.938H6.75493V30.693H1.35551V43.938Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M26.7847 47C26.7395 47 26.6898 46.9955 26.6447 46.9865L7.29257 42.9368C6.9808 42.8691 6.75488 42.594 6.75488 42.2738V31.2206C6.75488 30.968 6.89495 30.7335 7.12087 30.6208C7.73536 30.3051 8.3092 29.9173 8.83332 29.4708C10.0759 28.4065 12.0278 27.8834 13.9842 28.1314C14.8698 28.2442 15.7103 28.5148 16.4106 28.9161C17.7525 29.6783 19.1849 30.021 20.5494 29.9083L25.8223 29.4618C26.0121 29.4438 26.1973 29.5114 26.3374 29.6377C26.4775 29.764 26.5588 29.9443 26.5588 30.1337C26.5588 32.8847 24.2635 35.3154 21.444 35.5544L18.8957 35.7709L26.6175 38.7923L40.8278 34.8148C43.0734 34.0933 45.5314 35.2973 46.2859 37.4981C46.4847 38.0843 46.4441 38.7112 46.1639 39.2659C45.8793 39.8341 45.3777 40.258 44.7632 40.4564L27.0197 46.9594C26.9429 46.9865 26.8661 47 26.7847 47ZM8.11039 41.7236L26.735 45.62L44.325 39.1802C44.6187 39.0855 44.8355 38.9051 44.9575 38.6616C45.0705 38.4361 45.0886 38.1745 45.0072 37.9355C44.4921 36.4248 42.7932 35.604 41.2209 36.1091L26.7667 40.1543C26.6266 40.1949 26.473 40.1858 26.3374 40.1317L15.6967 35.9693C15.403 35.8565 15.2268 35.5544 15.272 35.2432C15.3172 34.9321 15.5702 34.693 15.8865 34.666L21.3311 34.206C23.2062 34.0482 24.7786 32.6186 25.131 30.8778L20.6669 31.2567C19.0267 31.3965 17.3233 30.9951 15.7419 30.0932C15.1906 29.782 14.5264 29.5655 13.8171 29.4753C12.2402 29.2724 10.6768 29.6783 9.71892 30.499C9.2219 30.9229 8.68422 31.3018 8.11039 31.6265V41.7236Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M24.5074 20.6724H22.8447C21.1774 20.6724 19.8174 19.315 19.8174 17.6509C19.8174 17.2766 20.1201 16.9745 20.4951 16.9745C20.8702 16.9745 21.1729 17.2766 21.1729 17.6509C21.1729 18.5709 21.9229 19.3195 22.8447 19.3195H24.5074C25.4292 19.3195 26.1792 18.5709 26.1792 17.6509V15.9914C26.1792 15.0714 25.4292 14.3228 24.5074 14.3228H22.8447C21.1774 14.3228 19.8174 12.9654 19.8174 11.3013V9.64173C19.8174 7.97315 21.1774 6.62024 22.8447 6.62024H24.5074C26.1747 6.62024 27.5347 7.97766 27.5347 9.64173V9.71389C27.5347 10.0882 27.232 10.3903 26.857 10.3903C26.4819 10.3903 26.1792 10.0882 26.1792 9.71389V9.64173C26.1792 8.72176 25.4292 7.97315 24.5074 7.97315H22.8447C21.9229 7.97315 21.1729 8.72176 21.1729 9.64173V11.3013C21.1729 12.2213 21.9229 12.9699 22.8447 12.9699H24.5074C26.1747 12.9699 27.5347 14.3273 27.5347 15.9914V17.6509C27.5347 19.315 26.1747 20.6724 24.5074 20.6724Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M23.6763 7.71156C23.3013 7.71156 22.9985 7.40941 22.9985 7.03511V4.53223C22.9985 4.15792 23.3013 3.85577 23.6763 3.85577C24.0513 3.85577 24.354 4.15792 24.354 4.53223V7.03511C24.354 7.40941 24.0513 7.71156 23.6763 7.71156Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M23.6763 23.1753C23.3013 23.1753 22.9985 22.8731 22.9985 22.4988V19.996C22.9985 19.6217 23.3013 19.3195 23.6763 19.3195C24.0513 19.3195 24.354 19.6217 24.354 19.996V22.4988C24.354 22.8731 24.0513 23.1753 23.6763 23.1753Z"
                                                    fill="#91B9C4"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div className="offer__list-text" data-editable="" data-name="offer__list-text_2">
                                        <p>Ertr&auml;ge in 24
                                            Stunden auf Ihrem Konto</p></div>
                                </li>
                                <li className="offer__list-item">
                                    <div className="offer__list-img">
                                        <div className="offer__list-img-box" data-editable=""
                                             data-name="offer__list-img_3">
                                            <svg width="47" height="47" viewBox="0 0 47 47" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M46.302 36.4936H0.69802C0.311782 36.4936 0 36.1824 0 35.7969V4.53326C0 4.14774 0.311782 3.83655 0.69802 3.83655H15.4821C15.8683 3.83655 16.1801 4.14774 16.1801 4.53326V6.15427H30.5593V4.53326C30.5593 4.14774 30.8711 3.83655 31.2573 3.83655H46.302C46.6882 3.83655 47 4.14774 47 4.53326V35.7969C47 36.1824 46.6882 36.4936 46.302 36.4936ZM1.39604 35.1002H45.604V5.22997H31.9553V6.85098C31.9553 7.23649 31.6436 7.54769 31.2573 7.54769H15.4821C15.0958 7.54769 14.7841 7.23649 14.7841 6.85098V5.22997H1.39604V35.1002Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M31.2572 7.54768H15.482C15.0957 7.54768 14.7839 7.23649 14.7839 6.85097V3.01907C14.7839 2.63356 15.0957 2.32236 15.482 2.32236H18.7115C19.6096 2.32236 20.3448 1.59314 20.3448 0.696709C20.3448 0.311197 20.6566 0 21.0428 0H25.701C26.0872 0 26.399 0.311197 26.399 0.696709C26.399 1.59314 27.1296 2.32236 28.0277 2.32236H31.2572C31.6434 2.32236 31.9552 2.63356 31.9552 3.01907V6.85097C31.9552 7.23649 31.6434 7.54768 31.2572 7.54768ZM16.18 6.15426H30.5592V3.71578H28.0277C26.5991 3.71578 25.3985 2.72181 25.0821 1.39342H21.6571C21.3407 2.72645 20.1401 3.71578 18.7115 3.71578H16.18V6.15426Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M34.9103 47C34.6637 47 34.4264 46.8699 34.2961 46.6377L28.5211 36.1359C28.335 35.7969 28.4606 35.3742 28.7957 35.1884C29.1354 35.0026 29.5588 35.128 29.745 35.4625L35.5199 45.9688C35.7061 46.3079 35.5804 46.7306 35.2454 46.9164C35.1383 46.9721 35.0267 47 34.9103 47Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M11.8057 47C11.6941 47 11.5777 46.9721 11.4707 46.9164C11.131 46.7306 11.01 46.3079 11.1961 45.9689L16.9711 35.4625C17.1572 35.1234 17.5807 35.0027 17.9204 35.1885C18.2601 35.3743 18.3811 35.7969 18.1949 36.136L12.42 46.6424C12.2897 46.8699 12.0524 47 11.8057 47Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M31.6435 27.3389C31.3783 27.3389 31.1363 27.1903 31.0153 26.9487L26.3572 17.5153L21.6991 26.9487C21.5827 27.1856 21.3407 27.3389 21.0708 27.3389H10.0049C9.61867 27.3389 9.30688 27.0277 9.30688 26.6422C9.30688 26.2567 9.61867 25.9455 10.0049 25.9455H20.6381L25.7336 15.6296C25.85 15.3927 26.0919 15.2394 26.3618 15.2394C26.6271 15.2394 26.8691 15.388 26.9901 15.6296L31.6435 25.0676L38.1165 11.9649C38.2887 11.6212 38.7075 11.4772 39.0518 11.649C39.3962 11.8209 39.5404 12.2389 39.3683 12.5826L32.2717 26.9487C32.1554 27.1856 31.9134 27.3389 31.6435 27.3389Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M39.6383 15.806L38.5633 12.643L35.3896 13.7159L36.2785 11.9184L39.4475 10.8455L40.5271 14.0085L39.6383 15.806Z"
                                                    fill="#91B9C4"></path>
                                                <path
                                                    d="M7.57133 29.4104C6.04034 29.4104 4.79321 28.1656 4.79321 26.6375C4.79321 25.1094 6.04034 23.8646 7.57133 23.8646C9.10232 23.8646 10.3494 25.1094 10.3494 26.6375C10.3494 28.1703 9.10232 29.4104 7.57133 29.4104ZM7.57133 25.2627C6.80816 25.2627 6.18925 25.8804 6.18925 26.6422C6.18925 27.4039 6.80816 28.0217 7.57133 28.0217C8.3345 28.0217 8.95341 27.4039 8.95341 26.6422C8.95341 25.8804 8.32985 25.2627 7.57133 25.2627Z"
                                                    fill="#91B9C4"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div className="offer__list-text" data-editable="" data-name="offer__list-text_3">
                                        <p>Einfache
                                            Schulungsinstrumente</p></div>
                                </li>
                                <li className="offer__list-item">
                                    <div className="offer__list-img">
                                        <div className="offer__list-img-box" data-editable=""
                                             data-name="offer__list-img_4">
                                            <svg width="46" height="47" viewBox="0 0 46 47" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0)">
                                                    <path
                                                        d="M3.75541 32.2752C1.68656 32.2752 0 30.5798 0 28.5002V22.3064C0 20.2268 1.68656 18.5314 3.75541 18.5314C5.82426 18.5314 7.51082 20.2268 7.51082 22.3064V28.5002C7.51082 30.5798 5.82426 32.2752 3.75541 32.2752ZM3.75541 19.8877C2.42865 19.8877 1.34925 20.9728 1.34925 22.3064V28.5002C1.34925 29.8339 2.42865 30.9189 3.75541 30.9189C5.08217 30.9189 6.16157 29.8339 6.16157 28.5002V22.3064C6.16157 20.9728 5.08217 19.8877 3.75541 19.8877Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M42.0696 32.6549C40.0008 32.6549 38.3142 30.9596 38.3142 28.8799V22.6862C38.3142 20.6066 40.0008 18.9112 42.0696 18.9112C44.1385 18.9112 45.825 20.6066 45.825 22.6862V28.8799C45.825 30.9596 44.1385 32.6549 42.0696 32.6549ZM42.0696 20.2675C40.7429 20.2675 39.6635 21.3525 39.6635 22.6862V28.8799C39.6635 30.2136 40.7429 31.2987 42.0696 31.2987C43.3964 31.2987 44.4758 30.2136 44.4758 28.8799V22.6862C44.4758 21.3525 43.3964 20.2675 42.0696 20.2675Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M28.3073 44.4773C28.006 44.4773 27.7316 44.2739 27.6552 43.9665C27.5652 43.6048 27.7811 43.2341 28.1409 43.1436C34.3924 41.5432 39.2587 37.1895 40.8418 31.787C40.9453 31.4298 41.323 31.2218 41.6784 31.3258C42.0337 31.4298 42.2405 31.8096 42.1371 32.1667C40.4191 38.0259 35.184 42.7322 28.4782 44.4547C28.4197 44.4683 28.3613 44.4773 28.3073 44.4773Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M40.6438 20.2404C40.4908 20.2404 40.3379 20.1861 40.2165 20.0867C40.0771 19.9736 39.9916 19.8064 39.9736 19.63C39.0741 10.7418 31.6847 4.04175 22.7842 4.04175C13.9871 4.04175 6.61119 10.6559 5.61724 19.4266C5.59925 19.6074 5.5093 19.7702 5.36538 19.8787C5.22596 19.9917 5.04606 20.0415 4.87066 20.0234L2.21713 19.7205C1.84834 19.6798 1.58299 19.3407 1.62346 18.97C2.84678 8.15583 11.9407 0 22.7842 0C33.7536 0 42.861 8.26433 43.9719 19.2232C44.0079 19.5939 43.738 19.9284 43.3692 19.9646L40.7112 20.2359C40.6887 20.2359 40.6662 20.2404 40.6438 20.2404ZM22.7842 2.68546C32.1525 2.68546 39.9691 9.57541 41.2329 18.8208L42.5462 18.6852C41.2149 8.75712 32.8316 1.35629 22.7842 1.35629C12.8537 1.35629 4.48837 8.65766 3.05367 18.4456L4.36694 18.5947C5.72518 9.47595 13.5283 2.68546 22.7842 2.68546Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M25.0645 47H21.453C19.5955 47 18.0889 45.481 18.0889 43.6183C18.0889 41.7512 19.6 40.2366 21.453 40.2366H25.0645C26.922 40.2366 28.4286 41.7557 28.4286 43.6183C28.4286 45.481 26.9175 47 25.0645 47ZM21.4485 41.5929C20.3376 41.5929 19.4336 42.5016 19.4336 43.6183C19.4336 44.735 20.3376 45.6437 21.4485 45.6437H25.06C26.1709 45.6437 27.0749 44.735 27.0749 43.6183C27.0749 42.5016 26.1709 41.5929 25.06 41.5929H21.4485Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M23.0542 33.6541C17.5583 33.6541 13.0833 29.1602 13.0833 23.6311C13.0833 18.1065 17.5538 13.6081 23.0542 13.6081C23.4275 13.6081 23.7288 13.911 23.7288 14.2862C23.7288 14.6615 23.4275 14.9644 23.0542 14.9644C18.3003 14.9644 14.4325 18.8524 14.4325 23.6311C14.4325 28.4097 18.3003 32.2978 23.0542 32.2978C27.2863 32.2978 30.8619 29.2552 31.559 25.0688C31.6219 24.698 31.9682 24.4494 32.337 24.5127C32.7058 24.576 32.9532 24.9241 32.8902 25.2948C32.0852 30.1368 27.952 33.6541 23.0542 33.6541Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M29.6836 27.6548L32.1707 25.5751L34.2396 28.0797L34.415 26.1357L32.3506 23.6311L29.8635 25.7107L29.6836 27.6548Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M29.2471 23.1474C26.1933 23.1474 23.7107 20.6518 23.7107 17.582C23.7107 14.5123 26.1933 12.0167 29.2471 12.0167C32.3009 12.0167 34.7835 14.5123 34.7835 17.582C34.7835 20.6518 32.3009 23.1474 29.2471 23.1474ZM29.2471 13.373C26.9399 13.373 25.0599 15.2628 25.0599 17.582C25.0599 19.9013 26.9399 21.7911 29.2471 21.7911C31.5543 21.7911 33.4343 19.9013 33.4343 17.582C33.4343 15.2628 31.5543 13.373 29.2471 13.373Z"
                                                        fill="#91B9C4"></path>
                                                    <path
                                                        d="M31.8872 18.5948H29.3192C28.9459 18.5948 28.6445 18.2918 28.6445 17.9166V15.3351C28.6445 14.9599 28.9459 14.657 29.3192 14.657C29.6925 14.657 29.9938 14.9599 29.9938 15.3351V17.2385H31.8872C32.2605 17.2385 32.5619 17.5414 32.5619 17.9166C32.5619 18.2918 32.2605 18.5948 31.8872 18.5948Z"
                                                        fill="#91B9C4"></path>
                                                </g>
                                                <defs>
                                                    <clippath id="clip0">
                                                        <rect width="45.825" height="47" fill="white"></rect>
                                                    </clippath>
                                                </defs>
                                            </svg>
                                        </div>
                                    </div>
                                    <div className="offer__list-text" data-editable="" data-name="offer__list-text_4">
                                        <p>24 Stunden Support
                                            und fortlaufende Unterst&uuml;tzung</p></div>
                                </li>
                            </ul>
                        </div>
                        <p>
                            <button data-editable="" data-name="offer__btn btn" data-scroll="form__top"
                                    style={{color: 'white'}}
                                    onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}

                                    className="offer__btn js-scroll">
                                Entdecken Sie, wie Sie von einer Investition in Amazon profitieren
                            </button>
                        </p>
                    </section>


                    <section className="article">
                        <div className="containerA article__containerA">
                            <div className="article__text">
                                <div data-editable="" data-name="article__title"><h2>Ein echtes<br
                                    className="mobile-only"/>&quot;Billionen-Unternehmen&quot;
                                </h2></div>
                                <div data-editable="" data-name="article__text-1" className="article__text_1"><p>Amazons
                                    Marktkapitalisierung hat im Jahr 2018 1 Billion Dollar erreicht. Laut Bloomberg
                                    k&ouml;nnte Amazon
                                    einen Jahres-Bruttoumsatz von &uuml;ber 1 Billion Dollar im Jahr 2025 erreichen.</p>
                                </div>
                                <div data-editable="" data-name="article__text-2" className="article__text_2"><p>Mit
                                    einem steigenden
                                    Marktumsatz wird Amazon unangefochtener Marktf&uuml;hrer sein. Anleger und Menschen,
                                    die sich am
                                    Amazon-Modell beteiligen, k&ouml;nnen weiterhin von den Entwicklungsperspektiven von
                                    Amazon
                                    profitieren.</p></div>
                            </div>

                            <div className="article__image desktop-only" data-editable=""
                                 data-name="article__image_desktop"><img alt=""
                                                                         src={ArtImage1920}/>
                            </div>
                        </div>
                        <p>
                            <button data-editable="" data-name="article__btn" data-scroll="form__top"
                                    style={{color: 'white'}}
                                    onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}

                                    className="article__btn offer__btn js-scroll">Entdecken Sie, wie Sie von einer
                                Investition in Amazon
                                profitieren
                            </button>
                        </p>
                        <div className="offer__payment-methods" data-editable="" data-name="offer__payment-methods"><img
                            alt=""
                            src={PaymentMethods}/>
                        </div>
                    </section>
                </div>

                <Footer
                    logoStatus={true}
                />
s
            </>
        )
    }
}

const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(AmazonPage2);
