import React from "react";
import {connect} from "react-redux";

import ReactCustomFlagSelect from "react-custom-flag-select";
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";

import HeaderImg from '../assets/img/header-img.png'
import AvailableAtAmazon from '../assets/img/available_at_amazon.png'

import Asset1 from '../assets/img/Asset1.png'
import Asset2 from '../assets/img/Asset2.png'
import Asset3 from '../assets/img/Asset3.png'
import Asset4 from '../assets/img/Asset4.png'
import Image from '../assets/img/image.png'
import PaymentMethods from '../assets/img/payment-methods.png'

import {Header} from "../components/layout/header";
import {Footer} from "../components/layout/footer";

import {checkCountryRequest, signUpSecondRequest} from '../store/actions'
import {FLAG_SELECTOR_OPTION_LIST} from '../helper/country'
import {CountryInitialName} from '../helper/countryName'
import {PhoneNumberLength} from '../helper/phoneNumberLength'
import {validateEmail, onlyStringAndSpice, onlyNumbers, sameLetters} from '../helper/regexs'
import {isPossiblePhoneNumber, isValidPhoneNumber} from "react-phone-number-input";

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;


class AmazonPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,
            redirect: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',

            emailErrMessage : 'Ungültige Adresse',
            firstNameErrMessage : 'Erforderlich',
            lastNameErrMessage : 'Erforderlich',
            phoneErrMessage : 'Ungültige Telefonnummer',

            phoneNumberMinLength :9,
            phoneMaxNumberLength : 10,


        };
        this.submit = this.submit.bind(this);

    }

    componentDidMount() {
        this.props.checkCountryRequest()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if(name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        }
        else if(name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        }
        else if(name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        }
        else if(name === 'email' ) {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    blurHandler = (e) =>{
       /* console.log("####", e.target.value)
        console.log("####2", e.target.name)*/
    }

    handleRegistration = (event) => {
        if (this.state.firstName) {
            if ( this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage : 'Erforderlich'})
            }else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage : 'Erforderlich'})
        }


        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }


        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phone) {
            if ( !PhoneNumberLength(this.state.areaCode, this.state.phone.length) || !onlyNumbers(this.state.phone)) {
                this.setState({phoneErr: true, phoneErrMessage : 'Ungültige Telefonnummer'})
            }else {
                this.setState({phoneErr: false})
            }

        } else {
            this.setState({phoneErr: true})
        }
        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()

        if (
            this.state.firstName && ( !this.state.firstName.length <= 3 || !this.state.firstName >= 16  ) &&
            this.state.lastName && (!this.state.lastName.length <= 3 || !this.state.lastName >= 16 ) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked)
        {

            let urlencoded = new URLSearchParams();
            urlencoded.append("Name", this.state.firstName);
            urlencoded.append("LastName", this.state.lastName);
            urlencoded.append("Email", this.state.email);
            urlencoded.append("Phone", '+' + this.state.areaCode + this.state.phone);
            urlencoded.append("Password", "demonPassword1234567");
            urlencoded.append("Country", this.state.areaCodeName);
            urlencoded.append("Source", process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1]);
            urlencoded.append("AffilateID", process.env.REACT_APP_ID);
            urlencoded.append("Token", process.env.REACT_APP_TOKEN);
            urlencoded.append("PromoCode", "");

            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }

            this.props.signUpSecondRequest(item)
        }
    }

    _scrollToTop() {
        window.scrollTo({top: 0, behavior: 'smooth'})
    }

    render() {

        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <>

                <Header/>
                <section id="headerSection" className="header-section">
                    <div className="container">
                        <img className="img header-img" src={HeaderImg} alt=""/>
                        <div className="row">
                            <div className="col-lg-4 col-md-12 col-8 offset-lg-4 offset-4">
                                <div className="logo-wrapper">
                                    <div className="heading-text">
                                        <div className="row-one">Mit</div>
                                        <div className="row-two">AMAZON</div>
                                        <div className="row-three">
                                            Geld verdienen. Steigern Sie Ihre potenziellen Gewinne
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 form-out form-desktop" data-select2-id="10">
                                <div className="form-wrapper" id="signin-d">
                                    <div className="form-text">
                                        Investieren Sie in <span className="font-weight-bold">Amazon</span>
                                    </div>
                                    <div className="reg-form">
                                        <form className="x-form registration-form ltr">
                                            <div className="x-content">
                                                <div className="x-row">
                                                    <div className="x-half-row">
                                                        <div
                                                            className={`input x-input-must-validate ${this.state.firstNameErr ? 'x-input-error' : ''}`}>
                                                            <input type="text" placeholder="Vorname"
                                                                   name="firstName"
                                                                   onBlur={this.blurHandler.bind(this)}
                                                                   onChange={this.handleChange("firstName")}
                                                                   value={this.state.firstName}
                                                            />
                                                            {
                                                                this.state.firstNameErr ? <label
                                                                    className="x-error">{this.state.firstNameErrMessage}</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                    <div className="x-half-row">
                                                        <div
                                                            className={`input x-input-must-validate ${this.state.lastNameErr ? 'x-input-error' : ''}`}>
                                                            <input type="text" placeholder="Nachname"
                                                                   onChange={this.handleChange("lastName")}
                                                                   value={this.state.lastName}
                                                            />
                                                            {
                                                                this.state.lastNameErr ? <label
                                                                    className="x-error">{this.state.lastNameErrMessage}</label> : null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="x-row">
                                                    <div
                                                        className={`input x-input-must-validate ${this.state.emailErr || this.props.auth.alert ? 'x-input-error' : ''}`}>
                                                        <label
                                                            className="icon-right" htmlFor="email"><i
                                                            className="x-icon icon-envelope-o"></i></label><input
                                                        type="email"
                                                        placeholder="E-Mail-Adresse"
                                                        onChange={this.handleChange("email")}
                                                        value={this.state.email}
                                                        autoComplete="off" className="field-error"/>
                                                        {
                                                            this.state.emailErr ? <label className="x-error">{this.state.emailErrMessage}</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <div className="x-country-replace"></div>
                                                <div className="x-phone-replace" data-select2-id="9">
                                                    <div className="x-flex-row" data-select2-id="8">
                                                        <div className='input'>
                                                            <ReactCustomFlagSelect
                                                                attributesWrapper={{
                                                                    id: "areaCodeWrapper",
                                                                    tabIndex: "1"
                                                                }} //Optional.[Object].Modify wrapper general attributes.
                                                                attributesButton={{id: "areaCodeButton"}} //Optional.[Object].Modify button general attributes.
                                                                attributesInput={{
                                                                    id: "areaCode",
                                                                    name: "areaCode"
                                                                }} //Optional.[Object].Modify hidden input general attributes.
                                                                value={currentItem.id} //Optional.[String].Default: "".
                                                                disabled={false} //Optional.[Bool].Default: false.
                                                                showSearch={true} ////Optional.[Bool].Default: false.
                                                                showArrow={true} //Optional.[Bool].Default: true.
                                                                animate={true} //Optional.[Bool].Default: false.
                                                                optionList={FLAG_SELECTOR_OPTION_LIST} //Required.[Array of Object(s)].Default: [].
                                                                // selectOptionListItemHtml={<div>us</div>} //Optional.[Html].Default: none. The custom select options item html that will display in dropdown list. Use it if you think the default html is ugly.
                                                                // selectHtml={<div>us</div>} //Optional.[Html].Default: none. The custom html that will display when user choose. Use it if you think the default html is ugly.
                                                                classNameWrapper="" //Optional.[String].Default: "".
                                                                classNameContainer="" //Optional.[String].Default: "".
                                                                classNameOptionListContainer="" //Optional.[String].Default: "".
                                                                classNameOptionListItem="" //Optional.[String].Default: "".
                                                                classNameDropdownIconOptionListItem={{color: 'red',}} //Optional.[String].Default: "".
                                                                customStyleWrapper={{}} //Optional.[Object].Default: {}.
                                                                customStyleContainer={{
                                                                    background: '#fff',
                                                                    border: "2px solid rgba(0,0,0,.12)",
                                                                    borderRadius: '3px',
                                                                    fontSize: '16px',
                                                                    height: '48px',
                                                                    width: '100%',
                                                                    outline: '0'
                                                                }} //Optional.[Object].Default: {}.
                                                                customStyleSelect={{width: "80px"}} //Optional.[Object].Default: {}.
                                                                customStyleOptionListItem={{}} //Optional.[Object].Default: {}.
                                                                customStyleOptionListContainer={{
                                                                    maxHeight: '300px',
                                                                    overflow: 'auto',
                                                                    width: '120px',
                                                                }} //Optional.[Object].Default: {}.
                                                                onChange={(areaCode) => {
                                                                    this.setState({areaCode: areaCode}, () => {
                                                                        this.handleChangeAreaCode(areaCode);
                                                                    });
                                                                }}
                                                            />
                                                        </div>

                                                        <div
                                                            className={`input x-input-must-validate ${this.state.phoneErr ? 'x-input-error' : ''}`}>
                                                            <label className="icon-right" htmlFor="phone"><i
                                                                className="x-icon icon-phone"></i></label><input
                                                            type="text" placeholder="555 12 34 56"
                                                            onChange={this.handleChange("phone")}
                                                            value={this.state.phone}
                                                            className="field-error"/>
                                                            {
                                                                this.state.phoneErr ? <label className="x-error">Ungültige Telefonnummer</label> : null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="x-recaptcha-replace"></div>


                                                <div className="x-row">
                                                    <div className="input x-input-error">
                                                        <input type="checkbox"
                                                               name="checked"
                                                               id="cb_cond_1_0"
                                                               checked={this.state.checked}
                                                               onChange={this.handleInputChange}
                                                               className="field-error"/>&nbsp;
                                                        <label htmlFor="cb_cond_1_0" className="checkbox">Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.</label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <button
                                                    onClick={this.handleRegistration}
                                                    type="button" className="x-form-button"
                                                    style={{background: '#FFF', color: '#000'}}>JETZT ANFANGEN
                                                </button>
                                                <div className="x-text-notice">
                                                    <div>Um in Aktien zu investieren, müssen Sie mindestens 18 Jahre alt
                                                        sein. Mindesteinzahlung erforderlich € 250
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="invest-section">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="container text-center">
                        <span className="invest-text text-center col-11">Investieren Sie in ein echtes
                          <br className="d-none d-md-inline"/>
                          "Billionen-Dollar-Unternehmen”
                          </span>
                                <img className="invest-arrow text-center col-11" src={AvailableAtAmazon}
                                     alt="available_at_amazon"/>
                            </div>
                            <div className="col-md-8 offset-md-2 form-out form-mobile">
                                <div className="form-wrapper" id="signin-m">
                                    <div className="reg-form">
                                        <form className="x-form registration-form ltr">
                                            <div className="x-content">
                                                <div className="x-row">
                                                    <div className="x-half-row">
                                                        <div
                                                            className={`input x-input-must-validate ${this.state.firstNameErr ? 'x-input-error' : ''}`}>
                                                            <input type="text" placeholder="Vorname"
                                                                   name="firstName"
                                                                   onChange={this.handleChange("firstName")}
                                                                   value={this.state.firstName}
                                                            />
                                                            {
                                                                this.state.firstNameErr ? <label
                                                                    className="x-error">Erforderlich</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                    <div className="x-half-row">
                                                        <div
                                                            className={`input x-input-must-validate ${this.state.lastNameErr ? 'x-input-error' : ''}`}>
                                                            <input type="text" placeholder="Nachname"
                                                                   onChange={this.handleChange("lastName")}
                                                                   value={this.state.lastName}
                                                            />
                                                            {
                                                                this.state.lastNameErr ? <label
                                                                    className="x-error">Erforderlich</label> : null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="x-row">
                                                    <div
                                                        className={`input x-input-must-validate ${this.state.emailErr ? 'x-input-error' : ''}`}>
                                                        <label
                                                            className="icon-right" htmlFor="email"><i
                                                            className="x-icon icon-envelope-o"></i></label><input
                                                        type="email"
                                                        placeholder="E-Mail-Adresse"
                                                        onChange={this.handleChange("email")}
                                                        value={this.state.email}
                                                        autoComplete="off" className="field-error"/>
                                                        {
                                                            this.state.emailErr ? <label className="x-error">{this.state.emailErrMessage}</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <div className="x-country-replace"></div>
                                                <div className="x-phone-replace" data-select2-id="9">
                                                    <div className="x-flex-row" data-select2-id="8">
                                                        <div className='input'>
                                                            <ReactCustomFlagSelect
                                                                attributesWrapper={{
                                                                    id: "areaCodeWrapper",
                                                                    tabIndex: "1"
                                                                }} //Optional.[Object].Modify wrapper general attributes.
                                                                attributesButton={{id: "areaCodeButton"}} //Optional.[Object].Modify button general attributes.
                                                                attributesInput={{
                                                                    id: "areaCode",
                                                                    name: "areaCode"
                                                                }} //Optional.[Object].Modify hidden input general attributes.
                                                                value={currentItem.id} //Optional.[String].Default: "".
                                                                disabled={false} //Optional.[Bool].Default: false.
                                                                showSearch={true} ////Optional.[Bool].Default: false.
                                                                showArrow={true} //Optional.[Bool].Default: true.
                                                                animate={true} //Optional.[Bool].Default: false.
                                                                optionList={FLAG_SELECTOR_OPTION_LIST} //Required.[Array of Object(s)].Default: [].

                                                                classNameWrapper="" //Optional.[String].Default: "".
                                                                classNameContainer="" //Optional.[String].Default: "".
                                                                classNameOptionListContainer="" //Optional.[String].Default: "".
                                                                classNameOptionListItem="" //Optional.[String].Default: "".
                                                                classNameDropdownIconOptionListItem={{color: 'red',}} //Optional.[String].Default: "".
                                                                customStyleWrapper={{}} //Optional.[Object].Default: {}.
                                                                customStyleContainer={{
                                                                    background: '#fff',
                                                                    border: "2px solid rgba(0,0,0,.12)",
                                                                    borderRadius: '3px',
                                                                    fontSize: '16px',
                                                                    height: '48px',
                                                                    width: '100%',
                                                                    outline: '0'
                                                                }} //Optional.[Object].Default: {}.
                                                                customStyleSelect={{width: "80px"}} //Optional.[Object].Default: {}.
                                                                customStyleOptionListItem={{}} //Optional.[Object].Default: {}.
                                                                customStyleOptionListContainer={{
                                                                    maxHeight: '300px',
                                                                    overflow: 'auto',
                                                                    width: '120px',
                                                                }} //Optional.[Object].Default: {}.
                                                                onChange={(areaCode) => {
                                                                    this.setState({areaCode: areaCode}, () => {
                                                                        this.handlePhoneChange(phone);
                                                                    });
                                                                }}
                                                            />
                                                        </div>

                                                        <div
                                                            className={`input x-input-must-validate ${this.state.phoneErr ? 'x-input-error' : ''}`}>
                                                            <label className="icon-right" htmlFor="phone"><i
                                                                className="x-icon icon-phone"></i></label><input
                                                            type="text" placeholder="555 12 34 56"
                                                            onChange={this.handleChange("phone")}
                                                            value={this.state.phone}
                                                            className="field-error"/>
                                                            {
                                                                this.state.phoneErr ? <label className="x-error">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="x-recaptcha-replace"></div>


                                                <div className="x-row">
                                                    <div className="input x-input-error">
                                                        <input type="checkbox"
                                                               name="checked"
                                                               id="cb_cond_1_0"
                                                               checked={this.state.checked}
                                                               onChange={this.handleInputChange}
                                                               className="field-error"/>&nbsp;
                                                        <label htmlFor="cb_cond_1_0" className="checkbox">Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.</label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <button
                                                    onClick={this.handleRegistration}
                                                    type="button" className="x-form-button"
                                                    style={{background: '#FFF', color: '#000'}}>JETZT ANFANGEN
                                                </button>
                                                <div className="x-text-notice">
                                                    <div>Um in Aktien zu investieren, müssen Sie mindestens 18 Jahre alt
                                                        sein. Mindesteinzahlung erforderlich € 250
                                                    </div>
                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="how-work-section">
                    <div className="container">
                        <div className="how-wrapper">
                            <h3 className="how-title text-center bold">
                                Das Starten ist einfach. Zugriff bekommen auf:
                            </h3>
                            <div className="row">
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset1} alt="Asset1"/>
                                        <div className="asset-text">Sichere und autorisierte Plattform</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset2} alt="Asset2"/>
                                        <div className="asset-text">
                                            Verdienen Sie in 24 Stunden auf Ihrem Konto
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset3} alt="Asset3"/>
                                        <div className="asset-text">Einfache Lernwerkzeuge</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3 text-center">
                                    <div className="how-wrapper">
                                        <img src={Asset4} alt="Asset4"/>
                                        <div className="asset-text-last">
                                            24 Stunden kontinuierliche Unterstützung
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="futures-section">
                    <div className="container">
                        <div className="
                        col-xl-6 col-lg-8 col-md-8
                        offset-xl-2 offset-lg-3 offset-md-2
                        futures-text
                      ">
                            <div className="futures-title">Eine echte "Billionärsfirma"</div>
                            <p>
                                Die Marktkapitalisierung von Amazon erreichte 2018 1 Billion Dollar.
                                Laut Bloomberg könnte Amazon bis 2025 einen jährlichen Bruttoumsatz
                                von über 1 Billion USD erzielen.
                            </p>
                            <p>
                                Mit steigenden Markteinnahmen ist Amazon der unbestrittene
                                Marktführer. Investoren und Personen, die am Amazon-Modell
                                teilnehmen, können weiterhin von den Entwicklungsaussichten für
                                Amazon profitieren.
                            </p>
                        </div>
                        <div className="futures-wrap">
                            <img className="hand" src={Image} alt="hand-image"/>
                            <button className="btn-d scroll-to-form-d"
                                    onClick={this._scrollToTop.bind(this)}
                            >
                                Entdecken Sie, wie Sie von Amazon-Investitionen profitieren können
                            </button>
                            <button className="btn-m scroll-to-form-m"
                                    onClick={this._scrollToTop.bind(this)}
                            >
                                Entdecken Sie, wie Sie von Amazon-Investitionen profitieren können
                            </button>
                            <img className="payments" src={PaymentMethods} alt="payment-methods"/>
                        </div>
                    </div>
                </section>


                <Footer
                    logoStatus={true}
                />

            </>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpSecondRequest
})(AmazonPage);

