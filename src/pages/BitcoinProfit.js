import React, {Component} from 'react'
import {connect} from "react-redux";
import {Container} from "react-bootstrap";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'

import Logo  from '../assets/img/bitcoin/logo.svg'
import VideoThumbnail  from '../assets/img/bitcoin/video-thumbnail-de-step1.png'
import AppPhoneMockup  from '../assets/img/bitcoin/appPhoneMockup.png'
import StoryImgEs1  from '../assets/img/bitcoin/story-img-es-1.png'
import StoryImgEs2  from '../assets/img/bitcoin/story-img-es-2.png'
import StoryImgEs3  from '../assets/img/bitcoin/story-img-es-3.png'
import StoryImgDe1  from '../assets/img/bitcoin/story-img-de-1.png'
import StoryImgDe2  from '../assets/img/bitcoin/story-img-de-2.png'
import StoryImgDe3  from '../assets/img/bitcoin/story-img-de-3.png'
import StoryImgDe4  from '../assets/img/bitcoin/story-img-de-4.png'
import {Footer} from "../components/layout/footer";
import ReactCustomFlagSelect from "react-custom-flag-select";
import {FLAG_SELECTOR_OPTION_LIST} from "../helper/country";


import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'

import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";
import {Player} from "video-react";
import MyVideo from "../assets/video/video.mp4"

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();
class BitcoinProfit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            popap : false,
            theposition: '',
            scrollPosition: false,
            fixPosition: false,
            fixPositionHeight: 0,
            phoneValue: '',
            messagesEndRef: React.createRef(),




        };
        this.submit = this.submit.bind(this);
    }


    _onMouseMove(e) {
        if (e.screenY <= 80) {
            this.setState({popap: true});
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop


        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled >= 0.9) {
            const fixTop = height - 550;
            this.setState({
                fixPosition: true,
                // fixPositionHeight: fixTop,
            })
        }else {
            this.setState({
                fixPosition: false,
            })
        }



        if (scrolled > 0) {
            this.setState({
                scrollPosition: true
            })
        } else {
            this.setState({
                scrollPosition: false
            })
        }

    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop / 3 - 60);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {

        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];

        return (
            <Container style={{padding:'0px'}} fluid >
                <section className="headerSection">
                    <div className="container">
                        <div className="headerImgWrapper">
                            <img src={Logo} alt="logo" />
                        </div>

                        <h1><span data-i18n="">Verdienen Sie Millionen mit</span> <br/> <span data-i18n="">Bitcoin, sogar, wenn die</span>
                            <br/> <span data-i18n="">Kryptomärkte einbrechen</span></h1>
                        <h3><a className="meldenStrongSpan"><strong data-i18n="">Sehen Sie sich das kurze Video unten
                            an,</strong> </a> <span data-i18n="">um zu sehen, was vor uns liegt,</span><br/><span
                            data-i18n=""> und füllen Sie dann das kurze Registrierungsformular aus,</span><br/><span
                            data-i18n=""> um heute noch zu beginnen.</span></h3>
                    </div>
                </section>

                <section className="videoFormSection">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-7">
                                <div className="videoContainer">
                                    <div className="">
                                        <div id="mainVideo" style={{position: 'relative'}}>
                                            <div id="MainVideobtn" style={{
                                                position: 'absolute',
                                                right: '5%',
                                                top: '5%',
                                                zIndex: '1',
                                                background: 'rgba(0, 0, 0, 0.7)',
                                                borderRadius: '30px',
                                                padding: '0 5px',
                                                color: '#fff'
                                            }}
                                            >
                                                {
                                                    this.state.muted
                                                        ? <img
                                                            onClick={this._handleMute.bind(this)}
                                                            src="https://media.giphy.com/media/gfHIf1cc9ACGzVgLu0/giphy.gif"
                                                            style={{width: '35px'}}/>
                                                        : null
                                                }

                                            </div>
                                            <Player
                                                autoPlay={true}
                                                muted={true}
                                                // poster="/assets/poster.png"
                                                src={MyVideo}
                                            />
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div className="col-md-5">
                                <div className="formOuterContainer">
                                    <p><span className="darkOrange" data-i18n="" id="firstFormHeader"><i
                                        className="fa fa-arrow-down"></i> <span data-i18n="">SCHLIESSEN SIE HIER</span> <i
                                        className="fa fa-arrow-down"></i></span><br/>
                                        <span data-i18n="">Ihre Registrierung ab</span></p>

                                </div>

                                <div className="formInnerContainer">
                                    <div className="formContainer">
                                        <div data-intgrtn-form-signup="">
                                            <form method="post" id="formfull" action=""  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset"  style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="Vorname"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset"  style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Nachname"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset" style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="E-Mail-Adresse" value=""
                                                               maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset"  style={{textAlign:'left'}}>
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">
                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='x-forms'  style={{textAlign:'left'}}>

                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>


                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="button" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{
                                                                width: '100%',
                                                                fontSize: '1.25em',
                                                                fontWeight: '700',
                                                                boxShadow: '0 2px 0 #1978a5',
                                                                background:' #2ba6df',
                                                                textTransform: 'uppercase',
                                                                whiteSpace: 'normal',
                                                                color: '#ffffff',
                                                                padding: '6px 12px',
                                                                border: '1px solid transparent',
                                                                borderRadius: '4px'


                                                            }}
                                                            className="lead-form-submit">
                                                        Informationen erhalten
                                                    </button>
                                                </div>
                                            </form>

                                        </div>

                                        <div className="disclaimer-wrapper hidden">
                                            <p className="form-disclaimers">*By submitting you confirm that you’ve read
                                                and accepted the <a data-intgrtn-link-agreements="3"
                                                                    href="https://tradedigitalapp.com/bitcoinprofit/lp.php"
                                                                    className="" data-i18n="">privacy policy</a> and <a
                                                    data-intgrtn-link-agreements="1"
                                                    href="https://tradedigitalapp.com/bitcoinprofit/lp.php" className=""
                                                    data-i18n="">terms of conditions</a>.</p>
                                            <p className="form-disclaimers">**By submitting this form, I agree to
                                                receive all marketing material by email, SMS and telephone.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>


                <section className="smartPhoneSection">
                    <div className="container">
                        <h1 data-i18n="">Willkommen bei Bitcoin Profit</h1>
                        <h3 data-i18n="">Unsere Plattform ermöglicht Ihnen einen einfachen Zugang zur Welt des
                            Bitcoin-Handels. Wenn Sie sich anmelden, können Sie unsere einzigartige Plattform nutzen, um
                            von überall auf der Welt zu handeln. Sie brauchen sich keine Sorgen machen, wenn Sie noch
                            nie gehandelt haben. Wir haben alle unnötigen Elemente, die von anderen Handels-Tools
                            verwendet werden, entfernt, um Ihnen den Einstieg so einfach wie möglich zu machen.</h3>
                        <div className="row phoneRow">
                            <div className="col-md-4 smartPhoneCol">
                                <p data-i18n="">Unsere Plattform ist leicht und so konzipiert, dass das Handeln ohne
                                    Verwirrung vonstattengeht. Sie wissen nicht, was eine HW-Anforderung ist? Machen Sie
                                    sich keine Sorgen, Sie müssen das nicht wissen. Das Einzige, was Sie brauchen, ist
                                    eine Internetverbindung.</p>
                            </div>
                            <div className="col-md-4 smartPhoneCol">
                                <p data-i18n="">Solange Sie Zugang zum Internet haben, können Sie überall und jederzeit
                                    verbunden sein. Unser Service funktioniert plattformübergreifend, d. h. Sie können
                                    mit Ihrem Laptop, PC oder Mobilgerät handeln.</p>
                            </div>
                            <div className="col-md-4 smartPhoneCol smartPhoneImgCol"><img
                                src={AppPhoneMockup} alt="Smart Phone" /></div>
                        </div>
                    </div>
                </section>


                <section className="infoSection">
                    <div className="container">
                        <h1>
                            <span data-i18n="">Wir machen den</span><br/><span data-i18n="">Mehraufwand, der Ihnen abgenommen wird.</span>
                        </h1>
                        <h3 data-i18n="">Wir haben ein Team von Experten, die sich der Verbesserung unserer Technologie
                            verschrieben haben.</h3>
                        <p data-i18n="">Hinter den Kulissen arbeiten sie stetig daran, sicherzustellen, dass Sie überall
                            und jederzeit den bestmöglichen Service erhalten. Wir wollten nicht noch irgendein altes
                            Handels-Tool schaffen. Wir wollten die Art und Weise, wie Menschen über den Handel
                            nachdenken, revolutionieren, indem wir die Datenanalyse überdenken. Auf diese Weise hoffen
                            wir, dass wir Ihnen eine beispiellose Leistung auf einer Plattform, die nicht nur sicher und
                            zuverlässig ist, sondern Ihnen auch ermöglicht, die besten Renditen für Ihre Investitionen
                            zu erzielen, anbieten können.</p>
                    </div>
                </section>


                <section className="faqSection">
                    <div className="container">
                        <div className="faqHeaderDiv">
                            <h1 className="darkOrange" data-i18n="">Häufig gestellte Fragen</h1>
                            <p><span data-i18n="">Hier sind die am häufigsten gestellten</span><br/> <span data-i18n="">Fragen und unsere Antworten darauf.</span>
                            </p>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <div className="faqBlock">
                                    <div className="faqQ" data-text="Q">
                                        <span data-i18n="">Ist Bitcoin sicher?</span>
                                    </div>
                                    <div className="faqA" data-text="A" data-i18n="">Wenn man sich die Geschichte von
                                        Bitcoin ansieht, gab es viele Höhen und Tiefen. Aber bei genauerer Betrachtung,
                                        erkennt man, dass er nach jedem Abstieg auf ein neues Hoch angestiegen ist. Dies
                                        liegt daran, dass Bitcoin ständig wächst.
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="faqBlock">
                                    <div className="faqQ" data-text="Q">
                                        <span data-i18n="">Was ist für uns drin?</span>
                                    </div>
                                    <div className="faqA" data-text="A" data-i18n="">Bei Bitcoin Profit sind wir alle
                                        Investoren in Bitcoins. Daher wissen wir, dass je mehr Menschen sich uns
                                        anschließen, desto stärker wird der Bitcoin wachsen.
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="faqBlock">
                                    <div className="faqQ" data-text="Q">
                                        <span data-i18n="">Was ist das Geheimnis?</span>
                                    </div>
                                    <div className="faqA" data-text="A" data-i18n="">Das Geheimnis ist, dass es kein
                                        Geheimnis gibt. Melden Sie sich heute an und wir zeigen Ihnen, wie einfach es
                                        ist, mit Bitcoins zu handeln.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="spaceBetweenFaqRows"></div>
                        <div className="row">
                            <div className="col-md-4">
                                <div className="faqBlock">
                                    <div className="faqQ" data-text="Q">
                                        <span data-i18n="">Ist es riskant?</span>
                                    </div>
                                    <div className="faqA" data-text="A" data-i18n="">Die Risikostufen hängen davon ab,
                                        wie Sie handeln. Besuchen Sie uns heute und wir zeigen Ihnen, wie Sie Ihre
                                        Gewinne maximieren und Ihr Risiko minimieren können.
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="faqBlock">
                                    <div className="faqQ" data-text="Q">
                                        <span data-i18n="">Warum sollte man Bitcoin Profit beitreten?</span>
                                    </div>
                                    <div className="faqA" data-text="A" data-i18n="">Mit Bitcoin Profit können Sie Ihre
                                        Bitcoins problemlos gegen echtes Geld eintauschen. Außerdem bieten wir eine
                                        Plattform mit der neusten Technik, die den Handel einfacher macht als je zuvor.
                                        Glauben Sie uns nicht? Melden Sie sich heute an und überzeugen Sie sich selbst.
                                        Sich an der Bitcoin-Revolution zu beteiligen, war noch nie einfacher.
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="faqBlock">
                                    <div className="faqQ" data-text="Q">
                                        <span data-i18n="">Was ist der Haken?</span>
                                    </div>
                                    <div className="faqA" data-text="A"><span data-i18n="">Keine Haken! Sie melden sich einfach an, hinterlegen einen anfänglichen Handelsbetrag von 250</span><span
                                        data-init="visitor-currency-symbol">$</span> <span data-i18n="">und wir erledigen den Rest. Unsere Plattform ermöglicht den einfachsten Einstieg in den Handel mit Bitcoins. Mühelos und ohne Scherereien, sodass Sie sofort anfangen können zu verdienen.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="storiesSection">
                    <div className="container">
                        <h1 data-i18n="">Was andere davon denken</h1>
                        <h2 data-i18n="">Wir haben einige Leute gebeten, uns ihre Meinung zu unserer App zu geben.</h2>


                        <div className="stories-3-images-design">
                            <div className="row">
                                <div className="col-md-4"><img src={StoryImgEs1}
                                                               id="story-img-1-design-3-images"
                                                               alt="Person story image"/></div>
                                <div className="col-md-4"><img src={StoryImgEs2}
                                                               id="story-img-2-design-3-images"
                                                               alt="Person story image"/></div>
                                <div className="col-md-4"><img src={StoryImgEs3}
                                                               id="story-img-2-design-3-images"
                                                               alt="Person story image"/></div>
                            </div>
                        </div>


                        <div className="stories-4-images-design">
                            <div className="row">
                                <div className="col-md-6"><img src={StoryImgDe1}
                                                               id="story-img-1" className="storyImageFloatedRight"
                                                               alt="Person story image"/></div>
                                <div className="col-md-6"><img src={StoryImgDe2}
                                                               id="story-img-2" alt="Person story image"/></div>
                            </div>
                            <div className="row">
                                <div className="col-md-6"><img src={StoryImgDe3}
                                                               id="story-img-3" className="storyImageFloatedRight"
                                                               alt="Person story image"/></div>
                                <div className="col-md-6"><img src={StoryImgDe4}
                                                               id="story-img-4" alt="Person story image"/></div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="secondFormSection">
                    <div className="container">
                        <h1 data-i18n="">Fangen Sie heute noch an zu handeln</h1>
                        <h2 data-i18n="">chließen Sie die Registrierung Ihres Kontos ab und werden Sie ein erfolgreicher
                            Online-Händler</h2>

                        <div className="secondFormOuterContainer">
                            <div className="formInnerContainer">
                                <div className="formContainer">
                                    <div data-intgrtn-form-signup-3-steps="">
                                        <form method="post" id="formfull" action=""  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                            <div className="fieldset_group">
                                                <div style={{padding :'5px'}} > </div>
                                                <div className="fieldset"  style={{textAlign:'left'}}>
                                                    <span className="fieldset-icon full_name-icon"></span>
                                                    <input className="text required full_name sk-input "
                                                           id="full_name"
                                                           type="text"
                                                           onChange={this.handleChange("firstName")}
                                                           value={this.state.firstName}
                                                           placeholder="Vorname"
                                                           autoComplete="name"
                                                    />
                                                    {
                                                        this.state.firstNameErr ? <label
                                                            className="x-error-x">Erforderlich</label> : null
                                                    }
                                                </div>
                                            </div>

                                            <div className="fieldset_group">
                                                <div style={{padding :'5px'}} > </div>
                                                <div className="fieldset"  style={{textAlign:'left'}}>
                                                    <span className="fieldset-icon full_name-icon"></span>
                                                    <input className="text required full_name sk-input "
                                                           id="full_name"
                                                           type="text"
                                                           onChange={this.handleChange("lastName")}
                                                           value={this.state.lastName}
                                                           placeholder="Nachname"
                                                           autoComplete="name"
                                                    />
                                                    {
                                                        this.state.lastNameErr ? <label
                                                            className="x-error-x">Erforderlich</label> : null
                                                    }
                                                </div>
                                            </div>

                                            <div className="fieldset_group">
                                                <div style={{padding :'5px'}} > </div>
                                                <div className="fieldset" style={{textAlign:'left'}}>
                                                    <span className="fieldset-icon user_email-icon"></span>
                                                    <input type="text" name="user_email" id="user_email"
                                                           className="user_email required sk-input"
                                                           placeholder="E-Mail-Adresse" value=""
                                                           maxLength="255"
                                                           onChange={this.handleChange("email")}
                                                           value={this.state.email}
                                                    />
                                                    {
                                                        this.state.emailErr ? <label className="x-error-x">Ungültige
                                                            Adresse</label> : null
                                                    }
                                                    {
                                                        this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                    }
                                                </div>
                                            </div>

                                            <div className="fieldset_group phone-fieldset"  style={{textAlign:'left'}}>
                                                <div style={{padding :'5px'}} > </div>
                                                <div className="x-phone-replace" data-select2-id="9">
                                                    <div className="x-flex-row" data-select2-id="8">
                                                        <PhoneInput
                                                            style={{
                                                                height: '40px',
                                                                border: '1px solid #b4b3b3',
                                                                borderRadius: '5px',
                                                                padding: '8px',
                                                                width: '100%'
                                                            }}
                                                            placeholder="Enter phone number"
                                                            defaultCountry={this.props.country.country}
                                                            international
                                                            onChange={(e) => {this.setState({phoneValue : e})}}
                                                            value={this.state.phoneValue}

                                                        />
                                                        {
                                                            this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                Telefonnummer</label> : null
                                                        }

                                                    </div>
                                                </div>
                                            </div>
                                            <div className='x-forms'  style={{textAlign:'left'}}>

                                                <div className=" ">

                                                    <label htmlFor="x-form-label" className="checkbox">
                                                        <input type="checkbox"
                                                               name="checked"
                                                               id="cb_cond_1_0"
                                                               checked={this.state.checked}
                                                               onChange={this.handleInputChange}
                                                               className="field-error"/>&nbsp;
                                                        <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                    </label>
                                                    {
                                                        this.state.checkedErr ?
                                                            <label className="x-error-red">Verpflichtend</label> : null
                                                    }

                                                </div>
                                            </div>


                                            <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                 data-editable="" data-name="step1-send" data-ce-tag="button">
                                                <button type="button" id="lead-form-submit"
                                                        onClick={this.handleRegistration}
                                                        style={{
                                                            width: '100%',
                                                            fontSize: '1.25em',
                                                            fontWeight: '700',
                                                            boxShadow: '0 2px 0 #1978a5',
                                                            background:' #2ba6df',
                                                            textTransform: 'uppercase',
                                                            whiteSpace: 'normal',
                                                            color: '#ffffff',
                                                            padding: '6px 12px',
                                                            border: '1px solid transparent',
                                                            borderRadius: '4px'


                                                        }}
                                                        className="lead-form-submit">
                                                    Informationen erhalten
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                    <div className="disclaimer-wrapper hidden">
                                        <p className="form-disclaimers">*By submitting you confirm that you’ve read and
                                            accepted the <a data-intgrtn-link-agreements="3" href="" className=""
                                                            data-i18n="">privacy policy</a> and <a
                                                data-intgrtn-link-agreements="1" href="" className="" data-i18n="">terms
                                                of conditions</a>.</p>
                                        <p className="form-disclaimers">**By submitting this form, I agree to receive
                                            all marketing material by email, SMS and telephone.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

{/*

                <div className="intgrtn-modal-holder intgrtn-modal-exit-popup" style={this.state.popap ?{display:'block'} : {display :'none'}}>
                    <div className="intgrtn-modal">
                        <button
                            onClick={()=>this.setState({popap : false})}
                            className="intgrtn-modal-btn-close">×</button>
                        <div className="intgrtn-modal-header"></div>
                        <div className="intgrtn-modal-body">
                            <div className="intgrtn-exit-popup-iframe-holder">

                                <div className="popup-container">
                                    <div className="background">
                                        <div className="exit-popup-wrapper">
                                            <div className="heading-text">
                                                <span className="heading" data-i18n="">Halt! Geh nicht!</span>
                                            </div>
                                            <div className="heading-wrapper">
                                                <div className="first-paragraph" data-i18n="">Registrieren und aktivieren Sie jetzt Ihr
                                                    Konto.
                                                </div>
                                                <div className="second-paragraph" data-i18n="">Schließen Sie sich Tausenden von Menschen
                                                    an, die diese Gelegenheit nutzen!
                                                </div>
                                            </div>

                                            <div className="register-form-wrapper">
                                                <div className="form-signup-holder">
                                                    <div data-intgrtn-form-signup="" data-intgrtn-on-success-redirect-top="1">
                                                        <form method="post" id="formfull" action=""  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                            <div className="fieldset_group">
                                                                <div style={{padding :'5px'}} > </div>
                                                                <div className="fieldset"  style={{textAlign:'left'}}>
                                                                    <span className="fieldset-icon full_name-icon"></span>
                                                                    <input className="text required full_name sk-input "
                                                                           id="full_name"
                                                                           type="text"
                                                                           onChange={this.handleChange("firstName")}
                                                                           value={this.state.firstName}
                                                                           placeholder="Vorname"
                                                                           autoComplete="name"
                                                                    />
                                                                    {
                                                                        this.state.firstNameErr ? <label
                                                                            className="x-error-x">Erforderlich</label> : null
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="fieldset_group">
                                                                <div style={{padding :'5px'}} > </div>
                                                                <div className="fieldset"  style={{textAlign:'left'}}>
                                                                    <span className="fieldset-icon full_name-icon"></span>
                                                                    <input className="text required full_name sk-input "
                                                                           id="full_name"
                                                                           type="text"
                                                                           onChange={this.handleChange("lastName")}
                                                                           value={this.state.lastName}
                                                                           placeholder="Nachname"
                                                                           autoComplete="name"
                                                                    />
                                                                    {
                                                                        this.state.lastNameErr ? <label
                                                                            className="x-error-x">Erforderlich</label> : null
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="fieldset_group">
                                                                <div style={{padding :'5px'}} > </div>
                                                                <div className="fieldset" style={{textAlign:'left'}}>
                                                                    <span className="fieldset-icon user_email-icon"></span>
                                                                    <input type="text" name="user_email" id="user_email"
                                                                           className="user_email required sk-input"
                                                                           placeholder="E-Mail-Adresse" value=""
                                                                           maxLength="255"
                                                                           onChange={this.handleChange("email")}
                                                                           value={this.state.email}
                                                                    />
                                                                    {
                                                                        this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                            Adresse</label> : null
                                                                    }
                                                                    {
                                                                        this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="fieldset_group phone-fieldset"  style={{textAlign:'left'}}>
                                                                <div style={{padding :'5px'}} > </div>
                                                                <div className="x-phone-replace" data-select2-id="9">
                                                                    <div className="x-flex-row" data-select2-id="8">

                                                                        <div className='input'
                                                                             style={{width: '30%', float: 'left'}}>
                                                                            <ReactCustomFlagSelect
                                                                                attributesWrapper={{
                                                                                    id: "areaCodeWrapper",
                                                                                    tabIndex: "1"
                                                                                }} //Optional.[Object].Modify wrapper general attributes.
                                                                                attributesButton={{id: "areaCodeButton"}} //Optional.[Object].Modify button general attributes.
                                                                                attributesInput={{
                                                                                    id: "areaCode",
                                                                                    name: "areaCode"
                                                                                }} //Optional.[Object].Modify hidden input general attributes.
                                                                                value={currentItem.id} //Optional.[String].Default: "".
                                                                                disabled={false} //Optional.[Bool].Default: false.
                                                                                showSearch={true} ////Optional.[Bool].Default: false.
                                                                                showArrow={false} //Optional.[Bool].Default: true.
                                                                                animate={true} //Optional.[Bool].Default: false.
                                                                                optionList={FLAG_SELECTOR_OPTION_LIST} //Required.[Array of Object(s)].Default: [].
                                                                                // selectOptionListItemHtml={<div>us</div>} //Optional.[Html].Default: none. The custom select options item html that will display in dropdown list. Use it if you think the default html is ugly.
                                                                                // selectHtml={<div>us</div>} //Optional.[Html].Default: none. The custom html that will display when user choose. Use it if you think the default html is ugly.
                                                                                classNameWrapper="" //Optional.[String].Default: "".
                                                                                classNameContainer="" //Optional.[String].Default: "".
                                                                                classNameOptionListContainer="" //Optional.[String].Default: "".
                                                                                classNameOptionListItem="" //Optional.[String].Default: "".
                                                                                classNameDropdownIconOptionListItem={{color: 'red',}} //Optional.[String].Default: "".
                                                                                customStyleWrapper={{}} //Optional.[Object].Default: {}.
                                                                                customStyleContainer={{
                                                                                    height: '40px',
                                                                                    border: '1px solid #b4b3b3',
                                                                                    borderRadius: '5px',
                                                                                    paddingBottom: '5px',
                                                                                    width: '100%',
                                                                                    backgroundColor:'white'
                                                                                }} //Optional.[Object].Default: {}.
                                                                                customStyleSelect={{width: "80px"}} //Optional.[Object].Default: {}.
                                                                                customStyleOptionListItem={{}} //Optional.[Object].Default: {}.
                                                                                customStyleOptionListContainer={{
                                                                                    maxHeight: '300px',
                                                                                    overflow: 'auto',
                                                                                    width: '90px',
                                                                                }} //Optional.[Object].Default: {}.
                                                                                onChange={(areaCode) => {
                                                                                    this.setState({areaCode: areaCode}, () => {
                                                                                        this.handleChangeAreaCode(areaCode);
                                                                                    });
                                                                                }}
                                                                            />
                                                                        </div>
                                                                        <div className="fieldset_group" style={{
                                                                            width: '70%',
                                                                            float: 'left',
                                                                            paddingLeft: '10px'
                                                                        }}>
                                                                            <div className="fieldset">
                                                                    <span
                                                                        className="fieldset-icon full_name-icon"></span>
                                                                                error
                                                                                <input className="text required full_name sk-input "
                                                                                       id="full_name"

                                                                                       onChange={this.handleChange("phone")}
                                                                                       value={this.state.phone}
                                                                                       type="text" placeholder="8710211"
                                                                                       autoComplete="name"
                                                                                />
                                                                                {
                                                                                    this.state.phoneErr ?
                                                                                        <label className="x-error-x">Ungültige
                                                                                            Telefonnummer</label> : null
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                           <div className='x-forms'  style={{textAlign:'left'}}>

                                                                <div className=" ">

                                                                    <label htmlFor="x-form-label" className="checkbox">
                                                                        <input type="checkbox"
                                                                               name="checked"
                                                                               id="cb_cond_1_0"
                                                                               checked={this.state.checked}
                                                                               onChange={this.handleInputChange}
                                                                               className="field-error"/>&nbsp;
                                                                        <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                                    </label>
                                                                    {
                                                                        this.state.checkedErr ?
                                                                            <label className="x-error-red">Verpflichtend</label> : null
                                                                    }

                                                                </div>
                                                            </div>


                                                            <div className="intgrtn-btn-submit-holder" style={{textAlign:'center'}}>

                                                                <button
                                                                    style={{
                                                                        maxWidth: '285px',
                                                                        height: '52px',
                                                                        color: '#000000',
                                                                        backgroundColor: '#ffb400',
                                                                        fontSize: '24px',
                                                                        fontWeight: '700',
                                                                        borderRadius: '4px',
                                                                        border: '1px solid #b27e00',
                                                                        borderBottomWidth: '4px'
                                                                    }}
                                                                    onClick={this.handleRegistration}
                                                                    className="intgrtn-btn-submit"
                                                                        type="submit">REGISTRIEREN
                                                                </button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

*/}


                <Footer
                    logoStatus={false}
                />



            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(BitcoinProfit);

