import React, {Component} from 'react'
import {connect} from "react-redux";
import {Container, Row, Col} from "react-bootstrap";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'
import MyVideo from "../assets/video/video_1_de.mp4";


import Testimonial1  from '../assets/img/testimonial-1.jpg'
import Testimonial2  from '../assets/img/testimonial-2.jpg'
import Testimonial3  from '../assets/img/testimonial-3.jpg'
import Testimonial4  from '../assets/img/testimonial-4.jpg'
import Testimonial5  from '../assets/img/testimonial-5.jpg'
import Testimonial6  from '../assets/img/testimonial-6.jpg'
import Testimonial7  from '../assets/img/testimonial-7.jpg'
import Testimonial8  from '../assets/img/testimonial-8.jpg'
import Testimonial9  from '../assets/img/testimonial-9.jpg'
import Man  from '../assets/img/new2/media/man.jpg'
import Sign  from '../assets/img/new2/media/sign.png'
import Founder  from '../assets/img/founder.png'
import Signature  from '../assets/img/signature.png'

import {Footer} from "../components/layout/footer";
import {FLAG_SELECTOR_OPTION_LIST} from "../helper/country";


import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'

import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";
import {Player} from "video-react";
import ReactCustomFlagSelect from "react-custom-flag-select";

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();
class BitcoinsWealth extends Component {

    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            popap : false,
            theposition: '',
            scrollPosition: false,
            fixPosition: false,
            fixPositionHeight: 0,
            phoneValue: '',
            messagesEndRef: React.createRef(),

        };
        this.submit = this.submit.bind(this);
    }


    _onMouseMove(e) {
        if (e.screenY <= 80) {
            this.setState({popap: true});
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop


        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled >= 0.9) {
            const fixTop = height - 550;
            this.setState({
                fixPosition: true,
                // fixPositionHeight: fixTop,
            })
        }else {
            this.setState({
                fixPosition: false,
            })
        }



        if (scrolled > 0) {
            this.setState({
                scrollPosition: true
            })
        } else {
            this.setState({
                scrollPosition: false
            })
        }

    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop / 3 - 60);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {

        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }

        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];

        return (
            <Container style={{padding:'0px'}} fluid >

                <header id="headerBtn">
                    <div className="header-content">
                        <a className="header-logo">Bitcoins Wealth</a>
                    </div>
                </header>
                <Container style={{padding:'0px'}} fluid >

                    <div className="section section-intro scroll-trigger">
                        <div className="section-container">
                            <div className="intro-title">
                                Reite auf der Erfolgswelle des
                                <div>
                                    Bitcoins
                                </div>und du kannst bis zu <span>€13.000 in den nächsten 24h verdienen</span>
                            </div>

                            <Row>
                                <Col md={8}>
                                    <Player
                                        autoPlay={true}
                                        muted={true}
                                        // poster="/assets/poster.png"
                                        src={MyVideo}
                                    />
                                </Col>

                                <Col md={4}>
                                    <div className="top-form-wrapper">
                                        <div className="form-title">
                                            Lass mich dir jetzt
                                            <div>
                                                <div>
                                                    LIVE zeigen
                                                </div>
                                                wie man sich Cash verdient!
                                            </div>
                                        </div>
                                        <div className="formInnerContainer--">
                                            <div className="formContai--ner">
                                                <div data-intgrtn-form-signup="">
                                                    <form method="post" id="formfull" action=""  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                        <div className="fieldset_group">
                                                            <div style={{padding :'5px'}} > </div>
                                                            <div className="fieldset"  style={{textAlign:'left'}}>
                                                                <span className="fieldset-icon full_name-icon"></span>
                                                                <input className="text required full_name sk-input "
                                                                       id="full_name"
                                                                       type="text"
                                                                       onChange={this.handleChange("firstName")}
                                                                       value={this.state.firstName}
                                                                       placeholder="Vorname"
                                                                       autoComplete="name"
                                                                />
                                                                {
                                                                    this.state.firstNameErr ? <label
                                                                        className="x-error-x">Erforderlich</label> : null
                                                                }
                                                            </div>
                                                        </div>

                                                        <div className="fieldset_group">
                                                            <div style={{padding :'5px'}} > </div>
                                                            <div className="fieldset"  style={{textAlign:'left'}}>
                                                                <span className="fieldset-icon full_name-icon"></span>
                                                                <input className="text required full_name sk-input "
                                                                       id="full_name"
                                                                       type="text"
                                                                       onChange={this.handleChange("lastName")}
                                                                       value={this.state.lastName}
                                                                       placeholder="Nachname"
                                                                       autoComplete="name"
                                                                />
                                                                {
                                                                    this.state.lastNameErr ? <label
                                                                        className="x-error-x">Erforderlich</label> : null
                                                                }
                                                            </div>
                                                        </div>

                                                        <div className="fieldset_group">
                                                            <div style={{padding :'5px'}} > </div>
                                                            <div className="fieldset" style={{textAlign:'left'}}>
                                                                <span className="fieldset-icon user_email-icon"></span>
                                                                <input type="text" name="user_email" id="user_email"
                                                                       className="user_email required sk-input"
                                                                       placeholder="E-Mail-Adresse" value=""
                                                                       maxLength="255"
                                                                       onChange={this.handleChange("email")}
                                                                       value={this.state.email}
                                                                />
                                                                {
                                                                    this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                        Adresse</label> : null
                                                                }
                                                                {
                                                                    this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                                }
                                                            </div>
                                                        </div>

                                                        <div className="fieldset_group phone-fieldset"  style={{textAlign:'left'}}>
                                                            <div style={{padding :'5px'}} > </div>
                                                            <div className="x-phone-replace" data-select2-id="9">
                                                                <div className="x-flex-row" data-select2-id="8">
                                                                    <PhoneInput
                                                                        style={{
                                                                            height: '40px',
                                                                            border: '1px solid #b4b3b3',
                                                                            borderRadius: '5px',
                                                                            padding: '8px',
                                                                            width: '100%'
                                                                        }}
                                                                        placeholder="Enter phone number"
                                                                        defaultCountry={this.props.country.country}
                                                                        international
                                                                        onChange={(e) => {this.setState({phoneValue : e})}}
                                                                        value={this.state.phoneValue}

                                                                    />
                                                                    {
                                                                        this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                            Telefonnummer</label> : null
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='x-forms'  style={{textAlign:'left'}}>
                                                            <div className=" ">

                                                                <label htmlFor="x-form-label" className="checkbox">
                                                                    <input type="checkbox"
                                                                           name="checked"
                                                                           id="cb_cond_1_0"
                                                                           checked={this.state.checked}
                                                                           onChange={this.handleInputChange}
                                                                           className="field-error"/>&nbsp;
                                                                    <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                                </label>
                                                                {
                                                                    this.state.checkedErr ?
                                                                        <label className="x-error-red">Verpflichtend</label> : null
                                                                }

                                                            </div>
                                                        </div>


                                                        <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                             data-editable="" data-name="step1-send" data-ce-tag="button">
                                                            <button type="button" id="lead-form-submit"
                                                                    onClick={this.handleRegistration}
                                                                    style={{
                                                                        width: '100%',
                                                                        fontSize: '1.25em',
                                                                        fontWeight: '700',
                                                                        boxShadow: '0 2px 0 #1978a5',
                                                                        background:' #2ba6df',
                                                                        textTransform: 'uppercase',
                                                                        whiteSpace: 'normal',
                                                                        color: '#ffffff',
                                                                        padding: '6px 12px',
                                                                        border: '1px solid transparent',
                                                                        borderRadius: '4px'


                                                                    }}
                                                                    className="lead-form-submit">
                                                                Informationen erhalten
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                                <div className="disclaimer-wrapper hidden">
                                                    <p className="form-disclaimers">*By submitting you confirm that you’ve read
                                                        and accepted the <a data-intgrtn-link-agreements="3"
                                                                            href="https://tradedigitalapp.com/bitcoinprofit/lp.php"
                                                                            className="" data-i18n="">privacy policy</a> and <a
                                                            data-intgrtn-link-agreements="1"
                                                            href="https://tradedigitalapp.com/bitcoinprofit/lp.php" className=""
                                                            data-i18n="">terms of conditions</a>.</p>
                                                    <p className="form-disclaimers">**By submitting this form, I agree to
                                                        receive all marketing material by email, SMS and telephone.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>


                        </div>
                    </div>


                </Container>

                <Container>
                    <Row>
                        <Col className='section section-tbc-description'>
                            <div className="section-container">
                                <div className="section-content">
                                    <div className="section-title">
                                        Trete dem <span>Bitcoins Wealth</span> bei
                                    </div>
                                    <div className="section-description">
                                        <p><b><i>Bitcoins Wealth</i></b> ist eine exklusive Gruppe für Leute die das
                                            wahnsinnige
                                            Finanzpotential von Bitcoin erkannten und damit ganz leise super reich
                                            geworden sind.
                                        </p>
                                        <p><b><i>Mitglieder des Bitcoins Wealth</i></b> genießen das Reisen um die
                                            ganze Welt
                                            während sie mit nur ein paar Minuten „Arbeit“ pro Tag von ihrem Laptop
                                            aus Geld
                                            verdienen.</p>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>

                <Container style={{padding:'0px'}} fluid >
                    <section className="section section-vacations">
                        <div className="section-container">
                            <div className="section-content">
                                <div className="section-title">
                                    <div>
                                        Hier sind ein paar unserer bisherigen Ausflüge:
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </Container>

                <Container style={{padding:'0px'}}  >
                    <section className="section section-testimonials">
                        <div className="section-container">
                            <div className="section-content">
                                <div className="section-title">
                                    Hör von unseren Mitgliedern die sich auf unsere Software verlassen um ihre
                                    Luxusleben zu
                                    finanzieren…
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <ul className="testimonials-list" style={{listStyle:'none'}}>
                                            <li>
                                                <img src={Testimonial1} />
                                                    <div className="name">
                                                        Laura Eckstein
                                                    </div>
                                                    <div className="description">
                                                        „Ich bin erst seit 47 Tagen Mitglied des Bitcoins Wealth. Aber
                                                        mein Leben
                                                        hat sich schon total verändert! Ich habe nicht nur meine ersten
                                                        €100K
                                                        verdient, sondern habe währenddessen auch einige unglaubliche
                                                        Leute
                                                        kennengelernt. Danke, Sven!“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial2} />
                                                    <div className="name">
                                                        Johann Schreiber
                                                    </div>
                                                    <div className="description">
                                                        „Als ich dem Bitcoins Wealth vor 2 Monaten beigetreten bin,
                                                        hätte ich mir
                                                        niemals vorstellen können was in den ersten paar Tagen nach der
                                                        Freischaltung der Software passieren würde. Ich konnte endlich
                                                        meine
                                                        Schulden von €131.382 abbezahlen. Es gibt kein besseres Gefühl
                                                        als
                                                        schuldenfrei zu sein. Jetzt bin ich gerade dabei mir mein
                                                        Traumhaus zu
                                                        kaufen. Ich kann immer noch nicht glauben, dass das hier
                                                        wirklich gerade
                                                        passiert… ich werde Sven für immer dankbar sein.“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial3} />
                                                    <div className="name">
                                                        Michael Steiger
                                                    </div>
                                                    <div className="description">
                                                        „Die Ergebnisse der Software sagen schon alles… genau wie
                                                        versprochen,
                                                        machte ich jeden Tag über €13.000. Muss ich da noch irgendetwas
                                                        sagen?“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial4} />
                                                    <div className="name">
                                                        Paolo Iovinelli
                                                    </div>
                                                    <div className="description">
                                                        „Gestern KÜNDIGTE ich meinen Job… und heute bin ich auf einer
                                                        Pool-Party in
                                                        Las Vegas! Das Leben ist VERRÜCKT! Und das alles dank dem
                                                        Bitcoins Wealth.
                                                        DANKE SVEN!“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial5} />
                                                    <div className="name">
                                                        Alex Fischer
                                                    </div>
                                                    <div className="description">
                                                        „Ist das wirklich real? Ich bin erst vor 2 Tagen beigetreten und
                                                        auf meinem
                                                        Konto sitzen schon wahnsinnige €27.484,98!!!“
                                                    </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-6">
                                        <ul className="testimonials-list" style={{listStyle:'none'}}>
                                            <li>
                                                <img src={Testimonial6} />
                                                    <div className="name">
                                                        Paul Berg
                                                    </div>
                                                    <div className="description">
                                                        „Jeden Morgen nachdem ich aufstehe und ich mir meinen Kontostand
                                                        ansehe sage
                                                        ich meiner Frau mich kurz zu kneifen. Ich habe vorher noch nie
                                                        so eine
                                                        RIESIGE Zahl auf MEINEM Bankkonto gesehen. Und die Zahl wächst
                                                        und wächst…
                                                        Darauf warte ich schon mein ganzes Leben. Jetzt weiß ich endlich
                                                        wie es ist
                                                        mein eigener Chef zu sein und jede Woche Tausende von Euro zu
                                                        verdienen. Ich
                                                        werde niemals zurückblicken!“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial7} />
                                                    <div className="name">
                                                        Daniel Rosenzweig
                                                    </div>
                                                    <div className="description">
                                                        „Ob ihr es glaubt oder nicht, ich war früher mal Investor an der
                                                        Wall
                                                        Street. Und so etwas habe ich in meinen 10 Jahren bei der Firma
                                                        noch nie
                                                        gesehen. Meine Kollegen dachten alle ich wäre verrückt als ich
                                                        bei der Firma
                                                        kündigte um vollzeitig mit der Bitcoins Wealth Software zu
                                                        investieren.
                                                        €385.595 in Profiten später und alle meine Kollegen FLEHEN mich
                                                        danach an
                                                        sie reinzulassen.“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial8} />
                                                    <div className="name">
                                                        Susanne Boltyanski
                                                    </div>
                                                    <div className="description">
                                                        „Nun weiß ich endlich wie es ist seinen Traum zu leben. Ich
                                                        fühle mich nicht
                                                        länger so als würden alle Spaß und Ruhm haben und ich schaue nur
                                                        zu. Der
                                                        Bitcoins Wealth hat es mir erlaubt frühzeitig in Rente zu gehen
                                                        und das
                                                        Leben der 1% zu genießen.“
                                                    </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial9} />
                                                    <div className="name">
                                                        Bruno Bauer
                                                    </div>
                                                    <div className="description">
                                                        „Vor zwei Wochen erhielt ich die Kündigung. Ohne Alternativen
                                                        dachte ich
                                                        mein Leben wäre zu ende. Jetzt mache ich über €13.261,42 jeden
                                                        einzelnen
                                                        Tag. Und zum aller ersten Mal in 2 Monaten stehe ich nicht im
                                                        Minus. Danke,
                                                        SVEN!“
                                                    </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section className="section section-man">
                        <div className="section-container">
                            <div className="section-content">
                                <img src={Man} />
                                <div className="content-wrapper">
                                    <div className="section-title">
                                        <div data-translate="meet-the-genius-title">Und das ist Sven Hegel</div>
                                        <span
                                            data-translate="meet-the-genius-sub">Das Genie hinter dem Bitcoin Code</span>
                                    </div>
                                    <div className="section-description">
                                        <p data-translate="meet-para-1">Hi - ich bin ein ehemaliger
                                            Software-Entwickler bei einem großen Unternehmen, welches ich lieber
                                            nicht nennen würde.</p>
                                        <p data-translate="meet-para-2">Ich habe eine Bitcoin-Trading-Software
                                            entwickelt die über €18,484,931.77 an Profiten in den letzten 6 Monaten
                                            eingebracht hat.</p>
                                        <p data-translate="meet-para-3">Die Software macht Leute schneller zu
                                            Millionären als die ersten Investoren bei Uber, Facebook oder
                                            AirBnB.</p>
                                        <p data-translate="meet-para-4">Wenn du selbst eine Million mit Bitcoin
                                            machen willst, guck dir das Video hier oben an um herauszufinden
                                            wie.</p>
                                        <img src={Sign} align="right" className="sign" alt="" />
                                        <p data-translate="meet-signature">Dein Freund, <br />Sven Hegel</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section className="section  ">
                        <div className="section-container">
                            <div className="top-form-wrapper">
                                <div className="form-title">
                                    Lass mich dir jetzt
                                    <div>
                                        <div>
                                            LIVE zeigen
                                        </div>
                                        wie man sich Cash verdient!
                                    </div>
                                </div>
                                <div className="formInnerContainer--">
                                    <div className="formContai--ner">
                                        <div data-intgrtn-form-signup="">
                                            <form method="post" id="formfull" action=""  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset"  style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="Vorname"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset"  style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Nachname"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset" style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="E-Mail-Adresse" value=""
                                                               maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset"  style={{textAlign:'left'}}>
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">
                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='x-forms'  style={{textAlign:'left'}}>
                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>


                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="button" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{
                                                                width: '100%',
                                                                fontSize: '1.25em',
                                                                fontWeight: '700',
                                                                boxShadow: '0 2px 0 #1978a5',
                                                                background:' #2ba6df',
                                                                textTransform: 'uppercase',
                                                                whiteSpace: 'normal',
                                                                color: '#ffffff',
                                                                padding: '6px 12px',
                                                                border: '1px solid transparent',
                                                                borderRadius: '4px'


                                                            }}
                                                            className="lead-form-submit">
                                                        Informationen erhalten
                                                    </button>
                                                </div>
                                            </form>

                                        </div>

                                        <div className="disclaimer-wrapper hidden">
                                            <p className="form-disclaimers">*By submitting you confirm that you’ve read
                                                and accepted the <a data-intgrtn-link-agreements="3"
                                                                    href="https://tradedigitalapp.com/bitcoinprofit/lp.php"
                                                                    className="" data-i18n="">privacy policy</a> and <a
                                                    data-intgrtn-link-agreements="1"
                                                    href="https://tradedigitalapp.com/bitcoinprofit/lp.php" className=""
                                                    data-i18n="">terms of conditions</a>.</p>
                                            <p className="form-disclaimers">**By submitting this form, I agree to
                                                receive all marketing material by email, SMS and telephone.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </Container>



                <Footer
                    logoStatus={false}
                />
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(BitcoinsWealth);

