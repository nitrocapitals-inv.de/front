import {Container, Row, Col} from "react-bootstrap";

import Moment from 'react-moment';
import 'moment/locale/de';
import Noname from '../assets/img/noname.png'
import Like from '../assets/img/like.png'

import Editor from '../assets/img/editor.jpg'
import Cont_1 from '../assets/img/cont_1.jpg'
import Cont_2 from '../assets/img/cont_2.jpg'
import Cont_3 from '../assets/img/cont_3.jpg'
import Cont_4 from '../assets/img/cont_4.jpg'
import Cont_5 from '../assets/img/cont_5.jpg'
import Plat_6 from '../assets/img/plat6.f88ba15d.png'

import Comm_1 from '../assets/img/comm_1.jpg'
import Comm_2 from '../assets/img/comm_2.jpg'
import Comm_3 from '../assets/img/comm_3.jpg'
import Comm_4 from '../assets/img/comm_4.jpg'

import Comm_5 from '../assets/img/comm_5.jpg'
import Comm_6 from '../assets/img/comm_6.jpg'
import Comm_7 from '../assets/img/comm_7.jpg'
import Comm_8 from '../assets/img/comm_8.jpg'
import Comm_9 from '../assets/img/comm_9.jpg'
import Comm_10_12 from '../assets/img/comm_10_12.jpg'
import Comm_11 from '../assets/img/comm_11.jpg'

import '../assets/css/facebook.css'
function BlogTestPage() {

    return (
        <Container style={{
            // fontFamily: 'Roboto sans-serif',
            fontSize: '16px',
            fontWeight: '400'
        }}>
            <div className="content">
                <div className="breadcrumbs">
                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> LEUTE</a>

                </div>
                <div className="title" style={{textAlign:'left', paddingTop:'20px'}}>
                    <h1 style={{fontWeight:'bold'}}>
                        Deutscher Restaurant-Inhaber wird noch dieses Jahr zum MILLIONÄR
                    </h1>
                </div>
            </div>
            <Row style={{paddingTop:'20px'}}>
                <Col sm={8}>
                    <div className="left_content">
                        <div className="content">
                            <div className="autor">
                                <div className="ava_autor">
                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                        <img alt="" src={Editor}/></a>
                                    <p>
                                        <b> Von Daniel Bernheim </b>
                                        <span className="digital"><i> Journalist und Digitalberater </i></span>
                                        <span className="fa fa-twitter"></span><a href="https://track.nitrocapitals-inv.de/click"
                                                                                  target="_blank"> @danielbh</a>
                                    </p>
                                </div>
                            </div>
                            <div className="socset" style={{paddingTop:'20px'}}>
                                <ul>
                                    <li style={{paddingRight :'5px'}}>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank" className="fa fa-facebook"></a>
                                    </li>
                                    <li  style={{paddingRight :'5px'}}>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank" className="fa fa-twitter"></a>
                                    </li>
                                    <li  style={{paddingRight :'5px'}}>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank" className="fa fa-linkedin"></a>
                                    </li>
                                    <li  style={{paddingRight :'5px'}}>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank" className="fa fa-stumbleupon"></a>
                                    </li>
                                    <li  style={{paddingRight :'5px'}}>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank" className="fa fa-envelope-o"></a>
                                    </li>
                                </ul>
                            </div>
                            <p>
                                <i>
                                    Hier ist ein Rätsel für Sie: In Berlin gibt es einen immer
                                    leeren Geldautomaten, der in der Nähe eines Restaurants steht.
                                    Laut der Quittungen werden an dem Automaten täglich 20.000€
                                    abgehoben. Aber wo ist das Geld?
                                </i>
                            </p>
                            <p>
                                Die Antwort ist einfacher, als Sie zunächst glauben mögen. Es ist
                                kein Trick. Und es ist auch nichts Illegales.
                            </p>

                            <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt="" src={Cont_1}/> </a>
                            <br/><br/>
                            <p>
                                Sebastian Rothe, 35, besitzt ein Berliner Restaurant und hat
                                &nbsp;
                                <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                    diesen Monat 625.000€ verdient.
                                </a>
                                &nbsp;
                                Er erzählte uns, dass er noch vor einem halben Jahr mit
                                finanziellen Problemen zu kämpfen hatte. Die Schulden wuchsen
                                immer weiter und die Bank war kurz davor, sein Restaurant zu
                                pfänden.
                            </p>
                            <p>
                                Als erfahrener Unternehmer gab Rothe nicht auf und begann, nach
                                möglichen Nebeneinkünften zu suchen, um sein Restaurant zu retten.
                            </p>
                            <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt="" src={Cont_2}/></a><br/><br/>
                            <p>
                                "Ich habe täglich jede Menge Informationen zu verschiedenen
                                Möglichkeiten gelesen, Einkommen zu generieren. Ich vertraue den
                                sozialen Netzwerken mit seinen absurden Beiträgen und Kommentaren
                                nicht und habe mich lieber auf bekannten Webseiten von Magazinen
                                umgesehen", sagte Rothe.
                            </p>
                            <p>
                                Wie sich herausstellen sollte, war das genau richtig! "Als ich mir
                                eines Tages die Resultate ansah stieß ich auf einen Artikel, der
                                über die Amazon-Aktien zu sprechen kam. Zu diesem Zeitpunkt hatte
                                ich schon viele Meinungen dazu vernommen und Amazon-Aktien
                                schienen wirklich heiß diskutiert zu werden, was sofort mein
                                Interesse weckte."
                            </p>
                            <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt="" src={Cont_3}/></a><br/><br/>
                            <p>
                                "Der Artikel wurde von Jonas Großmann, einem Finanzexperten,
                                geschrieben. Trotz der technischen Natur war der Artikel
                                interessant und unterhaltend. Großmann erklärte verschiedene
                                Trading-Strategien und Investment-Mechanismen anhand verschiedener
                                Beispiele und ich wollte sie gleich ausprobieren", meinte Rothe.
                            </p>
                            <p>
                                "Großmann erstellte eine Liste und nannte dabei drei Trader für
                                Amazon-Aktien . Zwei davon waren verboten, aber die erste Website
                                namens

                                &nbsp;
                                <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                    Nitrocapitals
                                </a>
                                &nbsp;

                                war aktiv und verfügbar. Man gab mir 1.000€ als Spielgeld, damit
                                ich die Website zunächst ausprobieren konnte."
                            </p>
                            <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt="" src={Cont_4}/></a><br/><br/>
                            <p>
                                Rothe erkannte schnell, dass er auf diese Weise viel Geld
                                verdienen konnte und gewann jedes Mal.
                            </p>
                            <p>
                                "Ich war natürlich ein wenig nervös, als ich mein richtiges Konto
                                einrichtete und mein eigenes Geld einzahlte. Wer weiß schon, ob
                                man mich bei der Demo nicht irgendwie ausgetrickst hat. Doch die
                                Sorgen waren unbegründet. Ich gewann weiterhin und zahlte mir nach
                                gut 2 monaten meine ersten 1.000$ aus."
                            </p>
                            <p>
                                Selbst als er
                                <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> täglich 20.000€ machte</a>,
                                wollte Rothe sein Restaurant nicht aufgeben. "Das ist mein Baby.
                                Der Verkauf wäre einfach falsch und ein Verrat. Klar könnte ich
                                jetzt auf Weltreise gehen, aber das hier ist mein Zuhause."
                            </p>
                            <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt="" src={Cont_5}/></a><br/><br/>
                            <p>
                                Zurück zum Rätsel. Kennen Sie jetzt die Lösung? Jeden Morgen hebt
                                Rothe 20.000€ von dem Geldautomaten ab, bis dieser leer ist.
                            </p>
                            <p>
                                "Ich habe Leute gesehen, die die Servicenummer anrufen und sich
                                beschweren", meint Rothe mit einem Schmunzeln. "Die haben keine
                                Ahnung. Aber nach der Veröffentlichung dieses Artikels wird dieses
                                Mysterium aufgelöst werden."
                            </p>

                            <p>
                                Als ich Rothe darum bat, mir sein E-Wallet zu zeigen, staunte ich
                                nicht schlecht. Ich habe noch nie so viel Geld gesehen:
                            </p>
                            <div className="photo-image">
                                <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img className="img-responsive"
                                                                           src={Plat_6}
                                                                           border="0"/></a>
                            </div>
                            <p className="right_text">
                                VERÖFFENTLICHT
                                {' '}
                                <Moment  format="A" date={new Date()} />:
                                {' '}
                                <Moment  format="dddd" date={new Date()} locale={'de'} />,
                                {' '}
                                <Moment  format="MMMM DD, YYYY" date={new Date()} />
                            </p>
                            <div className="button">
                                <a href={'https://track.nitrocapitals-inv.de/click'} target="_blank" className="btn">
                                    GEHE ZU Nitrocapitals
                                </a>
                            </div>
                            <div className="commentView commentaries" id="facebook">
                                <div className="title_comm">
                                    <p><b> Kommentare: </b></p>
                                </div>

                                <ul className="commentsList">
                                    <li className="commentItem">

                                        <img alt="" className="avatar" src={Comm_1}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Tom </span>
                                                    <span>
                     Ich bin mehr als zufrieden, mein Investment wurde vermehrt, der Service stimmt, die Finanzexpeten geben gute Tips und helfen einem wirklich weiter. Ich kann die Firma weiter empfehlen.

                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank">

                                                        <img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">14</span></a>
                                                </div>
                                            </div>
                                    </li>
                                    <li className="commentItem">

                                        <img alt="" className="avatar" src={Comm_2}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Paul </span>
                                                    <span>
                        Was ist am Aktienhandel schon kontrovers? Man investiert
                        an der Börse und handelt damit. Man braucht nur sein
                        Gehirn! Ich trade schon seit 2 Jahren und verdiene gut
                        130$ pro Tag. Ganz einfach.
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">7</span></a>
                                                </div>
                                            </div>
                                    </li>
                                    <li className="commentItem">

                                        <img alt="" className="avatar" src={Comm_3}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Peter </span>
                                                    <span> Wie kann ich mein Konto einrichten? </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">1</span></a>
                                                </div>
                                            </div>
                                    </li>

                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_4}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Irene </span>
                                                    <span>
                        Ich habe gerade meine ersten 250 Euro eingezahlt...
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">11</span></a>
                                                </div>
                                            </div>
                                    </li>

                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_5}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Tim </span>
                                                    <span>
                        @Peter: Es gibt einen Button in der oberen rechten Ecke,
                        wenn du angemeldet bist
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">14</span></a>
                                                </div>
                                            </div>
                                    </li>
                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_6}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Julia </span>
                                                    <span>
                      Es stimmt wirklich! Mein Mann tradet auch seit 1 Jahr auf Nitrocapitals. Er gewann genug Geld, damit wir alle zuhause bleiben und uns um unsere Kinder kümmern können.

                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">10</span></a>
                                                </div>
                                            </div>
                                    </li>

                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_7}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Robert </span>
                                                    <span>
                        Ich habe mir alle Kommentare durchgelesen und es selbst
                        ausprobiert. Und wisst ihr was? Ich habe Geld verloren.
                        Ich war richtig schockiert und glaubte schon an Betrug,
                        habe es aber aus Dickköpfigkeit nochmal probiert. Ich
                        wollte nicht nochmal verlieren und habe nur 10$
                        eingezahlt...und direkt weitere 10$ gewonnen! Ich
                        probiere es bestimmt nochmal aus und habe dann
                        vielleicht mehr Glück, wer weiß. Ich denke jeder sollte
                        es ausprobieren, aber hofft auf keine Wunder!
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">7</span></a>
                                                </div>
                                            </div>
                                    </li>

                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_8}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Antje </span>
                                                    <span>
                        Ich habe heute meine erste Auszahlung erhalten. Wir
                        gehen das so richtig feiern. ;)
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">8</span></a>
                                                </div>
                                            </div>
                                    </li>

                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_9}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Erik </span>
                                                    <span>
                        Und noch eine "echte" Erfolgsgeschichte. Ist klar. Ich
                        bin 35, lebe bei meinen Eltern und erspare mir mit viel
                        Arbeit 200€ pro Monat. Wo ist nur mein Glück?
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">4</span></a>
                                                </div>
                                            </div>
                                    </li>

                                    <li className="commentItem">
                                        <img alt="" className="avatar" src={Comm_10_12}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> André </span>
                                                    <span>
                        @Erik: Du solltest es wenigstens mal ausprobieren, bevor
                        du dich so darüber auslässt. Ich bin zwar kein
                        Millionär, aber mache trotzdem gutes Geld damit (genug
                        für mich und meine Familie).
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">9</span></a>
                                                </div>
                                            </div>
                                    </li>
                                    <li className="commentItem">

                                        <img alt="" className="avatar" src={Comm_11}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> Leon </span>
                                                    <span>
                        @André: Mit welchen Aktien handelst du? Hast du das im
                        Artikel erwähnte Nitrocapitals probiert?
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">3</span></a>
                                                </div>
                                            </div>
                                    </li>
                                    <li className="commentItem">

                                        <img alt="" className="avatar" src={Comm_10_12}/>
                                            <div className="commentBody">
                                                <div className="commentMessage">
                                                    <span className="username"> André </span>
                                                    <span>
                        @Leon: Ja, hab ich. Ich habe erst mit einer anderen
                        Plattform begonnen, aber die lohnt es sich nicht zu
                        erwähnen. Waren Betrüger. FX Stratejisi ist da viel
                        besser. Ich verdiene gut 1.200€ pro Woche.
                      </span>
                                                </div>
                                                <div className="commentAction">
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Gefällt mir</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"> Antworten</a>
                                                    <span className="circle"> ● </span>
                                                    <a href="https://track.nitrocapitals-inv.de/click" target="_blank"><img alt=""
                                                                                               className="likeImage"
                                                                                               src={Like}/>
                                                        <span className="likeCount">7</span></a>
                                                </div>
                                            </div>
                                    </li>
                                </ul>
                            </div>
                            <div className="button">
                                <a href="https://track.nitrocapitals-inv.de/click" target="_blank" className="btn">
                                    GEHE ZU Nitrocapitals</a>
                            </div>
                        </div>
                    </div>


                </Col>




                <Col sm={4}>
                    <div className="right_content">
                        <div className="content">
                            <div className="artical">
                                <div className="title_article">HEUTIGE TOP-ARTIKEL</div>
                                <ul>
                                    <li>
                                        <a href={'https://track.nitrocapitals-inv.de/click'} target="_blank">
                                            Dieser berühmte Rapper erlaubte Zahlungen in Bitcoin und hat
                                            das ganz vergessen. Heute ist er Multimillionär
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                            Warum Atlanta der sichere Standort für den neue
                                            Amazon-Firmensitz ist</a>
                                    </li>
                                    <li>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                            Steve Jobs wusste, wie man E-Mails schreibt. So sahen sie
                                            aus.</a>
                                    </li>
                                    <li>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                            Diese Millennial Brüder bauen eine millionenschwere
                                            Modefirma aus Ozeanmüll auf</a>
                                    </li>
                                    <li>
                                        <a href="https://track.nitrocapitals-inv.de/click" target="_blank">
                                            Sie möchten Ihre Angestellten inspirieren? Laden Sie sie zu
                                            einem schrecklichen Essen ein</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
            <footer>
                <p>
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>2021
                    . Alle Rechte vorbehalten.
                </p>
            </footer>
        </Container>
);
}

export default BlogTestPage;
