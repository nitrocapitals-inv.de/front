import BG from '../assets/img/bg.png'
import {Container, Row, Col} from "react-bootstrap";
import React from "react";
import {Footer} from "../components/layout/footer";


function ThanksPage() {

    return (
        <>
            <Container fluid style={{padding: '0px'}}>
                <section
                    className="elementor-29">
                    <div className="elementor-widget-container">
                        <h1 className="elementor-heading-title">Contact</h1>
                    </div>
                </section>

            </Container>
            <Container>
                <Row>
                    <Col xs={12} sm={12} lg={6} md={6}>
                        <div  style={{paddingTop:'110px'}}>
                             <span className="elementor-icon-box-title">
                                            <span className='contact-page-info'>KUNDENDIENST:</span>
                                        </span>
                            <p className="contact-page-info-description">SUPPORT@NITROCAPITALS.COM</p>
                        </div>

                        <div style={{}}>
                             <span className="elementor-icon-box-title">
                                            <span className='contact-page-info'>ADRESSE:</span>
                                        </span>
                            <p className="contact-page-info-description">YONGE STREET, TORONTO, M5B</p>
                        </div>
                        <div style={{}}>
                             <span className="elementor-icon-box-title">
                                            <span className='contact-page-info'>TELEFON:</span>
                                        </span>
                            <p className="contact-page-info-description">14382303161 - KANADAB</p>
                        </div>
                        <div style={{}}>
                             <span className="elementor-icon-box-title">
                                            <span className='contact-page-info'>TELEFON:</span>
                                        </span>
                            <p className="contact-page-info-description">442045489838 - VEREINIGTES KÖNIGREICH </p>
                        </div>
                        <div style={{}}>
                             <span className="elementor-icon-box-title">
                                            <span className='contact-page-info'>TELEFON:</span>
                                        </span>
                            <p className="contact-page-info-description">34930301568 - SPANIEN </p>
                        </div>
                        <div style={{}}>
                             <span className="elementor-icon-box-title">
                                            <span className='contact-page-info'>TELEFON:</span>
                                        </span>
                            <p className="contact-page-info-description">4961389994288 - DEUTSCHLAND </p>
                        </div>
                    </Col>

                    <Col lg={6} md={6} className='contact-form-el'>
                        <div className='contact-form ' >
                            <h2 className='contact-form-elementor-29'>Senden Sie uns Ihr Anliegen und wir melden uns bei Ihnen:</h2>
                            <form>
                               <div className='input-div-form'>
                                   <input size="1" type="text" name="fullName"
                                          className="elementor-field "
                                          placeholder="Vollständiger Name:*" />
                               </div>

                                <div className='input-div-form'>
                                    <input   type="text" name="fullName"
                                           className="elementor-field "
                                           placeholder="Email:*" />
                                </div>

                                <div className='input-div-form'>
                                    <input size="1" type="text" name="fullName"
                                           className="elementor-field "
                                           placeholder="Telefonnummer:*" />
                                </div>

                                <div className='input-div-form'>

                                <textarea className="elementor-field-textual elementor-field  elementor-size-lg"
                                          name="message" rows="8"
                                          placeholder="Ihre Nachricht:*"></textarea>

                                </div>

                                <button type="submit" className="elementor-button ">
						                    <span>
															<span className=" elementor-button-icon">
									<i className="fa fa-send" aria-hidden="true"></i>																	</span>
																						<span
                                                                                            className="elementor-button-text">SENDEN</span>
													</span>
                                </button>
                            </form>

                        </div>

                    </Col>
                </Row>
            </Container>
            <Footer
                logoStatus={false}
            />

        </>
    );
}

export default ThanksPage;
