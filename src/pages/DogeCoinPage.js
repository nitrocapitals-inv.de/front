import React, {Component} from 'react'
import {Container, Row, Col} from "react-bootstrap";
import {connect} from "react-redux";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'

import '../assets/css/style3.css'
import BgTopDesktop from '../assets/img/bg_top_desktop.jpg'
import BgTopTab from '../assets/img/bg_top_tab.jpg'
import BgTopMob from '../assets/img/bg_top_mob.jpg'
import Access_1 from '../assets/img/access_1.svg'
import Access_2 from '../assets/img/access_2.svg'
import Access_3 from '../assets/img/access_3.svg'
import Access_4 from '../assets/img/access_4.svg'
import DogImage from '../assets/img/dog-image.jpg'
import DogImageMob from '../assets/img/dog-image_mob.jpg'
import Chart from '../assets/img/chart.jpg'
import PaymentMethod3 from '../assets/img/payment-methods3.png'
import ArrowDesktop from '../assets/img/arrow_desktop.png'
import LogoDog from '../assets/img/logo-dog.svg'
import Slider from "react-rangeslider";

import {FLAG_SELECTOR_OPTION_LIST, Flags} from '../helper/country'
import {CountryInitialName, CountryName} from '../helper/countryName'
import {checkCountryRequest, signUpRequest} from '../store/actions'

import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";


import {Footer} from "../components/layout/footer";
import ReactCustomFlagSelect from "react-custom-flag-select";


const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};
const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;

class DogeCoinPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            phoneValue: '',
            value: 250,
            value1: (1250).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),
        }
    }

    handleValueChange = value => {
        this.setState({
            value: value,
            value1: (value*5).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
        })
    };


    componentDidMount() {
        this.props.checkCountryRequest()
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {
        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if (this.state.lastName.length <= 3 || this.state.lastName >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
            } else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
        }


        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                phone : this.state.phoneValue,
                areaCodeName: this.state.areaCodeName,
                Source: process.env.REACT_APP_IP_WEB + '' + window.location.pathname.split('/')[1],
                domenSource: process.env.REACT_APP_DOMEN_SOURCE,
                affiliateId: process.env.REACT_APP_ID,
                Token: process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)

        }
    }


    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <>
                <div className="wrap jquery_to_footer top_v1">
                    <div className="top1">
                        <div className="bg top1__bg top1__bg_desktop desktop_only" data-editable=""
                             data-name="top1__bg_desktop">
                            <div className="bg_wrap" data-ce-tag="bkgimg"
                                 style={{backgroundImage: `url(${BgTopDesktop})`}}>&nbsp;
                            </div>
                        </div>
                        <div className="bg top1__bg top1__bg_tab tablet_only" data-editable="" data-name="top1__bg_tab">
                            <div className="bg_wrap" data-ce-tag="bkgimg"
                                 style={{backgroundImage: `url(${BgTopTab})`}}>&nbsp;
                            </div>
                        </div>
                        <div className="bg top1__bg top1__bg_mob mobile_only" data-editable="" data-name="top1__bg_mob">
                            <div className="bg_wrap" data-ce-tag="bkgimg"
                                 style={{backgroundImage: `url(${BgTopMob})`}}>&nbsp;
                            </div>
                        </div>
                        <div className="top1__containerD containerD">
                            <div className="top1__wrapper">
                                <div className="top1__title" data-editable="" data-name="top1__title">
                                    <p>Investieren Sie in <i>Dogecoin</i><span className="mobile_only">,</span> mit
                                        20.000%
                                        Wachstum im letzten Jahr,
                                    </p>
                                </div>
                                <div className="top1__subtitle" data-editable="" data-name="top1__subtitle">
                                    <p>und verdienen Sie innerhalb von Minuten!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="wrapper__form-line">
                      {/*  <div className="line">
                            <div className="line__container container">
                                <div className="line__wrapper">

                                    <div className="line__arrow desktop_only" data-editable=""
                                         data-name="line__arrow--desktop"><img alt="arrow" loading="lazy"
                                                                               src={ArrowDesktop} />
                                    </div>
                                    <div className="line__text" data-editable="" data-name="line__text">
                                        <p>&ldquo;Ein 1.000 € Einsatz in Dogecoin Anfang 2021 reicht heute f&uuml;r den
                                            Kauf eines Tesla&rdquo;
                                        </p>
                                    </div>
                                    <div className="line__logo" data-editable="" data-name="line__logo"><img alt="Logo"
                                                                                                             loading="lazy"
                                                                                                             src={LogoDog} />
                                    </div>
                                </div>
                            </div>
                        </div>
*/}


                        <div className="form form_main" style={{padding:'0px'}}>
                            <div className="form__wrap">
                                <div className="form__title" data-editable="" data-name="form__title" style={{backgroundColor:'#BF0000'}}>
                                    <p>Verdienen Sie jetzt mit Dogecoin
                                    </p>
                                </div>
                                <div className="formA__body">
                                    <div id="formWrapper1" className="formWrapper">

                                        <form method="POST"
                                              className="formLead form-horizontal registration-form sv-skin">
                                            <div className="row">
                                                <div className="col-md-12 form-group"
                                                     style={{textAlign: 'left'}}>
                                                    <input type="text" name="first_name" placeholder="Ihr Name"
                                                           className="form-control bfh-firstname"
                                                           onChange={this.handleChange("firstName")}
                                                           value={this.state.firstName}
                                                    />
                                                    {
                                                        this.state.firstNameErr ? <label
                                                            className="x-error-red">Erforderlich</label> : null
                                                    }
                                                </div>

                                                <div className="col-md-12 form-group"
                                                     style={{textAlign: 'left'}}>
                                                    <input type="text" name="last_name"
                                                           placeholder="dein Nachname"
                                                           className="form-control"
                                                           onChange={this.handleChange("lastName")}
                                                           value={this.state.lastName}
                                                    />
                                                    {
                                                        this.state.lastNameErr ? <label
                                                            className="x-error-red">Erforderlich</label> : null
                                                    }
                                                </div>

                                                <div className="col-md-12 form-gro7up" style={{marginBottom:'0px'}}>
                                                    <PhoneInput
                                                        style={{
                                                            height: '40px',
                                                            border: '1px solid #616161',
                                                            borderRadius: '5px',
                                                            padding: '8px',
                                                            width: '100%'
                                                        }}
                                                        placeholder="Enter phone number"
                                                        defaultCountry={this.props.country.country}
                                                        international
                                                        onChange={(e) => {this.setState({phoneValue : e})}}
                                                        value={this.state.phoneValue}

                                                    />
                                                    {
                                                        this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                            Telefonnummer</label> : null
                                                    }


                                                </div>
                                                <br/>
                                                <br/>
                                                <div className="col-md-12 form-group"
                                                     style={{textAlign: 'left'}}>
                                                    <input type="text" name="email" placeholder="Email"
                                                           className="form-control bfh-email" required=""
                                                           onChange={this.handleChange("email")}
                                                           value={this.state.email}
                                                    />

                                                    {
                                                        this.state.emailErr ? <label className="x-error-red">Ungültige
                                                            Adresse</label> : null
                                                    }
                                                    {
                                                        this.props.auth.alert ?
                                                            <label className="x-error-red">Diese, E-Mail-Adresse ist
                                                                bereits registriert. Versuchen Sie es mit einem
                                                                anderen</label> : null
                                                    }
                                                </div>
                                            </div>
                                        </form>


                                        <button
                                            onClick={this.handleRegistration}
                                            type="button"
                                            className="btnDog">Infos erhalten
                                        </button>

                                    </div>
                                    <div  style={{    marginTop: '22px'}} className="form__text">

                                        <div>
                                            <input
                                                style={{  float:'left', marginTop:'6px'}}
                                                type="checkbox"
                                                checked={this.state.checked}
                                                onChange={this.handleInputChange}
                                                name="terms"/>
                                            <div style={{marginLeft:'20px'}}>
                                                <p>
                                                    Um zu investieren müssen Sie mindestens 18 Jahre alt sein. Erforderliches Mindestkapital 250€
                                                </p>
                                                <p style={{textAlign:'left'}}>
                                                    {
                                                        this.state.checkedErr ?
                                                            <label className="x-error-red">Verpflichtend</label> : null
                                                    }
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="access">
                        <div className="access__container container">
                            <div className="access__title" data-editable="" data-name="access__title">
                                <p>Erhalten Sie Zugang zu:
                                </p>
                            </div>
                            <div className="access__list">
                                <div className="access__item">
                                    <div className="access__img" data-editable="" data-name="access__img--1"><img
                                        alt="icon"
                                        loading="lazy"
                                        src={Access_1}/>
                                    </div>
                                    <div className="access__text" data-editable="" data-name="access__text--1">
                                        <p>Sichere und regulierte Plattform
                                        </p>
                                    </div>
                                </div>
                                <div className="access__item">
                                    <div className="access__img" data-editable="" data-name="access__img--2"><img
                                        alt="icon"
                                        loading="lazy"
                                        src={Access_2}/>
                                    </div>
                                    <div className="access__text" data-editable="" data-name="access__text--2">
                                        <p>Ertr&auml;ge in 24 Stunden
                                            <br className="mobile_only"/>auf Ihrem Konto
                                        </p>
                                    </div>
                                </div>
                                <div className="access__item">
                                    <div className="access__img" data-editable="" data-name="access__img--3"><img
                                        alt="icon"
                                        loading="lazy"
                                        src={Access_3}/>
                                    </div>
                                    <div className="access__text" data-editable="" data-name="access__text--3">
                                        <p>Einfache Schulungsinstrumente
                                        </p>
                                    </div>
                                </div>
                                <div className="access__item">
                                    <div className="access__img" data-editable="" data-name="access__img--4"><img
                                        alt="icon"
                                        loading="lazy"
                                        src={Access_4}/>
                                    </div>
                                    <div className="access__text" data-editable="" data-name="access__text--4">
                                        <p>24 Stunden Support und fortlaufende Unterst&uuml;tzung
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/*Slider*/}

                    <div className="rslider">
                        <div className="rslider__container container">
                            <div
                                onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}

                                className="js-scroll rslider__btn btn" data-scroll="form_main" data-editable=""
                                 data-name="rslider__btn">
                                <p>Fangen Sie heute
                                    <br className="mobile_only" />an zu verdienen!
                                </p>
                            </div>
                            <div className="rslider__wrapper">
                                <div className="rslider__title" data-editable="" data-name="rslider__title">
                                    <p>Entdecken Sie Ihre potenziellen kurzfristigen Gewinne (Max. 2 Monate)
                                    </p>
                                </div>
                                <div className="range range__label">
                                    <div className="range__inner">
                                        <div className="range__item"><span
                                            className="range__item-text range-text1"> </span>
                                            <p className="range__item-number range-initial-value">&nbsp;
                                            </p>
                                        </div>
                                        <div className="range__item"><span
                                            className="range__item-text range-text2"> </span>
                                            <p className="range__item-number range-potential-profit">&nbsp;
                                            </p>
                                        </div>
                                    </div>
                                    <div className="range-slider">
                                        <div className="range-slider-wrapper">
                                            <div id="counter-slider" className="no-ui-slider">
                                              {/*  <div className="slider" style={{direction: 'ltr'}}>&nbsp;
                                                </div>*/}
                                                <Slider
                                                    style={{height:'400px'}}
                                                    min={250}
                                                    max={20000}
                                                    value={this.state.value}
                                                    // onChangeStart={this.handleChangeStart}
                                                    onChange={this.handleValueChange}
                                                    // onChangeComplete={this.handleChangeComplete}
                                                />

                                            </div>

                                        </div>
                                        <div  >
                                                    <span className="irs-grid" style={{width: '90.1084%', left: '4.84578%', color:'black'}}><span
                                                        className="irs-grid-pol-dog" style={{left: '0%'}}></span><span
                                                        className="irs-grid-text-dog js-grid-text-0"
                                                        style={{left: '0%', marginLeft: '-1.66886%', fontSize: '14px'}}>250</span>
                                            <span
                                                className="irs-grid-pol-dog small" style={{left: '20%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '15%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '10%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '5%'}}></span><span
                                                            className="irs-grid-pol-dog" style={{left: '25%'}}></span><span
                                                            className="irs-grid-text-dog js-grid-text-1"
                                                            style={{
                                                                left: '25%',
                                                                visibility: 'visible',
                                                                marginLeft: '-3.19277%',
                                                                fontSize: '14px'
                                                            }}>5 000</span><span
                                                            className="irs-grid-pol-dog small" style={{left: '45%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '40%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '35%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '30%'}}></span><span
                                                            className="irs-grid-pol-dog" style={{left: '50%'}}></span><span
                                                            className="irs-grid-text-dog js-grid-text-2"
                                                            style={{
                                                                left: '50%',
                                                                visibility: 'visible',
                                                                marginLeft: '-3.19277%',
                                                                fontSize: '14px'
                                                            }}>10 000</span><span
                                                            className="irs-grid-pol-dog small" style={{left: '70%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '65%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '60%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '55%'}}></span><span
                                                            className="irs-grid-pol-dog" style={{left: '75%'}}></span><span
                                                            className="irs-grid-text-dog js-grid-text-3"
                                                            style={{
                                                                left: '75%',
                                                                visibility: 'visible',
                                                                marginLeft: '-3.19277%',
                                                                fontSize: '14px'
                                                            }}>15 000</span><span
                                                            className="irs-grid-pol-dog small" style={{left: '95%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '90%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '85%'}}></span><span
                                                            className="irs-grid-pol-dog small" style={{left: '80%'}}></span><span
                                                            className="irs-grid-pol-dog" style={{left: '100%'}}></span>
                                            <span
                                                className="irs-grid-text-dog js-grid-text-4"
                                                style={{left: '100%', marginLeft: '-3.84601%', fontSize: '14px'}}>20 000</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="r-slider__bottom"
                                     onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}
                                >
                                    <span className="r-slider__title">Potenzielle Gewinne</span>
                                    <span id="bottom-profit" className="r-slider__subtitle">€{this.state.value1}</span>
                                </div>
                            </div>

                        </div>

                    </div>
                    <Container style={{paddingTop: '140px'}}>
                        <Row>
                            <Col>
                                <div className="article__title" data-editable="" data-name="article__title">
                                    <p>Gestartet als Scherz und zur Legende geworden!
                                    </p>
                                </div>
                                <div className="article__text" data-editable="" data-name="article__text">
                                    <p>Dogecoin, die Meme-basierte Kryptow&auml;hrung, die urspr&uuml;nglich als Scherz
                                        geschaffen wurde, f&uuml;hrt die Krypto-Meute an und hat in den letzten 12
                                        Monaten
                                        ein rasantes Wachstum <b>von 20.000% erreicht - mit einer Analyse, die
                                            offenbart,
                                            dass drei US-Stimulus-Schecks im Wert von knapp &uuml;ber 3.000 Dollar jetzt
                                            atemberaubende 500.000 Dollar wert w&auml;ren, wenn sie zum Kauf von
                                            Dogecoin
                                            verwendet worden w&auml;ren.</b>
                                    </p>
                                    <p>Nach dieser ph&auml;nomenalen Wachstumsrate wird erwartet, dass Dogecoin weiter
                                        w&auml;chst und noch robuster wird. Dies ist der perfekte Zeitpunkt, um in
                                        Dogecoin
                                        zu investieren und von seinem enormen Erfolg zu profitieren. Beginnen Sie heute
                                        mit
                                        nur 250&euro;, und Sie werden direkt auf Ihr Bankkonto verdienen.
                                    </p>
                                </div>
                            </Col>
                            <Col>
                                <div className="article__img tablet_only " data-editable="" data-name="article__img">
                                    <img
                                        alt="Image" loading="lazy" src={DogImage}/>
                                </div>
                                <div className="article__img no_tablet" data-editable="" data-name="article__img--mob">
                                    <img
                                        alt="Image" loading="lazy" src={DogImageMob}/>
                                </div>
                            </Col>
                        </Row>
                    </Container>

                    <div className="chart">
                        <div className="chart__container container">
                            <div className="chart__wrapper">
                                <div className="chart__title" data-editable="" data-name="chart__title">
                                    <p>Dogecoin steigt auf wahnsinnige H&ouml;hen!
                                    </p>
                                </div>
                                <div className="chart__img" data-editable="" data-name="chart__img"><img alt="Chart"
                                                                                                         loading="lazy"
                                                                                                         src={Chart}/>
                                </div>
                            </div>
                            <div
                                onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}

                                className="js-scroll chart__btn btnDog2 " style={{width:'50%'}} data-scroll="form_main" data-editable=""
                                 data-name="chart__btn">
                                <p>Fangen Sie heute
                                    <br className="mobile_only"/>an zu verdienen!
                                </p>
                            </div>
                            <br/>
                            <div className="chart__payment" style={{textAlign: 'center'}}><img
                                alt="payment methods" loading="lazy" src={PaymentMethod3}/>
                            </div>
                        </div>
                    </div>
                </div>

                <Footer
                    logoStatus={false}
                />


            </>
        )
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest
})(DogeCoinPage);

