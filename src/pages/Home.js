import React from "react";
import {connect} from "react-redux";
import {Player} from 'video-react';
import {Container} from "react-bootstrap";

import ReactCustomFlagSelect from "react-custom-flag-select";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'


import Logo from '../assets/img/mini-logo2.png'
import A44 from '../assets/img/44.jpg'
import DataProtected from '../assets/img/data-protected.svg'
import Mastercard from '../assets/img/mastercard.svg'
import Visa from '../assets/img/visa.svg'
import Sepa from '../assets/img/sepa.png'
import Bitgo from '../assets/img/bitgo.svg'
import GirlWithBitcoin from '../assets/img/girl-with-bitcoin.png'
import AsSeenOn from '../assets/img/as-seen-on.svg'
import Person_1 from '../assets/img/person-1.png'
import Person_2 from '../assets/img/person-2.png'
import Person_3 from '../assets/img/person-3.png'
import Advantage_1 from '../assets/img/advantage-1.png'
import Advantage_2 from '../assets/img/advantage-2.png'
import Advantage_3 from '../assets/img/advantage-3.png'
import TableWin from '../assets/img/table-win.svg'
import How_1 from '../assets/img/how-1.png'
import How_2 from '../assets/img/how-2.png'
import How_3 from '../assets/img/how-3.png'
import MyVideo from "../assets/video/bitcoindeutschlandgewinnpage1de5.mp4";
import '../assets/css/checkbox-svg.css'
import '../assets/css/ion.rangeSlider.min.css'
import {Footer} from "../components/layout/footer";
import Slider from 'react-rangeslider'
import '../assets/css/range-slider.css'
import {FakeUsers} from '../helper/data'
import {FLAG_SELECTOR_OPTION_LIST, Flags} from '../helper/country'
import {CountryInitialName, CountryName} from '../helper/countryName'
import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";
import Moment from "react-moment";

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};


const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;


class HomesPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,
            popap: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            muted: true,

            x: 0, y: 0,

            value: 250,
            value1: (1250).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),

            fakeUserArray: [],
            skip: 0,
            limit: 10,
            timerId: '',
            phoneValue: '',

        };
        this.submit = this.submit.bind(this);
    }

    handleValueChange = value => {
        this.setState({
            value: value,
            value1: (value*5).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
        })
    };

    handleChangeComplete = () => {
        console.log('Change event completed')
    };

    _onMouseMove(e) {
        if (e.screenY <= 80) {
            this.setState({popap: true});
        }
    }

    componentDidMount() {
        this.setState({
            fakeUserArray: FakeUsers
        })
        this.state.timerId = setInterval(() => {
            if (this.state.skip < 180) {
                this.setState({
                    skip: this.state.skip + 1,
                    limit: this.state.limit + 1
                })
            }
        }, 10000)


        this.props.checkCountryRequest()
    }

    componentWillUnmount() {
        clearInterval(this.state.timerId)
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            // alert("You are good to go");
        }
    }

    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {
        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if (this.state.lastName.length <= 3 || this.state.lastName >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
            } else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
        }


        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }
    }

    popapTrue() {
        this.setState({popap: true});
    }

    handleClosePopap = (event) => {
        this.setState({
            popap: false
        })
    }

    _handleMute = () => {
        this.setState({
            muted: false
        })
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <>
                <div className="popup_custom" style={this.state.popap ? {visibility: 'visible'} : {}}>
                    <div className="popup_overlay"></div>
                    <a className="close_button"
                       onClick={this.handleClosePopap.bind(this)}
                    >×</a>
                    <div className="popup_inner">
                        <div className="popup_content">
                            <div className="popup_content_inner">
                                <div className="popup-content-wrapper">
                                    <div className="popup-header">
                                        <div className="title-p">
                                            Du hast gerade einen GROßEN Fehler gemacht...
                                        </div>
                                        <div className="subtitle-p">
                                            Dies ist deine <span className="yellow">LETZTE CHANCE</span> der &ensp;
                                            <span className="yellow"><u>Nitrocapitals</u></span>&ensp;
                                            <br/>beizutreten und dir deine finanzielle Zukunft
                                            abzusichern.
                                        </div>
                                    </div>
                                </div>
                                <div className="popup-form-wrapper">
                                    <div className="form-container-unique">
                                        <div className="form-block-2 whitee">
                                            <div className="hero-form form-home" style={{backgroundColor: 'red'}}>
                                                <div className="preloader"></div>
                                                <div className="form-head">
                                                    <h2 style={{textAlign: 'center', color: 'white', fontSize: '2rem'}}>
                                                        ÄNDERE DEIN <br/>
                                                        LEBEN HEUTE!
                                                    </h2>
                                                </div>

                                                <form method="POST"
                                                      className="formLead form-horizontal registration-form sv-skin">
                                                    <div className="row">
                                                        <div className="col-md-12 form-group"
                                                             style={{textAlign: 'left'}}>
                                                            <input type="text" name="first_name" placeholder="Ihr Name"
                                                                   className="form-control bfh-firstname"
                                                                   onChange={this.handleChange("firstName")}
                                                                   value={this.state.firstName}
                                                            />
                                                            {
                                                                this.state.firstNameErr ? <label
                                                                    className="x-error-w">Erforderlich</label> : null
                                                            }
                                                        </div>

                                                        <div className="col-md-12 form-group"
                                                             style={{textAlign: 'left'}}>
                                                            <input type="text" name="last_name"
                                                                   placeholder="dein Nachname"
                                                                   className="form-control"
                                                                   onChange={this.handleChange("lastName")}
                                                                   value={this.state.lastName}
                                                            />
                                                            {
                                                                this.state.lastNameErr ? <label
                                                                    className="x-error-w">Erforderlich</label> : null
                                                            }
                                                        </div>

                                                        <div className="col-md-12 form-group">
                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%',
                                                                    backgroundColor:'white'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-w" style={{color :'white'}}>Ungültige
                                                                    Telefonnummer</label> : null
                                                            }


                                                        </div>

                                                        <div className="col-md-12 form-group"
                                                             style={{textAlign: 'left'}}>
                                                            <input type="text" name="email" placeholder="Email"
                                                                   className="form-control bfh-email" required=""
                                                                   onChange={this.handleChange("email")}
                                                                   value={this.state.email}
                                                            />

                                                            {
                                                                this.state.emailErr ? <label className="x-error-w">Ungültige
                                                                    Adresse</label> : null
                                                            }
                                                            {
                                                                !this.props.auth.alert ?
                                                                    <label className="x-error-w">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                            }
                                                        </div>
                                                        <div className="col-md-12 form-group">
                                                            <button type="submit" name="button"
                                                                    onClick={this.handleRegistration}
                                                                    className="submit-btn lead-form-submit btn btn-warning btn-lg register-btn"
                                                                    style={{
                                                                        background: '#fff',

                                                                        borderColor: '#fff',
                                                                        width: '100%',
                                                                        textTransform: 'uppercase',
                                                                        color: 'black',
                                                                        height: '50px'
                                                                    }}>
                                                                Registrieren
                                                            </button>
                                                        </div>

                                                        <div className="checkbox-svg">
                                                            <input type="checkbox"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   id="cbx-2-pop" style={{display: 'none'}}/>
                                                            <label htmlFor="cbx-2-pop" className="checked-svg">
                                                                <svg width="20px" height="20px" viewBox="0 0 18 18">
                                                                    <path
                                                                        d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z"></path>
                                                                    <polyline points="1 9 7 14 15 4"></polyline>
                                                                </svg>
                                                            </label>
                                                            <div className="privacy-checkbox">
                                                                <p>
                                                                    Wenn Sie hier klicken, stimmen Sie der Verarbeitung und Weitergabe Ihrer Daten zu
                                                                    Um in Amazon zu investieren müssen Sie mindestens 18 Jahre alt sein. Das erforderliche Mindestkapital ist 250 €.
                                                                </p>
                                                                <p style={{textAlign:'left'}}>
                                                                    {
                                                                        this.state.checkedErr ?
                                                                            <label className="x-error-w">Verpflichtend</label> : null
                                                                    }
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div style={{display: 'none'}}>
                                                    <div className="lms-dialog dialogSuccess" title="">
                                                        <div className="widget-form lms-container">
                                                            <span className="close-lms-dialog"
                                                                  aria-hidden="true">X</span>
                                                            <div className="contents">
                                                                <div className="title">Danke fürs Anmelden!</div>
                                                                <div className="broker_logo"></div>
                                                                <div className="message">
                                                                    <p>
                                                                        In Kürze erhalten Sie eine Bestätigungs-E-Mail
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <Container
                    onMouseMove={this._onMouseMove.bind(this)}
                    fluid style={{padding: '0px'}}>

                    <section className="section-header">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-lg-4 pl-0 header-group" style={{padding: '10px'}}>
                                    <img src={Logo} alt="Company logo"
                                         style={{height: '90px', width: 'auto', padding: '0px'}}
                                         className="header-logo"/>
                                </div>
                                <div className="hide-mobile offset-lg-2 col-lg-4 pr-0 header-geo">
                                    <div className="header-geo--text">
                                        <span>EXKLUSIVES ANGEBOT FÜR</span> <br/>
                                        <span>
                                <b>HÄNDLER AUS</b> <span
                                            data-init="country-name">{CountryName(this.props.country.country)}</span>
                              </span>
                                    </div>
                                    <img data-init="country-flag"
                                         src={Flags[this.props.country.flagId]}
                                         alt="Flag"/>
                                </div>
                                <div className="hide-mobile col-lg-2 header-person" data-init="person-timeout">
                                    <div className="person-image">
                                        <img src={A44} alt="Person"/>
                                    </div>
                                    <div className="person-text">
                                        <span className="person-text--name name">Lucy O.</span>
                                        <span className="person-text--made">gerade erst gemacht</span>
                                        <span className="person-text--sum">
                                <span className="currency">€</span> <span className="sum">123</span>
                              </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section-hero">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <div className="hero-title">
                                        <h1>Der Bitcoin macht Menschen reich</h1>
                                    </div>
                                    <div className="hero-subtitle">
                                        <h3>
                                            Und du könntest zum nächsten
                                            <span className="yellow"> Millionär werden…</span>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-lg-8 video-container">
                                    <div className="row" style={{color: 'white'}}>
                                        <div className="col">
                                            <span className="btc-name btc">bitcoin Market Cap</span>
                                        </div>
                                        <div className="col">
                                            <span className="currency">€ 143,584,911,738</span>
                                        </div>
                                        <div className="col">
                                            <span className="currency">bitcoin</span>
                                        </div>
                                        <div className="col">
                                            <span className="currency">€8,874.68</span>
                                        </div>
                                    </div>

                                    <div className="">
                                        <div id="mainVideo" style={{position: 'relative'}}>
                                            <div id="MainVideobtn" style={{
                                                position: 'absolute',
                                                right: '5%',
                                                top: '5%',
                                                zIndex: '1',
                                                background: 'rgba(0, 0, 0, 0.7)',
                                                borderRadius: '30px',
                                                padding: '0 5px',
                                                color: '#fff'
                                            }}
                                            >
                                                {
                                                    this.state.muted
                                                        ? <img
                                                            onClick={this._handleMute.bind(this)}
                                                            src="https://media.giphy.com/media/gfHIf1cc9ACGzVgLu0/giphy.gif"
                                                            style={{width: '35px'}}/>
                                                        : null
                                                }

                                            </div>
                                            <Player
                                                autoPlay={true}
                                                muted={this.state.muted}
                                                // poster="/assets/poster.png"
                                                src={MyVideo}
                                            />
                                        </div>

                                    </div>
                                </div>
                                <div className="col-12 col-lg-4 form-block">
                                    <div className="hero-form form-2">
                                        <div className="preloader"></div>
                                        <div className="form-head">
                                            <h2 style={{textAlign: 'center', color: 'white'}}>
                                                ÄNDERE DEIN <br/>
                                                LEBEN HEUTE!
                                            </h2>
                                        </div>
                                        <form method="POST"
                                              className="formLead form-horizontal registration-form sv-skin">


                                            <div className="row">
                                                <div className="col-md-12 form-group">
                                                    <input type="text" name="first_name" placeholder="Ihr Name"
                                                           className="form-control bfh-firstname"
                                                           onChange={this.handleChange("firstName")}
                                                           value={this.state.firstName}
                                                    />
                                                    {
                                                        this.state.firstNameErr ? <label
                                                            className="x-error-w">Erforderlich</label> : null
                                                    }
                                                </div>

                                                <div className="col-md-12 form-group">
                                                    <input type="text" name="last_name" placeholder="dein Nachname"
                                                           className="form-control"
                                                           onChange={this.handleChange("lastName")}
                                                           value={this.state.lastName}
                                                    />
                                                    {
                                                        this.state.lastNameErr ? <label
                                                            className="x-error-w">Erforderlich</label> : null
                                                    }
                                                </div>

                                                <div className="col-md-12 form-group">

                                                        <PhoneInput
                                                            style={{
                                                                height: '40px',
                                                                border: '1px solid #b4b3b3',
                                                                borderRadius: '5px',
                                                                padding: '8px',
                                                                width: '100%',
                                                                backgroundColor:'white'
                                                            }}
                                                            placeholder="Enter phone number"
                                                            defaultCountry={this.props.country.country}
                                                            international
                                                            onChange={(e) => {this.setState({phoneValue : e})}}
                                                            value={this.state.phoneValue}

                                                        />
                                                        {
                                                            this.state.phoneErr ? <label className="x-error-x"  style={{color :'white'}}>Ungültige
                                                                Telefonnummer</label> : null
                                                        }

                                                </div>

                                                <div className="col-md-12 form-group">
                                                    <input type="text" name="email" placeholder="Email"
                                                           onChange={this.handleChange("email")}
                                                           value={this.state.email}
                                                           className="form-control bfh-email" required=""/>
                                                    {
                                                        this.state.emailErr ? <label className="x-error-w">Ungültige
                                                            Adresse</label> : null
                                                    }

                                                    {
                                                        this.props.auth.alert ?
                                                            <label className="x-error-w"> Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen </label> : null
                                                    }
                                                </div>



                                                <div className="col-md-12 form-group">
                                                    <button type="button" name="submit"
                                                            className="submit-btn lead-form-submit btn btn-warning btn-lg register-btn"
                                                            onClick={this.handleRegistration}
                                                            style={{
                                                                background: '#fff',
                                                                borderColor: '#fff',
                                                                width: '100%',
                                                                textTransform: 'uppercase',
                                                                color: 'black',
                                                                height: '50px'
                                                            }}>
                                                        Registrieren
                                                    </button>
                                                </div>

                                                <div className="checkbox-svg">
                                                    <input type="checkbox"
                                                           checked={this.state.checked}
                                                           onChange={this.handleInputChange}
                                                           id="cbx-2-popa" style={{display: 'none'}}/>
                                                    <label htmlFor="cbx-2-popa" className="checked-svg">
                                                        <svg width="20px" height="20px" viewBox="0 0 18 18">
                                                            <path
                                                                d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z"></path>
                                                            <polyline points="1 9 7 14 15 4"></polyline>
                                                        </svg>
                                                    </label>
                                                    <div className="privacy-checkbox">
                                                        <p>
                                                            Wenn Sie hier klicken, stimmen Sie der Verarbeitung und Weitergabe Ihrer Daten zu
                                                            Um in Amazon zu investieren müssen Sie mindestens 18 Jahre alt sein. Das erforderliche Mindestkapital ist 250 €.
                                                        </p>
                                                        <p style={{textAlign:'left'}}>
                                                            {
                                                                this.state.checkedErr ?
                                                                    <label className="x-error-w">Verpflichtend</label> : null
                                                            }
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section-info">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-lg-8 trust-block">
                                    <img src={Bitgo} alt="Trust image" className="trust-image trust-image--bitgo"/>

                                    <img src={Sepa} alt="Trust image"
                                         className="trust-image trust-image--norton"/>
                                    <img src={Visa} alt="Trust image"
                                         className="trust-image trust-image--visa"/>
                                    <img src={Mastercard} alt="Trust image"
                                         className="trust-image trust-image--mastercard"/>
                                </div>
                                <div className="col-12 col-lg-4 form-block">
                                    <div className="hero-form data-protect home-page-mob-top-padding">
                                        <img src={DataProtected} alt="Computer" className="computer-svg"/>
                                        <div className="data-protect--text">
                                            <b>Bei uns sind Ihre Daten immer geschützt.</b> <br/>
                                            <span>
                  Sie können Ihre Meinung jederzeit ändern, indem Sie auf den
                  Abmeldelink in der Fußzeile jeder E-Mail klicken, die Sie von
                  uns erhalten. Wir werden Ihre Daten mit Respekt behandeln. Mit
                  dem Klicken auf die obere Schaltfläche, erklären Sie sich
                  damit einverstanden, dass wir Ihre Daten gemäß diesen
                  Bedingungen verarbeiten.
                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section-join">
                        <div className="container">
                            <div className="row join-reverse">
                                <div className="col-lg-8 pr-0 pl-0 join-text">
                                    <h2 className="join-text--title">
                                        Treten Sie uns bei und werden Sie
                                        reich mit der
                                        <br/>
                                        <b className="btn-scroll">Nitrocapitals</b>
                                    </h2>
                                    <p className="join-text--content">
                                        Die Nitrocapitals ist eine Gruppe, die
                                        ausschließlich Personen vorbehalten ist, die auf die verrückten
                                        Renditen, die Bitcoin gewährt, angesprungen sind und dabei in
                                        aller Stille ein Vermögen verdient haben. Unsere Mitglieder
                                        genießen jeden Monat Retreats auf der ganzen Welt, während sie mit
                                        nur wenigen Minuten „Arbeit” täglich Geld an ihrem Laptop
                                        verdienen.
                                    </p>

                                    <img src={GirlWithBitcoin} alt="Girl with bitcoin"
                                         className="show-mobile mob-girl-img"/>
                                    <div className="join-text--trust">
                                        <span>Wie bei den:</span>
                                        <img src={AsSeenOn} alt="trust partner"/>
                                    </div>
                                </div>
                                <div className="col-lg-4 join-img hide-mobile">
                                    <h2 className="join-text--title show-mobile">
                                        Treten Sie uns bei und werden Sie <br/>
                                        reich mit der <b>Nitrocapitals&nbsp;</b>
                                    </h2>
                                    <img src={GirlWithBitcoin} alt="Girl with bitcoin" className=""/>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section-testimonials">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 testimonials-margin">
                                    <h2 className="testimonials-title title">
                                        ECHTE <span className="dark-red">TESTIMONIALS</span> UNSERER ANWENDER
                                    </h2>
                                </div>

                                <div className="col-lg-4 col-md-6 col-sm-12 testimonial testimonial-user-1">
                                    <div className="testimonial-container">
                                        <img src={Person_1} alt="Person testimonial"/>
                                        <div className="testimonial-group">
                <span className="testimonial-group--about">
                  Jonte Weber<br/>
                  München
                </span>
                                            <span className="testimonial-group--sum">
                  Gewinn: <span className="currency">€</span>10,987.98</span>
                                        </div>
                                    </div>
                                    <span className="testimonial-comment">
              “ Endlich weiß ich, was es heißt, einen Traum zu leben. Ich habe
              nicht mehr das Gefühl, von der Seite zuzusehen, während andere
              Spaß haben. Die
              Nitrocapitals
              hat es mir ermöglicht, die Arbeit vorzeitig zu verlassen und einen
              Lebensstil von 1% zu führen. ”
            </span>
                                </div>

                                <div className="col-lg-4 col-md-6 col-sm-12 testimonial testimonial-user-2">
                                    <div className="testimonial-container">
                                        <img src={Person_2} alt="Person testimonial"/>
                                        <div className="testimonial-group">
                <span className="testimonial-group--about">
                  Greta Müller<br/>
                  Berlin
                </span>
                                            <span className="testimonial-group--sum">
                  Gewinn: <span className="currency">€</span>6,109.09</span>
                                        </div>
                                    </div>
                                    <span className="testimonial-comment">
              “ Ich bin seit gerade einmal 47 Tagen Mitgtlied der
              Nitrocapitals. Aber mein Leben hat sich schon verändert! Ich habe nicht nur
              meine ersten <span className="currency">€</span> 10,000 gemacht,
              sondern auch einige der unglaublichsten Leute getroffen. Und das
              Dank der
             Nitrocapitals! ”</span>
                                </div>
                                <div className="col-lg-4 col-sm-12 testimonial testimonial-user-3">
                                    <div className="testimonial-container">
                                        <img src={Person_3} alt="Person testimonial"/>
                                        <div className="testimonial-group">
                <span className="testimonial-group--about">
                  Silas Schmidt<br/>
                  Frankfurt
                </span>
                                            <span className="testimonial-group--sum">
                  Gewinn: <span className="currency">€</span>8,938.79</span>
                                        </div>
                                    </div>
                                    <span className="testimonial-comment">
              “ Überraschenderweise war ich mal ein Investor an der Wall Street.
              Und so etwas habe ich in meiner 10-jährigen Betriebszugehörigkeit
              noch nie gesehen. Meine Kollegen hielten mich alle für verrückt,
              als ich die Firma verließ, um mit der
              Nitrocapitals
              Software in Vollzeit zu gehen.
              <span className="currency">€</span>38,459 Gewinn später und alle meine
              Kollegen FLEHEN mich an, sie hereinzulassen. ”</span>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section-advantages">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-4 col-md-6 col-sm-12">
                                    <div className="advantages-card advantages-card--laser">

                                        <img src={Advantage_1} alt="Advantage 1"/>
                                        <h3>LASERGENAUE LEISTUNG</h3>
                                        <span>
                                    Es gibt keine andere Handelsanwendung auf der Welt, die mit
                                    einer Genauigkeit von 99,4% arbeitet, die die
                                    <a href="#"
                                       className="black-link btn-scroll">Nitrocapitals</a>
                                    lyerreichen kann. Deshalb vertrauen unsere Mitglieder aus der
                                    ganzen Welt darauf, dass wir ihr hart verdientes Geld verdoppeln
                                    und vervielfachen.
                                  </span>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-sm-12">
                                    <div className="advantages-card advantages-card--superior">
                                        <img src={Advantage_2} alt="Advantage 2"/>
                                        <h3>ÜBERLEGENE TECHNOLOGIE</h3>
                                        <span>
                                    Die
                                    <a href="#"
                                       className="btn-scroll black-link">Nitrocapitals</a>
                                    Software wurde mit der fortschrittlichsten Programmierung
                                    entwickelt, die die Handelswelt je gesehen hat. Die Software ist
                                    den Märkten um 0,01 Sekunden voraus. Dieser „Zeitsprung“ macht
                                    die Software zur konsistentesten Handelsanwendung der Welt.
                                  </span>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-12">

                                    <div className="advantages-card advantages-card--award">
                                        <img src={Advantage_3} alt="Advantage 3"/>
                                        <h3>PREISGEKRÖNTE HANDELSAPPLIKATION</h3>
                                        <span>
                                    Die
                                    <a href="#"
                                       className="black-link btn-scroll">Nitrocapitals</a>
                                    App wurde mehrfach ausgezeichnet. Die letzte Auszeichnung, die
                                    wir erhalten haben, ist die Nummer 1 in der Kategorie
                                    Handelssoftware für die DE Trading Association.
                                  </span>
                                    </div>
                                </div>
                            </div>
                            <div className="row join-section">
                                <div className="col-12">
                                    <button
                                        onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}
                                        type="button" className="btn btn-scroll">
                                        Jetzt beitreten!
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section className="section-slider">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 section-slider-flag">
                                    <div className="slider-group">
                                        <h3 className="title title-slider">
                                            Wie viel werde ich verdienen?
                                        </h3>
                                        <div className="slider-earn">
                                            <label>Dein Profit:</label>
                                            <h3 id="calcResult">€{this.state.value1}</h3>
                                        </div>
                                        <div className="slider-invest">
                                            <label>Ihre Investition:</label>

                                        </div>
                                        <Slider
                                            min={250}
                                            max={20000}
                                            value={this.state.value}
                                            // onChangeStart={this.handleChangeStart}
                                            onChange={this.handleValueChange}
                                            // onChangeComplete={this.handleChangeComplete}
                                        />

                                        <span className="irs-grid" style={{width: '96.1084%', left: '1.84578%',}}><span
                                            className="irs-grid-pol" style={{left: '0%'}}></span><span
                                            className="irs-grid-text js-grid-text-0"
                                            style={{left: '0%', marginLeft: '-1.66886%', fontSize: '14px'}}>250</span>


                                            <span
                                                className="irs-grid-pol small" style={{left: '20%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '15%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '10%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '5%'}}></span><span
                                                className="irs-grid-pol" style={{left: '25%'}}></span><span
                                                className="irs-grid-text js-grid-text-1"
                                                style={{
                                                    left: '25%',
                                                    visibility: 'visible',
                                                    marginLeft: '-3.19277%',
                                                    fontSize: '14px'
                                                }}>5 000</span><span
                                                className="irs-grid-pol small" style={{left: '45%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '40%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '35%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '30%'}}></span><span
                                                className="irs-grid-pol" style={{left: '50%'}}></span><span
                                                className="irs-grid-text js-grid-text-2"
                                                style={{
                                                    left: '50%',
                                                    visibility: 'visible',
                                                    marginLeft: '-3.19277%',
                                                    fontSize: '14px'
                                                }}>10 000</span><span
                                                className="irs-grid-pol small" style={{left: '70%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '65%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '60%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '55%'}}></span><span
                                                className="irs-grid-pol" style={{left: '75%'}}></span><span
                                                className="irs-grid-text js-grid-text-3"
                                                style={{
                                                    left: '75%',
                                                    visibility: 'visible',
                                                    marginLeft: '-3.19277%',
                                                    fontSize: '14px'
                                                }}>15 000</span><span
                                                className="irs-grid-pol small" style={{left: '95%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '90%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '85%'}}></span><span
                                                className="irs-grid-pol small" style={{left: '80%'}}></span><span
                                                className="irs-grid-pol" style={{left: '100%'}}></span>

                                            <span
                                                className="irs-grid-text js-grid-text-4"
                                                style={{left: '100%', marginLeft: '-3.84601%', fontSize: '14px'}}>20 000</span></span>


                                        <div className="slider-scroll">
                                            <button
                                                onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}
                                                type="button" className="btn btn-scroll ">
                                                Eröffnen Sie ein <br/>
                                                KOSTENLOSES Konto
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section className="section-table">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <h2 className="title table-title">
                                        Live <span className="dark-red">Profit</span> Ergebnisse!
                                    </h2>
                                </div>
                                <div className="table-responsive">
                                    <table className="table table-striped" id="trade-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gewinn</th>
                                            <th>Handelszeit</th>
                                            <th>Kryptowährung</th>
                                            <th>Resultate</th>
                                        </tr>
                                        </thead>

                                        <tbody id="trade-table-body"
                                               style={{paddingBottom: '0px', marginBottom: '0px'}}>

                                        {
                                            this.state.fakeUserArray.slice(this.state.skip, this.state.limit).reverse().map((item, key) => {
                                                return (
                                                    <tr>
                                                        <td className="bold">{item.name}</td>
                                                        <td className="up">€ {item.amount}.00</td>
                                                        {/*<td className="trade-time-td padding-left-td">15/7/2021</td>*/}
                                                        <td className="trade-time-td padding-left-td"><Moment
                                                            format="DD/MM/YYYY" date={new Date()}/></td>
                                                        <td>{item.cripto}</td>
                                                        <td><img src={TableWin} alt="Win"/></td>
                                                    </tr>
                                                )
                                            })
                                        }

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section-how">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <h2 className="title title-how">WIE'S FUNKTIONIERT</h2>
                                </div>

                                <div className="col-lg-4 how--first-card">
                                    <div className="how-card how-card--register black-border">
                                        <img src={How_1} alt="Step 1"/>
                                        <h3>
                                            Schritt1: <br/>
                                            <span>AUF DER SEITE REGISTRIEREN</span>
                                        </h3>
                                        <span>
                                            Sobald Ihre Registration akzeptiert wird, werden Sie automatisch
                                            das neueste Mitglied der
                                            <a href="#" className="black-link btn-scroll">Nitrocapitals</a>. Und Sie erhalten unsere proprietäre bitcoin-Handelssoftware
                                            kostenlos.
                                          </span>
                                    </div>
                                </div>

                                <div className="col-lg-4">
                                    <div className="how-card how-card--register red-border">
                                        <img src={How_2} alt="Step 2"/>
                                        <h3>
                                            Schritt2: <br/>
                                            <span>AUF IHR KONTO EINZAHLEN</span>
                                        </h3>
                                        <span>
                Wie jedes Unternehmen benötigen Sie für den Start ein
                Betriebskapital. Um mit der
                <a href="##" className="black-link btn-scroll">Nitrocapitals</a>
                zu profitieren, müssen Sie einen beliebigen Betrag von
                <span className="currency">€</span>250 oder mehr investieren.</span>
                                    </div>
                                </div>
                                <div className="col-lg-4 how--last-card">
                                    <div className="how-card how-card--register yellow-border">
                                        <img src={How_3} alt="Step 3"/>
                                        <h3>
                                            Schritt3: <br/>
                                            <span>ABSCHLIEßEN</span>
                                        </h3>
                                        <span>
                                            Klicken Sie auf Handel, um den präzisen und akkuraten Freihandel
                                            mit unserem preisgekrönten Algorithmus zu genießen. Sie können
                                            den Handel auch manuell verwalten, wenn Sie es vorziehen, auf
                                            eigene Faust zu handeln.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className="row how-section">
                                <div className="col-12">
                                    <button
                                        onClick={(e) => window.scrollTo({top: 0, behavior: 'smooth'})}
                                        type="button" className="btn btn-scroll">
                                        Kostenloses Konto erstellen
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                    {/*
                    <section className="section-steps">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 steps-margin">
                                    <h2 className="title title-steps">
                                        <span className="yellow">Häufig</span> stellte Fragen
                                    </h2>
                                </div>
                                <div className="col-lg-6 column-1">
                                    <div className="steps-container step-1">
                                        <span className="number number-one"> 1 </span>
                                        <div className="step-group">
                                            <h3 className="step-group--title">
                                                Welche Art von Ergebnissen kann ich erwarten?
                                            </h3>
                                            <span className="step-group--text">
                  Nitrocapitals
                  Mitglieder profitieren in der Regel von
                  <span className="currency">$</span>1,100 täglich.</span>
                                        </div>
                                    </div>
                                    <div className="steps-container step-2">
                                        <span className="number number-two"> 2 </span>
                                        <div className="step-group">
                                            <h3 className="step-group--title">
                                                Wie viele Stunden muss ich täglich arbeiten?
                                            </h3>
                                            <span className="step-group--text">
                  Unsere Mitglieder arbeiten durchschnittlich 20 Minuten pro Tag
                  oder weniger. Da die Software den Handel abwickelt, ist der
                  Aufwand minimal.
                </span>
                                        </div>
                                    </div>
                                    <div className="steps-container step-3">
                                        <span className="number number-three"> 3 </span>
                                        <div className="step-group">
                                            <h3 className="step-group--title">
                                                Was ist der maximale Betrag, den ich verdienen kann?
                                            </h3>
                                            <span className="step-group--text">
                  Die
                  Nitrocapitals
                  kennt keine Grenzen. Einige Mitglieder verdienten ihre erste
                  Million innerhalb von nur 61 Tagen.
                </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 column-2">
                                    <div className="steps-container step-4">
                                        <span className="number number-four"> 4 </span>
                                        <div className="step-group">
                                            <h3 className="step-group--title">Wie viel kostet die Software?</h3>
                                            <span className="step-group--text">
                  Mitglieder von der
                  Nitrocapitals
                  erhalten kostenlos eine Kopie unserer proprietären Software.
                  Um ein Mitglied zu werden, füllen Sie einfach das Formular auf
                  dieser Seite aus.
                </span>
                                        </div>
                                    </div>
                                    <div className="steps-container step-5">
                                        <span className="number number-five"> 5 </span>
                                        <div className="step-group">
                                            <h3 className="step-group--title">
                                                Ist es MLM oder Affiliate Marketing ähnlich?
                                            </h3>
                                            <span className="step-group--text">
                  Es ist nicht wie MLM, Teilnehmer-Marketing oder ähnliches. Die
                  Software gewinnt Handel mit einer Genauigkeit von 99,4%.
                </span>
                                        </div>
                                    </div>
                                    <div className="steps-container step-6">
                                        <span className="number number-six"> 6 </span>
                                        <div className="step-group">
                                            <h3 className="step-group--title">Gibt es Gebühren?</h3>
                                            <span className="step-group--text">
                  Es gibt keine versteckten Gebühren. Keine Maklergebühren oder
                  Provisionen. Ihr gesamtes Geld gehört zu 100% Ihnen und Sie
                  können es jederzeit ohne Verzögerung abheben.
                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

*/}
                    <section className="section-info section-info--footer">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-lg-8 trust-block">
                                    <img src={Bitgo} alt="Trust image" className="trust-image trust-image--bitgo"/>
                                    <img src={Sepa} alt="Trust image"
                                         className="trust-image trust-image--norton"/>
                                    <img src={Visa} alt="Trust image"
                                         className="trust-image trust-image--visa"/>
                                    <img src={Mastercard} alt="Trust image"
                                         className="trust-image trust-image--mastercard"/>
                                </div>
                                <div className="col-12 col-lg-4 form-block">
                                    <div className="info-footer data-protect">
                                        <img src={DataProtected} alt="Computer" className="computer-svg"/>
                                        <div className="data-protect--text">
                                            <b>Bei uns sind Ihre Daten immer geschützt.</b> <br/>
                                            <span>
                                          Sie können Ihre Meinung jederzeit ändern, indem Sie auf den
                                          Abmeldelink in der Fußzeile jeder E-Mail klicken, die Sie von
                                          uns erhalten. Wir werden Ihre Daten mit Respekt behandeln. Mit
                                          dem Klicken auf die obere Schaltfläche, erklären Sie sich
                                          damit einverstanden, dass wir Ihre Daten gemäß diesen
                                          Bedingungen verarbeiten.
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <Footer
                        logoStatus={false}
                    />
                </Container>
            </>
        );
    }

}

const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(HomesPage);
