import React, {Component} from 'react'
import {connect} from "react-redux";
import {Container, Row, Col} from "react-bootstrap";
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'


import MyVideo from "../assets/video/Bitcoin_System_DE.mp4";


import Logo from '../assets/img/bitcoin2/logo.svg'
import Bitcoin from '../assets/img/bitcoin2/Bitcoin.svg'
import P256 from '../assets/img/bitcoin2/256.png'


import {Footer} from "../components/layout/footer";
import {FLAG_SELECTOR_OPTION_LIST} from "../helper/country";


import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'

import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";
import {Player} from "video-react";
import ReactCustomFlagSelect from "react-custom-flag-select";
import Testimonial1 from "../assets/img/testimonial-1.jpg";
import Testimonial2 from "../assets/img/testimonial-2.jpg";
import Testimonial3 from "../assets/img/testimonial-3.jpg";
import Testimonial4 from "../assets/img/testimonial-4.jpg";
import Testimonial5 from "../assets/img/testimonial-5.jpg";
import Testimonial6 from "../assets/img/testimonial-6.jpg";
import Testimonial7 from "../assets/img/testimonial-7.jpg";
import Testimonial8 from "../assets/img/testimonial-8.jpg";
import Testimonial9 from "../assets/img/testimonial-9.jpg";
import Testimonial41 from "../assets/img/bitcoin2/testimonial-41.jpg";
import SteveMcKay from "../assets/img/bitcoin2/Steve-McKay.jpeg";
import Founder from "../assets/img/founder.png";
import Signature from "../assets/img/bitcoin2/sign202c.png";

import Cars from "../assets/img/bitcoin2/cars.jpg";
import Cars2 from "../assets/img/bitcoin2/cars2.jpg";
import Euros from "../assets/img/bitcoin2/euros.jpg";
import Friends from "../assets/img/bitcoin2/Friends-pool.jpg";
import Privatejet from "../assets/img/bitcoin2/privatejet.jpg";
import SKI from "../assets/img/bitcoin2/SKI.jpg";
import Yacht from "../assets/img/bitcoin2/yacht.jpg";

const handleDragStart = (e) => e.preventDefault();
const items = [
    <img src={Cars} onDragStart={handleDragStart} />,
    <img src={Cars2} onDragStart={handleDragStart} />,
    <img src={Euros} onDragStart={handleDragStart} />,
    <img src={Friends} onDragStart={handleDragStart} />,
    <img src={Privatejet} onDragStart={handleDragStart} />,
    <img src={SKI} onDragStart={handleDragStart} />,
    // <img src={Gols} onDragStart={handleDragStart} />,
    <img src={Yacht} onDragStart={handleDragStart} />,
];
const responsive = {
    0: { items: 1 },
    568: { items: 2 },
    1024: { items: 3 },
};

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};



const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();

class IProfit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            popap: false,
            theposition: '',
            scrollPosition: false,
            fixPosition: false,
            fixPositionHeight: 0,
            phoneValue: '',
            messagesEndRef: React.createRef(),


        };
        this.submit = this.submit.bind(this);
    }


    _onMouseMove(e) {
        if (e.screenY <= 80) {
            this.setState({popap: true});
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop


        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled >= 0.9) {
            const fixTop = height - 550;
            this.setState({
                fixPosition: true,
                // fixPositionHeight: fixTop,
            })
        } else {
            this.setState({
                fixPosition: false,
            })
        }


        if (scrolled > 0) {
            this.setState({
                scrollPosition: true
            })
        } else {
            this.setState({
                scrollPosition: false
            })
        }

    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop / 3 - 60);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {

        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if (this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
            } else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage: 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {
            let item = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                phone : this.state.phoneValue,
                areaCodeName: this.state.areaCodeName,
                Source: process.env.REACT_APP_IP_WEB + '' + window.location.pathname.split('/')[1],
                domenSource: process.env.REACT_APP_DOMEN_SOURCE,
                affiliateId: process.env.REACT_APP_ID,
                Token: process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];

        return (
            <Container style={{padding: '0px'}} fluid>

                <header id="headerBtn">
                    <div className="header-content">
                        <img style={{height: '80px'}} src={Logo}/>
                    </div>
                </header>
                <Container style={{padding: '0px'}} fluid>

                    <div className="section section-intro-2 scroll-trigger">
                        <div className="section-container">
                            <div style={{textAlign: 'center', color: 'white'}}>
                                <h1 className="heading">Reiten Sie auf der <strong
                                    className="orange" style={{color: '#fbaf41'}}>Bitcoin-Welle</strong> und verdienen
                                    Sie garantierte <strong
                                        className="orange" style={{color: '#fbaf41'}}><span
                                        className="dudecurrency ">€</span>
                                        13.000 in genau 24 Stunden.</strong></h1>
                            </div>

                            <Row>
                                <Col md={8}>
                                    <Player
                                        autoPlay={true}
                                        muted={true}
                                        // poster="/assets/poster.png"
                                        src={MyVideo}
                                    />
                                </Col>

                                <Col md={4}>
                                    <div className="top-form-wrapper">
                                        <div className="text-block-2"> Lassen Sie sich von mir zeigen, wie man es
                                            macht<br/> <strong className="bold-text-2">Cash LIVE - gleich
                                                jetzt<br/></strong></div>
                                        <div className="formInnerContainer--">
                                            <div className="formContai--ner">
                                                <div data-intgrtn-form-signup="">
                                                    <form method="post" id="formfull"
                                                          className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                        <div className="fieldset_group">
                                                            <div style={{padding: '5px'}}></div>
                                                            <div className="fieldset" style={{textAlign: 'left'}}>
                                                                <span className="fieldset-icon full_name-icon"></span>
                                                                <input className="text required full_name sk-input "
                                                                       id="full_name"
                                                                       type="text"
                                                                       onChange={this.handleChange("firstName")}
                                                                       value={this.state.firstName}
                                                                       placeholder="Vorname"
                                                                       autoComplete="name"
                                                                />
                                                                {
                                                                    this.state.firstNameErr ? <label
                                                                        className="x-error-x">Erforderlich</label> : null
                                                                }
                                                            </div>
                                                        </div>

                                                        <div className="fieldset_group">
                                                            <div style={{padding: '5px'}}></div>
                                                            <div className="fieldset" style={{textAlign: 'left'}}>
                                                                <span className="fieldset-icon full_name-icon"></span>
                                                                <input className="text required full_name sk-input "
                                                                       id="full_name"
                                                                       type="text"
                                                                       onChange={this.handleChange("lastName")}
                                                                       value={this.state.lastName}
                                                                       placeholder="Nachname"
                                                                       autoComplete="name"
                                                                />
                                                                {
                                                                    this.state.lastNameErr ? <label
                                                                        className="x-error-x">Erforderlich</label> : null
                                                                }
                                                            </div>
                                                        </div>

                                                        <div className="fieldset_group">
                                                            <div style={{padding: '5px'}}></div>
                                                            <div className="fieldset" style={{textAlign: 'left'}}>
                                                                <span className="fieldset-icon user_email-icon"></span>
                                                                <input type="text" name="user_email" id="user_email"
                                                                       className="user_email required sk-input"
                                                                       placeholder="E-Mail-Adresse" value=""
                                                                       maxLength="255"
                                                                       onChange={this.handleChange("email")}
                                                                       value={this.state.email}
                                                                />
                                                                {
                                                                    this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                        Adresse</label> : null
                                                                }
                                                                {
                                                                    this.props.auth.alert ?
                                                                        <label className="x-error-x">Diese,
                                                                            E-Mail-Adresse ist bereits registriert.
                                                                            Versuchen Sie es mit einem
                                                                            anderen</label> : null
                                                                }
                                                            </div>
                                                        </div>

                                                        <div className="fieldset_group phone-fieldset"
                                                             style={{textAlign: 'left'}}>
                                                            <div style={{padding: '5px'}}></div>
                                                            <div className="x-phone-replace" data-select2-id="9">
                                                                <div className="x-flex-row" data-select2-id="8">
                                                                    <PhoneInput
                                                                        style={{
                                                                            height: '40px',
                                                                            border: '1px solid #b4b3b3',
                                                                            borderRadius: '5px',
                                                                            padding: '8px',
                                                                            width: '100%'
                                                                        }}
                                                                        placeholder="Enter phone number"
                                                                        defaultCountry={this.props.country.country}
                                                                        international
                                                                        onChange={(e) => {this.setState({phoneValue : e})}}
                                                                        value={this.state.phoneValue}

                                                                    />
                                                                    {
                                                                        this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                            Telefonnummer</label> : null
                                                                    }

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='x-forms' style={{textAlign: 'left'}}>
                                                            <div className=" ">

                                                                <label htmlFor="x-form-label" className="checkbox">
                                                                    <input type="checkbox"
                                                                           name="checked"
                                                                           id="cb_cond_1_0"
                                                                           checked={this.state.checked}
                                                                           onChange={this.handleInputChange}
                                                                           className="field-error"/>&nbsp;
                                                                    <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                                </label>
                                                                {
                                                                    this.state.checkedErr ?
                                                                        <label
                                                                            className="x-error-red">Verpflichtend</label> : null
                                                                }

                                                            </div>
                                                        </div>


                                                        <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                             data-editable="" data-name="step1-send"
                                                             data-ce-tag="button">
                                                            <button type="button" id="lead-form-submit"
                                                                    onClick={this.handleRegistration}
                                                                    style={{
                                                                        background: 'linear-gradient( 0deg ,#a06715,#fbaf41)',
                                                                        borderColor: '#9b6517',
                                                                        width: '100%',
                                                                        textTransform: 'uppercase',
                                                                        color: 'white',
                                                                        height: '50px',
                                                                        position :'relative',
                                                                        zIndex:'9999'

                                                                    }}
                                                                    className="lead-form-submit">
                                                                Jetzt registrieren
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                                <div className="disclaimer-wrapper hidden">
                                                    <p className="form-disclaimers">*By submitting you confirm that
                                                        you’ve read
                                                        and accepted the <a data-intgrtn-link-agreements="3"
                                                                            href="https://tradedigitalapp.com/bitcoinprofit/lp.php"
                                                                            className="" data-i18n="">privacy
                                                            policy</a> and <a
                                                            data-intgrtn-link-agreements="1"
                                                            href="https://tradedigitalapp.com/bitcoinprofit/lp.php"
                                                            className=""
                                                            data-i18n="">terms of conditions</a>.</p>
                                                    <p className="form-disclaimers">**By submitting this form, I agree
                                                        to
                                                        receive all marketing material by email, SMS and telephone.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>


                        </div>
                    </div>


                </Container>

                <Container>
                    <Row>
                        <Col className='sec n' style={{textAlign: 'center'}}>
                            <div data-w-id="b7d3a7fc-b2b2-3713-8a3b-fa5ee8a6ba0b" className="card"
                                 style={{backgroundColor: 'unset', border: '0px'}}
                            >

                                <img
                                    className='image-bitcoin'
                                    style={{height: '150px',
                                        }}
                                    src={Bitcoin}
                                />
                            </div>

                            <h1 className="heading">Dem Bitcoin-System beitreten </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <div className="div-block-3" style={{textAlign: 'center'}}><img style={{height: '80px'}}
                                                                                            src={P256} alt="Bitcoin"
                                                                                            data-w-id="7fd9725c-80c6-50f7-f5cf-3c3cb3b39f0b"
                                                                                            className="image"/>
                                <div className="text-block-4"><strong className="blue">Das
                                    Bitcoin-System</strong> ist eine Gruppe, die ausschließlich Menschen
                                    vorbehalten ist, die auf die wahnsinnigen Renditen von Bitcoin
                                    aufmerksam gemacht wurden und dabei leise ein Vermögen gesammelt haben.
                                </div>
                            </div>
                        </Col>

                        <Col md={6}>
                            <div className="div-block-3" style={{textAlign: 'center'}}><img style={{height: '80px'}}
                                                                                            src={P256} alt="Bitcoin"
                                                                                            data-w-id="7fd9725c-80c6-50f7-f5cf-3c3cb3b39f0b"
                                                                                            className="image"/>
                                <div className="text-block-4"><strong className="blue">Mitglieder des
                                    Bitcoin-Systems</strong> genießen jeden Monat Rückzugsmöglichkeiten auf
                                    der ganzen Welt, während sie mit nur wenigen Minuten „Arbeit“ jeden Tag
                                    Geld mit ihrem Laptop verdienen.
                                </div>
                            </div>
                        </Col>

                    </Row>
                </Container>

                <br/>
                <br/>
                <br/>
                <Container style={{padding: '0px'}} fluid>
                    <div className="text-block-5"
                         style={{
                             backgroundColor: '#fbaf41',
                             color: '#000',
                             fontSize: '35px',
                             lineHeight: '70px',
                             textAlign: 'center'
                         }}
                    >Hier sind einige unserer vergangenen Urlaube
                    </div>
                    <div>
                        <AliceCarousel
                            disableDotsControls
                            disableButtonsControls
                            mouseTracking items={items} responsive={responsive}
                                       controlsStrategy="alternate" />
                    </div>

                </Container>


                <Container style={{padding: '0px'}}>
                    <section className="section section-testimonials">
                        <div className="section-container">
                            <div className="section-content">
                                <div className="section-title" style={{textAlign: 'center'}}>
                                    <h1>
                                        <strong>
                                            Hören Sie von unseren Mitgliedern, die sich auf unsere Software verlassen,
                                            um ihren Luxuslebensstil zu finanzieren

                                        </strong>
                                    </h1>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <ul className="testimonials-list" style={{listStyle: 'none'}}>
                                            <li>
                                                <img src={Testimonial2}/>
                                                <div className="name">
                                                    Joey Feldman
                                                </div>
                                                <div className="description">
                                                    Als ich vor 2 Monaten dem Bitcoin-System beitrat, hätte ich mir nie
                                                    vorstellen können, welche Serie von Ereignissen sich nur wenige Tage
                                                    entwickeln würde, nachdem ich meine kostenlose Software erhalten
                                                    hatte. Ich konnte meine € 131.382 Schulden begleichen. Es gibt kein
                                                    größeres Gefühl, als schuldenfrei zu sein. Jetzt bin ich dabei, mein
                                                    Traumhaus zu kaufen. Ich kann immer noch nicht glauben, dass das
                                                    alles wirklich passiert... Ich bin Steve für immer dankbar.
                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial1}/>
                                                <div className="name">

                                                    Laura Abenstein
                                                </div>
                                                <div className="description">
                                                    Ich bin erst seit 47 Tagen Mitglied im Bitcoin-System. Aber mein
                                                    Leben hat sich bereits verändert! Ich habe nicht nur meine ersten €
                                                    100.000 verdient, sondern auch einige der unglaublichsten Leute
                                                    getroffen. Danke, Steve!
                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial41}/>
                                                <div className="name">
                                                    Michael Zusman
                                                </div>
                                                <div className="description">
                                                    Die Ergebnisse der Software sprechen für sich... wie versprochen,
                                                    habe ich jeden Tag über € 13.000 verdient. Muss ich wirklich mehr
                                                    sagen?
                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial5}/>
                                                <div className="name">
                                                    Lewis H.
                                                </div>
                                                <div className="description">
                                                    Ist das alles wirklich wahr? Ich bin erst vor 2 Tagen beigetreten
                                                    und mein Kontostand ist bereits auf atemberaubende € 27.484,98
                                                    gestiegen.
                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial4}/>
                                                <div className="name">

                                                    Paulo Fonzi
                                                </div>
                                                <div className="description">
                                                    Gestern habe ich meinen Job gekündigt... und heute bin ich auf einer
                                                    Poolparty in Vegas! Das Leben ist verrückt. Und das alles dank des
                                                    Bitcoin-Systems. DANKE STEVE!
                                                </div>
                                            </li>
                                        </ul>
                                    </div>


                                    <div className="col-md-6">
                                        <ul className="testimonials-list" style={{listStyle: 'none'}}>
                                            <li>
                                                <img src={Testimonial6}/>
                                                <div className="name">
                                                    Chris Hadid
                                                </div>
                                                <div className="description">
                                                    Ich bitte meine Frau, mich jeden Morgen zu kneifen, wenn ich
                                                    aufwache und meinen Kontostand überprüfe. Ich habe noch nie eine so
                                                    große Zahl auf meinem Bankkonto gesehen. Und sie wächst und wächst
                                                    einfach weiter... das ist es, worauf ich mein ganzes Leben gewartet
                                                    habe. Jetzt, da ich eine Kostprobe davon habe, wie es sich wirklich
                                                    anfühlt, mein eigener Chef zu sein und jede Woche Zehntausende von
                                                    Dollar zu verdienen, werde ich nie wieder zurückblicken!
                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial7}/>
                                                <div className="name">
                                                    Daniel Pelts
                                                </div>
                                                <div className="description">
                                                    Überraschenderweise war ich früher ein Investor an der Wall Street.
                                                    Und so etwas habe ich in meiner 10-jährigen Tätigkeit im Unternehmen
                                                    noch nie gesehen. Meine Kollegen hielten mich alle für verrückt, als
                                                    ich die Firma verließ, um in Vollzeit mit der
                                                    Bitcoin-System-Software zu investieren. € 384.594 an Gewinnen
                                                    später, alle meine Kollegen bitten mich jetzt, mitmachen zu dürfen.
                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial8}/>
                                                <div className="name">
                                                    Magda Boltyanski
                                                </div>
                                                <div className="description">
                                                    Ich weiß endlich, wie es ist, den Traum zu leben. Ich habe nicht
                                                    mehr das Gefühl, dass ich außen vorgelassen bin, während alle
                                                    anderen den ganzen Spaß haben. Das Bitcoin-System hat es mir
                                                    ermöglicht, mich vorzeitig zurückzuziehen und den Lebensstil der 1 %
                                                    zu leben.

                                                </div>
                                            </li>
                                            <li>
                                                <img src={Testimonial9}/>
                                                <div className="name">
                                                    Carl Edwards
                                                </div>
                                                <div className="description">
                                                    Vor zwei Wochen wurde ich entlassen. Da es keine Optionen mehr gab,
                                                    dachte ich, mein Leben sei vorbei. Jetzt verdiene ich jeden Tag über
                                                    € 13.261,42. Und zum ersten Mal seit 2 Monaten ist mein Konto nicht
                                                    überzogen. Danke, STEVE!
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <Row>
                        <Col md={6}>
                            <img src={SteveMcKay}

                                 style={{width: '100%'}}
                                 alt="" className="image-2"/>
                        </Col>

                        <Col md={6}>
                            <h1 style={{
                                marginTop: '0px',
                                marginBottom: '10px',
                                color: '#fbaf41',
                                fontSize: '30px',
                                lineHeight: '30px',
                                textAlign: 'left'
                            }
                            }><strong>Lernen Sie Steve McKay kennen</strong><br/></h1>
                            <p className="paragraph-3" ><em style={{
                                color: '#0c5275',
                                fontStyle: 'normal',
                                fontSize:'18px'
                            }} className="italic-text">Das Genie hinter dem
                                Bitcoin-System.‍</em>‍<br/>Hi - Ich bin ein ehemaliger Softwareentwickler für eine große
                                Firma, deren Namen ich lieber nicht preisgeben möchte.<br/>‍<br/>Ich habe eine
                                Bitcoin-Trading-Software entwickelt, die allein in den letzten 6 Monaten über <span
                                    className="dudecurrency ">€</span> 18.484.931,77 an Gewinnen erwirtschaftet
                                hat.<br/>Diese
                                Software sorgt schneller für Millionäre als frühe Investoren von Uber, Facebook
                                oder AirBnB.<br/>‍<br/>Wenn Sie eine Million mit Bitcoin verdienen möchten,
                                schauen Sie sich das Video oben an, um zu erfahren, wie es geht.<br/>Ihr
                                Freund,<br/>Steve McKay</p>
                            <img align="right"  style={{width:'50%'}}

                                 src={Signature}/>
                        </Col>
                    </Row>

                </Container>
                <br/>
                <br/>
                <Container fluid style={{
                    padding: '55px 10px 0',
                    marginBottom: '0px',
                    paddingBottom: '55px',
                    backgroundColor: '#0c5275'
                }
                }>
                    <Container>
                        <Row>
                            <Col md={3}>

                            </Col>
                            <Col md={6}>
                                <div className="formInnerContainer">
                                    <div className="formContai--ner">
                                        <div data-intgrtn-form-signup="">
                                            <form method="post" id="formfull" action=""  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset"  style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="Vorname"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset"  style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Nachname"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="fieldset" style={{textAlign:'left'}}>
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="E-Mail-Adresse" value=""
                                                               maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset"  style={{textAlign:'left'}}>
                                                    <div style={{padding :'5px'}} > </div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">

                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='x-forms'  style={{textAlign:'left'}}>
                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>


                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="button" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{
                                                                width: '100%',
                                                                fontSize: '1.25em',
                                                                fontWeight: '700',
                                                                boxShadow: '0 2px 0 #1978a5',
                                                                background:' #2ba6df',
                                                                textTransform: 'uppercase',
                                                                whiteSpace: 'normal',
                                                                color: '#ffffff',
                                                                padding: '6px 12px',
                                                                border: '1px solid transparent',
                                                                borderRadius: '4px'
                                                            }}
                                                            className="lead-form-submit">
                                                        Jetzt registrieren
                                                    </button>
                                                </div>
                                            </form>

                                        </div>

                                        <div className="disclaimer-wrapper hidden">
                                            <p className="form-disclaimers">*By submitting you confirm that you’ve read
                                                and accepted the <a data-intgrtn-link-agreements="3"
                                                                    href="https://tradedigitalapp.com/bitcoinprofit/lp.php"
                                                                    className="" data-i18n="">privacy policy</a> and <a
                                                    data-intgrtn-link-agreements="1"
                                                    href="https://tradedigitalapp.com/bitcoinprofit/lp.php" className=""
                                                    data-i18n="">terms of conditions</a>.</p>
                                            <p className="form-disclaimers">**By submitting this form, I agree to
                                                receive all marketing material by email, SMS and telephone.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>

                            <Col md={3}>

                            </Col>
                        </Row>
                    </Container>
                </Container>


                <Footer
                    logoStatus={false}
                />
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(IProfit);

