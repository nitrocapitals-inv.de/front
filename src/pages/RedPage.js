import React from "react";
import {connect} from "react-redux";
import {Container, Row, Col} from "react-bootstrap";
import ReactCustomFlagSelect from "react-custom-flag-select";
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";
import HeaderLogo from '../assets/img/header-logo.webp'
import Brands from '../assets/img/finance-concept.jpg'
import Graph from '../assets/img/graph.png'
import PatmentMethod from '../assets/img/payment-methods2.png'
import {Footer} from "../components/layout/footer";
import {FLAG_SELECTOR_OPTION_LIST} from '../helper/country'
import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'

import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();

class RedPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',

            theposition: '',
            scrollPosition: false,
            fixPosition: false,
            fixPositionHeight: 0,
            phoneValue: '',
            messagesEndRef: React.createRef(),


        };
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop


        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled >= 0.9) {
            const fixTop = height - 550;
            this.setState({
                fixPosition: true,
              // fixPositionHeight: fixTop,
            })
        }else {
            this.setState({
                fixPosition: false,
            })
        }



        if (scrolled > 0) {
            this.setState({
                scrollPosition: true
            })
        } else {
            this.setState({
                scrollPosition: false
            })
        }

    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop / 3 - 60);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {

        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }



        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }


    render() {

        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <>

                <Container fluid style={{padding: '0px'}}>
                    <header style={{
                        height: '90px',
                        paddingTop: '21px',
                        background: '#941a1d',
                    }}>
                        <div className="container">
                            <img src={HeaderLogo}/>
                        </div>
                    </header>
                </Container>
                <Container style={{fontSize: '1.2rem'}}>
                    <Row>
                        <Col xs={12} sm={12} lg={8} md={12}>
                            <div className="l-column">
                                <h1 className="l-column__title">
                                    Einfaches, simples, erschwingliches Investieren: So fangen Sie an
                                </h1>
                                <img style={{width: '100%'}} src={Brands}/>

                                <p className="l-column__desktop">
                                    Registrieren Sie sich und erfahren Sie mehr darüber, wie Investieren
                                    Ihnen helfen kann, Ihre finanziellen Ziele zu erreichen.
                                </p>

                                <p>
                                    Waren Sie schon einmal interessiert an Investition, fühlten sich
                                    aber etwas verunsichert? Denken Sie, Sie benötigen eine hohe Summe
                                    an Bargeld dafür? Oder sind Sie darüber besorgt, dass die Sprache
                                    der Finanzbranche zu verwirrend und voller Fachausdrücke ist?
                                </p>

                                <p>
                                    Heute gibt es Möglichkeiten zu investieren, die einfach, online und
                                    zugänglich sind – hervorragend für Menschen, für die Investieren
                                    etwas Neues ist, und auch für erfahrene Anleger. In diesem Artikel
                                    werden wir das Verfahren erläutern und Ihnen zeigen, wie Sie, mit
                                    einem kleinen Betrag von €250, mit dem Sie schnell und sicher zu
                                    investieren beginnen und Ihr Vermögen steigern können.
                                </p>


                                <button onClick={this._scrollBottom.bind(this)}
                                        className="cta-btn js-scroll">Informationen anfordern
                                </button>
                                <p>
                                    Mit einem Online-Investmenthaus sind Investitionen viel
                                    zugänglicher, als Sie denken. Sie können Ihre ersten Schritte als
                                    Anleger beginnend ab nur €250 machen oder mehr investieren, um
                                    höhere Renditen zu erzielen. Sie können Ihr Geld jeden Monat
                                    hinzufügen, als einmalige Summe oder wann immer Sie möchten.
                                </p>

                                <p className="l-column__desktop">
                                    Registrieren Sie sich und erfahren Sie mehr darüber, wie Investieren
                                    Ihnen helfen kann, Ihre finanziellen Ziele zu erreichen.
                                </p>

                                <p>
                                    Aber wichtiger ist, dass die Flexibilität großartig für Einsteiger
                                    ist – Investitionen können klein und kostengünstig sein, aber
                                    dennoch das Potenzial haben, Ihnen beim Wachstum Ihres Kapitals zu
                                    helfen. Dies wiederum kann Ihnen helfen, große Ziele zu erreichen,
                                    wie beispielsweise die Schaffung einer Altersvorsorge, dem Kauf
                                    eines Hauses, oder kleinerer Ziele wie dem Aufbau von Ersparnissen
                                    für die Zukunft Ihrer Kinder oder der Buchung eines schönen Urlaubs.
                                </p>

                                <div className="l-column__h2">
                                    Vergleichen Sie die potenziellen Erträge aus wachstumsstarken Aktien
                                    mit den durchschnittlichen Bankzinsen.
                                </div>

                                <div className="l-column__graph">
                                    <img src={Graph} alt="graph" style={{width: '100%'}}/>
                                </div>

                                <div className="form form__mobile js-form" style={{padding: '0px'}}>
                                    <div className="form__top">
                                        <div className="form__title">Jetzt investieren</div>
                                    </div>
                                    <div className="form__body">
                                        <div id="formWrapper2" className="formWrapper">
                                            <form method="post" id="formfull" action=""
                                                  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Vorname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="Klaus"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Nachname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Meier"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">E-Mail-Adresse</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="z.B. name@gmail.com" value=""
                                                               maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset">
                                                    <div className="fieldset__title">Telefonnummer</div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">

                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='x-forms'>
                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red" >Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>
                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="button" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{width: '100%'}}
                                                            className="lead-form-submit">
                                                        Informationen erhalten
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div className="form__info">
                                        Um in Amazon zu investieren müssen Sie mindestens 18 Jahre alt
                                        sein. Erforderliches Mindestkapital 250 €.
                                    </div>
                                    <div className="form-mob-padding form__footer" style={{padding: '10px'}}>
                                        <div style={{padding : '50px'}}>
                                            <img style={{width: '100%'}}
                                                 src={PatmentMethod}
                                                 alt="payment methods"/>
                                        </div>
                                    </div>

                                </div>

                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <p className="l-column__desktop">
                                    Registrieren Sie sich und erfahren Sie mehr darüber, wie Investieren
                                    Ihnen helfen kann, Ihre finanziellen Ziele zu erreichen.
                                </p>

                                <p>
                                    Wenn Sie zum Beispiel vor ein paar Jahren €1.000 in eine dieser
                                    Aktien investiert hätten, würden Sie heute auf einem Stapel Geld
                                    sitzen. Sie hätten eine Rendite zwischen €46.670 von Tesla bis
                                    €206,680 von Netflix sowie sagenhafte und unvorstellbare €735.452
                                    von Amazon erhalten.
                                </p>


                                <p>
                                    Interessiert? Wenn Sie jemals über Investieren nachgedacht haben,
                                    jetzt könnte der Zeitpunkt für den Einstieg sein – registrieren Sie
                                    sich, um zu erfahren, wie Sie die ersten Schritte machen.
                                </p>

                                <p>
                                    Wenn Sie über einen mittel- bis langfristigen Zeitraum (mindestens
                                    fünf Jahre) anlegen, könnten Sie mehr Kapital aufbauen, als mit
                                    einem herkömmlichen Sparkonto. Zurzeit entsprechen die Zinsen für
                                    Ersparnisse nicht einmal der Inflationsrate, wodurch die Kaufkraft
                                    Ihres Geldes nicht mit den steigenden Preisen Schritt hält.
                                </p>
                                <button onClick={this._scrollBottom.bind(this)}
                                        className="cta-btn js-scroll">Informationen anfordern
                                </button>
                                <p>
                                    Infolgedessen entdecken immer mehr von uns Investitionen zusätzlich
                                    zu unseren Ersparnissen als Möglichkeit, um mit unserem Geld ohne
                                    große Anstrengungen mehr Rendite zu erzielen.
                                </p>

                                <p className="l-column__desktop">
                                    Registrieren Sie sich und erfahren Sie mehr darüber, wie Investieren
                                    Ihnen helfen kann, Ihre finanziellen Ziele zu erreichen.
                                </p>

                                <p>
                                    In 3 einfachen Schritten können Sie Ihr Anlagekonto in weniger als 4
                                    Minuten einrichten und anfangen mehr Einkommen zu erzielen. Wenn Sie
                                    über den technischen Teil besorgt sind, können wir Sie telefonisch
                                    unterstützen, und als Einsteiger haben an fünf Tagen in der Woche 24
                                    Stunden Zugang zu einem persönlichen Kundenbetreuer, der Sie
                                    unterstützt und alle Fragen beantwortet, die Sie möglicherweise
                                    haben.
                                </p>

                                <button onClick={this._scrollBottom.bind(this)}
                                        className="cta-btn js-scroll">Informationen anfordern
                                </button>

                                <div className="l-column__h2">
                                    Sie sind bereit: Zeit für Ihre erste Investition
                                </div>

                                <p>
                                    Wenn Investitionen etwas Neues für Sie sind, suchen Sie vielleicht
                                    nach einer erschwinglichen und einfachen Option. Aus diesem Grund
                                    ist ein reguliertes Investmenthaus eine besonders gute Option für
                                    Einsteiger.
                                </p>

                                <ul className="l-column-list" style={{listStyle: 'none'}}>
                                    <li className="l-column-list__item">
                                        Es ist ein Online-Service, so dass Sie jederzeit sehen können, wie
                                        Ihre Anlage sich entwickelt.
                                    </li>
                                    <li className="l-column-list__item">
                                        Sie können mit einer Investition ab nur €250 beginnen.
                                    </li>
                                    <li className="l-column-list__item">
                                        Sie können monatliche Zahlungen oder einen Einmalbetrag leisten.
                                    </li>
                                    <li className="l-column-list__item">
                                        Die Gebühren und Kosten sind niedrig, und Sie sehen diese, bevor
                                        Sie Entscheidungen treffen.
                                    </li>
                                    <li className="l-column-list__item">
                                        Sie könnten klein anfangen und sehen, was eine kleine Investition
                                        für Ihr Geld tun kann.
                                    </li>
                                </ul>

                                <div className="l-column__h3" ref={ref}>
                                    Registrieren Sie sich und erfahren Sie mehr darüber, wie Investieren
                                    Ihnen helfen kann, Ihre finanziellen Ziele zu erreichen.
                                </div>

                                <div className="form form__mobile js-form" style={{padding: '0px'}}>
                                    <div className="form__top">
                                        <div className="form__title">Jetzt investieren</div>
                                    </div>
                                    <div className="form__body">
                                        <div id="formWrapper2" className="formWrapper">
                                            <form method="post" id="formfull" action=""
                                                  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Vorname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="Klaus"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Nachname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Meier"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">E-Mail-Adresse</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="z.B. name@gmail.com" value=""
                                                               maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset">
                                                    <div className="fieldset__title">Telefonnummer</div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">

                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }


                                                        </div>
                                                    </div>
                                                </div>

                                                <div className='x-forms'>
                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>

                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="submit" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{width: '100%'}}
                                                            className="lead-form-submit">
                                                        Informationen erhalten
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div className="form__info">
                                        Um in Amazon zu investieren müssen Sie mindestens 18 Jahre alt
                                        sein. Erforderliches Mindestkapital 250 €.
                                    </div>
                                    <div className="form-mob-padding form__footer" style={{padding: '10px'}}>
                                        <div style={{padding : '50px'}}>
                                            <img style={{width: '100%'}}
                                                 src={PatmentMethod}
                                                 alt="payment methods"/>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </Col>

                        <Col xs={12} lg={4} md={12}>
                            {/*<div className={`r-column form-red-page `}*/}
                            <div className={this.state.scrollPosition ? 'r-column form-red-page ' : 'r-column form-red-page'}
                                 style={this.state.fixPosition ? { bottom : '470px'} : this.state.scrollPosition ? {top: '-20px'} : {} }

                            >
                                <div className="form__desktop">
                                    <div className="form__top">
                                        <div className="form__title">Jetzt investieren</div>
                                    </div>
                                    <div className="form__body">
                                        <div id="formWrapper2" className="formWrapper">
                                            <form method="post" id="formfull" action=""
                                                  className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Vorname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="Klaus"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Nachname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Meier"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">E-Mail-Adresse</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="z.B. name@gmail.com" value=""
                                                               maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset">
                                                    <div className="fieldset__title">Telefonnummer</div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">

                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />
                                                            {
                                                                this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                    Telefonnummer</label> : null
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='x-forms'>
                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red">Verpflichtend</label> : null
                                                        }

                                                    </div>
                                                </div>


                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="button" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{width: '100%'}}
                                                            className="lead-form-submit">
                                                        Informationen erhalten
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                    <div className="form__info">
                                        Um in Amazon zu investieren müssen Sie mindestens 18 Jahre alt
                                        sein. Erforderliches Mindestkapital 250 €.
                                    </div>
                                    <div className="form-mob-padding form__footer" style={{padding: '10px'}}>
                                        <div style={{padding : '50px'}}>
                                            <img style={{width: '100%'}}
                                                 src={PatmentMethod}
                                                 alt="payment methods"/>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </Col>
                    </Row>
                </Container>

                <div style={{height: '50px'}}>

                </div>
                <Footer
                    logoStatus={false}
                />

            </>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(RedPage);

