import {Container, Row, Col} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import Moment from 'react-moment';
import 'moment/locale/de';

import Logo_WSM from '../assets/img/logo_WSM.png'
import Logo_WSM_WHITE from '../assets/img/logo_WSM-white.png'
import Likethumb from '../assets/img/likethumb.png'
import Main from '../assets/img/main.jpg'
import User from '../assets/img/user.jpg'
import F41554_50302938_1878686864_q from '../assets/img/41554_50302938_1878686864_q.jpg'
import F370176_564964504_308463864_q from '../assets/img/370176_564964504_308463864_q.jpg'
import F157689_1027278331_1478344009_q from '../assets/img/157689_1027278331_1478344009_q.jpg'
import F157804_21416303_1043059674_q from '../assets/img/157804_21416303_1043059674_q.jpg'
import F572741_30110787_2084442239_q from '../assets/img/572741_30110787_2084442239_q.jpg'
import F174008_50902984_682021130_q from '../assets/img/174008_50902984_682021130_q.jpg'
import F273930_20904468_1027986766_q from '../assets/img/273930_20904468_1027986766_q.jpg'
import F173211_1135451090_1466382495_q from '../assets/img/173211_1135451090_1466382495_q.jpg'
import F369223_12411516_333332392_q from '../assets/img/369223_12411516_333332392_q.jpg'
import F371738_1363268399_1637317047_q from '../assets/img/371738_1363268399_1637317047_q.jpg'
import F48783_12401144_1332233149_q from '../assets/img/48783_12401144_1332233149_q.jpg'

import F187364_20501998_2048679844_q from '../assets/img/187364_20501998_2048679844_q.jpg'
import F273549_7706291_1106946751_q from '../assets/img/273549_7706291_1106946751_q.jpg'
import F370345_7008369_2025512953_q from '../assets/img/370345_7008369_2025512953_q.jpg'
import F371925_1426200070_1825128294_q from '../assets/img/371925_1426200070_1825128294_q.jpg'
import F275712_1815883270_368899092_q from '../assets/img/275712_1815883270_368899092_q.jpg'
import F371788_39603151_990746142_q from '../assets/img/371788_39603151_990746142_q.jpg'
import F370953_20903876_26789988_q from '../assets/img/370953_20903876_26789988_q.jpg'
import F173605_1387563113_14543618_q from '../assets/img/173605_1387563113_14543618_q.jpg'
import F70524_1387164496_88414351_q from '../assets/img/70524_1387164496_88414351_q.jpg'
import F369872_722424386_1857330401_q from '../assets/img/369872_722424386_1857330401_q.jpg'
import F224406_100629153374069_2784614_n from '../assets/img/224406_100629153374069_2784614_n.jpg'
import F224406_100629153374069_2784614_r from '../assets/img/224406_100629153374069_2784614_r.jpg'
import F224406_100629153374069_2784614_l from '../assets/img/224406_100629153374069_2784614_l.jpg'
import F224406_100629153374069_2784614_p from '../assets/img/224406_100629153374069_2784614_p.jpg'
import F224406_100629153374069_2784614_c from '../assets/img/224406_100629153374069_2784614_c.jpg'

import '../assets/css/facebook.css'

function Remgue1() {

    return (
        <Container fluid style={{backgroundColor: '#ececec'}}>
            <Container style={{
                backgroundColor: 'white'
            }}>
                <div id="header_rangu">
                    <div className="container mgid-hide">
                        <div className="row">
                            <div className="col-sm-12" style={{padding :'0px'}}>

                                <div className="clearfix"></div>
                                 <ul className="list-unstyled main-menu">
                                    <li className="logo"><a
                                        href="remgue_22_de"
                                        target="_blank"><img src={Logo_WSM}
                                                             className="img-responsive mobile-off"/><img
                                        src={Logo_WSM_WHITE}
                                        className="img-responsive margin mobile-on" /></a></li>
                                    <li className="hidden-sm hidden-xs"><a
                                        href="remgue_22_de"
                                        target="_blank">Märkte</a><i className="glyphicon glyphicon-chevron-down"></i>
                                    </li>
                                    <li className="hidden-sm hidden-xs"><a
                                        href="remgue_22_de"
                                        target="_blank">Ihre Finanzen</a><i
                                        className="glyphicon glyphicon-chevron-down"></i></li>
                                    <li className="hidden-sm hidden-xs"><a
                                        href="remgue_22_de"
                                        target="_blank">Business</a><i className="glyphicon glyphicon-chevron-down"></i>
                                    </li>
                                    <li className="hidden-sm hidden-xs"><a
                                        href="remgue_22_de"
                                        target="_blank">Forum</a><i className="glyphicon glyphicon-chevron-down"></i>
                                    </li>
                                     <br/>
                                     <br/>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col>
                        <p className="date">
                            <Moment  format="DD.MM.YYYY" date={new Date()} />
                        </p>
                        <h1>Vom armen Oberschüler zum Millionär. Seine Methode ermöglicht es jedem, 10.510 Euro in 21 Tagen zu verdienen</h1>
                    </Col>
                </Row>
                <Row>
                    <Col md={8}>
                        <div style={{padding :'5px'}}>
                        </div>
                        <img src={Main} style={{width : '100%'}} alt="" className="img-responsive" />

                        <p className="bold" style={{fontWeight:'bold'}}>Die neue Methode ermöglicht es Ihnen, 10 510 Euro in
                            21 Tagen zu gewinnen. Und das ist nur der Anfang, denn nach einem Monat
                            können Sie 15.000 und in einem Jahr sogar 100.000 Euro haben.
                            Unabhängig von Ausbildung, Alter oder finanziellen Kenntnissen. Die
                            Methode wurde bereits von 27.000 Deutschen getestet. Sie alle verdienten
                            sehr viel Geld und wurden ihre Probleme mit Geldmangel los. Das alles
                            ist dem Engagement eines Oberschülers aus Solingen zu verdanken. Ein
                            junges Genie, das um jeden Preis seine Familie vor der Armut retten
                            wollte.
                        </p>

                        <p>Diese Geschichte zeigt, wie unvorhersehbar das Schicksal eines
                            jungen Mannes ist. Noch als Oberschüler beschloss Thomas Kühn, sein
                            Leben zu ändern und großen Erfolg zu erzielen. Sowohl Lehrer als auch
                            Kollegen lachten jedoch über seine Entdeckung - <strong>eine bahnbrechende Methode, mit der
                                Sie in 21 Tagen 10.510 Euro verdienen können</strong>.
                            Oft hatte er von ihnen gehört: „Sei nicht albern. Finde besser einen
                            Job nebben der Schule!”. Die Lehrer verspotteten ihn. Sie glaubten
                            nicht, dass man im Internet viel Geld verdienen kann, ohne die
                            Grundlagen der Finanzen zu kennen.
                        </p>
                        <p className="bold subtitle">„Sie lachten mich aus und erniedrigten mich...”</p>
                        <p>Solche Kommentare ließen den Jungen zuerst zusammenbrechen. Er
                            wusste nicht mehr, ob seine Entdeckung wirklich etwas wert war. Er schob
                            die Idee wochenlang beiseite. Die Tragödie in seiner Familie ließ ihn
                            jedoch zu ihrer Umsetzung zurückkehren. Er begann Tag und Nacht an
                            seiner Methode zu arbeiten. Nach einem Monat fühlte er sich endlich
                            bereit, am schulübergreifenden Wettbewerb für Jugendliche mit
                            Unternehmergeist teilzunehmen. <strong>Dort wollte er seine Methode für 10.510 Euro Gewinn
                                vorstellen. Und es war ein echter Durchbruch für Thomas.</strong>
                            Obwohl er diesen Wettbewerb nicht gewonnen hat. Niemand interessierte
                            sich für seine Entdeckung. Klassenkameraden und Lehrer verspotteten ihn
                            weiterhin. Alle unterschätzten seine Berechnungen. Die Jury sah über
                            seine Arbeit weg. Trotzdem wird sein Vermögen heute auf... 6 Millionen
                            Euro geschätzt!
                        </p>

                        <p>Thomas wurde von Experten verspottet, die sein Genie nicht
                            erkannten. Er wiederum beschloss, seine Entdeckung anders zu nutzen. Er
                            schwor sich, dass gewöhnliche Menschen diese einzigartige Methode zu
                            schätzen wissen würden.
                        </p>
                        <p className="bold subtitle">Sie verdienen 10.510 Euro und viel mehr</p>
                        <p>Die von Thomas Kühn entwickelte Methode ist einfach anzuwenden
                            und allgemein zugänglich. Auch ältere Menschen können damit umgehen.
                            Diese Methode wird problemlos von Menschen ohne Ausbildung angewendet,
                            die nicht wissen, wie man viel Geld verdient. Dennoch wird ihrem Konto
                            mindestens 10.510 Euro gutgeschrieben. Viele von ihnen verdienen viel,
                            viel mehr und müssen nicht ihr Haus verlassen. Es ist alles nur dank
                            einer einfachen Methode.
                        </p>
                        <p>Die Oberschule, die Thomas bis vor kurzem besuchte, lehnte es ab,
                            sich zu dem Fall zu äußern. Wir konnten jedoch einen der dort tätigen
                            Lehrer kontaktieren. Er bat darum, dass sein Name nicht auf unserer
                            Website veröffentlicht wird. Der Mann verriet, dass der Lehrer, der über
                            die Entdeckung des begabten Schülers lachte, deswegen seinen Job
                            verlieren könnte. Das geschah nicht, weil Thomas sich beim Schulleiter
                            für ihn einsetzte.
                        </p>
                        <p className="bold subtitle">„Ich freue mich, dass ich anderen helfen kann”</p>
                        <p>Das junge Genie fand für uns Zeit für ein Telefoninterview, in
                            dem er sein Vorgehen erklärt. Auf die Frage, warum er sich für den
                            Lehrer eingesetzt hat, antwortet er:
                        </p>
                        <p>„Leute machen Fehler. Wir sollten ihnen immer eine zweite Chance
                            geben. Das haben mir meine Eltern beigebracht. Ich komme aus einer armen
                            Familie. Ich lerne viel und fleißig, weil ich mir und ihnen eine
                            bessere Zukunft sichern möchte. Ich habe mir immer sehr gewünscht, dass
                            meine Eltern sich ein schönes Haus, ein komfortables Auto und Urlaube
                            leisten können, die sie nie gemacht haben. Ich glaubte, dass die
                            Methode, die ich entdeckt hatte, dies bewirken würde. Sie verstehen
                            also, dass ich unbedingt an dem schulübergreifenden Wettbewerb für
                            Jugendliche mit Unternehmergeist teilnehmen wollte. Auch wenn ich keine
                            Unterstützung hatte. Ich bin aber froh, dass ich dort jemanden getroffen
                            habe, der an meine Idee geglaubt hat.&nbsp;<strong>Dadurch konnte ich
                                nicht nur meinen Eltern, sondern auch Tausenden von anderen Menschen
                                helfen. Sie hatten oft keine Hoffnung, jemals auch nur 1500 Euro pro
                                Monat zu verdienen, geschweige denn 10.510 Euro in 21 Tagen. Jetzt
                                erfüllen diese Menschen alle ihre Bedürfnisse und helfen ihren
                                Familienangehörigen finanziell.</strong>”
                        </p>
                        <p>Trotz der Tatsache, dass früher niemand das Genie des Jungen zu
                            schätzen wusste, kämpfen heute die größten Finanzunternehmen um seine
                            Methode. Der junge Mann hat bereits einige der Angebote wahrgenommen und
                            wurde so zum jüngsten Millionär in Deutschland. Er betont aber immer
                            wieder, dass er nicht nur in die eigene Tasche arbeitet. Es freut sich,
                            dass er anderen Menschen helfen kann: "Während Experten über meine
                            Entdeckung lachten, wussten die einfachen Deutschen sie zu schätzen. Für
                            sie habe ich die Möglichkeit geschaffen, schnell und einfach Geld zu
                            verdienen. Ich helfe anderen gerne, denn das haben mir meine Eltern
                            beigebracht. Das sind die Werte, denen ich in meinem Leben folgen
                            möchte." Thomas Kühn hat bereits so viel verdient, dass er sich nicht um
                            Geld kümmert und versichert, dass seine Methode auch weiterhin für
                            jeden zugänglich sein wird. Wir können uns nur fragen, womit uns dieser
                            außergewöhnlich einfühlsame junge Mann in ein paar Jahren überraschen
                            wird!
                        </p>


                        <p><strong>Jetzt kann jeder diese einfache Methode anwenden und in 21 Tagen mindestens 10.510
                            Euro mit Online-Währungen verdienen.</strong></p>
                        <NavLink to="remgue_22_de"
                           target="_blank"> <img src={User} style={{width : '100%'}} alt="" className="img-responsive" />
                        </NavLink>
                        <p className="under_picture">Herr Markus (58 Jahre alt) aus Kiel kaufte
                            sein Traumauto nach ein paar Wochen des Geldverdienens dank der Methode
                            von Thomas Kühn
                        </p>


                        <p className="bold subtitle">27.000 Deutsche verdienen sehr viel Geld</p>
                        <p>Seit einem Monat ist die von dem jungen Genie entwickelte Methode
                            in unserem Land erhältlich. Das Interesse ist jedoch so groß, dass <strong>bereits 27.000
                                Deutsche sie genutzt haben</strong>.&nbsp;Methoden,
                            die der Erfindung von Thomas Kühn ähnlich sind, begannen ebenfalls in
                            großer Zahl auf dem Markt zu erscheinen. Warum ist das Interesse so
                            groß?
                        </p>
                        <p>Prof. Adam Wagner, ein Spezialist für Wirtschaft und Finanzen, erklärte sich bereit, unsere
                            Frage zu beantworten.</p>
                        <p>„Die Deutschen wenden die Methode von Thomas Kühn gern an. Sie
                            verdienen damit große Summen und verbessern ihre finanzielle Situation
                            dauerhaft. Wenn ich mit ihnen darüber spreche, warum sie sich überhaupt
                            für diese Erfindung entschieden haben, nennen sie oft diese Argumente:
                        </p>
                        <ul className="list-unstyled check_list">
                            <li><strong>Erstens müssen Sie nicht über ein großes Finanzwissen verfügen.</strong>
                                Sie müssen praktisch nichts darüber wissen, denn der Algorithmus von
                                Thomas Kühn verarbeitet all die komplizierten Daten von selbst. Und am
                                Ende präsentiert es Ihnen eine fertige Lösung. Es ist ein sehr einfacher
                                Weg, mit Online-Währungen Geld zu verdienen, ohne jegliche Kenntnisse
                                über Finanz- oder Investmentwelt. So wie Sie keine Kenntnisse im
                                Motorenbau brauchen, um ein Auto zu fahren, brauchen Sie hier keine
                                finanziellen Kenntnisse, um ein festes Einkommen zu haben.
                            </li>
                            <li><strong>Zweitens: Sie riskieren nicht Ihre Ersparnisse.</strong>
                                Der große Vorteil von Online-Währungen ist, dass schon 100-200 Euro
                                ausreichen, um viel mehr zu verdienen. Es ist auch eine großartige
                                Option für Leute, die anfangs nicht viel Geld ausgeben wollen. Und das
                                müssen sie nicht einmal tun, denn dank des Algorithmus können sie auch
                                mit kleinen Beträgen 200%, 300% oder sogar 500% verdienen. In kürzester
                                Zeit und ohne Ihre Ersparnisse zu riskieren.
                            </li>
                            <li><strong>Drittens: Diese Methode erfordert keinen Aufwand.</strong>
                                Der Algorithmus erledigt alles für Sie. Alles, was Sie tun müssen, ist
                                3-5 Minuten pro Tag ihm zu widmen. Dann können Sie einfach beobachten,
                                wie Ihre Gewinne wachsen. Sie werden viel Zeit für Familie und Freunde
                                haben. Während Sie sich entspannen, wird das Geld auf Ihr Konto
                                überwiesen.
                            </li>
                        </ul>
                        <p>Viele Deutsche haben bereits erfahren, wie einfach es ist, den
                            Algorithmus zu nutzen und verdienen gern mit Online-Währungen. Thomas
                            Kühns Methode half ihnen riesige Gewinne zu erzielen. Und bis vor kurzem
                            klagten viele Menschen über ihre finanzielle Situation.
                        </p>
                        <p>Wie konnte sich ihr Leben in so kurzer Zeit verändern? Alles nur,
                            weil die Methode, die unser junges Genie entdeckt hat, Geldprobleme
                            dauerhaft löst. Fangen Sie einfach an, sie zu benutzen und nach 21 Tagen
                            werden Sie 10.510 Euro auf Ihrem Konto haben. Aber das ist nicht alles!
                            Nach einem Monat werden Sie 15.000 Euro haben. Und dann noch mehr.
                            Alles hängt davon ab, wie lange Sie die Methode anwenden.”
                        </p>
                        <p>Prof. Wagner warnt, dass Menschen, die die Thomas Kühns Methode
                            anwenden wollen, nicht auf ähnliche Methoden hereinfallen sollten, die
                            im Internet zu finden sind. Sie haben nichts mit der Erfindung des
                            jungen Genies zu tun. Sie sind oft unwirksam, erfordern einen hohen
                            Geldbetrag oder man muss einen ungünstigen Vertrag unterschreiben. „Sie
                            mögen zwar sehr vielversprechend aussehen und enorme Gewinne bringen,
                            aber da endet es schon auch. Leider gab es schon Fälle von Leuten, die
                            solche dubiosen Methoden benutzt haben, um Geld zu verlieren, anstatt es
                            zu gewinnen. Es lohnt sich also nicht zu riskieren” – kommentiert der
                            Professor.
                        </p>

                        <fieldset className="attentionBorder">
                            <legend>ACHTUNG</legend>
                            <p>Deshalb stellen wir unseren Lesern einen verifizierten <strong>Link
                                zur Verfügung, unter dem Sie die Originalmethode von Thomas Kühn finden
                                können. Er ermöglicht jedem, 10.510 Euro in 21 Tagen zu
                                verdienen.</strong> Erwähnenswert ist, dass man auf dieser Seite <strong>die 50%
                                Finanzierung in Anspruch nehmen kann</strong>, die der junge Erfinder speziell für
                                Interessenten aus seinem Heimatland ausgehandelt hat.
                            </p>
                        </fieldset>

                        <p className="cta">
                            <NavLink
                            to={'remgue_2_de'}
                            target="_blank">&gt;&gt; Klicken Sie hier, um mindestens 10.510 Euro in 21 Tagen zu
                            verdienen &gt;&gt;</NavLink>

                        </p>


                        <div id="feedback_1HsYymlsW4NLzXtW1"  >
                            <div className="fbFeedbackContent" id="uz1cxy_1">

                                <div className="stat_elem">
                                    <div className="uiHeader uiHeaderTopBorder uiHeaderNav composerHider">
                                        <div className="clearfix uiHeaderTop">
                                            <a href="remgue_22_de"
                                               target="_blank" className="uiHeaderActions rfloat">Kommentar
                                                hinzufügen</a>
                                            <div>
                                                <h4 tabIndex="0" className="uiHeaderTitle">
                                                    <div
                                                        className="uiSelector inlineBlock orderSelector lfloat uiSelectorNormal">
                                                        <div className="-">
                                                            <a href="remgue_22_de"
                                                               target="_blank"
                                                               className="uiSelectorButton uiButton uiButtonSuppressed"
                                                               rel="toggle" aria-expanded="false" aria-haspopup="1"
                                                               role="button" data-label="683 comments" data-length="60"><span
                                                                className="uiHeaderActions">Letze Kommentare auf Facebook</span></a>
                                                            <div className="uiSelectorMenuWrapper uiToggleFlyout">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span className="phm indicator"></span>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul className="uiList fbFeedbackPosts"
                                    style={{listStyle : 'none'}}
                                >
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de"
                                               tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                     src={F41554_50302938_1878686864_q}
                                                                                     alt="" /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Alex</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Hey, hat schon jemand probiert?</div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de"
                                                                   target="_blank" id="uz1cxy_5">Antworten</a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de"
                                                                   target="_blank" className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb} /> </i>
                                                                    <span className="text">13</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de"
                                                                   target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    12 Minuten</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true">
                                                <img className="img"
                                                     src={F370176_564964504_308463864_q}
                                                                                                        alt="" /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Tobias</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Ich habe 10 Euro eingezahlt und warte
                                                            jetzt. Nach 14 Tagen bin ich jetzt auf 1200Eur gestiegwn,
                                                            ich würde
                                                            jetzt schon auszahlen, da ich die Kohle brauche, aber es
                                                            wird immer
                                                            mehr, also muss ich mich etwas gedulden
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank">Antworten</a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb} /></i><span
                                                                    className="text">6</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    13 Minuten</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F157804_21416303_1043059674_q}
                                                                                                        alt="" /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Error</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Mich übezeugt's nicht. Ich ziehe es
                                                            vor, mein Geld in der Bank zu vermehren
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">19</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    25 Minuten</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="postReplies fsm fwn fcg">
                                                    <div id="uz1cxy_4">
                                                        <ul className="uiList fbFeedbackReplies">
                                                            <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbCommentAntworten uiListItem  uiListVerticalItemBorder"
                                                                id="fbc_10150877337728759_22500369_10150877995903759_Antworten">
                                                                <div className="UIImageBlock clearfix">
                                                                    <a href="remgue_22_de" target="_blank"
                                                                       className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                                                       tabIndex="-1" aria-hidden="true"><img
                                                                        className="img"
                                                                        src={F157689_1027278331_1478344009_q}
                                                                        alt="" /></a>
                                                                    <div
                                                                        className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                                        <div className="postContainer fsl fwb fcb">
                                                                            <a target="_blank" className="profileName"
                                                                               href="remgue_22_de">Sotnes an Error</a>
                                                                            <div className="postContent fsm fwn fcg">
                                                                                <div className="postText">Auf der
                                                                                    Bankeinlage hast du gewisse
                                                                                    Verluste du Honk, schau dir doch die
                                                                                    Inflation und Steuern auf den
                                                                                    Balken an, das frisst den ganzen
                                                                                    Gewinn. Und dein Geld rottet auf
                                                                                    deinem
                                                                                    Konto, anstatt selbst zu verdienen
                                                                                </div>
                                                                                <div className="stat_elem">
                                                                                    <div
                                                                                        className="action_links fsm fwn fcg">
                                                                                        <a href="remgue_22_de"
                                                                                           target="_blank"
                                                                                           id="uz1cxy_8">Antworten</a>
                                                                                        <span
                                                                                            className="dotpos">.</span>
                                                                                        <a href="remgue_22_de"
                                                                                           target="_blank"
                                                                                           className="fbUpDownVoteOption hidden_elem"
                                                                                           rel="async-post">Gefällt
                                                                                            mir!</a>
                                                                                        <span
                                                                                            className="dotpos">.</span>
                                                                                        <abbr
                                                                                            title="Thursday, May 31, 2012 at 4:23am"
                                                                                            data-utime="1338463406"
                                                                                            className="timestamp">Vor 46
                                                                                            Minuten</abbr>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="postReplies fsm fwn fcg"></div>
                                                                        <div className="fsm fwn fcg"></div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F572741_30110787_2084442239_q}
                                                                                                        alt=""/></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Phibi</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Wenn Sie etwas verdienen wollen,
                                                            müssen Sie
                                                            mindestens 10-15% Gewinn haben, so dass es tatsächlich
                                                            finanzielle
                                                            Ergebnisse gibt. Die Anzahlung beträgt 3% und ist ne
                                                            Lachnummer... und
                                                            hier sieht es sehr vielversprechend aus, ich habe
                                                            beschlossen, dass ich
                                                            in ein paar Hunds reinwerfen werde
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">53</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    etwa einer Stunde</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F174008_50902984_682021130_q}
                                                                                                        alt=""/></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName" href="remgue_22_de">Allar
                                                        an Tobias</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">aber wie funzt das?</div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">3</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    1 Stunde</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F370176_564964504_308463864_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName" href="remgue_22_de">Tobias
                                                        an Allar</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Ganz einfach, man muss nur beobachten
                                                            wenn
                                                            der Wert der Währung wächst. Alles, was man dann tun muss,
                                                            ist
                                                            reinzugehen und zu sehen, obs aufm Plus oder Minus ist. Man
                                                            braucht
                                                            dafür kein Mathe oder Wirtschaftswissenschaften zu kennen
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">1</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F273930_20904468_1027986766_q}
                                                                                                        alt=""/></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Merry</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">
                                                            Es ist wahr, das ist nicht neu, Tausende von Menschen haben
                                                            bereits Geld an digitalen Währungen verdient. Schauen Sie
                                                            sich Bitcoin
                                                            an, jeder, der es vor ein paar Jahren gekauft hat, hat jetzt
                                                            Geld. Aber
                                                            jetzt ist es zu spät für Bitcoin, weil es zu teuer ist. Sie
                                                            müssen nach
                                                            anderen Lösungen suchen. Diese Methode ist sehr interessant
                                                            und bringt
                                                            wirklich tolle Ergebnisse. Bis zu 500-1000% Rendite in einem
                                                            Monat
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">12</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F173211_1135451090_1466382495_q}/></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">VernunftPur</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">
                                                            Ich habe schon ein paar Kryptos probiert und tatsächlich
                                                            habe
                                                            ich ein paar Hundert Prozent davon verdient. Mit der
                                                            Tatsache, dass ich
                                                            aufgrund von Marktschwankungen keine großen Geldsummen
                                                            anlegen würde
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">30</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F369223_12411516_333332392_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">ASS</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Ich bestätige es, ich habe 100
                                                            eingezahlt, 2500 Euronen rausgezogen und sitze jetzt auf
                                                            Malle :D:D
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">53</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F371738_1363268399_1637317047_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Sotnes</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">gutes System, ich spiel damit seit 2
                                                            Wochen rum - Startkapital um 4000 vermehrt
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">16</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F48783_12401144_1332233149_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName" href="remgue_22_de">Thommy
                                                        60+</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">ch bin 63 Jahre alt und hatte keine
                                                            Ahnung,
                                                            dass ich mich wieder so aufregen würde. Vorläufig habe ich
                                                            in diese
                                                            Internetwährung 1.200 Euro investiert und mein Geld
                                                            verdreifacht. Da ich
                                                            in ein oder zwei Monaten weitermachen werde, werde ich eine
                                                            Wohnung für
                                                            meine Tochter haben
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">2</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>




                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F187364_20501998_2048679844_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Worzz</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Ich kann es wahrlich jedem empfehlen.
                                                            Ich
                                                            dachte, es wäre nicht für mich, weil ich nicht weiß, ich
                                                            habe eine
                                                            hoffnungslose Geschäftsidee. Als ich es zum ersten Mal
                                                            gelesen habe,
                                                            dachte ich, wenn es nicht investiert, kann es überprüft
                                                            werden. Für
                                                            jetzt bin ich 125 EUR voraus und dies ist das erste Geld,
                                                            das ich
                                                            verdient habe, nicht mit einer Schaufel winken, ich zähle
                                                            das nicht die
                                                            letzte
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">11</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>


                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F273549_7706291_1106946751_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">~mona3</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">
                                                            Es funktionierte für meinen Kumpel, es funktioniert für
                                                            alle.
                                                            Er hat keine Ahnung von Wirtschaft und im Internet hat er
                                                            ein Auto und
                                                            eine Wohnung gebaut
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">33</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    2 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>


                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F370345_7008369_2025512953_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Dr_Cognac</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">
                                                            Ich gebe meinen Senf dazu. Ich hatte vor Kurzem darüber eine
                                                            Vorlesung an der Uni. Es stellt sich heraus, dass
                                                            Kryptowährung
                                                            tatsächlich DIE Währung der Zukunft ist und richtig Gewinne
                                                            machen
                                                            lässt. Als jemand vor ein paar Jahren sogar kleine Summen in
                                                            den Bitcoin
                                                            investiert hat, kann er sich heute an Millionen erfreuen.
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">23</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    3 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>


                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F371925_1426200070_1825128294_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Michi</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Ich hab's vor ein paar Tagen gekauft
                                                            und habe 54% gewonnen. Nicht so viel wie versprochen, aber
                                                            immerhin Gewinn.
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">6</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    3 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>


                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F275712_1815883270_368899092_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Sonde</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Kannst du das mal klarer erklären?
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">2</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    3 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>




                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F371788_39603151_990746142_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">P@@@</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Und ich werde noch eine Frage stellen
                                                            Gilt es nur für die Amis oder für jeden?
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">17</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    4 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>




                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F370953_20903876_26789988_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName" href="remgue_22_de">Erik
                                                        an P.</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">
                                                            Für alle - Sie können Kryptowährungen kaufen, Sie müssen nur
                                                            den Link am Ende des Artikels eingeben, dort wird gesagt,
                                                            wie man kauft
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">8</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    6 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>




                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F173605_1387563113_14543618_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">KathiKatz</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Die Methode dieses Profesors ist
                                                            eigentlich
                                                            Ripple, aber mit Börsennotierung. Ich habe im Internet
                                                            gelesen, dass es
                                                            bis Ende des Jahres um bis zu 1000% steigen kann. Viele
                                                            Spezialisten
                                                            nennen sie den Cousin von Bitcoin, weil ihr Wert genauso
                                                            steigt wie der
                                                            Wert von Bitcoin früher. Ich habe mich entschlossen, sich
                                                            dafür zu
                                                            engagieren, weil es die beste Gelegenheit ist, die ich je
                                                            gesehen habe
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">20</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    8 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>



                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F70524_1387164496_88414351_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">shu</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Wie viel sollte man am Anfang
                                                            reinstecken um tatsächlich groß zu verdienen?
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span
                                                                    className="text">10</span></a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    8 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>





                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F369872_722424386_1857330401_q} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName" href="remgue_22_de">Sotnes
                                                        an shu</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">
                                                            Ich kam mit der ersten Zahlung von 50 Euro rein, nach drei
                                                            Wochen habe ich 510 er, ein kleiner Gewinn ist nicht gering,
                                                            aber ich
                                                            lerne immer noch
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">3</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    8 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>



                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F224406_100629153374069_2784614_n} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Erik</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Ich denke, ich werde prüfen,hätte ich
                                                            damals mich für den Bitcoin interessiert, wäre ich jetzt ein
                                                            Millionär
                                                            ... Ich möchte die Chance auf großes Geld nicht verpassen,
                                                            viel Glück
                                                            für alle
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">5</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    9 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>



                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F224406_100629153374069_2784614_r} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">dumbDumb</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">2.500 in einem Monat, ich glaub es
                                                            nicht, mir würden gerade mal 500 Euro nebenbei reichen...
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">5</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    9 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>



                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F224406_100629153374069_2784614_l} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">iop</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">ein Kumpel hat es mir empfohlen, ich
                                                            bereue es kein stück. Ich habe ein paar hundert reingesteckt
                                                            und nach 2
                                                            Wochen schon waren es 1100 Euro, jetzt nehme ich Frau und
                                                            Kinder in den
                                                            Sommerurlaub
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">5</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    9 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>



                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F224406_100629153374069_2784614_p} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Poirot</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">ok, ich bin überzeugt</div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">5</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    9 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>



                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F224406_100629153374069_2784614_c} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName"
                                                       href="remgue_22_de">Prollos</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">hahahaha, die Assisi sind mal wieder
                                                            daran zu schauen, wie sie mit Nichtstun an Kohle kommen
                                                            :D:D:
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">5</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    9 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>

                                    <li className="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder "
                                        id="fbc_10150877187848759_22497027_10150877337728759">
                                        <div className="UIImageBlock clearfix">
                                            <a target="_blank"
                                               className="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                               href="remgue_22_de" tabIndex="-1" aria-hidden="true"><img className="img"
                                                                                                        src={F224406_100629153374069_2784614_l} /></a>
                                            <div className="UIImageBlock_Content UIImageBlock_MED_Content">
                                                <div className="postContainer fsl fwb fcb">
                                                    <a target="_blank" className="profileName" href="remgue_22_de">Iop an
                                                        Prollos</a>
                                                    <div className="postContent fsm fwn fcg">
                                                        <div className="postText">Wenn man Bock darauf hat sich die
                                                            Hände
                                                            aufm Bau schmutzig zu machen oder Klos zu putzen, dann wird
                                                            es ja keinem
                                                            verboten. Man kann aber auch anders du spacko
                                                        </div>
                                                        <div className="stat_elem">
                                                            <div className="action_links fsm fwn fcg">
                                                                <a href="remgue_22_de" target="_blank"
                                                                   id="uz1cxy_5">Antworten</a> <span
                                                                className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="uiBlingBox postBlingBox"
                                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                                    className="img sp_comments sx_comments_like"><img
                                                                    src={Likethumb}/></i><span className="text">5</span></a>
                                                                <span className="dotpos">.</span>
                                                                <a href="remgue_22_de" target="_blank"
                                                                   className="fbUpDownVoteOption hidden_elem"
                                                                   rel="async-post">Gefällt mir!</a>
                                                                <span className="dotpos">.</span>
                                                                <abbr title="Wednesday, May 30, 2012 at 8:06pm"
                                                                      data-utime="1338433588" className="timestamp">Vor
                                                                    9 Stunden</abbr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="fsm fwn fcg"></div>
                                            </div>
                                        </div>
                                    </li>


                                </ul>
                                <div className="fbConnectWidgetFooter">
                                    <div className="fbFooterBorder">
                                        <div className="clearfix uiImageBlock">
                                            <a href="remgue_22_de" target="_blank"
                                               className="uiImageBlockImage uiImageBlockSmallImage lfloat"><i
                                                className="img sp_comments sx_comments_cfavicon"></i></a>
                                            <div className="uiImageBlockContent uiImageBlockSmallContent">
                                                <div className="fss fwn fcg">
                                       <span>
                                       <a href="remgue_22_de" target="_blank" className="uiLinkSubtle">Facebook social plugin</a>
                                       </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <footer>
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm-12 text-center">
                                        <a href="remgue_22_de" target="_blank"> <img src={Logo_WSM}
                                                                                    alt=""
                                                                                    className="img-responsive logo" /></a>
                                        <p className="stopka">Informationen zu Anlagerisiken: Das Risiko des
                                            Investieren in virtuelle Währungen (Kryptowährungen) birgt ein hohes
                                            Risiko und ist wahrscheinlich nicht für jeden geeignet. Es ist möglich,
                                            das investierte Kapital ganz oder teilweise zu verlieren. Investieren
                                            Sie kein Geld, das nicht verloren gehen kann. Stellen Sie sicher, dass
                                            Sie die Risiken und Kosten verstehen.
                                        </p>
                                        <div id="links">
                                            <a href="remgue_22_de" target="_blank"
                                               style={{fontSize: '12px',  color: '#929292'}}
                                               onClick="window.open('../privacypolicy.html', 'windowname1', 'width=670, height=765, scrollbars=1'); return false;">

                                                Datenschutzerklärung </a>
                                        </div>
                                    </div>
                                </div>

                                <div className="bibliography" style={{textAlign :'left'}}>
                                    <h2>Bibliografie:</h2>
                                    <ul className="legal-list">
                                        <li>1. Susan Alan, Blockchain, Publisher: American Library Association, 2019</li>
                                        <li>2. Imran Bashir, Mastering blockchain : distributed ledger technology,
                                            decentralization, and smart contracts explained, Publisher: Packt
                                            Publishing, 2018,
                                        </li>
                                        <li>3. Vaneetvelde Kenny, Ethereum projects for beginners : build
                                            blockchain-based cryptocurrencies, smart contracts, and DApps,
                                            Publisher: Packt Publishing, 2017,
                                        </li>
                                        <li>4. Kiana Danial, Cryptocurrency Investing For Dummies, Publisher : For Dummies,
                                            2013,
                                        </li>
                                        <li>5. Goldman Glen, The Crypto Trader: How anyone can make money trading
                                            Bitcoin and other cryptocurrencies, Publisher: Harriman House, 2016
                                        </li>
                                        <li>6. Jack Tatar, Cryptoassets: The Innovative Investor's Guide to Bitcoin and
                                            Beyond, Publisher: Back Cover, 2017,
                                        </li>
                                        <li>7. Don Tapscot, Blockchain Revolution: How the Technology Behind
                                            Bitcoin Is Changing Money, Business, and the World, Publisher: Reprint
                                            edition, 2017
                                        </li>
                                        <li>8. Antony Lewis, The Basics of Bitcoins and Blockchains, Publisher: Illustrated
                                            edition, 2016,
                                        </li>
                                        <li>9. Elad Elrom, The Blockchain Developer, Publisher: Apress, 2016,</li>
                                        <li>10. Edward Felten, Bitcoin and Cryptocurrency Technologies: A
                                            Comprehensive Introduction, Publisher: Princeton University Press, 2015,
                                        </li>
                                        <li>11. Kiana Danial, Cryptocurrency Investing, Publisher: For Dummies, 2016,</li>
                                        <li>12. Robert J. Morales, Cryptocurrency For Beginners: A Guide To Grow
                                            Your Financial Future in 2021, Publisher: Packt Publishing, 2018,
                                        </li>
                                        <li>13. Alan Norman, CRYPTOTRADING PRO, Publisher: Independently published, 2015,
                                        </li>
                                        <li>14. Ryan Deroussol, The Everything Guide to Investing in Cryptocurrency,
                                            Publisher: Harriman House, 2016
                                        </li>
                                        <li>15. Peter Bryant, Crypto Profit: Your Expert Guide to Financial Freedom
                                            through Cryptocurrency Investing, Publisher: Cryptoprof Publications,
                                            2017,
                                        </li>
                                    </ul>
                                </div>
                                <br />
                                    <center>Copyright 2021
                                    </center>
                            </div>
                        </footer>
                    </Col>

                    <Col md={4}  className='sidebar' >
                        <div className="btcwdgt-chart" bw-cash="true" bw-noshadow="true"></div>

                        <h3>Populärste:</h3>
                        <p><a href="remgue_22_de" target="_blank">Wenn Millenials Chefs werden</a></p>
                        <p><a href="remgue_22_de" target="_blank">Dinge, die Sie niemals auf der Arbeit tun sollten</a>
                        </p>
                        <p><a href="remgue_22_de" target="_blank">Kanarische Luftlinien haben erzielen immer höheren
                            Wert</a></p>
                        <p><a href="remgue_22_de" target="_blank">Tesla überrascht. Interessante Daten</a></p>
                        <p><a href="remgue_22_de" target="_blank">Der Markt ist wieder offen für kühne Investitionen</a>
                        </p>
                        <p><a href="remgue_22_de" target="_blank">Historische Stände auf der Wall Street</a></p>

                    </Col>
                </Row>



            </Container>
        </Container>
    );
}

export default Remgue1;
