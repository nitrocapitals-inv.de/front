import React, {Component} from 'react'
import {connect} from "react-redux";
import {Container, Row, Col} from "react-bootstrap";
import 'react-phone-number-input/style.css'
import PhoneInput, {isPossiblePhoneNumber, isValidPhoneNumber} from 'react-phone-number-input'

import Author  from '../assets/img/author.jpg'
import Testim0  from '../assets/img/testim0.jpg'
import Testim1  from '../assets/img/testim1.jpg'
import Testim2  from '../assets/img/testim2.jpg'
import Testim3  from '../assets/img/testim3.jpg'
import Testim4  from '../assets/img/testim4.jpg'
import Testim5  from '../assets/img/testim5.jpg'
import Testim6  from '../assets/img/testim6.jpg'
import Investor1  from '../assets/img/investor1.jpg'
import Investor2  from '../assets/img/investor2.jpg'
import Testims  from '../assets/img/testims.jpg'
import Main1  from '../assets/img/main1.jpg'
import Pic1  from '../assets/img/1.png'
import Pic2  from '../assets/img/2.png'
import Pic3  from '../assets/img/3.png'
import GPS  from '../assets/img/gps.png'
import Pc_5_small  from '../assets/img/pc_5_small.png'
import De_satisfaction  from '../assets/img/de_satisfaction.png'


import {FLAG_SELECTOR_OPTION_LIST} from "../helper/country";


import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'

import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";
import ReactCustomFlagSelect from "react-custom-flag-select";
import {Footer} from "../components/layout/footer";


const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();
class Remgue2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            popap : false,
            theposition: '',
            scrollPosition: false,
            fixPosition: false,
            fixPositionHeight: 0,
            phoneValue: '',
            messagesEndRef: React.createRef(),




        };
        this.submit = this.submit.bind(this);
    }


    _onMouseMove(e) {
        if (e.screenY <= 80) {
            this.setState({popap: true});
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country,
                // phoneValue :prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop


        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled >= 0.9) {
            const fixTop = height - 550;
            this.setState({
                fixPosition: true,
                // fixPositionHeight: fixTop,
            })
        }else {
            this.setState({
                fixPosition: false,
            })
        }



        if (scrolled > 0) {
            this.setState({
                scrollPosition: true
            })
        } else {
            this.setState({
                scrollPosition: false
            })
        }

    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop / 3 - 60);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {

        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }
        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            this.state.firstName && (!this.state.firstName.length <= 3 || !this.state.firstName >= 16) &&
            this.state.lastName && (!this.state.lastName.length <= 3 || !this.state.lastName >= 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {

            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }

    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];

        return (
            <Container fluid style={{backgroundColor: '#ececec'}}>
                <Container className='ramgueContainer' style={{
                    backgroundColor: 'white',
                }}>

                    <Row>
                        <Col >
                            <h1 onClick={this._scrollBottom.bind(this)}
                                style={{color: '#820000', marginBottom : '70px', cursor :'pointer'}}
                            >Ein junges Genie hat einen Algorithmus
                                entwickelt, der es jedem ermöglicht, einen Gewinn von 10510 Euro auf die
                                Währung namens Ripple zu machen. Bereits 27000 Deutsche haben davon
                                Gebrauch gemacht
                            </h1>
                            <Row>
                                <Col md={8}>
                                    <p>Guten Tag!</p>
                                    <p>Ich heiße Thomas Renck. Die Webseite, auf der Sie sich gerade
                                        befinden, befasst sich mit der Methode, die einer Internet-Währung
                                        namens Ripple gewidmet ist. Sie ermöglicht es <strong> jedem, 10510 Euro in 21
                                            Tagen zu gewinnen. Oder sogar noch mehr nach 30 Tagen.</strong>
                                        Ich habe sie mit Blick auf meine in Armut lebenden Eltern entwickelt.
                                        Dank dessen können sie sich jetzt ein großes Auto, eine vollständige
                                        Renovierung ihres Hauses und andere Annehmlichkeiten leisten. Aber das
                                        Wichtigste ist, dass sie sich nicht mehr sorgen müssen, dass ihnen in
                                        Zukunft das Geld ausgeht.
                                    </p>
                                    <p>Es stellte sich jedoch heraus, dass die Methode, die
                                        ich entdeckt habe, so gewinnbringend war, dass sie nicht nur meinen
                                        Eltern, sondern auch 27 Tausend anderen gewöhnlichen Deutschen geholfen
                                        hat, die zusätzliches Geld brauchten. Jede einzelne dieser Personen
                                        genießt nun die volle finanzielle Unabhängigkeit. Dazu gehören auch sehr
                                        schwierige Fälle von Menschen, die so hoch verschuldet waren, dass sie
                                        Angst um ihr Leben hatten. Andere waren nicht in der Lage, ihre
                                        Rechnungen zu bezahlen. Noch andere haben ihr gesamtes Vermögen fast
                                        verloren. Das ist auch zum Beispiel bei Frau Barbara der Fall gewesen.
                                    </p>
                                    <div className="clearfix"></div>
                                </Col>
                                <Col md={4}>
                                    <div className="history">
                                        <img onClick={this._scrollBottom.bind(this)} src={Author} style={{width:'100%',cursor:'pointer'}} className="photo" />
                                        <div className="clearfix"></div>
                                        <p>Thomas Renck – der Erfinder der Methode, mit der man 10510
                                            Euro verdienen kann, die bereits von 27000 Deutschen genutzt wurde. Bis
                                            vor kurzem war er noch ein armer Gymnasiast, dessen Traum es war, ein
                                            gutes Studium aufzunehmen und seine Eltern finanziell zu unterstützen.
                                            Derzeit ist er der jüngste deutsche Millionär, dessen Vermögen auf rund
                                            6
                                            Millionen Euro geschätzt wird.
                                        </p>
                                    </div>
                                </Col>
                            </Row>

                        </Col>
                    </Row>

                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim0} style={{float: 'left',  marginRight: '15px', cursor:'pointer'}} alt="" className="img-responsive" />

                            <p>Der Himmel hat mir diesen Jungen und seine
                                innovative Methode geschickt. Ich habe sie entdeckt, kurz nachdem mich
                                die Bank angerufen hat, um mir mitzuteilen, dass ich mit den Kreditraten
                                im Rückstand bin... ich weinte wie ein Schlosshund, weil ich das
                                Vermögen meines Lebens verlieren konnte! Dank dieser besonderen
                                Entdeckung habe ich nicht nur meine Schulden zurückgezahlt, sondern auch
                                genug Geld, um ein ruhiges Leben zu führen. Ich! Eine Person, an deren
                                Türen bereits ein Schuldeneintreiber geklopft hat, um die Schulden
                                einzutreiben.
                            </p>
                            <h4 className="text-right bold">Barbara Schliff, 64 Jahre, Köln</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col>

                            <div className="clearfix"></div>
                            <p>Meine Methode ist aber nicht nur für Personen
                                gedacht, denen eine Pleite droht. Sie ist für alle gedacht, die
                                zusätzlich 10510 Euro Gewinn in kurzer Zeit machen wollen. Das ist auch
                                bei Merida der Fall gewesen, die nur etwas älter ist als ich.
                            </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim1}
                                 style={{float: 'left',  marginRight: '15px', cursor:'pointer'}}
                                 alt=""
                                 className="img-responsive" />
                            <p>Ich habe mir gedacht, dass ich diese Methode mal
                                ausprobieren werde, als ich noch eine Studentin war. Meine Freundinnen
                                prahlten ständig damit, dass sie neue Klamotten kauften und überall hin
                                in den Urlaub fuhren. Aber ich hatte einfach so viel Geld nicht... Ich
                                war schockiert, als ich nach 3 Wochen meinen Kontostand sah: 10 Tsd.
                                Euro. Ich habe sofort Flugscheine nach Spanien für mich und meine
                                Schwester gekauft. Wir haben dort zwei Wochen verbracht, ohne uns um die
                                Ausgaben Sorgen zu machen. Ich konnte endlich mit meiner Urlaubsreise
                                vor meinen Freundinnen prahlen. Ich bin sehr froh, dass ich die
                                Entdeckung von Thomas genutzt habe.
                            </p>
                            <h4 className="text-right bold">Merida Salzmann, 22 Jahre, Regensburg</h4>

                        </Col>
                    </Row>
                    <Row>
                        <Col  >
                            <div className="clearfix"></div>
                            <p><strong>Also, wenn Sie eine Person sind, die:</strong></p>
                            <ul className="list-unstyled arrow_list" style={{fontSize: '17px'}}>
                                <li>sich täglich Sorgen um die Finanzen macht,</li>
                                <li>nicht in der Lage ist, die Schulden alleine zurückzuzahlen,</li>
                                <li>alles kaufen will, was sie sich wünscht,</li>
                                <li>Personen in der Familie hat, die sie finanziell unterstützen möchte</li>
                            </ul>
                            <p>...lesen Sie unbedingt, was ich Ihnen sagen möchte. </p>
                            <p>Die Methode, die auf der Währung Ripple basiert, kann
                                Ihre Finanzen und die Finanzen Ihrer Nächsten retten. Und sie wird den
                                Komfort Ihres Lebens dank der zusätzlichen Gewinne bestimmt wesentlich
                                erhöhen.
                            </p>
                            <p><strong>Und was am wichtigsten ist, ist all das... </strong></p>
                            <ul className="list-unstyled arrow_list" style={{fontSize: '17px'}}>
                                <li>In kurzer Zeit</li>
                                <li>ohne das Haus verlassen zu müssen</li>
                                <li>ohne jegliche Kenntnisse im Bereich Ökonomie,</li>
                                <li>ohne Verträge zu unterschreiben und ohne Teilzahlungen zu leisten.</li>
                            </ul>

                        </Col>
                    </Row>

                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim2}
                                 style={{float: 'left',  marginRight: '15px', cursor :'pointer'}}
                                 alt="" className="img-responsive" />
                            <p>Bei uns gab es nie Probleme mit Geld. Sowohl
                                meine Frau als auch ich haben 20 Jahre lang Vollzeit gearbeitet. Und
                                dann, Bumm! Sie hat ihre Arbeit ganz plötzlich verloren. Sie konnte
                                lange Zeit keine neue Arbeit finden... Wir haben also beschlossen, dass
                                sie sich um unsere Kinder kümmert, während sie nach einer zusätzlichen
                                Einkommensquelle suchen wird. Wir brauchten das Geld, denn es ist nicht
                                einfach, 4 Kinder zu versorgen. Nicht einmal mit meinem Gehalt. Eines
                                Tages hat meine Frau diese Methode im Internet gefunden.&nbsp;<strong>Und
                                    heute können wir uns dank ihr wieder eine Privatschule für die Kinder
                                    und ein ruhiges Leben leisten, weil wir jetzt zusätzlich 45000 Euro
                                    monatlich verdienen.</strong>
                            </p>
                            <h4 className="text-right bold">Peter Köhler, 56 Jahre alt, Duisburg</h4>
                        </Col>
                    </Row>


                    <Row>
                        <Col>
                            <h3 onClick={this._scrollBottom.bind(this)} style={{cursor:'pointer'}} className="underTheTitle">Wie ist es möglich, dass ich es geschafft habe, eine Methode
                                zu entdecken, mit der man 10510 Euro verdienen kann?</h3>
                            <p>Als ich diese Methode entwickelt habe, war ich gerade
                                einmal 18 Jahre alt. Ich lebte mit meinen Eltern in einem kleinen Dorf
                                und besuchte eine Schule, die 14 km entfernt war. Ich lernte sehr viel,
                                um eine Chance auf ein besseres Leben zu bekommen. Meine Eltern waren
                                sehr stolz auf mich und sie wollten, dass ich ein Studium aufnehme. Sie
                                waren aber nicht in der Lage, mich daran finanziell zu unterstützen.
                                Also habe ich mich während meiner gesamten Zeit am Gymnasium bemüht, ein
                                Stipendium zu bekommen, das alle Kosten decken würde. Ich habe Tag und
                                Nacht gelernt, und ich habe auch an Wettbewerben in allen Schulfächern
                                teilgenommen. Aber nur einer davon hat meine Aufmerksamkeit wirklich
                                erregt.
                            </p>
                            <p>Ich habe das Plakat des Interschulischen
                                Unternehmerwettbewerbs in dem Schulkorridor bemerkt. Ich wusste sofort,
                                dass ich daran teilnehmen will. Das war meine Chance, nicht nur für ein
                                Studium angenommen zu werden, sondern auch an der Methode zu arbeiten,
                                die ich dort präsentieren wollte.
                            </p>
                            <p>Damals hätte ich nicht einmal gedacht, dass ich auf meinem Weg auf so viele Widrigkeiten
                                stoßen würde.</p>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>Ich hatte das Gefühl, eine brillante Idee zu haben</h3>
                            <p>Dieser Wettbewerb war meine große Chance. Dort
                                sollten nicht nur Lehrer von vielen verschiedenen Schulen anwesend sein,
                                sondern auch Talentsucher von verschiedenen Akademien und
                                Universitäten. Ich hatte gehofft, dass sich jemand für meine Erfindung
                                interessieren würde. Meinen auf Internet-Währungen basierenden
                                Algorithmus, mit dem einfach jeder 10510 Euro verdienen kann. Und mir
                                dann helfen würde, ihn so zu perfektionieren, damit er auch Menschen in
                                Not helfen kann.
                            </p>
                            <p>Ich glaubte, dass mein Algorithmus in der Lage ist, die Finanzlage vieler Menschen zu
                                verbessern, weil:</p>
                            <ul className="list-unstyled arrow_list" style={{fontSize: '17px'}} >
                                <li>Erstens, Kryptowährungen haben nicht die Nachteile anderer Geldgewinnungsmethoden.
                                </li>
                                <li>Zweitens, sie bedürfen keiner großen Kenntnisse auf dem Gebiet der Ökonomie und des
                                    Finanzwesens.
                                </li>
                                <li><strong>Drittens, ihr Wert steigert sich von Tag zu Tag um 200 %, 500 % oder sogar
                                    um 1000 %.</strong></li>
                            </ul>
                            <p>Dank der von mir entdeckten Methode kann jeder
                                feststellen, in welche Währung es sich lohnt zu investieren. Einfach so,
                                ohne dass der Durchschnittsmensch irgendwelche Berechnungen machen oder
                                spezielle Kenntnisse haben muss. Denn alles sollte der Algorithmus
                                berechnen.
                            </p>
                        </Col>
                    </Row>

                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim3}
                                 style={{float: 'left',  marginRight: '15px',cursor :'pointer'}}
                                 alt="" className="img-responsive" />
                            <p>Nach vielen Jahren, in denen man kaum bis zum
                                nächsten Zahltag überleben konnte (2000 Eur!) Ich fühlte mich sehr
                                erleichtert. Ich freue mich, dass ich nicht mehr gespannt auf jede
                                Überweisung warten werden muss. <strong> Vor nicht allzu langer Zeit
                                    habe ich ein Auto für 68 Tsd. Euro gekauft und musste dafür nicht einmal
                                    viele Jahre Geld ansammeln.</strong> Dank dieser Erfindung habe ich zusätzliches
                                Geld zur Verfügung! Und es hat nur ein paar Tage gedauert. Etwas Unglaubliches.
                            </p>
                            <h4 className="text-right bold">Peter Thomsinn, 36 Jahre, Aachen</h4>

                        </Col>
                    </Row>

                    <Row>
                        <Col>

                            <div className="clearfix"></div>
                            <p><strong>Wie ich bereits erwähnt habe, sollte meine
                                Methode sehr einfach und gleichzeitig effektiv sein.&nbsp;Sie sollte
                                auch einen schnellen Zufluss von Geld garantieren.</strong>
                            </p>
                            <p>Nachdem ich den Algorithmus so gut wie möglich
                                ausgearbeitet hatte, brachte ich ihn eines Tages mit zu einer
                                Unterrichtsstunde in Grundlagen des Unternehmertums. Ich wollte ihn
                                meinem Lehrer zeigen. Er war die einzige Person in meiner Schule, die
                                mir sagen konnte, ob meine Erfindung gut genug ist, um sie zum
                                Wettbewerb zu bringen.
                            </p>
                            <p>Wir haben uns nicht besonders gemocht, weil ich seine
                                Fehler während des Unterrichts in der Vergangenheit mehrfach korrigiert
                                habe. Ob er es mochte oder nicht, ich war wirklich gut, wenn es um die
                                Grundlagen des Unternehmertums geht. Das hat ihm nicht gefallen. Einmal
                                sagte er mir, dass ich seine Autorität untergrabe, wenn ich ihn auf
                                seine Fehler hinweise. Ich hätte nicht gedacht, dass sie mir so etwas
                                antun würde...
                            </p>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>Er hat meine Erfindung vor meinen Kollegen ausgelacht</h3>
                            <p>Nach dem Unterricht, als meine Klassenkameraden sich bereit
                                machten, das Klassenzimmer zu verlassen, habe ich ihn angesprochen und
                                ihm erklärt, worin meine Methode besteht. Ich habe ihm die erste Version
                                gezeigt. Aber anstelle von ermutigenden Worten oder irgendwelcher
                                Fragen habe ich von ihm nur gehört: „Na und? Du glaubst, dass du
                                jemandem auf diese Weise helfen wirst, Geld zu verdienen? Meine Güte,
                                Renck, Was für einen Blödsinn hast du dir dieses Mal ausgedacht... Das
                                ist überhaupt nicht nützlich.&nbsp;<strong>Lass den Unsinn! Auf keinen
                                    Fall, ich werde etwas so mieses niemals empfehlen... Ich würde mich
                                    sogar schämen, mich mit so etwas vor dem Direktor zu zeigen!“.</strong>
                            </p>
                            <p>Ich fühlte, dass ich vor Scham rot wurde. Als sie diese Worte
                                hörten, hörten alle auf, das Klassenzimmer zu verlassen. Sie
                                betrachteten alles als Show. Der Lehrer hat das bemerkt und vor allen
                                Anwesenden ganz gemein hinzugefügt: „Wenn du so stark an diese Erfindung
                                glaubst, dann kannst du auch gerne alleine zum Wettbewerb fahren. Aber
                                ich werde nicht einmal einen Finger rühren. An deiner Stelle würde ich
                                zu Hause bleiben, sonst wirst du dich nur lächerlich machen. Aber wenn
                                du trotzdem teilnimmst, werden wir alle mit Vergnügen hören, wie es
                                gelaufen ist“.
                            </p>
                            <p>Ich glaube nicht, dass ich mich jemals in meinem Leben so
                                gedemütigt gefühlt habe. Ich habe mein Projekt eingepackt und das
                                Klassenzimmer ohne ein Wort verlassen, begleitet von den blöden Ausrufen
                                meiner Klassenkameraden. Das hat mich gezwungen, über meine Entdeckung
                                für viele Wochen zu vergessen. Ich habe auch angefangen, mich zu fragen,
                                ob ich überhaupt an dem Wettbewerb teilnehmen werde...
                            </p>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>Dann, wegen einer Tragödie in der Familie, habe ich meine
                                Meinung geändert</h3>
                            <p>Die Situation in der Schule gab mir das Gefühl, als
                                würde ich als Strafe dorthin gehen. Ich lernte weniger als vorher und
                                fühlte mich einfach total niedergeschlagen. Ich habe nicht einmal
                                bemerkt, dass zu Hause etwas Schlimmes passierte. In letzter Zeit wurde
                                die finanzielle Situation meiner Eltern immer schlechter. Das ganze
                                Leben lang kamen sie kaum über die Runden, aber in letzter Zeit konnten
                                sie sich nicht einmal mehr die nötigsten Dinge leisten. Ich habe einen
                                Vorschlag gemacht, dass ich mir vielleicht nach der Schule einen Job
                                suchen sollte, um sie zu unterstützen. Aber sie sagten nur immer wieder,
                                dass ich mich auf das Lernen konzentrieren muss, damit ich mich in
                                Zukunft nicht in der gleichen Situation befinde wie sie jetzt...
                            </p>
                            <p>Auf der einen Seite fühlte ich mich schrecklich, weil
                                ich ihnen nicht helfen konnte. Und auf der anderen Seite – ich habe es
                                nicht einmal versucht. Die Lust, an dem Wettbewerb teilzunehmen, wurde
                                immer geringer und ich hörte schließlich auf, überhaupt über den
                                Algorithmus zu denken. Ich fühlte mich traurig und kraftlos und konnte
                                nur daran denken, was ich falsch gemacht habe.
                            </p>
                            <p>Obwohl ich also wusste, dass meine Eltern in einer
                                schwierigen Situation waren, dachte ich nicht, dass es eigentlich so
                                schlimm war. Aus diesem Grund werde ich mir leider selbst die Schuld
                                daran geben, was später passiert ist...
                            </p>
                            <p><strong>Eines Tages rief mich meine Mutter nach der
                                Schule an und weinte. Es stellte sich heraus, dass ein
                                Gerichtsvollzieher uns besucht hat, um unser Haus zu übernehmen. Er hat
                                uns nur 3 Wochen für den Auszug gegeben.</strong>
                            </p>
                            <p>Ich war gleichzeitig niedergeschlagen und wütend. Ich
                                habe einen Bus genommen und bin nach Hause gefahren. Als ich das Haus
                                betreten habe, wollte ich meine Eltern schelten: „Wie konntet ihr so
                                etwas zulassen?“. Aber als ich einen Stapel unbezahlter Rechnungen auf
                                dem Tisch und meine verzweifelten Eltern sah, war ich nicht in der Lage,
                                ein Wort zu sagen. Ich habe herausgefunden, dass mein Vater vor einigen
                                Monaten seine Arbeit verloren hat. Meine Eltern haben beide
                                Arbeitslosengeld bezogen und sie haben mir nichts davon erzählt. Ich
                                bedauerte, dass ich nicht früher begonnen hatte, sie finanziell zu
                                unterstützen.
                            </p>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>Ich habe beschlossen, meinen Eltern um jeden Preis zu
                                helfen</h3>
                            <p>Trotz der tragischen Situation haben mich meine
                                Eltern nicht nach einem Job suchen lassen. Sie waren der Meinung, dass
                                sie mit ihren eigenen Problemen selbst zurechtkommen müssen. Aber ich
                                hatte eine andere Idee, die ich fast aufgegeben habe. In einem einzigen
                                Moment habe ich beschlossen, dass ich zum Interschulischen
                                Unternehmerwettbewerb fahren werde. Ich wollte meine Erfindung um jeden
                                Preis präsentieren, jemanden finden, der mir helfen würde, sie zu
                                verbessern, und genug Geld verdienen, um das Haus meiner Eltern zu
                                retten.
                            </p>
                            <p>Ich wusste, dass in den vergangenen Jahren viele
                                Schüler dank dieses Wettbewerbs ihre Projekte verwirklichen konnten. Es
                                war mein Traum, einer von ihnen zu werden.
                            </p>
                            <h3 className="underTheTitle"  style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>Aber ich hatte Angst, dass meine Erfindung niemandem gefallen
                                wird</h3>
                            <p>Endlich ist der Tag gekommen. Nach verschiedenen
                                organisatorischen Problemen, mit denen ich Sie jetzt nicht langweilen
                                werde, habe ich den Wettbewerbsort erreicht, wo die Teilnehmer ihre
                                Projekte präsentierten. Da habe ich oft große Augen gemacht, wenn ich
                                gesehen habe, was für unglaubliche Dinge sich andere Leute in meinem
                                Alter ausgedacht haben...
                            </p>
                            <p>Indessen näherte sich ABSOLUT NIEMAND meinem Stand.
                                Trotz des großen Andrangs in der Halle hat sich kein Besucher auch nur
                                einen Moment lang für meine Entdeckung interessiert. Das
                                Wettbewerbskomitee hat mir aus Höflichkeit ein paar Fragen gestellt. Ich
                                habe bemerkt, dass sie spöttisch lächelten, als ich die nach dem
                                Algorithmus berechneten Beträge erwähnt habe. Sie haben aber schnell zum
                                nächsten Tisch weitergemacht.
                            </p>
                            <p>Ich war depressiv. In den Ohren konnte ich das
                                klingende Echo der Worte meines Lehrers hören, der mir gesagt hatte, ich
                                sollte besser aufhören, herumzualbern... Hatte meine Erfindung wirklich
                                keinen Wert? Es sah ziemlich unauffällig aus, aber die
                                Geldautomatenkarte ist auch nichts Besonderes... Und wie viel man damit
                                machen kann!
                            </p>
                            <p>Meine Gedanken wurden von einem Mann unterbrochen, der gerade näher an mein Projekt
                                gekommen ist.&nbsp;<strong>Er hat mir seine Visitenkarte gegeben und gesagt, dass er ein
                                    Wirtschaftsdozent an einer Universität ist.</strong>
                                Er fing sofort an, mir ziemlich detaillierte Fragen zu stellen. Er
                                sprach eine Stunde lang mit mir und notierte. Wir waren so in das
                                Gespräch vertieft, dass wir gar nicht bemerkt haben, dass der Wettbewerb
                                schon vorbei war und die Gewinner ausgezeichnet wurden. Natürlich habe
                                ich nicht gewonnen... Der Mann hat mir auch keine Zusammenarbeit
                                angeboten. Er hat nur nach meiner Telefonnummer gefragt und mir gesagt,
                                dass er mich in ein paar Tagen kontaktieren wird. Ich habe daran nicht
                                ernsthaft geglaubt und... bin mit einem Gefühl einer kompletten
                                Niederlage nach Hause gefahren. Ich stellte mir schon vor, wie mein
                                Lehrer mich vor der ganzen Klasse bittet, über meine Leistung im
                                Wettbewerb zu sprechen...
                            </p>
                            <p><strong>Einige Tage später erhielt ich einen Anruf, der mein ganzes Leben
                                veränderte.</strong></p>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>„Das ist ein echtes Novum auf dem Finanzmarkt“</h3>
                            <p>Ich war schockiert, als mich derselbe Mann, mit dem
                                ich während des Wettbewerbs gesprochen habe, angerufen hat. „Dein
                                Projekt ist gut, aber es benötigt einige Verbesserungen. Du hast es so
                                gut entwickelt, wie es mit deinen Kenntnissen und Fähigkeiten nur
                                möglich war. Wenn du es perfektionieren willst, helfe ich dir sehr
                                gerne. Ich arbeite an einer Universität, die solche Schlauköpfe wie dich
                                schätzten kann. Wir würden uns freuen, dich als Student in unserem
                                Institut zu haben, wo du auch an der Verbesserung deiner Erfindung
                                arbeiten könntest. <strong>Besuch uns und mit unserer Hilfe erstellst du
                                    einen professionellen Algorithmus. Mit seiner Hilfe werden wir in der
                                    Lage sein, eine Kryptowährung zu wählen, mit der man großes Geld
                                    verdienen kann. Auf diese Weise hilfst du nicht nur deinen Eltern,
                                    sondern auch vielen anderen Menschen“.</strong>
                            </p>
                            <p>Er musste mich lange nicht überzeugen. Und dann ging
                                alles sehr schnell, obwohl es in Wirklichkeit bis zu 1,5 Jahre dauerte.
                                Ich habe meine Studien begonnen und gleichzeitig hart daran gearbeitet,
                                einen neuen, verbesserten Algorithmus zu erstellen.
                            </p>
                            <p><u>Nach vielen Tests zeigt meine Erfindung eindeutig,
                                dass Ripple die DERZEIT profitabelste Internetwährung der Welt ist.
                                Aber das ist nicht alles! Finanzexperten, Ökonomen und die reichsten
                                Investoren der Welt schätzen, dass der Preis von Ripple innerhalb der
                                nächsten Tage auf Rekordwerte steigen kann.</u>
                            </p>
                            <p><strong>Es ist also der letzte Moment, sich Ripple zu besorgen, wenn Sie damit riesiges
                                Geld machen wollen.</strong></p>
                            <p><strong>Solche Gelegenheit wird sich vielleicht nicht
                                mehr wiederholen! In einer Woche wird der Preis dieser Währung viel
                                höher und ihr Wert viel niedriger sein, deshalb...</strong> muss sich jeder, der Ripple
                                besitzen und mindestens 10510 Euro gewinnen möchte, beeilen.
                            </p>
                            <p>Der beste Weg, diese Währung zu erhalten, sind die
                                fertigen Pakete von Ripple. Ich habe sie speziell für Neuling-Investoren
                                wie Sie vorbereitet. Wenn Sie ein solches Paket wählen, zahlen Sie 90 %
                                weniger als beim Kauf von Ripple direkt an der Börse. Darüber hinaus
                                müssen Sie nichts weiter tun – nur die Bestellung aufgeben. Und danach
                                werden Sie nur noch beobachten, wie sich Ihr Geld vermehrt.
                            </p>
                            <p>Und das Beste daran ist, dass:</p>
                            <ul className="list-unstyled arrow_list" style={{fontSize: '17px'}}>
                                <li><strong>Sie brauchen keine Fachkenntnisse</strong>, um hohe
                                    Geldbeträge zu gewinnen. Die Analytiker schätzen, dass der Wert von
                                    Ripple in nächster Zeit sowieso steigen wird, und zwar bis zu 1000 %.
                                    Also, jeder, der die Währung besitzt, wird davon profitieren.
                                </li>
                                <li><strong>Sie müssen sich mit keinen Verträgen binden</strong>.
                                    Sobald Sie Ripple erhalten, können Sie mit ihnen machen, was immer Sie
                                    wollen. Sie machen entweder eine Auszahlung oder behalten die Währung
                                    auf dem Konto, damit sie Ihnen regelmäßige Gewinne bringt.
                                </li>
                                <li>In 21 Tagen verbessern Sie Ihre finanzielle Situation ohne Anstrengung und sogar
                                    ohne das Haus zu verlassen.
                                </li>
                            </ul>

                            <h3 className="underTheTitle"
                                onClick={this._scrollBottom.bind(this)}
                                style={{
                                    color: '#820000',
                                    fontSize: '30px',
                                    marginBottom: '20px',
                                    marginTop: '20px',
                                    cursor:'pointer'

                                }}

                            ><u>JETZT ist der beste Moment, um Ripple zu erwerben und Ihr
                                Einkommen um 100 %, 500 % oder sogar 1000 % zu erhöhen.</u></h3>
                            <p>Die Geschichte wiederholt sich! Erinnern Sie sich
                                noch daran, als Bitcoin im Jahr 2009 so populär war? Die Klügsten haben
                                damals nur mickrige 200 Euro investiert und heute sind sie Millionäre. <strong>Nur weil
                                    der Wert von Bitcoin um 23521 % gestiegen ist.</strong>
                            </p>
                            <p><strong>Das Gleiche kann in diesem Jahr mit Ripple passieren. Unten finden Sie einige
                                Aussagen von Experten, die dies bestätigen:</strong></p>


                        </Col>
                    </Row>


                    <Row>
                        <Col md={1}>

                        </Col>
                        <Col md={10}>
                            <div className="mobile-off">
                                <div className="col-sm-12 col-sm-offset-1 testim">
                                    <div className="col-sm-2 col-sm-offset-0 col-xs-3 col-xs-offset-4"  ><img
                                        src={Investor1}  style={{float: 'left',  marginRight: '15px'}} className="img-responsive" /></div>
                                    <div className="col-sm-10 col-xs-12" style={{textAlign:'center', paddingTop:'50px'}}>
                                        <h3>Jens Kritzer, Wirtschaftsprofessor:</h3>
                                    </div>
                                    <div className="clearfix"></div>
                                    <p>„Seit 10 Jahren investiere ich in Kryptowährungen. Durch den
                                        Kauf von 20 Paketen von Btc im letzten Jahr habe ich bereits 678200 Euro
                                        verdient. Der Gewinn steigt immer weiter an und ich muss kaum etwas
                                        tun. In diesem Jahr habe ich zusätzlich 8 Pakete von Ripple gekauft und
                                        habe bereits 70 % des letzten Betrags verdient. Eine solche Gelegenheit
                                        wird sich bestimmt nicht schnell wiederholen.“
                                    </p>

                                </div>
                                <div className="clearfix"></div>
                                <div className="col-sm-12 col-sm-offset-1 testim">
                                    <div className="col-sm-2 col-sm-offset-0 col-xs-3 col-xs-offset-4"><img
                                        src={Investor2}  style={{float: 'left',  marginRight: '15px'}} className="img-responsive" /></div>
                                    <div className="col-sm-10 col-xs-12" style={{textAlign:'center', paddingTop:'50px'}}>
                                        <h3>Edward Doumann, Finanzexperte:</h3>
                                    </div>
                                    <div className="clearfix"></div>
                                    <p>„Jeder kauft jetzt Ripple, weil jeder weiß, dass sein Wert
                                        steigen wird. Schon in ein paar Tagen kann er sogar 1000 % mehr wert
                                        sein. Alles, weil so viele Personen im Wettlauf mit der Zeit sind, um
                                        ihn zu erhalten.“
                                    </p>
                                </div>
                            </div>
                        </Col>
                        <Col md={1}>

                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <div className="clearfix"></div>
                            <p>Wie auch immer, schauen Sie sich einfach die
                                Ergebnisse unseres Tests an, der das Potenzial von Ripple misst. Ich
                                habe ihn vor ein paar Tagen zusammen mit meinem Dozenten durchgeführt.
                                Die Ergebnisse haben die Wirtschaftsdozenten anderer Universitäten
                                verblüfft. Stellen Sie sich vor...
                            </p>

                            <div className="gps" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>
                                <h3>Die Testgruppe – alle 1260 Personen, die die
                                    Währung Ripple getestet haben, hatten mindestens 10510 Euro auf ihren
                                    Konten nach nur 21 Tagen. Was bedeutet das?
                                </h3>
                            </div>
                            <p><strong>Alle Teilnehmer waren damit in der Lage, ihr Budget auf einfache Weise deutlich zu verbessern.</strong></p>
                            <p>...und alle davon waren Personen, die das speziell
                                vorbereitete Paket von Ripple bestellt haben. Während der nächsten 21
                                Tage haben sie zusätzliches Geld gewonnen und konnten es ausgeben, wie
                                sie wollten. Für Vergnügungen oder für ernsthafte Bedürfnisse. Aber das
                                ist nicht alles! Denn Personen, die mehr als ein Paket bestellt haben,
                                haben ihren Gewinn im Zeitraum von einem Monat um 200 %, 400 % oder
                                sogar 700 % gesteigert. Sie haben angefangen, 20000, 40000 oder sogar
                                100000 Euro zu verdienen.
                            </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <img onClick={this._scrollBottom.bind(this)} src={Testims} className="img-responsive"
                                 style={{marginBottom: '0px', marginTop: '30px', width:'100%', cursor :'pointer'}} />

                            <p className="signature">Tausende von Personen auf der ganzen Welt vervielfachen ihr
                                Einkommen sogar um 1000 % innerhalb eines Monats</p>
                            <p><strong>Hier ist die Zusammenfassung, die wir mit unserem Forschungsteam nach dem
                                Test erstellt haben:</strong></p>
                            <ul className="list-unstyled arrow_list" style={{fontSize: '17px'}}>
                                <li>964 Personen haben alle ihre Schulden abgezahlt</li>
                                <li>100 Personen hatten einen Traumurlaub</li>
                                <li>286 Personen haben ein neues Auto gekauft</li>
                                <li><strong>Alle 1350 Personen haben sich zu 100 % von finanziellen Problemen
                                    befreit</strong> und seitdem haben sie immer Geld für alle ihre Ausgaben!
                                </li>
                            </ul>
                        </Col>
                    </Row>

                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim4} style={{float: 'left',  marginRight: '15px',cursor :'pointer'}} alt="" className="img-responsive" />
                            <p>Einmal hatte ich sogar einen Tag nach der
                                Auszahlung meiner Altersversorgung finanzielle Probleme. Es wurde für
                                die Miete, Rechnungen und Medikamente ausgegeben... Es blieb nicht viel
                                übrig und ich musste immer zwischen dem Kauf von Essen oder neuer
                                Kleidung wählen, wenn meine alte Sachen in schlechtem Zustand waren.
                                Bevor ich von dieser Methode erfahren habe, war ich nur ein armer
                                Rentner. Ich musste meine Kinder um Hilfe bitten. Und jetzt wiederum
                                unterstütze ich sie finanziell und bin völlig unabhängig.
                            </p>
                            <h4 className="text-right bold">Franz Krasinger, 79 Jahre, Stuttgart</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <div className="clearfix"></div>
                            <p>Unter den ersten Personen, die mein Projekt getestet
                                haben, waren natürlich meine Eltern. Mein Vater rief mich alle paar Tage
                                an und teilte mir mit zunehmender Freude mit, dass das Geld dank Ripple
                                ständig auf sein Konto kommt. Beim nächsten Mal hat er gesagt: Wir
                                haben jetzt genug Geld, um unsere Hausschulden abzuzahlen. Und es wird
                                uns noch etwas übrig bleiben! „Mein Sohn, ich wusste immer, dass du
                                irgendwann viel erreichen wirst...Dank dir müssen wir uns nicht mehr um
                                finanzielle Angelegenheiten sorgen“ – Mein Vater hat seine Rührung und
                                seinen Stolz nicht versteckt.
                            </p>
                            <img onClick={this._scrollBottom.bind(this)} src={Main1} className="img-responsive"
                                 style={{marginBottom: '0px', marginTop: '30px', width:'100%', cursor :'pointer'}} />
                            <p className="signature">Dank der Methode, die ich entdeckt
                                habe, konnten meine Eltern ihre Hausschulden abzahlen und haben jetzt
                                viel Geld, nachdem sie in Ripple investiert haben
                            </p>
                            <p>Die in dem Test erzielten Ergebnisse wurden durch
                                einem unabhängigen Finanzinstitut in Berlin bestätigt.&nbsp;Bis jetzt
                                haben bereits 27000 Personen in Deutschland dank der Pakete von Ripple
                                eine Menge Geld verdient. Alle davon haben ihre Finanzen verbessert und
                                zusätzliches Geld verdient. Das macht die Währung Ripple die beste
                                Methode des Geldverdienens im Jahr 2020. Ihre Effektivität bei der
                                Wiederherstellung der finanziellen Liquidität beträgt 100 %.&nbsp;Ohne
                                Kredite, ohne Angst, auf völlig legale und einfache Weise.
                            </p>

                            <Row>
                                <Col md={2}>
                                    <img style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)} src={Pic1}
                                         className="img-responsive" />
                                </Col>
                                <Col md={10}>

                                    <h3>100% Sicherheit</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2}>
                                    <img style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)} src={Pic2}
                                         className="img-responsive" />
                                </Col>
                                <Col md={10}>
                                    <h3>GARANTIE DER ERGEBNISSE</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2}>
                                    <img style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)} src={Pic3}
                                         className="img-responsive" />
                                </Col>
                                <Col md={10}>
                                    <h3>VERBESSERUNG DER FINANZIELLEN SITUATION NACH 21 TAGEN</h3>
                                </Col>
                            </Row>

                            <br/>
                            <br/>

                            <div className="clearfix"></div>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>SIE BRAUCHEN KEINE KOMPLIZIERTEN METHODEN</h3>
                            <p><strong>Jetzt können auch Sie Ihre Finanzen in 21 Tagen reparieren, unabhängig davon, wie
                                hoch Ihre Ausgaben sind!</strong></p>
                            <p>Ich empfehle Ihnen von ganzem Herzen, selbst
                                herauszufinden, wie viel Sie mit dem Paket von Ripple gewinnen können.
                                Es ist eine einfache und schnelle Methode für zusätzlichen Gewinn...
                                27000 Personen können sich nicht irren! Und ihre Empfehlungen werden von
                                den Ergebnissen von <strong>436 Tests und 8 Zertifikaten</strong> der renommiertesten
                                Forschungseinheiten auf der ganzen Welt begleitet.
                            </p>
                            <p>Das Paket Ripple ist für jeden, der zusätzliches Geld
                                braucht. Alter, Wohnort und die Schwere des Problems sind hier nicht
                                wichtig. Und das liegt daran, dass der Umgang mit Internet-Währungen...
                            </p>
                            <ul className="list-unstyled arrow_list" style={{fontSize: '17px'}}>
                                <li><strong>ein Kinderspiel ist</strong>. Sie müssen kaum etwas
                                    tun, um nach nur 21 Tagen 10510 Euro auf Ihrem Konto erscheinen zu
                                    sehen, und nach einem Monat – noch mehr.
                                </li>
                                <li><strong>Auf legale Weise und völlig sicher für Ihre Brieftasche</strong>.&nbsp;Um
                                    es zu nutzen, müssen Sie keine komplizierten Verträge unterschreiben
                                    oder irgendwelche Anfangsbeträge bezahlen, bei denen es keine
                                    Gewinngarantie gibt.
                                </li>
                                <li>Sicher. Als eine Weltklasse-Erfindung hat meine Methode <strong>die dreifache
                                    Zufriedenheitsgarantie erhalten: für Originalität, für Qualität und für
                                    Kundenzufriedenheit</strong>.&nbsp;Dank
                                    dieser Garantie können Sie sicher sein, dass Sie über diese Webseite
                                    Zugang zu dem originalen Paket von Ripple für den Erwerb des Zugangs zu
                                    unseren Bildungsdienstleistungen erhalten. Die höchste Qualität wird
                                    Ihre Anforderungen zu 100 % erfüllen.
                                </li>
                            </ul>
                            <div style={{textAlign:'center'}}>
                                <img style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)} src={GPS} className="img-responsive"   />
                            </div>

                        </Col>
                    </Row>
                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim5} style={{float: 'left',  marginRight: '15px',cursor :'pointer'}} alt="" className="img-responsive" />
                            <p>Mit einer solchen Garantie habe ich keinen Moment
                                gezögert. Gut, denn ich habe es geschafft, 56 Tsd. Euro für die
                                Hochzeit meiner Tochter zusammenzusparen. Dank dessen konnte ich die
                                Hochzeit mitfinanzieren, die das Hochzeitspaar verdient hat. Alles war
                                großartig und es hat nichts gemangelt. Meine Tochter war so gerührt und
                                so dankbar für den so einen schönen Abend, dass sie vor Glück weinte!
                                Hätte ich die Erfindung von Herrn Thomas nicht benutzt, wäre das ein
                                Fehler meines Lebens.
                            </p>
                            <h4 className="text-right bold">Elisa Löwenzahn, 63 Jahre, Siegburg</h4>

                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <h3 className="underTheTitle" style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}>Sehen Sie, wie einfach es ist... und kostengünstig!</h3>
                            <p>Das Paket von Ripple erhalten Sie für den Kauf des
                                Zugangs zu unseren Bildungsdienstleistungen. Und zwar auf eine sehr
                                einfache Weise. Füllen Sie einfach das Formular in 2 Minuten aus und
                                nach einigen Tagen erhalten Sie das Paket. Dies ist der einfachste Weg
                                für Sie. Und gleichzeitig der kostengünstigste. Warum?
                            </p>
                            <p>Ich werde offen sein... Die Währung Ripple gewinnt an
                                Wert, also steigt ihr Preis von Tag zu Tag an. Ich wollte wirklich,
                                dass Ripple für jeden normalen Menschen verfügbar wird. Wie ich am
                                Anfang gesagt habe – jeder sollte die Chance haben, viel Geld auf
                                einfache Weise zu gewinnen. Deshalb habe ich ausgehandelt, dass Sie über
                                diese Webseite die Pakete von Ripple beim Kauf des Zugangs zu den
                                Bildungsdienstleistungen zu einem niedrigeren Preis erhalten können. Und
                                später werden Sie nur noch zusehen müssen, wie Ihr Einkommen steigt.
                            </p>

                        </Col>
                    </Row>

                    <Row>
                        <Col className='testim2'>
                            <img onClick={this._scrollBottom.bind(this)} src={Testim6} style={{float: 'left',  marginRight: '15px',cursor :'pointer'}} alt="" className="img-responsive" />
                            <p>Ich glaube, ich habe noch nichts so schnell
                                erledigt. Gerade habe ich das Formular ausgefüllt und schon nach 3 Tagen
                                habe ich das Paket von Ripple zu Hause getestet. Und dann tauchten mit
                                jeder Woche mehrere tausend Euro auf meinem Konto auf... Und obwohl ich
                                früher zwei Stellen auf einmal annehmen musste, um über die Runden zu
                                kommen, denke ich jetzt sogar darüber nach, ob ich sie kündigen soll. In
                                meinem Leben hätte ich nicht gedacht, dass etwas, das mich so wenig
                                Mühe und Geld kostet, mir so helfen könnte. Danke!
                            </p>
                            <h4 className="text-right bold">Ileya Kristall, 52 Jahre, Saarbrücken</h4>

                        </Col>
                    </Row>

                    <Row>
                        <Col  >
                            <p>Vielen Dank, dass Sie sich die Zeit genommen haben,
                                meine Nachricht zu lesen. Ich bin sicher, dass das Paket von Ripple, das
                                Sie für den Kauf des Zugangs zu den Bildungsdienstleistungen erhalten,
                                Ihnen einen Gewinn von 10510 Euro in 21 Tagen bringen wird. Genau wie im
                                Fall meiner Familie und tausenden von anderen Personen. Ich wünsche
                                Ihnen, dass Ihr Konto bis zum Rand gefüllt wird. Sie werden endlich in
                                der Lage sein, all die Dinge zu tun, die Sie sich vorher nicht leisten
                                konnten. Denken Sie aber daran, dass sich eine solche Chance nicht
                                unbedingt wiederholen muss, nutzen Sie sie also so schnell wie möglich.
                                Am besten schon jetzt, um einen Gewinn von 200 %, 300 % oder sogar 1000 %
                                dank der Pakete von Ripple zu erzielen.
                            </p>
                            <p style={{textAlign: 'right'}}>Viel Glück,<br/>Thomas Renck<br />
                            </p>
                            <div className="box_achtung">
                                <h3 className="red">UNERLÄSSLICHE ANMERKUNG</h3>
                                <p>Die Anzahl der Pakete mit der Währung Ripple ist
                                    begrenzt. Buchen Sie deshalb JETZT Ihr Paket. Sie müssen keinen Vertrag
                                    unterschreiben. Sie müssen nicht im Voraus zahlen. Dies ist nicht
                                    erforderlich. Füllen Sie einfach das Formular aus und wählen Sie aus,
                                    wie viele Ripple-Pakete Sie erhalten möchten.
                                </p>
                            </div>


                            <div id="productCounter">
                                <p>Anzahl der erhältlichen Pakete Ripple:</p>
                                <div className="counterFlex">
                                    <div>
                                        <span className="numberProducts" id="hundredth">0</span>
                                        <span className="numberProducts" id="hundredth">0</span>
                                        <span className="numberProducts" id="decimal">7</span>
                                        <span className="numberProducts" id="unity">9</span>
                                    </div>
                                    <span className="text_Counter">z</span>
                                    <div>
                                        <span className="numberProducts" id="hundredth">5</span>
                                        <span className="numberProducts" id="hundredth">0</span>
                                        <span className="numberProducts" id="decimal">0</span>
                                        <span className="numberProducts" id="unity">0</span>
                                    </div>
                                </div>
                            </div>
                            <p style={{textAlign: 'center'}}>Erhalten Sie das Paket Ripple 150 ﻿€ günstiger<br /> für den
                                Kauf des Zugangs zu Bildungsdienstleistungen<br /></p>
                            <div id="bottle">
                                <img onClick={this._scrollBottom.bind(this)} src={Pc_5_small}
                                     style={{width: '50%', marginTop:'20px', marginRight: 'auto', marginLeft: 'auto', display: 'block', cursor:'pointer'}} />
                                <strong>
                                    <center><s style={{color:'#ff0000'}}>300 ﻿€</s> <span
                                        style={{color:'#008800'}}>150﻿ €</span></center>
                                </strong>
                                <div id="bottle_g"><img onClick={this._scrollBottom.bind(this)} src={De_satisfaction}
                                                        style={{width: '80px', height: '80px', position: 'absolute', top: '0', right: '25px', cursor :'pointer'}} />
                                </div>
                            </div>
                            <p>
                                Füllen Sie das untenstehende Formular aus, um das Paket
                                von Ripple für den Kauf des Zugangs zu den Bildungsdienstleistungen zu
                                erhalten und maximale Gewinne zu erzielen.
                            </p>
                            <p className="add_info">
                                Das Startpaket enthält Online-Schulungen mit kostenlosen Kryptowährungseinheiten
                            </p>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12} lg={12} md={12} >
                            <legend className="collapse-processed">
                                <h3>Bestellformular</h3>
                            </legend>
                            <p className="morepad"><strong>JA!</strong> Ich möchte das Ripple Paket
                                für den Kauf des Zugangs zu Bildungsdienstleistungen erhalten. Dies
                                bedeutet, dass meine Wahl absolut sicher ist, da ich kein Geld im Voraus
                                bezahle und eine dreifache Zufriedenheitsgarantie bekomme.
                            </p>

                            <div  ref={ref} className={'spigelForm'} >
                                <div style={{border : '2px solid #2f2d2b', borderRadius:'15px'}}>
                                    <div className="form__top">
                                        <div className="form__title">Jetzt investieren</div>
                                    </div>
                                    <div className="form__body">
                                        <div id="formWrapper2" className="formWrapper">
                                            <form method="post" id="formfull" action=""  className="cpigel-form-post cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Voller Name</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input " id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("firstName")}
                                                               value={this.state.firstName}
                                                               placeholder="z.B. Klaus Meier"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.firstNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>


                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">Nachname</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon full_name-icon"></span>
                                                        <input className="text required full_name sk-input "
                                                               id="full_name"
                                                               type="text"
                                                               name="firstName"
                                                               onChange={this.handleChange("lastName")}
                                                               value={this.state.lastName}
                                                               placeholder="Meier"
                                                               autoComplete="name"
                                                        />
                                                        {
                                                            this.state.lastNameErr ? <label
                                                                className="x-error-x">Erforderlich</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group">
                                                    <div className="fieldset__title">E-Mail-Adresse</div>
                                                    <div className="fieldset">
                                                        <span className="fieldset-icon user_email-icon"></span>
                                                        <input type="text" name="user_email" id="user_email"
                                                               className="user_email required sk-input"
                                                               placeholder="z.B. name@gmail.com" value="" maxLength="255"
                                                               onChange={this.handleChange("email")}
                                                               value={this.state.email}
                                                        />
                                                        {
                                                            this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                Adresse</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset_group phone-fieldset">
                                                    <div className="fieldset__title">Telefonnummer</div>
                                                    <div className="x-phone-replace" data-select2-id="9">
                                                        <div className="x-flex-row" data-select2-id="8">
                                                            {/*
                                                                <div className='input' style={{ width: '30%', float: 'left'}}>
                                                                    <ReactCustomFlagSelect
                                                                        attributesWrapper={{
                                                                            id: "areaCodeWrapper",
                                                                            tabIndex: "1"
                                                                        }} //Optional.[Object].Modify wrapper general attributes.
                                                                        attributesButton={{id: "areaCodeButton"}} //Optional.[Object].Modify button general attributes.
                                                                        attributesInput={{
                                                                            id: "areaCode",
                                                                            name: "areaCode"
                                                                        }} //Optional.[Object].Modify hidden input general attributes.
                                                                        value={currentItem.id} //Optional.[String].Default: "".
                                                                        disabled={false} //Optional.[Bool].Default: false.
                                                                        showSearch={true} ////Optional.[Bool].Default: false.
                                                                        showArrow={false} //Optional.[Bool].Default: true.
                                                                        animate={true} //Optional.[Bool].Default: false.
                                                                        optionList={FLAG_SELECTOR_OPTION_LIST} //Required.[Array of Object(s)].Default: [].
                                                                        // selectOptionListItemHtml={<div>us</div>} //Optional.[Html].Default: none. The custom select options item html that will display in dropdown list. Use it if you think the default html is ugly.
                                                                        // selectHtml={<div>us</div>} //Optional.[Html].Default: none. The custom html that will display when user choose. Use it if you think the default html is ugly.
                                                                        classNameWrapper="" //Optional.[String].Default: "".
                                                                        classNameContainer="" //Optional.[String].Default: "".
                                                                        classNameOptionListContainer="" //Optional.[String].Default: "".
                                                                        classNameOptionListItem="" //Optional.[String].Default: "".
                                                                        classNameDropdownIconOptionListItem={{color: 'red', }} //Optional.[String].Default: "".
                                                                        customStyleWrapper={{}} //Optional.[Object].Default: {}.
                                                                        customStyleContainer={{
                                                                            height: '40px',
                                                                            border: '1px solid #b4b3b3',
                                                                            borderRadius: '5px',
                                                                            paddingBottom: '5px',
                                                                            width: '100%'
                                                                        }} //Optional.[Object].Default: {}.
                                                                        customStyleSelect={{width: "80px"}} //Optional.[Object].Default: {}.
                                                                        customStyleOptionListItem={{

                                                                        }} //Optional.[Object].Default: {}.
                                                                        customStyleOptionListContainer={{
                                                                            maxHeight: '300px', overflow: 'auto', width: '90px',
                                                                        }} //Optional.[Object].Default: {}.
                                                                        onChange={(areaCode) => {
                                                                            this.setState({areaCode: areaCode}, () => {
                                                                                this.handleChangeAreaCode(areaCode);
                                                                            });
                                                                        }}
                                                                    />
                                                                </div>

                                                                */}
                                                            <PhoneInput
                                                                style={{
                                                                    height: '40px',
                                                                    border: '1px solid #b4b3b3',
                                                                    borderRadius: '5px',
                                                                    padding: '8px',
                                                                    width: '100%'
                                                                }}
                                                                placeholder="Enter phone number"
                                                                defaultCountry={this.props.country.country}
                                                                international
                                                                onChange={(e) => {this.setState({phoneValue : e})}}
                                                                value={this.state.phoneValue}

                                                            />

                                                            {/*
                                                                <div className="fieldset_group" style={{width:'70%', float: 'left', paddingLeft :'10px'}}>
                                                                    <div className="fieldset">
                                                                        <span className="fieldset-icon full_name-icon"></span>
                                                                        error
                                                                        <input className="text required full_name sk-input " id="full_name"

                                                                               onChange={this.handleChange("phone")}
                                                                               value={this.state.phone}
                                                                               type="text" placeholder="8710211"
                                                                               autoComplete="name"
                                                                        />
                                                                        {
                                                                            this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                                Telefonnummer</label> : null
                                                                        }
                                                                    </div>
                                                                </div>

                                                                */}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className='x-forms'>
                                                    <div className=" ">

                                                        <label htmlFor="x-form-label" className="checkbox">
                                                            <input type="checkbox"
                                                                   name="checked"
                                                                   id="cb_cond_1_0"
                                                                   checked={this.state.checked}
                                                                   onChange={this.handleInputChange}
                                                                   className="field-error"/>&nbsp;
                                                            <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                        </label>
                                                        {
                                                            this.state.checkedErr ?
                                                                <label className="x-error-red">Verpflichtend</label> : null
                                                        }
                                                        {
                                                            this.props.auth.alert ? <label className="x-error">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                     data-editable="" data-name="step1-send" data-ce-tag="button">
                                                    <button type="button" id="lead-form-submit"
                                                            onClick={this.handleRegistration}
                                                            style={{width:'100%'}}
                                                            className="lead-form-submit">
                                                        Informationen erhalten
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br/>
                            <br/>
                            <br/>
                        </Col>

                    </Row>

                    <Row>
                        <Col>
                            <p style={{textAlign:'center'}}><strong>PS.</strong> Ich erinnere Sie daran, dass nur noch <span id="countL">79</span>
                                Pakete Ripple übrig sind. Später können Sie sie nicht mehr online
                                bestellen.  Die Anzahl der verfügbaren Einheiten nimmt mit jeder Stunde
                                ab. Geben Sie also eine Bestellung auf, um das Paket schon jetzt zu
                                erhalten, und lernen Sie, wie Sie mit dem Verdienen beginnen können.
                            </p>
                        </Col>

                    </Row>

                </Container>

                <Footer
                    logoStatus={false}
                />
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(Remgue2);



