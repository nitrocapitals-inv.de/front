import {Container, Row, Col} from "react-bootstrap";
import React from "react";
import {connect} from "react-redux";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'


import Moment from 'react-moment';
import 'moment/locale/de';

import HeaderImg from '../assets/img/header-img.png'
import LionsMain from '../assets/img/lions_main.jpg'
import Lions3 from '../assets/img/lions3.jpg'
import Lions7 from '../assets/img/lions7.jpg'
import Lions1 from '../assets/img/step1.jpg'
import Lions6 from '../assets/img/lions6.jpg'
import Step2 from '../assets/img/step2.jpg'
import Img6 from '../assets/img/img6.jpg'

import Prof1 from '../assets/img/prof1.jpg'
import Prof2 from '../assets/img/prof2.jpg'
import Prof3 from '../assets/img/prof3.jpg'
import Prof4 from '../assets/img/prof4.jpg'
import Prof5 from '../assets/img/prof5.jpg'
import Logo from '../assets/img/spiegellogo.png'
import {NavLink} from "react-router-dom";
import ReactCustomFlagSelect from "react-custom-flag-select";
import {FLAG_SELECTOR_OPTION_LIST} from "../helper/country";
import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'

import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};



const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();

class SpiegelDe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeId: 0,
            areaCodeName: 'AT',
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            phoneValue: '',
            theposition: '',
            scrollPosition: false,
            messagesEndRef : React.createRef()

        };
        this.submit = this.submit.bind(this);

    }

    componentDidMount() {
        // this.scrollToBottom();
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }


    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop

        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled > 0) {
            this.setState({
                scrollPosition : true
            })
        }else {
            this.setState({
                scrollPosition : false
            })
        }
    }

    handleScroll (event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop/3 - 60);

        /* this.setState({
             transform: itemTranslate
         });*/
    }
    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
          //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if(name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        }
        else if(name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        }
        else if(name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        }
        else if(name === 'email' ) {
            this.setState({
                [name]: event.target.value
            })
        }
    }
    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    handleRegistration = (event) => {
        if (this.state.firstName) {
            if ( this.state.firstName.length <= 3 || this.state.firstName >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage : 'Erforderlich'})
            }else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage : 'Erforderlich'})
        }

        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }

        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        )
        {
            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });

    }
    render() {

        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
            <div style={{backgroundColor: 'rgb(221, 219, 217)', height: '100%'}}>

                <Container fluid style={{padding: '0px', backgroundColor: '#e64415'}}>
                    <Container>
                        <nav className="navbar navbar-light spigel-navbar spigel-navbar"  >
                            <a className="navbar-brand">
                                <img
                                    style={{width: '80%'}}
                                    src={Logo}/>
                            </a>
                            <div className="form-inline">

                                <a href={null}
                                   style={{cursor:'pointer'}}
                                   onClick={this._scrollBottom.bind(this)}
                                   title="Abonnement"
                                   className="abonnement"
                                   > <span className="sm:hidden border-b">Abonnement</span>
                                    <span
                                        className="abonnement-mob">Abo</span>
                                </a>
                                <a href={null} onClick={this._scrollBottom.bind(this)} className='Anmelden' title="Jetzt anmelden" style={{cursor:'pointer'}}>

                                   <span
                                       className="relative bottom-px">Anmelden</span>
                                </a>
                                {/*
                               <input className="form-control mr-sm-2" type="search" placeholder="Search"
                                      aria-label="Search" />
                               <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>*/}
                            </div>
                        </nav>
                    </Container>
                </Container>


                <br/>
                <br/>
                <Container style={{backgroundColor: 'white', padding: '30px'}}>
                    <Row>
                        <Col>
                            <div>
                                <p className='text-primary-base'>
                                    Spiegel sucht die Wahrheit über den geheimen Weg, Geld zu verdienen.
                                </p>
                            </div>

                            <h2>
                                Die "Höhle der Löwen" macht deutsche Bürger reich mit einem neuen Geheimsystem! Die
                                Folge wird nicht ausgestrahlt, weil der Sender wütend ist
                            </h2>

                            <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}> <img style={{width: '100%', height: 'auto'}} src={LionsMain}
                                                      title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                                                      alt="&quot;Unzureichend&quot;: Bundeskanzleramt"/>
                            </a>
                            <div className="  text-shade-dark mb-4">Von <a
                                className="text-black font-bold border-b border-shade-light hover:border-black" href={null}
                                style={{cursor:'pointer'}}
                                 title="Marcel Rosenbach"> Marcel Rosenbach</a>
                                <br/>

                                <time className="timeformat" dateTime="2020-09-28 15:41:22">
                                    <Moment  format="dddd" date={new Date()} locale={'de'} />, <Moment  format="MMMM DD, YYYY" date={new Date()} />
                                    , 15.41 Uhr
                                </time>
                            </div>


                            <div style={{
                                borderTop: '1px dotted #dddbd9',

                            }}>

                            </div>
                        </Col>
                    </Row>

                    <Row className='RichText'>
                        <p>(Spiegel) – Die achte Staffel von "Die Höhle der Löwen" ist vorbei. Die Zuschauer erwarten
                            neue Produkte und interessante Ideen, aber eine Folge der Show wurde nicht ausgestrahlt,
                            weil die Genehmigung widerrufen wurde. In dieser Folge wurde ein
                            System gezeigt, mit dem deutsche Staatsbürger bequem von zu Hause aus 7.380,10 €
                            verdienen können – PRO TAG. Ja, Sie haben richtig gelesen – täglich.</p>
                        <p><i>BILD</i> hat die Folge verboten. Wir wollen euch also genau erklären, was es ist und warum
                            der Sender diese Folge verboten hat.</p>
                        <p><i>Spiegel</i> traf Carsten Maschmeyer, einen der Investoren von „Die Höhle der Löwen“, der
                            von diesem System sehr begeistert war. "Es ist wirklich schade, dass die Politiker wütend
                            werden und es geheim halten wollen. Ich hoffe, es wird im Internet bekannt und hilft
                            möglichst vielen Deutschen, finanzielle Freiheit zu erlangen", sagte Maschmeyer im Interview
                            mit unserer Redaktion.</p>

                        <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}>
                            <img style={{width: '100%', height: 'auto'}} src={Lions3}
                                 title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                                 alt="&quot;Unzureichend&quot;: Bundeskanzleramt" width="948" height="536"/> </a>

                        <p>Analysieren wir nun das berühmte System: Jeder Bürger kann ein monatliches Einkommen im 5-
                            oder gar 6-stelligen Bereich erzielen. Zwei namhafte deutsche Informatiker haben diese
                            automatisierte Handelsplattform „"<a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}>Nitrocapitals</a>" auf den Markt
                            gebracht.</p>

                        <p>Die Idee ist einfach: Dem Durchschnittsbürger den Zugang zu diesem vollautomatisierten
                            Verfahren zu ermöglichen, um in Börsenschwankungen investieren zu können, auch wenn er kein
                            Geld für eine Investition hat oder keine Branchenkenntnis hat. Ein von den beiden
                            Informatikern entwickelter Algorithmus wählt den perfekten Zeitrahmen aus, um günstige
                            Aktien zu kaufen und diese dann gewinnmaximierend zu verkaufen. Die besten aktuellen
                            Beispiele für dieses Genre sind Tesla (TSLA) und GameStop (GME). Dank dieser beiden Titel
                            haben es einige Leute geschafft, dank <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}>Nitrocapitals</a> mehrere
                            Tausend Euro pro Tag zu verdienen.</p>

                        <p>Die beiden Jungunternehmer wollen eine kleine Provision verdienen, aber nur von den bereits
                            erzielten Gewinnen. Der Geschäftswert der Software wird bereits auf über 20 Millionen Euro geschätzt.</p>

                        <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}><img style={{width: '100%', height: 'auto'}} src={Lions7}
                                                 title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                                                 alt="&quot;Unzureichend&quot;: Bundeskanzleramt"/>
                        </a>


                        <p>In dem zu Spiegel gehörenden Videomaterial der Sendung sind die
                            "Löwen" skeptisch und glauben den beiden Informatikern nicht. Sätze wie "ob es wirklich
                            funktioniert, weil es nicht bekannt ist oder weil es nicht schon jeder macht" werden am
                            häufigsten gesagt.</p>
                        <p>Informatiker erklären, dass dieser Trend in den USA schon seit einiger Zeit besteht. In den
                            USA ist dies jedoch nur mit Software möglich, die für wenige Personen zugänglich ist. „Wir
                            wollten diese Technologie der breiten Masse zugänglich machen und unser Algorithmus sorgt
                            dafür, dass der Gewinn maximiert wird“, sagen sie.</p>
                        <p>Die Löwen bleiben skeptisch, bis Nico Rosberg, Internet- und Technologieexperte bei
                            Löwenrunde, bestätigt, dass es nur eine Frage der Zeit war, bis jemand eine solche Software
                            entwickelt.</p>
                        <p>Er ist der erste, der <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}> Nitrocapitals</a> direkt in der Show testet.</p>
                        <p>„Ich habe schon von der Börse gehört und von der Möglichkeit, viel Geld zu verdienen, aber
                            ich habe noch nie teilgenommen. Man weiß nicht genau, wo man anfangen soll, wenn man sich
                            mit der Börse nicht auskennt. Es war sehr In diesem Fall habe ich einfach Geld mit meiner
                            Kreditkarte hinterlegt und das System erledigt den Rest.“ sagt Nico.</p>
                        <p>Nach der Einzahlung in Höhe von €250, hat die Trading-Plattform damit
                            angefangen günstig einzukaufen und teuer zu verkaufen. So ist das Startkapital innerhalb von
                            nur drei Minuten auf €323,18 gestiegen, ein Gewinn von €73,18.</p>

                        <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}> <img style={{width: '100%', height: 'auto'}} src={Lions1}
                                                  title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                                                  alt="&quot;Unzureichend&quot;: Bundeskanzleramt"/> </a>

                        <p>Nach nur 8 Minuten bereits auf 398,29€.</p>
                        <p>Alle Löwen wollten plötzlich investieren und es entstand ein richtiger Wettlauf. Am Ende der
                            Show hatte Nico Rosberg mit der automatisierten Software 613,79 Euro von den 250 Euro, die
                            er eingezahlt hatte, verdient. Kein Wunder, dass Eliten und Politiker in Deutschland nicht
                            wollen, dass die einfachen Leute so leicht Geld verdienen. <i>Spiegel</i> findet, dass JEDER
                            das Recht haben sollte, diese Chance zu nutzen.</p>
                        <h4><b>Wir haben <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}> Nitrocapitals</a> selbst getestet</b></h4>
                        <p>Zuerst haben wir ein kostenloses Konto beim deutschen Broker <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}>Nitrocapitals</a> eröffnet.</p>
                        <p>Stellen Sie sicher, dass Sie bei der Registrierung eine echte Handynummer angeben, da der
                            Kundenservice Sie kontaktieren wird, um Sie darüber zu informieren, dass Sie viel Geld
                            verdienen. </p>
                        <p>Der Broker verdient nur, wenn Sie auch verdienen, da er 1% von erfolgreichen Trades erhält.
                            Das heißt: Verliert der Kunde Geld, verdient der Broker NICHTS! Deshalb helfen wir Ihnen
                            jederzeit gerne weiter.</p>

                        <p>Klicken Sie nach dem Öffnen auf Einzahlung. Dort haben Sie mehrere Einzahlungsmöglichkeiten,
                            um sofort loszulegen!</p>

                        <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}><img style={{width: '100%', height: 'auto'}} src={Lions6}
                                                 title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                                                 alt="&quot;Unzureichend&quot;: Bundeskanzleramt"/> </a>


                        <h3>Tag 1</h3>
                        <p>In der Show hat Nico nach 3 Minuten 73,18 € verdient, bei mir hat es aber etwas länger
                            gedauert. Die Plattform machte nach rund 30 Minuten nur einen Gewinn von 80,19 €, immer noch
                            beeindruckend! Ich hatte vorher keine Erfahrung an der Börse, aber mit <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}>Nitrocapitals</a> werden sie nicht benötigt, da ich sofort Gewinne gemacht habe.</p>
                        <p>Ich verbrachte ungefähr 5 Minuten pro Tag damit, meine Ergebnisse zu überprüfen. Nach 1 Tag
                            weist mein Konto einen Saldo von 1.150 € aus. Eine unglaubliche Steigerung gegenüber meiner
                            ersten Einzahlung. Ich wurde ein echter Fan der Plattform.</p>
                        <h3>Tag 2</h3>
                        <p>Nach 2 Tagen war meine Anfangsinvestition auf 2.357 € gestiegen. An diesem Punkt begann ich
                            darüber nachzudenken, wie ich das Geld ausgeben sollte. Als Journalist verdiene ich nicht
                            einmal in einem Monat so viel Geld, während ich hier weniger als 30
                            Minuten damit verbracht habe, mein Konto zu überprüfen.</p>
                        <h3>Tag 3</h3>
                        <p>Ich habe beschlossen, die Gewinne nach 3 Tagen auszuzahlen, nur um zu testen, ob das auch
                            wirklich funktioniert. Am Ende hatte ich €3.784 Gewinn und €79.51 negative Handeln auf
                            der Plattform. Ich habe meine Handel- Geschichte (meine Handeln-Protokolle) durchgesehen und
                            festgestellt, dass nicht jeder Handel profitabel ist, manche sogar Geld verlieren.</p>
                        <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}> <img style={{width: '100%', height: 'auto'}} src={Step2}
                                                  title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                                                  alt="&quot;Unzureichend&quot;: Bundeskanzleramt"/> </a>


                        <p>Die Plattform kann natürlich nicht zaubern, aber 92% meiner Trades waren profitabel, und
                            somit das Ergebnis mehr als zufriedenstellend: ein Reingewinn von €3.784 auf meine
                            anfängliche Einzahlung von €250. Es hat mich weniger als 30 Minuten Arbeit pro Woche
                            gekostet und ich hatte keinerlei Erfahrung in diesem Bereich, weder mit Aktien, noch mit
                            irgendwelchen technischen Spielereien. Nun kann ich meiner Frau und mir endlich unseren
                            jahrelangen Traum erfüllen unseren Garten umzubauen.</p>

                        <img style={{width: '100%', height: 'auto'}} src={Img6}
                             title="&quot;Unzureichend&quot;: Bundeskanzleramt"
                             alt="&quot;Unzureichend&quot;: Bundeskanzleramt"/>

                        <p>Ps. Das Geld war nach drei Werktagen auf meinem Konto bei der Sparkasse Berlin.</p>


                        <h3>Nach 2 Wochen</h3>
                        <p>Es ging täglich so weiter, allerdings war ich nun beruhigter, dass es auch wirklich
                            funktioniert nachdem die ersten Auszahlungen auf mein Bankkonto überwiesen wurden. Jetzt
                            nach knapp 2 Wochen hat <a onClick={this._scrollBottom.bind(this)} style={{color : 'rgb(0, 177, 221)', cursor : 'pointer'}} href={null}> Nitrocapitals</a> €13.352 für mich verdient.
                            Ich kann es immer noch nicht ganz glauben. Es ist wirklich atemberaubend, wenn man überlegt,
                            was man mit dem Geld alles kaufen kann.</p>

                        <h3>Ist die Software auch für Sie geeignet? Unser Urteil:</h3>

                        <p>Wie wir bei <i>Spiegel</i> stehen Sie dem Internet-Verdienstsystem vielleicht etwas skeptisch
                            gegenüber. So waren die Löwen zuvor. Jetzt sind alle begeistert und wir können nur jedem
                            raten, diesen Weg einzuschlagen.</p>
                        <p>Sie werden davon begeistert sein. Die Software bietet auch eine Zufriedenheitsgarantie. Dies
                            bedeutet, dass Sie Ihre Anzahlung sofort erhalten, wenn Sie nicht zufrieden sind.</p>

                    </Row>

                    <Row>

                        <Col xs={12} lg={12} md={12} >
                          <div  ref={ref} className={'spigelForm'} >
                              <div style={{border : '2px solid #2f2d2b', borderRadius:'15px'}}>
                                  <div className="form__top">
                                      <div className="form__title">Jetzt investieren</div>
                                  </div>
                                  <div className="form__body">
                                      <div id="formWrapper2" className="formWrapper">
                                          <form method="post" id="formfull" action=""  className="cpigel-form-post cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                              <div className="fieldset_group">
                                                  <div className="fieldset__title">Voller Name</div>
                                                  <div className="fieldset">
                                                      <span className="fieldset-icon full_name-icon"></span>
                                                      <input className="text required full_name sk-input " id="full_name"
                                                             type="text"
                                                             name="firstName"
                                                             onChange={this.handleChange("firstName")}
                                                             value={this.state.firstName}
                                                             placeholder="z.B. Klaus Meier"
                                                             autoComplete="name"
                                                      />
                                                      {
                                                          this.state.firstNameErr ? <label
                                                              className="x-error-x">Erforderlich</label> : null
                                                      }
                                                  </div>
                                              </div>


                                              <div className="fieldset_group">
                                                  <div className="fieldset__title">Nachname</div>
                                                  <div className="fieldset">
                                                      <span className="fieldset-icon full_name-icon"></span>
                                                      <input className="text required full_name sk-input "
                                                             id="full_name"
                                                             type="text"
                                                             name="firstName"
                                                             onChange={this.handleChange("lastName")}
                                                             value={this.state.lastName}
                                                             placeholder="Meier"
                                                             autoComplete="name"
                                                      />
                                                      {
                                                          this.state.lastNameErr ? <label
                                                              className="x-error-x">Erforderlich</label> : null
                                                      }
                                                  </div>
                                              </div>

                                              <div className="fieldset_group">
                                                  <div className="fieldset__title">E-Mail-Adresse</div>
                                                  <div className="fieldset">
                                                      <span className="fieldset-icon user_email-icon"></span>
                                                      <input type="text" name="user_email" id="user_email"
                                                             className="user_email required sk-input"
                                                             placeholder="z.B. name@gmail.com" value="" maxLength="255"
                                                             onChange={this.handleChange("email")}
                                                             value={this.state.email}
                                                      />
                                                      {
                                                          this.state.emailErr ? <label className="x-error-x">Ungültige
                                                              Adresse</label> : null
                                                      }
                                                  </div>
                                              </div>

                                              <div className="fieldset_group phone-fieldset">
                                                  <div className="fieldset__title">Telefonnummer</div>
                                                  <div className="x-phone-replace" data-select2-id="9">
                                                      <div className="x-flex-row" data-select2-id="8">
                                                          <PhoneInput
                                                              style={{
                                                                  height: '40px',
                                                                  border: '1px solid #b4b3b3',
                                                                  borderRadius: '5px',
                                                                  padding: '8px',
                                                                  width: '100%'
                                                              }}
                                                              placeholder="Enter phone number"
                                                              defaultCountry={this.props.country.country}
                                                              international
                                                              onChange={(e) => {this.setState({phoneValue : e})}}
                                                              value={this.state.phoneValue}

                                                          />
                                                          {
                                                              this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                  Telefonnummer</label> : null
                                                          }
                                                      </div>
                                                  </div>
                                              </div>

                                              <div className='x-forms'>
                                                  <div className=" ">

                                                      <label htmlFor="x-form-label" className="checkbox">
                                                          <input type="checkbox"
                                                                 name="checked"
                                                                 id="cb_cond_1_0"
                                                                 checked={this.state.checked}
                                                                 onChange={this.handleInputChange}
                                                                 className="field-error"/>&nbsp;
                                                          <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                      </label>
                                                      {
                                                          this.state.checkedErr ?
                                                              <label className="x-error-red">Verpflichtend</label> : null
                                                      }
                                                      {
                                                          this.props.auth.alert ? <label className="x-error">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                      }
                                                  </div>
                                              </div>

                                              <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                   data-editable="" data-name="step1-send" data-ce-tag="button">
                                                  <button type="button" id="lead-form-submit"
                                                          onClick={this.handleRegistration}
                                                          style={{width:'100%'}}
                                                          className="lead-form-submit">
                                                      Informationen erhalten
                                                  </button>
                                              </div>
                                          </form>

                                      </div>
                                  </div>
                              </div>

                          </div>
                            <br/>
                            <br/>
                            <br/>
                        </Col>

                    </Row>
                    <Row style={{padding : '0px 50px'}}>


                        <Col>
                            <p>(321) nutzen gerade das Angebot</p>
                            <h3>Spezialangebot für unsere Leser</h3>
                            <p>
                                <br/>
                            </p>
                            <p>
                               <h3> <a href={null}  onClick={this._scrollBottom.bind(this)} style={{color: '#00B1DD'}}>  Hier klicken um  Nitrocapitals  auszuprobieren </a></h3>
                            </p>


                            <p style={{margin: '5px 0', fontSize: '15px', color: '#aaa', lineHeight: '26px'}}>Benutzen Sie
                                diesen exklusiven Formular und melden Sie sich heute noch an. <br/>100%ige
                                Zufriedenheitsgarantie oder Sie bekommen Ihre Einzahlung Zurück!</p>

                            <p style={{margin: '5px 0', fontSize: '15px', color: '#aaa', lineHeight: '26px'}}>
                                <Moment  format="dddd" date={new Date()} locale={'de'} />, <Moment  format="MMMM DD, YYYY" date={new Date()} />

                            </p>
                        </Col>
                    </Row>

                    <Row>
                       <Col>
                           <div className="comments-inner">
                               <h1>Kommentare</h1>
                               <div className="comment-box">
                                   <div className="comment-pic inl">
                                       <a href="#" >
                                           <img src={Prof1}/>
                                       </a>
                                   </div>
                                   <div className="comment-text comment-text-padding-left">
                                       <div className="comment-text-inner">
                                           <h1>Tony Lewis</h1>
                                           <p>Die Software ist großartig! Ich benutze sie jetzt schon seit zwei Tagen, und
                                               ich habe bereits über 6000 Euro verdient!</p>
                                           <h2><span>Reply . 13 . Like .</span> 12 minutes ago</h2>
                                       </div>
                                   </div>
                               </div>
                               <div className="comment-box">
                                   <div className="comment-pic inl">
                                       <a href="#" >
                                           <img src={Prof2}/>
                                       </a>
                                   </div>
                                   <div className="comment-text comment-text-padding-left">
                                       <div className="comment-text-inner">
                                           <h1>Elias Porquez</h1>
                                           <p>Seit knapp einer Woche nutze ich die Software nun und ich habe mir 7000 Euro
                                               ausgezahlt und bin bereits wieder bei 5000 Euro plus :D</p>
                                           <h2><span>Reply . 14 . Like .</span> 16 minutes ago</h2>
                                       </div>
                                   </div>
                               </div>
                               <div className="comment-box">
                                   <div className="comment-pic inl">
                                       <a href="#" >
                                           <img src={Prof3}/>
                                       </a>
                                   </div>
                                   <div className="comment-text comment-text-padding-left">
                                       <div className="comment-text-inner">
                                           <h1>Armando J.</h1>
                                           <p>Das ist ja nichts neues. Mein Bruder lebt in den USA und verdient damit seit
                                               Monaten mit Aktienkurse ein Vermögen. Schön, dass es jetzt für den
                                               Aktien-Crash nach Deutschland gekommen ist. Ferrari oder Lambo? Was kaufe
                                               ich
                                               mir denn nächsten Monat? :))))</p>
                                           <h2><span>Reply . 2 . Like .</span> 1 hour ago</h2>
                                       </div>
                                   </div>
                               </div>
                               <div className="comment-box">
                                   <div className="comment-pic ">
                                       <a href="#" >
                                           <img src={Prof4}/>
                                       </a>
                                   </div>
                                   <div className="comment-text inl">
                                       <div className="comment-text-inner comment-text-padding-left">
                                           <h1>Paul Kash</h1>
                                           <p>Geiles Teil aber eine Frechheit was die Politik wieder mal unterm Tisch
                                               halten
                                               will. Ich werde es jedenfalls jetzt testen.</p>
                                           <h2><span>Reply . 13 . Like .</span> 12 minutes ago</h2>
                                       </div>
                                   </div>
                               </div>
                               <div className="comment-box">
                                   <div className="comment-pic  ">
                                       <a href="#" >
                                           <img src={Prof5}/>
                                       </a>
                                   </div>
                                   <div className="comment-text inl">
                                       <div className="comment-text-inner comment-text-padding-left">
                                           <h1>Daniel Barrot</h1>
                                           <p>Kann nur bestätigen wie gut es funktioniert. Am ersten Tag bin ich auf über 4
                                               mille gekommen. Eigentlich unglaublich.</p>
                                           <h2><span>Reply . 13 . Like .</span> 12 minutes ago</h2>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </Col>
                    </Row>
                </Container>
                <br/>
                <br/>

                <Container  fluid style={{backgroundColor:'#2f2d2b',  textAlign : 'center', height :'200px', padding :'25px'}}>

                    <div
                        style={{fontWeight : '700', fontSize : '21px', color :'white'}}
                         >SPIEGEL Gruppe
                        <br/>
                        <br/>
                    </div>

                    <Container >
                        <Row>
                            <Col>
                                <a href={null} style={{cursor:'pointer'}}
                                   title="Abo"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Abo</a>
                            </Col>
                            <Col>
                                <a href={null} style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)}
                                   title="Shop"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Shop</a>
                            </Col>
                            <Col>
                                <a href={null} style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="bento"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">bento</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="buchreport"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">buchreport</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="Werbung"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Werbung</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="Jobs"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Jobs</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="MANUFACT"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">MANUFACT</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="SPIEGEL  Ed"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">SPIEGEL  Ed</a>
                            </Col>
                        </Row>
                    </Container>


                    <div style={{padding :'10px 0 10px 0', width:'100%'}}>
                        <div style={{borderTop: '2px dotted  #807e7c', width:'100%'}}>  </div>
                    </div>

                    <Container >
                        <Row>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)}
                                   title="Impressum"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Impressum</a>
                            </Col>
                            <Col>
                                <a href={null} style={{cursor:'pointer'}}  onClick={this._scrollBottom.bind(this)}
                                   title="Datenschutz"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Datenschutz</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="Nutzungsbedingungen"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Nutzungsbedingungen</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)} title="Kontakt"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Kontakt</a>
                            </Col>
                            <Col>
                                <a href={null}  style={{cursor:'pointer'}} onClick={this._scrollBottom.bind(this)}  title="Hilfe"
                                   className="text-shade-light text-s pr-8 hover:text-shade-lightest focus:text-white inline-block sm:pr-16 sm:pb-18">Hilfe</a>
                            </Col>
                        </Row>
                    </Container>


                    <div
                        className="flex lg:justify-center md:justify-center sm:justify-start lg:text-center md:text-center sm:flex-wrap sm:-mt-18">

                        <a href={null}  style={{cursor:'pointer'}} title="Wo Sie uns noch folgen können"
                           className="flex items-center text-shade-light hover:text-shade-lightest focus:text-white text-s font-sansUI font-normal lg:px-12 md:px-12 sm:pr-24 sm:mt-18">
                            <span className="ml-8 text-left"> Wo Sie uns noch folgen können </span>
                        </a>
                    </div>
                </Container>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}
export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(SpiegelDe);


