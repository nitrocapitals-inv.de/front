import React, {Component} from 'react'
import {connect} from "react-redux";

import {Container, Row, Col} from "react-bootstrap";
import "../assets/css/carousel.css";
import { Carousel } from 'react-responsive-carousel';
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'


import TeslaMotors from '../assets/img/tesla/Tesla_Motors.png'
import TESLA from '../assets/img/tesla/TESLA.png'
import DssForm from '../assets/img/tesla/dss-form.png'

import D222 from '../assets/img/tesla/222.png'
import Noun_Profit_3038634_1 from '../assets/img/tesla/noun_Profit_3038634_1.png'
import Noun_Value_865914_1 from '../assets/img/tesla/noun_Value_865914_1.png'
import Pic1 from '../assets/img/tesla/pic1.png'
import Pic2 from '../assets/img/tesla/pic2.png'
import Pic3 from '../assets/img/tesla/pic3.png'


import Milan from '../assets/img/tesla/milan.png'
import D11 from '../assets/img/tesla/11.png'
import D22 from '../assets/img/tesla/22.png'
import D33 from '../assets/img/tesla/33.png'
import Ellipse6 from '../assets/img/tesla/Ellipse-6.png'
import Ellipse8 from '../assets/img/tesla/Ellipse-8.png'
import Ellipse11 from '../assets/img/tesla/Ellipse-11.png'
import Ellipse12 from '../assets/img/tesla/Ellipse-12.png'
import Ellipse10 from '../assets/img/tesla/Ellipse-10.png'
import {Footer} from "../components/layout/footer";

import {FLAG_SELECTOR_OPTION_LIST} from '../helper/country'

import {checkCountryRequest, signUpRequest, signUpForCRMRequest} from '../store/actions'
import {CountryInitialName} from "../helper/countryName";
import {onlyNumbers, onlyStringAndSpice, sameLetters, validateEmail} from "../helper/regexs";
import {PhoneNumberLength} from "../helper/phoneNumberLength";
import ReactCustomFlagSelect from "react-custom-flag-select";

const find = (arr, obj) => {
    const res = [];
    arr.forEach((o) => {
        let match = false;
        Object.keys(obj).forEach((i) => {
            if (obj[i] === o[i]) {
                match = true;
            }
        });
        if (match) {
            res.push(o);
        }
    });
    return res;
};

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;
const ref = React.createRef();

class TeslaInvestmentDe extends Component {

    constructor(props) {
        super(props);
        this.state = {
            areaCode: DEFAULT_AREA_CODE,
            areaCodeName: CountryInitialName(DEFAULT_AREA_CODE),
            areaCodeId: 0,
            hasPhoneError: true,
            validate: false,

            checked: false,
            lgShow: false,
            firstNameErr: false,
            lastNameErr: false,
            emailErr: false,
            phoneErr: false,
            checkedErr: false,

            firstName: '',
            lastName: '',
            phone: '',
            email: '',

            theposition: '',
            scrollPosition: false,
            fixPosition: false,
            fixPositionHeight: 0,
            priceA :'',
            priceB :'',
            phoneValue: '',
            messagesEndRef: React.createRef(),


        };
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        this.props.checkCountryRequest()
        window.addEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.country.flagId !== prevState.areaCodeId) {
            this.setState({
                areaCodeId: prevProps.country.flagId,
                areaCode: FLAG_SELECTOR_OPTION_LIST[prevProps.country.flagId].id,
                areaCodeName: prevProps.country.country
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleChangeAreaCode(res) {
        this.setState({
            areaCodeName: CountryInitialName(res)
        })
    }

    listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop


        const height =
            document.documentElement.scrollHeight -
            document.documentElement.clientHeight

        const scrolled = winScroll / height

        if (scrolled >= 0.9) {
            const fixTop = height - 550;
            this.setState({
                fixPosition: true,
                // fixPositionHeight: fixTop,
            })
        }else {
            this.setState({
                fixPosition: false,
            })
        }



        if (scrolled > 0) {
            this.setState({
                scrollPosition: true
            })
        } else {
            this.setState({
                scrollPosition: false
            })
        }

    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop,
            itemTranslate = Math.min(0, scrollTop / 3 - 60);
    }

    handlePhoneChange(res) {
        this.setState({phone: res});
    }

    toggleValidating(validate) {
        this.setState({validate});
    }

    submit(e) {
        e.preventDefault();
        this.toggleValidating(true);
        const {hasPhoneError} = this.state;
        if (!hasPhoneError) {
            //  alert("You are good to go");
        }
    }


    handleInputChange = (event) => {
        this.setState({
            checked: !this.state.checked
        })
    }

    handleChange = name => event => {
        if (name === 'priceA' && onlyNumbers(event.target.value)){
            this.setState({
                [name]: event.target.value
            })
        }


        if (name === 'firstName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'lastName' && onlyStringAndSpice(event.target.value)) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'phone' && onlyNumbers(event.target.value) || event.target.value.length < 1) {
            this.setState({
                [name]: event.target.value
            })
        } else if (name === 'email') {
            this.setState({
                [name]: event.target.value
            })
        }
    }

    handleRegistration = (event) => {

        if (this.state.firstName) {
            if (this.state.firstName.length <= 3 || this.state.firstName.length >= 16 || !onlyStringAndSpice(this.state.firstName) || !sameLetters(this.state.firstName)) {
                this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
            } else {
                this.setState({firstNameErr: false})
            }
        } else {
            this.setState({firstNameErr: true, firstNameErrMessage: 'Erforderlich'})
        }


        if (this.state.lastName) {
            if ( this.state.lastName.length <= 3 || this.state.lastName.length >= 16 || !onlyStringAndSpice(this.state.lastName) || !sameLetters(this.state.lastName)) {
                this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
            }else {
                this.setState({lastNameErr: false})
            }
        } else {
            this.setState({lastNameErr: true, emailErrMessage : 'Erforderlich'})
        }

        if (this.state.email && validateEmail(this.state.email)) {
            this.setState({emailErr: false})
        } else {
            this.setState({emailErr: true})
        }


        if (this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue)) {
            this.setState({phoneErr: false})
        }else {
            this.setState({phoneErr: true})
        }
        if (this.state.checked) {
            this.setState({checkedErr: false})
        } else {
            this.setState({checkedErr: true})
        }
        event.preventDefault()


        if (
            sameLetters(this.state.firstName) && sameLetters(this.state.lastName) &&
            this.state.firstName && (this.state.firstName.length > 3 || this.state.firstName < 16  )  &&
            this.state.lastName && (this.state.lastName.length > 3 || this.state.lastName < 16) &&
            this.state.email && validateEmail(this.state.email) &&
            this.state.phoneValue && isPossiblePhoneNumber(this.state.phoneValue) && isValidPhoneNumber(this.state.phoneValue) &&
            this.state.checked
        ) {
            let item = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                email : this.state.email,
                phone : this.state.phoneValue,
                areaCodeName : this.state.areaCodeName,
                Source : process.env.REACT_APP_IP_WEB+''+window.location.pathname.split('/')[1],
                domenSource : process.env.REACT_APP_DOMEN_SOURCE ,
                affiliateId : process.env.REACT_APP_ID,
                Token : process.env.REACT_APP_TOKEN,
                urlSearch : window.location.search
            }
            this.props.signUpForCRMRequest(item)
        }


    }

    _scrollBottom() {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }


    render() {
        const {areaCode, phone, validate} = this.state;
        const currentItem = find(FLAG_SELECTOR_OPTION_LIST, {id: areaCode})[0];
        return (
         <>
             <section id="section-1-8" className="ct-section">
                 <img src={TeslaMotors} className="tesla-logo" alt=""/>
                     <div className="ct-section-inner-wrap">
                         <div id="new_columns-26-8" className="ct-new-columns">

                             <Container>
                                 <Row>
                                     <Col xs={12} md={8}>
                                         <div id="div_block-27-8" className="ct-div-block hero-text">
                                             <h2>250€ Investment in Tesla-Aktien kann Ihnen 2021 einen super Gewinn einbringen!</h2>
                                             <p>Profitieren auch Sie dauerhaft von Teslas ansteigender Reichweite. Wir
                                                 zeigen<br/> Ihnen wie´s geht.
                                             </p>
                                             <a href="#calculator">Geschätzter Gewinn</a>
                                             <h4>Registrieren Sie sich jetzt und einer unserer Agenten wird sich<br/> direkt
                                                 telefonisch bei Ihnen
                                                 melden.</h4>
                                         </div>
                                     </Col>
                                     <Col xs={12} md={4}>
                                         <div id="div_block-28-8" className="cdt-div-block">
                                             <div id="div_block-179-31" className="ct-div-block">
                                                 <h2 id="headline-180-31" className="ct-headline">Investieren <img
                                                     src={TESLA} alt=""/> <br/></h2>
                                                 <div id="code_block-181-31" className="ct-code-block">
                                                     <div className="reg-form" style={{textAlign:'left'}}>
                                                         <form method="post" id="formfull" action="" className="form-mob-padding cpmn-form lp-form lead-form placeholder-form tradingapp_emailForm form-de sv-gen-2 sv-skin sv-skin-default form-fullname">
                                                             <div className="fieldset_group">
                                                                 <div className="fieldset__title">Vorname</div>
                                                                 <div className="fieldset">
                                                                     <span className="fieldset-icon full_name-icon"></span>
                                                                     <input className="text required full_name sk-input "
                                                                            id="full_name"
                                                                            type="text"
                                                                            onChange={this.handleChange("firstName")}
                                                                            value={this.state.firstName}
                                                                            placeholder="Klaus"
                                                                            autoComplete="name"
                                                                     />
                                                                     {
                                                                         this.state.firstNameErr ? <label
                                                                             className="x-error-x">Erforderlich</label> : null
                                                                     }
                                                                 </div>
                                                             </div>

                                                             <div className="fieldset_group">
                                                                 <div className="fieldset__title">Nachname</div>
                                                                 <div className="fieldset">
                                                                     <span className="fieldset-icon full_name-icon"></span>
                                                                     <input className="text required full_name sk-input "
                                                                            id="full_name"
                                                                            type="text"
                                                                            onChange={this.handleChange("lastName")}
                                                                            value={this.state.lastName}
                                                                            placeholder="Meier"
                                                                            autoComplete="name"
                                                                     />
                                                                     {
                                                                         this.state.lastNameErr ? <label
                                                                             className="x-error-x">Erforderlich</label> : null
                                                                     }
                                                                 </div>
                                                             </div>

                                                             <div className="fieldset_group">
                                                                 <div className="fieldset__title">E-Mail-Adresse</div>
                                                                 <div className="fieldset">
                                                                     <span className="fieldset-icon user_email-icon"></span>
                                                                     <input type="text" name="user_email" id="user_email"
                                                                            className="user_email required sk-input"
                                                                            placeholder="z.B. name@gmail.com" value=""
                                                                            maxLength="255"
                                                                            onChange={this.handleChange("email")}
                                                                            value={this.state.email}
                                                                     />
                                                                     {
                                                                         this.state.emailErr ? <label className="x-error-x">Ungültige
                                                                             Adresse</label> : null
                                                                     }
                                                                     {
                                                                         this.props.auth.alert ? <label className="x-error-x">Diese, E-Mail-Adresse ist bereits registriert. Versuchen Sie es mit einem anderen</label> : null
                                                                     }
                                                                 </div>
                                                             </div>

                                                             <div className="fieldset_group phone-fieldset">
                                                                 <div className="fieldset__title">Telefonnummer</div>
                                                                 <div className="x-phone-replace" data-select2-id="9">
                                                                     <div className="x-flex-row" data-select2-id="8">


                                                                         <PhoneInput
                                                                             style={{
                                                                                 height: '40px',
                                                                                 border: '1px solid #b4b3b3',
                                                                                 borderRadius: '5px',
                                                                                 padding: '8px',
                                                                                 width: '100%'
                                                                             }}
                                                                             placeholder="Enter phone number"
                                                                             defaultCountry={this.props.country.country}
                                                                             international
                                                                             onChange={(e) => {this.setState({phoneValue : e})}}
                                                                             value={this.state.phoneValue}

                                                                         />
                                                                         {
                                                                             this.state.phoneErr ? <label className="x-error-x">Ungültige
                                                                                 Telefonnummer</label> : null
                                                                         }

                                                                     </div>
                                                                 </div>
                                                             </div>
                                                             <div className='x-forms'>
                                                                 <div className=" ">

                                                                     <label htmlFor="x-form-label" className="checkbox">
                                                                         <input type="checkbox"
                                                                                name="checked"
                                                                                id="cb_cond_1_0"
                                                                                checked={this.state.checked}
                                                                                onChange={this.handleInputChange}
                                                                                className="field-error"/>&nbsp;
                                                                         <span className='x-form-label'>
                                                                Durch Eingabe
                                                            Ihres Namens, Ihrer E-Mail-Adresse und Ihrer Telefonnummer
                                                            akzeptieren Sie unsere Richtlinien und stimmen zu,
                                                            Mitteilungen von uns gemäß unseren Bestimmungen zu
                                                            erhalten.
                                                            </span>
                                                                     </label>
                                                                     {
                                                                         this.state.checkedErr ?
                                                                             <label className="x-error-red" >Verpflichtend</label> : null
                                                                     }

                                                                 </div>
                                                             </div>
                                                             <div className="fieldset submit-fieldset" id="submit-fieldset"
                                                                  data-editable="" data-name="step1-send" data-ce-tag="button">
                                                                 <button type="button" id="lead-form-submit"

                                                                         onClick={this.handleRegistration}
                                                                         style={{width: '100%', backgroundColor:'#CC0000', color :'#FFF',
                                                                             border: '0',
                                                                             padding: '15px',
                                                                             borderRadius: '3px',
                                                                             fontSize: '20px',
                                                                             cursor: 'pointer'
                                                                         }}
                                                                         className="lead-form-submit ">
                                                                     Informationen erhalten
                                                                 </button>
                                                             </div>
                                                         </form>

                                                     </div>
                                                 </div>
                                                 <img id="image-104-22" alt="" src={DssForm} className="ct-image"
                                                      style={{maxHeight:'53px', marginBottom: '15px'}} />
                                             </div>
                                         </div>

                                     </Col>
                                 </Row>
                             </Container>



                         </div>
                         <Container>
                             <Row>
                                 <Col>
                                     <div className="hero-box">
                                         <div className="box-1">
                                             <div className="icon-box">
                                                 <img src={D222} alt=""/>
                                             </div>
                                             <div className="num-box">
                                                 <h2>705%</h2>
                                                 <p>Anstieg der Aktienpreise 2020</p>
                                             </div>
                                         </div>
                                         <div className="box-2">
                                             <div className="icon-box">
                                                 <img src={Noun_Profit_3038634_1} alt=""/>
                                             </div>
                                             <div className="num-box">
                                                 <h2>$566M</h2>
                                                 <p>Rekordgewinne am Ende des 3. Quartals 2020</p>
                                             </div>
                                         </div>
                                         <div className="box-3">
                                             <div className="icon-box">
                                                 <img src={Noun_Value_865914_1} alt=""/>
                                             </div>
                                             <div className="num-box">
                                                 <h2>800.976B</h2>
                                                 <p>Billionenschwere Marktkapitalisierung 2021</p>
                                             </div>
                                         </div>
                                     </div>
                                 </Col>
                             </Row>
                         </Container>

                     </div>
             </section>


             <section className="quick">
                 <div className="quick_box">
                     <img src={Pic1} />
                         <h1>Sicher und geschützt</h1>
                         <p>Fühlen Sie sich mit der autorisierten Investment-Plattform und der Firma Ihres Vertrauens
                             sicher und
                             geschützt.</p>
                 </div>
                 <div className="quick_box">
                     <img src={Pic2} />
                         <h1>Einfache Handhabung</h1>
                         <p>Egal, ob Sie bereits Erfahrung haben oder nicht, unser Team wird Ihnen behilflich sein und
                             Sie durch Ihr
                             Investement führen.</p>
                 </div>
                 <div className="quick_box">
                     <img src={Pic3} />
                         <h1>Schnelle Auszahlungen</h1>
                         <p>Gewinne werden innerhalb von 24h auf Ihr Konto ausgezahlt. Keine Wartezeiten, erweiterte
                             Formulare oder
                             Beantragungen.</p>
                 </div>
             </section>


             <section>
                 <div className="calc_title">
                     <h1>Entdecken Sie, wie viel Sie vorraussichtlich gewinnen können</h1>
                 </div>
                 <div id="calculator" className="calc-box">
                     <div className="init">
                         <h1>Anfangs-Investment</h1>
                         <div className="input-group mb-3 calcbox">
                             <div className="input-group-prepend paybox">
                                 <span className="input-group-text">&euro;</span>
                             </div>
                             <input type="text" className="form-control payinp"
                                    onChange={this.handleChange("priceA")}
                                    value={this.state.priceA}
                             />
                         </div>
                         <button
                            onClick={() => this.setState({priceB : this.state.priceA * 5})}
                         >Ausrechnen</button>
                     </div>
                     <div className="profit">
                         <h1>Geschätzter Gewinn</h1>
                         <div className="input-group mb-3 calcbox">
                             <div className="input-group-prepend paybox">
                                 <span className="input-group-text">&euro;</span>
                             </div>
                             <input type="text" className="form-control valueinp"
                                    onChange={this.handleChange("priceB")}
                                    disabled={true}
                                    value={this.state.priceB}
                                  />
                         </div>
                         <p>Nur maximal 2 Monate bis zu den ersten Gewinnen</p>
                     </div>
                 </div>
             </section>

             <section className="steps">
                 <div className="steps_title">
                     <h1>Beginnen Sie in nur 3 einfachen Schritten mit dem Investment</h1>
                 </div>
                 <div className="step-block">
                     <div className="step-img">
                         <img src={Milan} />
                     </div>
                     <div className="steps-box">
                         <div className="step-num">
                             <div className="div-1">
                                 <a href="#div_block-28-8" className="contnone"><img src={D11} /></a>
                             </div>
                             <div className="div-2">
                                 <a href="#div_block-28-8">Formular ausfüllen</a>
                                 <p>Hinterlassen Sie uns Ihre Kontaktdaten und einer unserer <b>autorisierten
                                     Mitarbeiter wird Sie
                                     direkt zurückrufen, um Ihnen zur</b> Seite zu stehen und alle offenen Fragen zu
                                     beantworten.
                                 </p>
                             </div>
                         </div>
                         <div className="step-num">
                             <div className="div-1">
                                 <img src={D22} />
                             </div>
                             <div className="div-2 pl-20">
                                 <h3>In Kontakt mit unserem Team</h3>
                                 <p>Sobald Ihr Konto freigeschaltet wurde, wird unser Team Ihnen bei all Ihren Schritten
                                     behilflich
                                     sein. </p>
                             </div>
                         </div>
                         <div className="step-num">
                             <div className="div-1">
                                 <img src={D33} />
                             </div>
                             <div className="div-2 pl-20">
                                 <h3>Beginnen Sie mit dem Investment</h3>
                                 <p>Sobald alle möglichen Fragen beantwortet sind, können Sie auch schon mit dem
                                     Investment in Ihre
                                     finanzielle Zukunft beginnen!</p>
                             </div>
                         </div>
                         <div className="interes">

                             <a href={null}
                                style={{color : 'white', cursor:'pointer'}}
                                // style={{color :'white', cursor:'pointer'}}
                                onClick={() => window.scrollTo({top :0, behavior : 'smooth'})}
                             >Ich bin interessiert</a>
                         </div>
                     </div>
                 </div>
             </section>

             <section className="testimonials-section">
                 <div className="testimonials_title">
                     <h1>What other investors are saying</h1>
                 </div>
                 <Carousel  showThumbs={true}    showStatus={false}  >
                     <div>
                         <div className="slide_item">

                             <img src={Ellipse6}
                                  style={{
                                      height: '95px',
                                      width: 'auto'
                                  }}
                             />
                             <p>Seit Beginn meiner Kontoeröffnung, mache ich pausenlos Gewinn. Die Corona Pandemie hat
                                 meiner Familie das
                                 Leben wirklich schwer gemacht, aber seitdem ich mit dem Investieren begonnen habe,
                                 können wir uns wieder
                                 über Wasser halten. DANKESCHÖN! </p>
                             <h1>Hans Reinart</h1>
                             <h6>Bremen</h6>
                         </div>
                     </div>
                     <div>
                         <div className="slide_item">
                             <img src={Ellipse11}
                                  style={{
                                      height: '95px',
                                      width: 'auto'
                                  }}/>
                                 <p>Dies war in diesem Jahr wirklich die beste Entscheidung überhaupt! Meine erste
                                     Auszahlung bekam ich schon
                                     nach nur einem kleinen Investmentbetrag. Auch die Mitarbeiter sind echt sehr
                                     hilfsbereit, ich werden auf
                                     jeden Fall mehr investieren!</p>
                                 <h1>Christina Gens</h1>
                                 <h6>Berlin</h6>
                         </div>
                     </div>
                     <div>
                         <div className="slide_item">
                             <img src={Ellipse8}
                                  style={{
                                      height: '95px',
                                      width: 'auto'
                                  }}/>
                             <p>Nachdem ein Freund von mir durchs Investieren ständig Gewinne verbuchte, wollte ich es
                                 auch mal
                                 versuchen. Ahnung davon hatte ich nicht wirklich, fand die Prozedur aber recht einfach
                                 und sah auch
                                 schon kurz nach meiner Kontoeröffnung die ersten Gewinne einfliegen. Ich kann´s
                                 wirklich allen, die ihre
                                 Fonds steigern wollen, nur empfehlen!</p>
                             <h1>Anthony Schmidt</h1>
                             <h6>Berlin</h6>
                         </div>
                     </div>
                     <div>
                         <div className="slide_item">
                             <img src={Ellipse12}
                                  style={{
                                      height: '95px',
                                      width: 'auto'
                                  }}/>
                             <p>Nach den Gewinnen, die ich erzielte, war auch der Service einer der Gründe, die mich
                                 dazu brachten weiter
                                 zu investieren. Nach einem harten Jahr kann ich wirklich sagen, eine vertrauenswürdige,
                                 seriöse
                                 Investment-Plattform gefunden zu haben und sehe auch wirklich schon Renditen!</p>
                             <h1>Victoria Tenkmann</h1>
                             <h6>Bonn</h6>
                         </div>
                     </div>
                     <div>
                         <div className="slide_item">
                             <img src={Ellipse10}
                                  style={{
                                      height: '95px',
                                      width: 'auto'
                                  }}/>
                             <p>Einfach, vertrauenswürdig, und konsequent - genau das, nach dem ich gesucht habe! Der
                                 Vertrauensfaktor
                                 war mir wirklich wichtig und ich werde ab jetzt mit niemand anderem mehr investieren;
                                 Es geht wirklich
                                 einfach nur darum, mit wem du investierst. Ich traf auf jeden Fall die richtige
                                 Entscheidung!</p>
                             <h1>Noah Bühler</h1>
                             <h6>Kiel</h6>
                         </div>
                     </div>
                 </Carousel>
                 <div className="owl-carousel">

                 </div>
             </section>

             <footer className="footer">
                 <div>
                     <div className="open_account">
                         <a
                             href={null}
                             style={{color :'white', cursor:'pointer'}}
                             onClick={() => window.scrollTo({top :0, behavior : 'smooth'})}
                         >KONTO ERÖFFNEN</a>
                     </div>
                 </div>
             </footer>

             <Footer
                 logoStatus={false}
             />
         </>
        )
    }
}


const mapStateToProps = (state) => {
    const {
        country,
        auth
    } = state
    return {
        country,
        auth
    }
}

export default connect(mapStateToProps, {
    checkCountryRequest,
    signUpRequest,
    signUpForCRMRequest
})(TeslaInvestmentDe);
