import BG from '../assets/img/bg.png'
import {Container, Row, Col} from "react-bootstrap";
import React from "react";

const H1Style = {
    // fontFamily: 'Work Sans sans-serif',
    color: '#009555',
    fontSize: '80px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    margin: 0
}

const H2Style = {
    // fontFamily: 'Work Sans sans-serif',
    color: '#009555',
    fontSize: '54px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    margin: 0
}
const pStyle = {
    fontSize: '28px',
    marginTop: '10px'
}

function ThanksPage() {

    return (
        <>
            <Container fluid style={{padding: '0px'}}>
                <Row>
                    <Col xs={12} sm={12} lg={6} md={6}>
                        <div className="left"  >
                            <img src={BG} alt="" style={{width: '90%', height: 'auto'}}/>
                        </div>
                        <img src="https://track.nitrocapitals-inv.de/conversion.gif" width="1" height="1"/>
                    </Col>

                    <Col lg={6} md={6}>
                        <div className="right"  >
                            <h1 className='thank-page-text' style={H1Style}>VIELEN DANK<br/> FÜR IHRE <br/>REGISTRIERUNG!</h1>
                            <p style={pStyle}>Unser Mitarbeiter wird sich kahl mit Ihnen in Verbindung setzen.</p>
                            <h2 style={H2Style}>ERINNERN SIE SICH</h2>
                            <p style={pStyle}>Verpassen Sie nicht unseren Anruf!<br/> Seien Sie telefonisch verfügbar und
                            </p>
                            <p style={pStyle} className="p_bold">es wird sich lohnen!</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default ThanksPage;
