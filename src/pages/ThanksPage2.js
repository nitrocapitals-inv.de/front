import BG from '../assets/img/bg.png'
import {Container, Row, Col} from "react-bootstrap";
import React from "react";

const H1Style = {
    // fontFamily: 'Work Sans sans-serif',
    color: '#009555',
    fontSize: '80px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    margin: 0
}

const H2Style = {
    // fontFamily: 'Work Sans sans-serif',
    color: '#009555',
    fontSize: '54px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    margin: 0
}
const pStyle = {
    fontSize: '28px',
    marginTop: '10px'
}

const pStyle2 = {
    fontSize: '22px',
    marginTop: '10px'
}

function ThanksPage() {

    return (
        <>
            <Container fluid style={{padding: '0px'}}>
                <Row>
                    <Col xs={12} sm={12} lg={6} md={6}>
                        <div className="left"  >
                            <img src={BG} alt="" style={{width: '90%', height: 'auto'}}/>
                        </div>
                        <img src="https://track.nitrocapitals-inv.de/conversion.gif" width="1" height="1"/>
                    </Col>

                    <Col lg={6} md={6}>
                        <div className="right" style={{padding: '10px'}}  >
                            <h1 className='thank-page-text' style={H1Style}>Vielen Dank,<br/> dass Ihre Daten in unserem System registriert wurden. </h1>
                            <p style={pStyle}>  Einer unserer Kundenbetreuer wird Sie innerhalb der nächsten 24 Stunden anrufen.</p>
                            {/*<h2 style={H2Style}>ERINNERN SIE SICH</h2>*/}
                            <p style={pStyle}>Wir wünschen Ihnen alles Gute und viel Erfolg </p>
                            <br/>
                            <p style={pStyle}>
                              <h2>
                                  Vielen Dank
                              </h2>
                            </p>
                            <p style={pStyle2} className="p_bold">Bitte bestätigen Sie Ihre E-Mail-Adresse   <br/>in einer E-Mail in Ihrem Posteingang oder Spam-Ordner,
                                <br/>
                                um Ihre Registrierung abzuschließen.
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default ThanksPage;
