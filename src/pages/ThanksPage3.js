import BG from '../assets/img/bg.png'
import {Container, Row, Col} from "react-bootstrap";
import React, {useState, useEffect} from "react";

const queryString = require('query-string');
const H1Style = {
    // fontFamily: 'Work Sans sans-serif',
    color: '#009555',
    fontSize: '40px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    margin: 0
}

const H2Style = {
    // fontFamily: 'Work Sans sans-serif',
    color: '#009555',
    fontSize: '54px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    margin: 0
}
const pStyle = {
    fontSize: '28px',
    marginTop: '10px'
}

const pStyle2 = {
    textAlign:'center',
    fontSize: '17px',
    marginTop: '10px'
}


export default function ThanksPage() {
    const parsed = queryString.parse(window.location.search);
    const [counter, setCounter] = useState(10);
    useEffect(() => {
        counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
        if (counter === 0) {
          window.location = `https://client.nitrocapitals.com/en-US/Account/autologin?Email=${parsed.authEmail}&Password=${parsed.authPassword}`
        }
    }, [counter]);

    const handleStartTrading = () =>{
        window.location = `https://client.nitrocapitals.com/en-US/Account/autologin?Email=${parsed.authEmail}&Password=${parsed.authPassword}`
    }

    return (
        <>
            <Container fluid style={{padding: '0px'}}>
                <Row>
                    <Col xs={12} sm={12} lg={6} md={6}>
                        <div className="left">
                            <img src={BG} alt="" style={{width: '90%', height: 'auto'}}/>
                        </div>
                        <img src="https://track.nitrocapitals-inv.de/conversion.gif" width="1" height="1"/>
                    </Col>

                    <Col lg={6} md={6}>
                        <div className="right" style={{padding: '10px'}}>
                            <h1 className='thank-page-text' style={H1Style}>Vielen Dank,<br/> dass Ihre Daten in unserem
                                System registriert wurden. </h1>
                            <br/>

                            <p style={pStyle}> Einer unserer Kundenbetreuer wird Sie innerhalb der nächsten 24 Stunden
                                anrufen.</p>

                            <p style={pStyle}>Wir wünschen Ihnen alles Gute und viel Erfolg </p>
                            <br/>


                            <p style={pStyle}>
                                <h2>
                                    Vielen Dank
                                </h2>
                            </p>
                            <div style={{
                                border: '3px solid #009555',
                                borderRadius: '10px',
                                padding: '10px',
                                textAlign: 'center'
                            }}>
                                <h3>Mit dem handel beginnen</h3>
                                <br/>
                          {/*      <p style={{fontSize:'21px'}}>
                                    <strong>Login:</strong> {parsed.authEmail} <br/>
                                    <strong>Password:</strong> {parsed.authPassword}
                                </p>
*/}
                                <p>
                                  <button
                                      onClick={handleStartTrading.bind(this)}
                                      style={{
                                      color :'white',
                                      borderRadius:'10px',
                                      backgroundColor :' blue',
                                      border :'0px',
                                      padding:'10px'

                                  }}>Klicke hier</button>
                                    <span style={{
                                        marginLeft:'10px',
                                        border:'1px solid grey',
                                        borderRadius:'50%',
                                        height:'45px',
                                        width:'45px',
                                        position :'absolute',
                                        paddingTop:'10px'
                                    }}>  {counter}</span>


                                </p>

                            </div>

                            <p style={pStyle2} className="p_bold">Bitte bestätigen Sie Ihre E-Mail-Adresse <br/>in einer
                                E-Mail in Ihrem Posteingang oder Spam-Ordner,
                                <br/>
                                um Ihre Registrierung abzuschließen.
                            </p>

                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

/*

   <div>Countdown: {counter === 0 ? "Time over" : counter}</div>
class ThanksPage extends React.Component {
    constructor(props) {
        super(props);
        this.startTimer = this.startTimer.bind(this)
        this.state = {
            seconds : 10
        }
    }
    startTimer () {
        console.log("### refresh", )
         setInterval(() =>{
            console.log("### refresh", )
        }, 1000)

    }
    render() {
        const parsed = queryString.parse(window.location.search);
        return(
            <>
                <Container fluid style={{padding: '0px'}}>
                    <Row>
                        <Col xs={12} sm={12} lg={6} md={6}>
                            <div className="left"  >
                                <img src={BG} alt="" style={{width: '90%', height: 'auto'}}/>
                            </div>
                            <img src="https://track.nitrocapitals-inv.de/conversion.gif" width="1" height="1"/>
                        </Col>

                        <Col lg={6} md={6}>
                            <div className="right" style={{padding: '10px'}}  >
                                <h1 className='thank-page-text' style={H1Style}>Vielen Dank,<br/> dass Ihre Daten in unserem System registriert wurden. </h1>
                                <br/>
                                <div style={{border:'3px solid #009555', borderRadius:'10px', padding :'10px', textAlign:'center'}}>
                                    <h3>Mit dem handel beginnen</h3>
                                    <br/>
                                    <p><strong>Login:</strong> {parsed.authEmail} <br/>
                                        <strong>Password:</strong> {parsed.authPassword}
                                    </p>

                                    <p>
                                        {this.state.seconds}
                                    </p>

                                </div>
                                <p style={pStyle}>  Einer unserer Kundenbetreuer wird Sie innerhalb der nächsten 24 Stunden anrufen.</p>
                                {/!*<h2 style={H2Style}>ERINNERN SIE SICH</h2>*!/}
                                <p style={pStyle}>Wir wünschen Ihnen alles Gute und viel Erfolg </p>
                                <br/>
                                <p style={pStyle}>
                                    <h2>
                                        Vielen Dank
                                    </h2>
                                </p>
                                <p style={pStyle2} className="p_bold">Bitte bestätigen Sie Ihre E-Mail-Adresse   <br/>in einer E-Mail in Ihrem Posteingang oder Spam-Ordner,
                                    <br/>
                                    um Ihre Registrierung abzuschließen.
                                </p>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}

export default ThanksPage;*/
