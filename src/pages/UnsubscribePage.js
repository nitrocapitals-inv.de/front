import React, {Component} from 'react'
import {connect} from "react-redux";

import EmailVerified from '../assets/img/email-verification.png'

import {Container, Row, Col} from "react-bootstrap";
class UnsubscribePage extends Component {
    constructor() {
        super();
        this.state = {

        }
    }
    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col style={{textAlign : 'center', paddingTop:'70px'}}>
                            <img
                                src={EmailVerified}
                            />
                          <div style={{paddingTop :'50px'}}>
                              <h1>
                                  Success
                              </h1>

                              <h3>
                                  We will no longer send automated emails to your email.

                              </h3>

                          </div>
                        </Col>
                    </Row>
                </Container>

            </div>
        )
    }
}

export default UnsubscribePage
