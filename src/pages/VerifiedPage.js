import React, {Component} from 'react'
import {connect} from "react-redux";

import EmailVerified from '../assets/img/email-verification.png'

import {Container, Row, Col} from "react-bootstrap";

class BlankPage extends Component {
    constructor() {
        super();
        this.state = {
            verifiedCode: window.location.pathname.split('/')[2],
            verifiedStatus: false,
            verifiedStatusRequest: true
        }

    }

    componentDidMount() {

        if (this.state.verifiedCode) {
            this.emailVerified()
        }

    }

    emailVerified = async () => {
        await fetch(`${process.env.REACT_APP_SERVER_URL}/api2/email/verified`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "content-Type": "Application/json",
            },
            body: JSON.stringify({
                verifiedCode: this.state.verifiedCode
            })
        })
            .then(response => response.json())
            .then(data => {
                if(data.success) {
                    this.setState({
                        verifiedStatus: true,
                        verifiedStatusRequest : true
                    })
                }else {
                    this.setState({
                        verifiedStatus: true,
                        verifiedStatusRequest : false
                    })
                }
            })
            .catch(error => console.log(error))

    }

    render() {

        return (
            <div>
                <Container>
                    <Row>
                        <Col style={{textAlign: 'center', paddingTop: '70px'}}>
                            <img
                                src={EmailVerified}
                            />
                            <div style={{paddingTop: '50px'}}>
                                <h1>
                                    Ihre E-Mail-Adresse wurde verifiziert
                                </h1>
                            </div>
                        </Col>
                    </Row>
                </Container>

            </div>
        )
    }
}

export default BlankPage
