import {ALERT_SUCCESS, SIGN_UP_REQUEST, SIGN_UP_SECOND_REQUEST,SIGN_UP_CRM_REQUEST, SIGN_UP_CRM_AUTO_LOGIN_REQUEST} from "../actions";


export const signUpRequest = (data) => ({
    type: SIGN_UP_REQUEST,
    payload : data
});


export const signUpForCRMRequest = (data) => ({
    type: SIGN_UP_CRM_REQUEST,
    payload : data
});

export const signUpForCRMAutoLoginRequest = (data) => ({
    type: SIGN_UP_CRM_AUTO_LOGIN_REQUEST,
    payload : data
});


export const signUpSecondRequest = (data) => ({
    type: SIGN_UP_SECOND_REQUEST,
    payload : data
});

export const alertSuccess = () => ({
    type: ALERT_SUCCESS
});
