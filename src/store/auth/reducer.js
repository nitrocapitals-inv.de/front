import {
    ALERT_SUCCESS
} from '../actions';
const INIT_STATE = {
    alert: false,
    authLogin :'',
    authPassword:''
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ALERT_SUCCESS:
            return {...state, alert: true};
        default:
            return {...state};
    }
}
