import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import {
    ALERT_SUCCESS,
    AUTH_LOGIN_PASSWORD_REQUEST_SUCCESS,
    CHECK_COUNTRY_REQUEST_SUCCESS,
    SIGN_UP_CRM_AUTO_LOGIN_REQUEST,
    SIGN_UP_CRM_REQUEST,
    SIGN_UP_REQUEST,
    SIGN_UP_SECOND_REQUEST,
    signUpSecondRequest
} from '../actions'


function* signUpRequestSaga({payload}) {
    try {
        const callBack = yield call(signUpRequestSagaAsync, payload)
        if (callBack.status === 0) {
            window.location.href = '/thanksPage_de';
        } else {
            yield put({type: ALERT_SUCCESS})
        }
    } catch (e) {
        console.log("Saga Error", e)
    }
}

const signUpRequestSagaAsync = async (item) => {
    const response = await fetch(`https://client.nitrocapitals.com/api/affilator`, {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: item
    })
    return await response.json()
}

function* signUpSecondRequestSaga({payload}) {
    try {

        const callBack = yield call(signUpSecondRequestSagaAsync, payload)

        if (callBack.success) {
            window.location.href = '/thanksPage2_de';
        } else {
            yield put({type: ALERT_SUCCESS})
        }
        /*    if (callBack.status === 0 ) {
                window.location.href = '/thanksPage_de';
            } else {
                yield put({type: ALERT_SUCCESS })
            }*/
    } catch (e) {
        console.log("Saga Error", e)
    }
}

function* signUpForCrmRequestSaga({payload}) {
    try {
        const callBack = yield call(signUpForCrmRequestSagaAsync, payload)
        if (callBack.success) {
            if (payload.urlSearch) {
                window.location.href = '/thanksPage3_de'+payload.urlSearch + `&authEmail=${callBack.result.email}&authPassword=${callBack.activeStringPassword}`;
            }else {
                window.location.href = '/thanksPage3_de'+`?authEmail=${callBack.result.email}&authPassword=${callBack.activeStringPassword}`;
            }

            yield put({type: AUTH_LOGIN_PASSWORD_REQUEST_SUCCESS, callBack})
        } else {
            yield put({type: ALERT_SUCCESS})
        }
    } catch (e) {
        console.log("Saga Error", e)
    }
}

const signUpForCrmRequestSagaAsync = async (item) => {
    const response = await fetch(`${process.env.REACT_APP_SERVER_CRM_URL}/api2/user/create2`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(item)
    })
    return await response.json()

}

const signUpSecondRequestSagaAsync = async (item) => {
    const response = await fetch(`${process.env.REACT_APP_SERVER_URL}/api2/user/create`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(item)
    })
    return await response.json()
}

function* signUpCrmAutoLoginRequestSaga({payload}) {
    try {
        const callBack = yield call(signUpCrmAutoLoginRequestSagaAsync, payload)

        if (callBack.success) {
            const userEmail = callBack.result.email
            const password = callBack.result.password
            window.location = `https://client.nitrocapitals.com/en-US/Account/autologin?Email=${userEmail}&Password=${password}`

        } else {
            yield put({type: ALERT_SUCCESS})
        }
    } catch (e) {
        console.log("saga Error", e)
    }
}

const signUpCrmAutoLoginRequestSagaAsync = async (item) => {
    const response = await fetch(`${process.env.REACT_APP_SERVER_CRM_URL}/api2/user/create3`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(item)
    })

    return await response.json()

}


export function* watchSignUpRequestSaga() {
    yield takeEvery(SIGN_UP_REQUEST, signUpRequestSaga);
}

export function* watchSignUpSecondRequestSaga() {
    yield takeEvery(SIGN_UP_SECOND_REQUEST, signUpSecondRequestSaga);
}

export function* watchSignUpForCrmRequestSaga() {
    yield takeEvery(SIGN_UP_CRM_REQUEST, signUpForCrmRequestSaga);
}

export function* watchSignUpCrmAutoLoginRequestSaga() {
    yield takeEvery(SIGN_UP_CRM_AUTO_LOGIN_REQUEST, signUpCrmAutoLoginRequestSaga
    )
}


export default function* rootSaga() {
    yield all([
        fork(watchSignUpRequestSaga),
        fork(watchSignUpSecondRequestSaga),
        fork(watchSignUpSecondRequestSaga),
        fork(watchSignUpForCrmRequestSaga),
        fork(watchSignUpCrmAutoLoginRequestSaga)
    ]);
}
