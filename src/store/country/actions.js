import {
    CHECK_COUNTRY_REQUEST
} from '../actions';


export const checkCountryRequest = () => ({
    type: CHECK_COUNTRY_REQUEST
});
