import {
    CHECK_COUNTRY_REQUEST_SUCCESS,
} from '../actions';
import {FLAG_SELECTOR_OPTION_LIST} from '../../helper/country'
const INIT_STATE = {
    flagId: 0,
    country: 'AT',
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case CHECK_COUNTRY_REQUEST_SUCCESS:

            FLAG_SELECTOR_OPTION_LIST.map((item, key) => {
                if (item.name === action.callBack.countryCode) {
                    state.flagId = key
                    state.country = action.callBack.countryCode
                }
            })

            return {...state};
        default:
            return {...state};
    }
}
