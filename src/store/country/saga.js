import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import {
    CHECK_COUNTRY_REQUEST,
    CHECK_COUNTRY_REQUEST_SUCCESS
} from '../actions'


function* checkCountrySaga({payload}) {
    try {
        const callBack = yield call(checkCountrySagaAsync, payload)
        if (callBack) {
            yield put({type: CHECK_COUNTRY_REQUEST_SUCCESS, callBack})
        } else {

        }

    } catch (e) {
        console.log("Saga Error", e)
    }
}

const checkCountrySagaAsync = async (item) => {
    // const response = await fetch(`https://ipinfo.io/json?token=${process.env.REACT_APP_IP_INFO_TOKEN}`, {
    const response = await fetch(`https://extreme-ip-lookup.com/json/`, {
        method: "GET",
        body: item
    })
    return await response.json()


    /*
        return await fetch('https://api.ipify.org/?format=json', {
            method: "GET",
        }).then(results => results.json())
            .then( async data => {
                const response = await fetch(`https://ipinfo.io/${data.ip}?token=${process.env.REACT_APP_IP_INFO_TOKEN}`, {
                    method: "GET",
                })
                return await response.json()
            })
    */

}


export function* watchCheckCountrySaga() {
    yield takeEvery(CHECK_COUNTRY_REQUEST, checkCountrySaga);
}


export default function* rootSaga() {
    yield all([
        fork(watchCheckCountrySaga)
    ]);
}
