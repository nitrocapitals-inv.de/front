import { combineReducers } from 'redux';
import auth from './auth/reducer';
import country from './country/reducer';
const reducers = combineReducers({
    country,
    auth
});

export default reducers;
