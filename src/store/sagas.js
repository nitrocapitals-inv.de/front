import { all } from 'redux-saga/effects';
import countrySaga from './country/saga';
import authSaga from './auth/saga';

export default function* rootSaga(getState) {
    yield all([
        countrySaga(),
        authSaga(),
    ]);
}
